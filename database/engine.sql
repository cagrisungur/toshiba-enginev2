-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 31, 2018 at 09:04 AM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `engine`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `campaign`
--

INSERT INTO `campaign` (`id`, `country_id`, `start_date`, `end_date`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '2018-07-08', '2018-07-14', 1, '2018-07-09 12:09:00', '2018-07-10 11:47:53', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `campaign_action`
--

CREATE TABLE `campaign_action` (
  `id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `command` varchar(32) NOT NULL,
  `cron_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `campaign_action`
--

INSERT INTO `campaign_action` (`id`, `campaign_id`, `command`, `cron_date`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'campaign action', '2018-07-10', 1, '2018-07-09 14:30:31', '2018-07-09 14:31:08', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `campaign_text`
--

CREATE TABLE `campaign_text` (
  `campaign_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` text,
  `link` varchar(64) NOT NULL,
  `short_description` text,
  `long_description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `campaign_text`
--

INSERT INTO `campaign_text` (`campaign_id`, `lang_id`, `name`, `link`, `short_description`, `long_description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'campaign text', 'campaign text', 'campaign text', 'campaign text', '2018-07-09 14:13:56', NULL, NULL),
(1, 2, 'campaign', 'campaign', 'campaign', 'campaign', '2018-07-10 14:28:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `link` varchar(64) NOT NULL,
  `keywords` text,
  `active` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category`
--

INSERT INTO `category` (`id`, `name`, `link`, `keywords`, `active`, `parent_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'test', 'test', 1, NULL, 0, '2018-06-07 14:36:34', '2018-07-04 12:04:16', NULL),
(2, 'new category', 'new category', 'new category', 1, 1, 0, '2018-07-04 12:34:47', '2018-07-05 10:53:31', NULL),
(3, 'category', 'category', 'category', 1, 1, 0, '2018-07-05 10:53:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category_feature`
--

CREATE TABLE `category_feature` (
  `category_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category_feature`
--

INSERT INTO `category_feature` (`category_id`, `feature_id`, `type_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 0, '2018-06-07 11:34:31', '2018-06-07 11:34:31', '2018-06-07 11:34:31'),
(2, 1, 1, 0, '2018-07-04 12:35:18', NULL, NULL),
(2, 1, 2, 0, '2018-07-04 13:14:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category_file`
--

CREATE TABLE `category_file` (
  `category_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category_file`
--

INSERT INTO `category_file` (`category_id`, `file_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-07-04 12:07:31', NULL, NULL),
(2, 1, 0, '2018-07-04 12:40:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category_image`
--

CREATE TABLE `category_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category_image`
--

INSERT INTO `category_image` (`id`, `category_id`, `name`, `path`, `type_id`, `active`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'category image', 'category image', 1, 1, 0, '2018-07-05 07:06:02', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category_spec`
--

CREATE TABLE `category_spec` (
  `category_id` int(11) NOT NULL,
  `spec_id` int(11) NOT NULL,
  `value` varchar(128) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category_spec`
--

INSERT INTO `category_spec` (`category_id`, `spec_id`, `value`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'spec', 0, '2018-07-05 07:19:37', '2018-07-05 07:22:09', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `category_text`
--

CREATE TABLE `category_text` (
  `category_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `short_description` text,
  `long_description` text,
  `link` varchar(64) NOT NULL,
  `keywords` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `category_text`
--

INSERT INTO `category_text` (`category_id`, `country_id`, `lang_id`, `name`, `short_description`, `long_description`, `link`, `keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 2, 'category text', 'category text', 'category text', 'category text', 'category text', '2018-07-05 08:47:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `contact_mail`
--

CREATE TABLE `contact_mail` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `orig_name` varchar(255) NOT NULL,
  `code` varchar(3) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `maintance` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `country`
--

INSERT INTO `country` (`id`, `name`, `orig_name`, `code`, `active`, `maintance`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Turkey', 'turkiye', 'tr', 1, 1, 1, '2018-06-05 12:04:41', '2018-06-05 12:17:53', NULL),
(2, 'England', 'england', 'en', 1, 1, 2, '2018-06-05 12:10:14', '2018-06-05 12:17:04', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `country_lang`
--

CREATE TABLE `country_lang` (
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `main` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `country_lang`
--

INSERT INTO `country_lang` (`country_id`, `lang_id`, `main`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 0, '2018-06-07 08:00:34', '2018-06-12 06:24:59', NULL),
(2, 1, 0, 0, '2018-06-07 11:49:04', NULL, NULL),
(2, 2, 1, 1, '2018-07-02 06:55:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `custom_page`
--

CREATE TABLE `custom_page` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `custom_page_text`
--

CREATE TABLE `custom_page_text` (
  `page_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `feature`
--

INSERT INTO `feature` (`id`, `name`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 1, '2018-06-07 14:36:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `feature_image`
--

CREATE TABLE `feature_image` (
  `id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `feature_text`
--

CREATE TABLE `feature_text` (
  `feature_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `short_description` text,
  `long_description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `feature_type`
--

CREATE TABLE `feature_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `feature_type`
--

INSERT INTO `feature_type` (`id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'test', '2018-06-07 14:37:06', NULL, NULL),
(2, 'type', 'type', '2018-07-04 13:07:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `file`
--

INSERT INTO `file` (`id`, `name`, `path`, `type_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'file', 'file', 1, '2018-07-04 12:07:12', '2018-07-11 14:28:04', '2018-07-23 21:00:00');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `file_type`
--

CREATE TABLE `file_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `group_lang`
--

CREATE TABLE `group_lang` (
  `group_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `group_module`
--

CREATE TABLE `group_module` (
  `group_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `highlighted_text`
--

CREATE TABLE `highlighted_text` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `highlighted_text`
--

INSERT INTO `highlighted_text` (`id`, `product_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-07-12 07:59:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `highlighted_text_translate`
--

CREATE TABLE `highlighted_text_translate` (
  `text_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `highlighted_text_translate`
--

INSERT INTO `highlighted_text_translate` (`text_id`, `country_id`, `lang_id`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'text', '2018-07-12 08:05:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `image_type`
--

CREATE TABLE `image_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `image_type`
--

INSERT INTO `image_type` (`id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'image type', 'type1', '2018-07-05 07:05:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `orig_name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `language`
--

INSERT INTO `language` (`id`, `name`, `orig_name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'turkish', 'turkce', 'tr', '2018-06-05 13:10:05', '2018-06-05 13:11:28', NULL),
(2, 'English', 'english', 'en', '2018-07-02 06:54:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mail_type`
--

CREATE TABLE `mail_type` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(128) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `client_type` int(11) NOT NULL,
  `package_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_client_endpoints`
--

CREATE TABLE `oauth_client_endpoints` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_sessions`
--

CREATE TABLE `oauth_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('client','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_session_access_tokens`
--

CREATE TABLE `oauth_session_access_tokens` (
  `id` int(10) NOT NULL,
  `session_id` int(10) NOT NULL,
  `access_token` text NOT NULL,
  `access_token_expires` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_session_authcodes`
--

CREATE TABLE `oauth_session_authcodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `auth_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `auth_code_expires` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_session_redirects`
--

CREATE TABLE `oauth_session_redirects` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `oauth_session_refresh_tokens`
--

CREATE TABLE `oauth_session_refresh_tokens` (
  `session_access_token_id` int(10) UNSIGNED NOT NULL,
  `refresh_token` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `refresh_token_expires` int(11) NOT NULL,
  `client_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `page_type`
--

CREATE TABLE `page_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(128) NOT NULL,
  `link` varchar(64) NOT NULL,
  `keywords` text,
  `category_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product`
--

INSERT INTO `product` (`id`, `name`, `code`, `link`, `keywords`, `category_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'product', '1', '1', '21', 1, 1, '2018-06-26 12:58:49', '2018-07-06 07:46:38', NULL),
(2, 'product', 'product', 'product', 'product', 1, 1, '2018-06-28 09:11:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product_country`
--

CREATE TABLE `product_country` (
  `product_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product_country`
--

INSERT INTO `product_country` (`product_id`, `country_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-07-06 07:33:39', NULL, NULL),
(2, 1, 1, '2018-07-05 13:00:47', '2018-07-11 08:41:51', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product_feature`
--

CREATE TABLE `product_feature` (
  `product_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product_feature`
--

INSERT INTO `product_feature` (`product_id`, `feature_id`, `type_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 2, 0, '2018-07-11 09:22:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product_file`
--

CREATE TABLE `product_file` (
  `product_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product_file`
--

INSERT INTO `product_file` (`product_id`, `file_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-07-11 12:45:07', '2018-07-11 14:59:49', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product_spec`
--

CREATE TABLE `product_spec` (
  `product_id` int(11) NOT NULL,
  `spec_id` int(11) NOT NULL,
  `value` varchar(128) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product_spec`
--

INSERT INTO `product_spec` (`product_id`, `spec_id`, `value`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'spec', 1, '2018-07-06 09:19:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `product_text`
--

CREATE TABLE `product_text` (
  `product_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `subtitle` varchar(128) DEFAULT NULL,
  `short_description` text,
  `long_description` text,
  `link` varchar(64) NOT NULL,
  `keywords` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `product_text`
--

INSERT INTO `product_text` (`product_id`, `country_id`, `lang_id`, `subtitle`, `short_description`, `long_description`, `link`, `keywords`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'product text', 'product text', 'product text', 'product text', 'product text', '2018-07-05 13:15:00', '2018-07-11 09:07:21', NULL),
(2, 1, 2, 'as', 'as', 'as', 'as', 'as', '2018-07-11 08:52:48', '2018-07-11 15:28:50', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `related_product`
--

CREATE TABLE `related_product` (
  `host_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `related_product`
--

INSERT INTO `related_product` (`host_id`, `related_id`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, '2018-07-11 11:53:54', NULL, NULL),
(1, 2, 1, '2018-07-11 11:53:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sales_point`
--

CREATE TABLE `sales_point` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `website` varchar(128) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `fax` varchar(24) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(128) NOT NULL,
  `lat` varchar(11) NOT NULL,
  `lng` varchar(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider`
--

INSERT INTO `slider` (`id`, `country_id`, `type_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, 1, '2018-06-05 13:59:00', '2018-06-28 12:56:19', NULL),
(2, 1, 1, 1, '2018-06-25 13:16:23', '2018-06-28 12:56:24', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider_category`
--

CREATE TABLE `slider_category` (
  `slider_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider_category`
--

INSERT INTO `slider_category` (`slider_id`, `category_id`, `priority`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, '2018-06-26 13:00:25', NULL, NULL),
(2, 1, 0, 1, '2018-06-28 12:38:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider_item`
--

CREATE TABLE `slider_item` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `path` text NOT NULL,
  `mime_type` varchar(25) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider_item`
--

INSERT INTO `slider_item` (`id`, `slider_id`, `path`, `mime_type`, `active`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'slider item', 'item', 1, 0, '2018-06-27 08:24:21', '2018-06-29 11:19:14', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider_item_text`
--

CREATE TABLE `slider_item_text` (
  `slider_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` text,
  `text` text,
  `link` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider_item_text`
--

INSERT INTO `slider_item_text` (`slider_id`, `lang_id`, `title`, `text`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 'slider item text', 'slider item text', 'slider item text', '2018-06-27 08:55:42', NULL, NULL),
(2, 2, 'test', 'test', 'test', '2018-07-02 14:29:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider_product`
--

CREATE TABLE `slider_product` (
  `slider_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider_product`
--

INSERT INTO `slider_product` (`slider_id`, `product_id`, `priority`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, 0, '2018-06-26 12:59:02', NULL, NULL),
(2, 1, 1, 0, '2018-06-26 15:18:05', '2018-07-11 11:42:21', NULL),
(2, 2, 0, 1, '2018-06-28 09:12:01', '2018-06-28 11:30:43', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider_type`
--

CREATE TABLE `slider_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `slider_type`
--

INSERT INTO `slider_type` (`id`, `name`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'slide_type', 'main', '2018-06-05 13:50:11', '2018-06-28 11:35:34', NULL),
(2, 'index', 'index', '2018-06-28 12:55:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `smtp_setting`
--

CREATE TABLE `smtp_setting` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `port` int(5) NOT NULL,
  `use_ssl` int(11) NOT NULL DEFAULT '0',
  `password` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `social_media`
--

CREATE TABLE `social_media` (
  `type_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `social_media_type`
--

CREATE TABLE `social_media_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `specification`
--

CREATE TABLE `specification` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(64) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `specification`
--

INSERT INTO `specification` (`id`, `category_id`, `name`, `code`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'specification', 'specification', 0, '2018-07-05 07:19:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `specification_category`
--

CREATE TABLE `specification_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `specification_category`
--

INSERT INTO `specification_category` (`id`, `name`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'specification category', 0, '2018-07-05 07:18:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `specification_text`
--

CREATE TABLE `specification_text` (
  `spec_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `static_text`
--

CREATE TABLE `static_text` (
  `id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `static_text_translate`
--

CREATE TABLE `static_text_translate` (
  `static_text_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(32) NOT NULL,
  `mail` varchar(128) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `mail`, `phone`, `group_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sa', 'sa', 'sa', 'sa', 'sa', 1, '2017-09-21 05:14:55', '2018-06-21 05:15:01', NULL),
(2, 'burcu', 'burcu', 'burcu', 'burcu@gmail.com', '123456789', 1, '2018-05-30 21:00:00', '2018-05-30 21:00:00', '2018-05-30 21:00:00'),
(3, 'as', 'as', 'as', 'as@gmail.com', '123456789', 1, '2018-05-30 21:00:00', '2018-05-30 21:00:00', '2018-05-30 21:00:00'),
(4, 'cagri', 'cagri', '12345', 'cagri@gmail.com', '123456789', 1, '2018-05-30 21:00:00', '2018-05-30 21:00:00', '2018-05-30 21:00:00'),
(6, 'test', 'test', '$2y$13$QSqFhpuXj9ux.sr1FRAQPe67w', 'test', 'test', 1, '2018-06-04 06:46:42', '2018-06-04 06:46:42', '2018-06-04 06:46:42'),
(7, 'test2', 'test2', 'aZa1mXKIjfg0XlFgGOaSUg==', 'test2', 'test2', 1, '2018-06-04 14:24:21', '2018-06-04 14:24:21', '2018-06-04 14:24:21'),
(8, 'deneme', 'deneme', 'aZa1mXKIjfg0XlFgGOaSUg==', 'deneme', 'deneme', 1, '2018-06-05 07:53:31', NULL, NULL),
(9, 'deneme2', 'deneme2', 'aZa1mXKIjfg0XlFgGOaSUg==', 'deneme2', 'deneme2', 1, '2018-06-27 13:38:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '2018-05-30 21:00:00', '2018-05-30 21:00:00', '2018-05-30 21:00:00');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_lang`
--

CREATE TABLE `user_lang` (
  `user_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_module`
--

CREATE TABLE `user_module` (
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Tablo için indeksler `campaign_action`
--
ALTER TABLE `campaign_action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`);

--
-- Tablo için indeksler `campaign_text`
--
ALTER TABLE `campaign_text`
  ADD PRIMARY KEY (`campaign_id`,`lang_id`),
  ADD KEY `lang_id` (`lang_id`),
  ADD KEY `campaign_id` (`campaign_id`);

--
-- Tablo için indeksler `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `category_feature`
--
ALTER TABLE `category_feature`
  ADD PRIMARY KEY (`category_id`,`feature_id`,`type_id`) USING BTREE,
  ADD KEY `feature_id` (`feature_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Tablo için indeksler `category_file`
--
ALTER TABLE `category_file`
  ADD PRIMARY KEY (`category_id`,`file_id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Tablo için indeksler `category_image`
--
ALTER TABLE `category_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `category_spec`
--
ALTER TABLE `category_spec`
  ADD PRIMARY KEY (`category_id`,`spec_id`),
  ADD KEY `spec_id` (`spec_id`);

--
-- Tablo için indeksler `category_text`
--
ALTER TABLE `category_text`
  ADD PRIMARY KEY (`category_id`,`country_id`,`lang_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `contact_mail`
--
ALTER TABLE `contact_mail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_id` (`country_id`,`type_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `country_id_2` (`country_id`);

--
-- Tablo için indeksler `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `country_lang`
--
ALTER TABLE `country_lang`
  ADD PRIMARY KEY (`country_id`,`lang_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `custom_page`
--
ALTER TABLE `custom_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `custom_page_text`
--
ALTER TABLE `custom_page_text`
  ADD PRIMARY KEY (`page_id`,`country_id`,`lang_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `lang_id` (`lang_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Tablo için indeksler `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `feature_image`
--
ALTER TABLE `feature_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feature_id` (`feature_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `feature_text`
--
ALTER TABLE `feature_text`
  ADD PRIMARY KEY (`feature_id`,`country_id`,`lang_id`),
  ADD KEY `feature_id` (`feature_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `feature_type`
--
ALTER TABLE `feature_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `group_lang`
--
ALTER TABLE `group_lang`
  ADD PRIMARY KEY (`group_id`,`country_id`,`lang_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `group_module`
--
ALTER TABLE `group_module`
  ADD PRIMARY KEY (`group_id`,`module_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Tablo için indeksler `highlighted_text`
--
ALTER TABLE `highlighted_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Tablo için indeksler `highlighted_text_translate`
--
ALTER TABLE `highlighted_text_translate`
  ADD PRIMARY KEY (`text_id`,`country_id`,`lang_id`),
  ADD KEY `text_id` (`text_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `image_type`
--
ALTER TABLE `image_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mail_type`
--
ALTER TABLE `mail_type`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth_clients_id_unique` (`id`),
  ADD UNIQUE KEY `oauth_clients_id_secret_unique` (`id`,`secret`);

--
-- Tablo için indeksler `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_client_endpoints_client_id_foreign` (`client_id`);

--
-- Tablo için indeksler `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_sessions_client_id_owner_type_owner_id_index` (`client_id`,`owner_type`,`owner_id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Tablo için indeksler `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token_session_id` (`access_token`(325),`session_id`),
  ADD KEY `session_id` (`session_id`);

--
-- Tablo için indeksler `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_session_authcodes_session_id_index` (`session_id`);

--
-- Tablo için indeksler `oauth_session_redirects`
--
ALTER TABLE `oauth_session_redirects`
  ADD PRIMARY KEY (`session_id`);

--
-- Tablo için indeksler `oauth_session_refresh_tokens`
--
ALTER TABLE `oauth_session_refresh_tokens`
  ADD PRIMARY KEY (`session_access_token_id`),
  ADD KEY `oauth_session_refresh_tokens_client_id_index` (`client_id`),
  ADD KEY `refresh_token` (`refresh_token`);

--
-- Tablo için indeksler `page_type`
--
ALTER TABLE `page_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Tablo için indeksler `product_country`
--
ALTER TABLE `product_country`
  ADD PRIMARY KEY (`product_id`,`country_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Tablo için indeksler `product_feature`
--
ALTER TABLE `product_feature`
  ADD PRIMARY KEY (`product_id`,`feature_id`),
  ADD KEY `feature_id` (`feature_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Tablo için indeksler `product_file`
--
ALTER TABLE `product_file`
  ADD PRIMARY KEY (`product_id`,`file_id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Tablo için indeksler `product_spec`
--
ALTER TABLE `product_spec`
  ADD PRIMARY KEY (`product_id`,`spec_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `spec_id` (`spec_id`);

--
-- Tablo için indeksler `product_text`
--
ALTER TABLE `product_text`
  ADD PRIMARY KEY (`product_id`,`country_id`,`lang_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `related_product`
--
ALTER TABLE `related_product`
  ADD PRIMARY KEY (`host_id`,`related_id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `related_id` (`related_id`);

--
-- Tablo için indeksler `sales_point`
--
ALTER TABLE `sales_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Tablo için indeksler `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`) USING BTREE,
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `slider_category`
--
ALTER TABLE `slider_category`
  ADD PRIMARY KEY (`slider_id`,`category_id`),
  ADD KEY `slider_id` (`slider_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Tablo için indeksler `slider_item`
--
ALTER TABLE `slider_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slider_id` (`slider_id`);

--
-- Tablo için indeksler `slider_item_text`
--
ALTER TABLE `slider_item_text`
  ADD PRIMARY KEY (`slider_id`,`lang_id`),
  ADD KEY `slider_id` (`slider_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `slider_product`
--
ALTER TABLE `slider_product`
  ADD PRIMARY KEY (`slider_id`,`product_id`),
  ADD KEY `slider_id` (`slider_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Tablo için indeksler `slider_type`
--
ALTER TABLE `slider_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `smtp_setting`
--
ALTER TABLE `smtp_setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_id` (`country_id`);

--
-- Tablo için indeksler `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`type_id`,`country_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Tablo için indeksler `social_media_type`
--
ALTER TABLE `social_media_type`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `specification`
--
ALTER TABLE `specification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Tablo için indeksler `specification_category`
--
ALTER TABLE `specification_category`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `specification_text`
--
ALTER TABLE `specification_text`
  ADD PRIMARY KEY (`spec_id`,`country_id`,`lang_id`),
  ADD KEY `spec_id` (`spec_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `static_text`
--
ALTER TABLE `static_text`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Tablo için indeksler `static_text_translate`
--
ALTER TABLE `static_text_translate`
  ADD PRIMARY KEY (`static_text_id`,`country_id`,`lang_id`),
  ADD KEY `static_text_id` (`static_text_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Tablo için indeksler `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `user_lang`
--
ALTER TABLE `user_lang`
  ADD PRIMARY KEY (`user_id`,`country_id`,`lang_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `lang_id` (`lang_id`);

--
-- Tablo için indeksler `user_module`
--
ALTER TABLE `user_module`
  ADD PRIMARY KEY (`user_id`,`module_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `campaign_action`
--
ALTER TABLE `campaign_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `category_image`
--
ALTER TABLE `category_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `contact_mail`
--
ALTER TABLE `contact_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `custom_page`
--
ALTER TABLE `custom_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `feature_image`
--
ALTER TABLE `feature_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `feature_type`
--
ALTER TABLE `feature_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `highlighted_text`
--
ALTER TABLE `highlighted_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `image_type`
--
ALTER TABLE `image_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `mail_type`
--
ALTER TABLE `mail_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;

--
-- Tablo için AUTO_INCREMENT değeri `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=494;

--
-- Tablo için AUTO_INCREMENT değeri `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;

--
-- Tablo için AUTO_INCREMENT değeri `page_type`
--
ALTER TABLE `page_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `sales_point`
--
ALTER TABLE `sales_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `slider_item`
--
ALTER TABLE `slider_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `slider_type`
--
ALTER TABLE `slider_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `smtp_setting`
--
ALTER TABLE `smtp_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `social_media_type`
--
ALTER TABLE `social_media_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `specification`
--
ALTER TABLE `specification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `specification_category`
--
ALTER TABLE `specification_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `static_text`
--
ALTER TABLE `static_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Tablo için AUTO_INCREMENT değeri `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `campaign`
--
ALTER TABLE `campaign`
  ADD CONSTRAINT `campaign_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `campaign_action`
--
ALTER TABLE `campaign_action`
  ADD CONSTRAINT `campaign_action_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `campaign_text`
--
ALTER TABLE `campaign_text`
  ADD CONSTRAINT `campaign_text_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `campaign_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `category_feature`
--
ALTER TABLE `category_feature`
  ADD CONSTRAINT `category_feature_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_feature_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `feature_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_feature_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `category_file`
--
ALTER TABLE `category_file`
  ADD CONSTRAINT `category_file_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_file_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `category_image`
--
ALTER TABLE `category_image`
  ADD CONSTRAINT `category_image_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_image_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `image_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `category_spec`
--
ALTER TABLE `category_spec`
  ADD CONSTRAINT `category_spec_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `category_spec_ibfk_2` FOREIGN KEY (`spec_id`) REFERENCES `specification` (`id`);

--
-- Tablo kısıtlamaları `category_text`
--
ALTER TABLE `category_text`
  ADD CONSTRAINT `category_text_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_text_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_text_ibfk_3` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `contact_mail`
--
ALTER TABLE `contact_mail`
  ADD CONSTRAINT `contact_mail_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contact_mail_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `mail_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `country_lang`
--
ALTER TABLE `country_lang`
  ADD CONSTRAINT `country_lang_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `country_lang_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `custom_page`
--
ALTER TABLE `custom_page`
  ADD CONSTRAINT `custom_page_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `page_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `custom_page_text`
--
ALTER TABLE `custom_page_text`
  ADD CONSTRAINT `custom_page_text_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `custom_page_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `custom_page_text_ibfk_3` FOREIGN KEY (`page_id`) REFERENCES `custom_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `feature_image`
--
ALTER TABLE `feature_image`
  ADD CONSTRAINT `feature_image_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feature_image_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `image_type` (`id`);

--
-- Tablo kısıtlamaları `feature_text`
--
ALTER TABLE `feature_text`
  ADD CONSTRAINT `feature_text_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feature_text_ibfk_2` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feature_text_ibfk_3` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`);

--
-- Tablo kısıtlamaları `group_lang`
--
ALTER TABLE `group_lang`
  ADD CONSTRAINT `group_lang_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_lang_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`),
  ADD CONSTRAINT `group_lang_ibfk_3` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`);

--
-- Tablo kısıtlamaları `group_module`
--
ALTER TABLE `group_module`
  ADD CONSTRAINT `group_module_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_module_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `highlighted_text`
--
ALTER TABLE `highlighted_text`
  ADD CONSTRAINT `highlighted_text_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `highlighted_text_translate`
--
ALTER TABLE `highlighted_text_translate`
  ADD CONSTRAINT `highlighted_text_translate_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `highlighted_text_translate_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `highlighted_text_translate_ibfk_3` FOREIGN KEY (`text_id`) REFERENCES `highlighted_text` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product_country`
--
ALTER TABLE `product_country`
  ADD CONSTRAINT `product_country_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_country_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product_feature`
--
ALTER TABLE `product_feature`
  ADD CONSTRAINT `product_feature_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_feature_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `feature_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_feature_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product_file`
--
ALTER TABLE `product_file`
  ADD CONSTRAINT `product_file_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_file_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product_spec`
--
ALTER TABLE `product_spec`
  ADD CONSTRAINT `product_spec_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_spec_ibfk_2` FOREIGN KEY (`spec_id`) REFERENCES `specification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `product_text`
--
ALTER TABLE `product_text`
  ADD CONSTRAINT `product_text_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_text_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `related_product`
--
ALTER TABLE `related_product`
  ADD CONSTRAINT `related_product_ibfk_1` FOREIGN KEY (`host_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `related_product_ibfk_2` FOREIGN KEY (`related_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `sales_point`
--
ALTER TABLE `sales_point`
  ADD CONSTRAINT `sales_point_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `slider`
--
ALTER TABLE `slider`
  ADD CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `slider_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `slider_category`
--
ALTER TABLE `slider_category`
  ADD CONSTRAINT `slider_category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_category_ibfk_2` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `slider_item`
--
ALTER TABLE `slider_item`
  ADD CONSTRAINT `slider_item_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `slider_item_text`
--
ALTER TABLE `slider_item_text`
  ADD CONSTRAINT `slider_item_text_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_item_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `slider_product`
--
ALTER TABLE `slider_product`
  ADD CONSTRAINT `slider_product_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `smtp_setting`
--
ALTER TABLE `smtp_setting`
  ADD CONSTRAINT `smtp_setting_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `social_media`
--
ALTER TABLE `social_media`
  ADD CONSTRAINT `social_media_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `social_media_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `social_media_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `specification`
--
ALTER TABLE `specification`
  ADD CONSTRAINT `specification_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `specification_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `specification_text`
--
ALTER TABLE `specification_text`
  ADD CONSTRAINT `specification_text_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `specification_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `specification_text_ibfk_3` FOREIGN KEY (`spec_id`) REFERENCES `specification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `static_text_translate`
--
ALTER TABLE `static_text_translate`
  ADD CONSTRAINT `static_text_translate_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `static_text_translate_ibfk_2` FOREIGN KEY (`static_text_id`) REFERENCES `static_text` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `static_text_translate_ibfk_3` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `user_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `user_lang`
--
ALTER TABLE `user_lang`
  ADD CONSTRAINT `user_lang_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_lang_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_lang_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
