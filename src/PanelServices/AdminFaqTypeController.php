<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.01.2019
 * Time: 14:05
 */

namespace App\PanelServices;


use App\Entity\FaqType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminFaqTypeController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("faqtype", name="createFaqTypeAdmin", methods={"POST"})
     */
    public function createFaqTypeAdmin() {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $faqType) {
            $addFaqType = new FaqType();
            $addFaqType->setName($faqType["name"]);
            $addFaqType->setCode($faqType["code"]);

            $em->persist($addFaqType);
            $em->flush();
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("faqtype/{id}", name="updateFaqTypeAdmin", methods={"PUT"})
     */
    public function updateFaqTypeAdmin($id) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $updateFaqType = $em->getRepository(FaqType::class)->find($id);
        $updateFaqType->setName($data["name"]);
        $updateFaqType->setCode($data["code"]);
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("faqtype/{id}", name="getFaqTypeByIdAdmin", methods={"GET"})
     */
    public function getFaqTypeByIdAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $updateFaqType = $em->getRepository(FaqType::class)->getFaqTypeById($id);

        return new JsonResponse($updateFaqType[0]);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("faqtype/{id}", name="deleteFaqTypeByIdAdmin", methods={"DELETE"})
     */
    public function deleteFaqTypeByIdAdmin($id) {
        $em = $this->getDoctrine()->getManager();
        $deleteFaqType = $em->getRepository(FaqType::class)->find($id);
        $deleteFaqType->setDeletedAt(new \DateTime());
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("faqtype", name="listAllFaqTypeByAdmin", methods={"GET"})
     */
    public function listAllFaqTypeByAdmin() {
        $em = $this->getDoctrine()->getManager();

        $faqType = $em->getRepository(FaqType::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($faqType as $faq) {
            $dataArray[] = array(
                "id" => $faq->getId(),
                "name" => $faq->getName(),
                "code" => $faq->getCode(),
            );
        }

        return new JsonResponse($dataArray);
    }
}