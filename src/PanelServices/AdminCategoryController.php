<?php

namespace App\PanelServices;

use App\Entity\Category;
use App\Entity\CategoryFeature;
use App\Entity\CategoryFile;
use App\Entity\CategoryImage;
use App\Entity\CategorySpec;
use App\Entity\CategoryText;
use App\Entity\Country;
use App\Entity\Feature;
use App\Entity\FeatureType;
use App\Entity\File;
use App\Entity\FileType;
use App\Entity\ImageType;
use App\Entity\Language;
use App\Entity\Specification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategoryController extends Controller
{
    /**
     * @param $request
     * @Route("category", name="createCategory", methods={"POST"})
     * @return JsonResponse
     */
    public function createCategory(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $category) {
            $generateCategoryLink = AdminHelperController::getInstance()->buildSef($category["name"]);

            $addCategory = new Category();
            $addCategory->setName($category["name"]);
            $addCategory->setActive($category["active"]);
            $addCategory->setKeywords($category["keywords"]);
            if (isset($category["parentId"]) && $category["parentId"] != null) {
                $parentCategory = $em->getRepository(Category::class)->find($category["parentId"]);
                $addCategory->setParentId($parentCategory->getId());
            }
            $addCategory->setPriority($category["priority"]);
            $addCategory->setLink($generateCategoryLink);

            $em->persist($addCategory);
            $em->flush();

            if (isset($category["feature"]) && !empty($category["feature"])) {
                foreach ($category["feature"] as $catFeature) {
                    $feature = $em->getRepository(Feature::class)->find($catFeature["featureId"]);
                    $featureType = $em->getRepository(FeatureType::class)->find($catFeature["typeId"]);

                    $addCategoryFeature = new CategoryFeature();
                    $addCategoryFeature->setPriority($catFeature["priority"]);
                    $addCategoryFeature->setType($featureType);
                    $addCategoryFeature->setFeature($feature);
                    $addCategoryFeature->setCategory($addCategory);

                    $em->persist($addCategoryFeature);
                }
            }

            if (isset($category["file"]) && !empty($category["file"])) {
                foreach ($category["file"] as $catFile) {
                    $categoryFileType = $em->getRepository(FileType::class)->findOneBy(array(
                        "code" => $catFile["fileCode"]
                    ));

                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    define('file_upload','./uploads/files/file');

                    $replace = explode('base64,', $catFile["fileBase64"])[1];
//                $replace = str_replace('data:file/base64,','', $catFile["fileBase64"]);
                    $replace = str_replace(' ', '+', $replace);
                    $fileDecode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $fileMime = $split[1];

                    $mimeFunction = $this->getMimeType($fileMime);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse("dosya tipi hatali");
                    }
                    else if ($mimeFunction !== 'application/octet-stream') {
                        $split = explode( '/', $mimeType );
                        $fileExtension = $split[1];

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');

                    } else if ($mimeType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        $fileExtension = "xlsx";

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');
                    }

                    $addFile = new File();
                    $addFile->setName($catFile["fileName"]);
                    $addFile->setType($categoryFileType);
                    $addFile->setPath($baseUrl . $trimPath);
                    $em->persist($addFile);
                    $em->flush();

                    $categoryFile = new CategoryFile();
                    $categoryFile->setCategory($addCategory);
                    $categoryFile->setFile($addFile);
                    $categoryFile->setPriority($catFile["priority"]);
                    $em->persist($categoryFile);
                    $em->flush();
                }
            }

            if (isset($category["image"]) && !empty($category["image"])) {
                foreach ($category["image"] as $catImage) {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $catImage["imageCode"]));

                    if(!defined('UPLOAD_DIR')) {
                        define('UPLOAD_DIR','./uploads/images/images');
                    }

                    $replace = explode(',', $catImage["imageFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];

                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $addCategoryImage = new CategoryImage();
                    $addCategoryImage->setPriority($catImage["priority"]);
                    $addCategoryImage->setCategory($addCategory);
                    $addCategoryImage->setType($imageTypeCode);
                    $addCategoryImage->setPath($baseUrl . $trimPath);
                    $addCategoryImage->setActive($catImage["active"]);
                    $addCategoryImage->setName($catImage["name"]);

                    $em->persist($addCategoryImage);
                }
            }

            if (isset($category["spec"]) && !empty($category["spec"])) {
                foreach ($category["spec"] as $catSpec) {
                    $specification = $em->getRepository(Specification::class)->find($catSpec["specId"]);

                    $addCategorySpec = new CategorySpec();
                    $addCategorySpec->setCategory($addCategory);
                    $addCategorySpec->setSpec($specification);
                    $addCategorySpec->setValue($catSpec["value"]);
                    $addCategorySpec->setPriority($catSpec["priority"]);

                    $em->persist($addCategorySpec);
                    $em->flush();
                }
            }

            if (isset($category["text"]) && !empty($category["text"])) {
                foreach ($category["text"] as $catText) {
                    $country = $em->getRepository(Country::class)->find($catText["countryId"]);
                    $lang = $em->getRepository(Language::class)->find($catText["langId"]);

                    $generateLink = AdminHelperController::getInstance()->buildSef($catText["name"]);

                    $addCategoryText = new CategoryText();
                    $addCategoryText->setCategory($addCategory);
                    $addCategoryText->setName($catText["name"]);
                    $addCategoryText->setShortDescription($catText["shortDesc"]);
                    $addCategoryText->setLongDescription($catText["longDesc"]);
                    $addCategoryText->setButtonLink($catText["buttonLink"]);
                    $addCategoryText->setButton($catText["buttonName"]);
                    $addCategoryText->setSubtitle($catText["subTitle"]);
                    $addCategoryText->setSubDescription($catText["subDesc"]);
                    $addCategoryText->setLink($generateLink);
                    $addCategoryText->setKeywords($catText["keywords"]);
                    $addCategoryText->setCountry($country);
                    $addCategoryText->setLang($lang);

                    $em->persist($addCategoryText);
                }
            }

            $em->flush();
         }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @Route("category/{id}", name="updateCategory", methods={"PUT"})
     * @return JsonResponse
     */
    public function updateCategory($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);


        $generateCategoryLink = AdminHelperController::getInstance()->buildSef($data["name"]);

        $updateCategory = $em->getRepository(Category::class)->find($id);
        $updateCategory->setName($data["name"]);
        $updateCategory->setActive($data["active"]);
        $updateCategory->setKeywords($data["keywords"]);
        $updateCategory->setPriority($data["priority"]);
        $updateCategory->setLink($generateCategoryLink);
        if (isset($data["parentId"]) && $data["parentId"] != null) {
            $parentCategory = $em->getRepository(Category::class)->find($data["parentId"]);
            $updateCategory->setParentId($parentCategory->getId());
        }

        if (isset($data["feature"]) && !empty($data["feature"])) {
            foreach ($data["feature"] as $catFeature) {
                if(isset($catFeature["isDeleted"]) && $catFeature["isDeleted"] == true) {
                    $feature = $em->getRepository(Feature::class)->find($catFeature["featureId"]);
                    $featureType = $em->getRepository(FeatureType::class)->find($catFeature["featureType"]);
                    $removeCatFeature = $em->getRepository(CategoryFeature::class)->findOneBy(array(
                        "feature" => $feature,
                        "category" => $updateCategory,
                        "type" => $featureType
                    ));
                    $em->remove($removeCatFeature);
                    $em->flush();
                } else {
                    $isCatPersist = false;
                    $feature = $em->getRepository(Feature::class)->find($catFeature["featureId"]);
                    $featureType = $em->getRepository(FeatureType::class)->find($catFeature["featureType"]);

                    $addCategoryFeature = $em->getRepository(CategoryFeature::class)->findOneBy(array(
                        "feature" => $feature,
                        "category" => $updateCategory,
                        "type" => $featureType
                    ));
                    if(!$addCategoryFeature) {
                        $addCategoryFeature = new CategoryFeature();
                        $addCategoryFeature->setFeature($feature);
                        $addCategoryFeature->setCategory($updateCategory);
                        $isCatPersist = true;
                    }
                    $addCategoryFeature->setPriority($catFeature["priority"]);
                    $addCategoryFeature->setType($featureType);
                    $addCategoryFeature->setFeature($feature);
                    if ($isCatPersist) {
                        $em->persist($addCategoryFeature);
                    }
                    $em->flush();
                }
            }
        }
        if (isset($data["file"]) && !empty($data["file"])) {
            foreach ($data["file"] as $catFile) {
                if(isset($catFile["isDeleted"]) && $catFile["isDeleted"] == true) {
                    $fileId = $em->getRepository(File::class)->find($catFile["id"]);

                    $categoryFile = $em->getRepository(CategoryFile::class)->findOneBy(array(
                        "category" => $updateCategory,
                        "file" => $fileId
                    ));

                    $em->remove($categoryFile);
                    $em->flush();
                } else if(isset($catFile["isOld"]) && $catFile["isOld"] == true) {
                    $categoryFileType = $em->getRepository(FileType::class)->findOneBy(array(
                        "code" => $catFile["fileCode"]
                    ));

                    $fileId = $em->getRepository(File::class)->find($catFile["id"]);
                    $categoryFile = $em->getRepository(CategoryFile::class)->findOneBy(array(
                        "category" => $updateCategory,
                        "file" => $fileId
                    ));
                    $categoryFile->setCategory($updateCategory);
                    $categoryFile->setFile($fileId);
                    $categoryFile->setPriority($catFile["priority"]);
                    $fileId->setName($catFile["fileName"]);
                    $fileId->setType($categoryFileType);

                    $em->flush();
                } else {

                    $categoryFileType = $em->getRepository(FileType::class)->findOneBy(array(
                        "code" => $catFile["fileCode"]
                    ));

                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    define('file_upload','./uploads/files/file');

                    $replace = explode('base64,', $catFile["fileBase64"])[1];
//                  $replace = str_replace('data:file/base64,','', $catFile["fileBase64"]);
                    $replace = str_replace(' ', '+', $replace);
                    $fileDecode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $fileMime = $split[1];

                    $mimeFunction = $this->getMimeType($fileMime);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse("dosya tipi hatali");
                    }
                    else if ($mimeFunction !== 'application/octet-stream') {
                        $split = explode( '/', $mimeType );
                        $fileExtension = $split[1];

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');

                    } else if ($mimeType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        $fileExtension = "xlsx";

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');
                    }
                    $newFile = new File();
                    $newFile->setName("fileName");
                    $newFile->setPath($baseUrl . $trimPath);
                    $newFile->setType($categoryFileType);
                    $em->persist($newFile);
                    $em->flush();

                    $newCatFile = new CategoryFile();
                    $newCatFile->setCategory($updateCategory);
                    $newCatFile->setFile($newFile);
                    $newCatFile->setPriority($catFile["priority"]);
                    $em->persist($newCatFile);
                    $em->flush();
                }
            }
        }
        if (isset($data["image"]) && !empty($data["image"])) {
            foreach ($data["image"] as $catImage) {
                if(isset($catImage["isDeleted"]) && $catImage["isDeleted"] == true) {
                    $addCategoryImage = $addCategoryImage = $em->getRepository(CategoryImage::class)->findOneBy(array(
                        "category" => $updateCategory,
                        "id" => $catImage["id"]
                    ));
                    $em->remove($addCategoryImage);
                    $em->flush();
                } else if(isset($catImage["isOld"]) && $catImage["isOld"] == true) {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $catImage["imageCode"]));

                    $addCategoryImage = $addCategoryImage = $em->getRepository(CategoryImage::class)->findOneBy(array(
                        "category" => $updateCategory,
                        "id" => $catImage["id"]
                    ));
                    $addCategoryImage->setPriority($catImage["priority"]);
                    $addCategoryImage->setCategory($updateCategory);
                    $addCategoryImage->setType($imageTypeCode);
                    $addCategoryImage->setActive($catImage["active"]);
                    $addCategoryImage->setName($catImage["name"]);
                    $em->flush();
                } else {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $catImage["imageCode"]));
                    $newCatImage = new CategoryImage();
                    $newCatImage->setPriority($catImage["priority"]);
                    $newCatImage->setCategory($updateCategory);
                    $newCatImage->setType($imageTypeCode);
                    $newCatImage->setActive($catImage["active"]);
                    $newCatImage->setName($catImage["name"]);
                    if (isset($catImage["imageFile"]) && $catImage["imageFile"] != null) {
                        if(!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }

                        $replace = explode(',', $catImage["imageFile"])[1];
                        $replace = str_replace(' ', '+', $replace);
                        $decode = base64_decode($replace);

                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $imageType = $split[1];

                        $baseUrl = $this->container->getParameter('baseUrlHttp');

                        $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                        file_put_contents($file, $decode);
                        $trimPath = ltrim($file, '.');

                        $newCatImage->setPath($baseUrl . $trimPath);
                    }
                    $em->persist($newCatImage);
                    $em->flush();
                }
            }
        }
        if (isset($data["spec"]) && !empty($data["spec"])) {
            $addCategorySpec = $em->getRepository(CategorySpec::class)->findBy(array(
                "category" => $updateCategory,
            ));
            foreach ($addCategorySpec as $catSpecs) {
                $em->remove($catSpecs);
                $em->flush();
            }

            foreach ($data["spec"] as $catSpec) {
                $specification = $em->getRepository(Specification::class)->find($catSpec["specId"]);

                $createCategorySpec = new CategorySpec();
                $createCategorySpec->setCategory($updateCategory);
                $createCategorySpec->setSpec($specification);
                $createCategorySpec->setValue($catSpec["value"]);
                $createCategorySpec->setPriority($catSpec["priority"]);

                $em->persist($createCategorySpec);

                $em->flush();

            }
        }
         if (isset($data["text"]) && !empty($data["text"])) {
             foreach ($data["text"] as $catText) {
                 $isPersist = false;
                 $country = $em->getRepository(Country::class)->find($catText["countryId"]);
                 $lang = $em->getRepository(Language::class)->find($catText["langId"]);

                 $generateLink = AdminHelperController::getInstance()->buildSef($catText["name"]);

                 $addCategoryText = $em->getRepository(CategoryText::class)->findOneBy(array(
                     "category" => $updateCategory,
                     "country" => $country,
                     "lang" => $lang
                 ));
                 if (!$addCategoryText) {
                     $addCategoryText = new CategoryText();
                     $isPersist = true;
                 }
                 $addCategoryText->setCategory($updateCategory);
                 $addCategoryText->setName($catText["name"]);
                 $addCategoryText->setShortDescription($catText["shortDesc"]);
                 $addCategoryText->setLongDescription($catText["longDesc"]);
                 $addCategoryText->setButtonLink($catText["buttonLink"]);
                 $addCategoryText->setButton($catText["buttonName"]);
                 $addCategoryText->setSubtitle($catText["subTitle"]);
                 $addCategoryText->setSubDescription($catText["subDesc"]);
                 $addCategoryText->setLink($generateLink);
                 $addCategoryText->setKeywords($catText["keywords"]);
                 $addCategoryText->setCountry($country);
                 $addCategoryText->setLang($lang);
                 if ($isPersist) {
                     $em->persist($addCategoryText);
                 }
                 $em->flush();
             }
         }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @Route("category/{id}", name="deleteCategory", methods={"DELETE"})
     * @return JsonResponse
     */
    public function deleteCategory($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteCategory = $em->getRepository(Category::class)->findOneBy(array(
            "id" => $id
        ));

        $deleteCategory->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("/category/{id}", name="getAdminCategoryById", methods={"GET"})
     */
    public function getAdminCategoryById($id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository(Category::class)->getAllCategoryAdmin($id);

        return new JsonResponse($category[0]);
    }

    /**
     * @return JsonResponse
     * @Route("/category", name="getAllCategoryAdmin", methods={"GET"})
     */
    public function getAllCategoryAdmin() {
        $em = $this->getDoctrine()->getManager();

        $allCategory = $em->getRepository(Category::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($allCategory as $cat) {
            $dataArray[] = array(
                "id" => $cat->getId(),
                "name" => $cat->getName()
            );
        }
        return new JsonResponse($dataArray);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}