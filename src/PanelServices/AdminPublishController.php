<?php

namespace App\PanelServices;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminPublishController extends Controller
{
  /**
     * @Route("publish", name="publish")
     */
    public function publish(Request $request) {

		chdir($this->container->getParameter('projectDir'));
		exec("mysqldump -u root -placivert sharp_engine > sharp_engine.sql");
		exec("git add .");
		exec("git commit -m 'publish'");
		exec("git push origin master");

		return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }
}