<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 12:09
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\FeaturedSpec;
use App\Entity\FeaturedSpecImage;
use App\Entity\ImageType;
use App\Entity\Language;
use App\Entity\Specification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminFeaturedSpecController extends Controller
{
    /**
     * @param $request
     * @Route("featuredspec", name="createFeatureSpec", methods={"POST"})
     * @return JsonResponse
     */
    public function createFeatureSpec(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $featuredSpec) {
            $spec = $em->getRepository(Specification::class)->find($featuredSpec["specId"]);
            $lang = $em->getRepository(Language::class)->find($featuredSpec["langId"]);
            $country = $em->getRepository(Country::class)->find($featuredSpec["countryId"]);

            $addFeaturedSpec = new FeaturedSpec();
            $addFeaturedSpec->setSpec($spec);
            $addFeaturedSpec->setLang($lang);
            $addFeaturedSpec->setCountry($country);
            $addFeaturedSpec->setDescription($featuredSpec["description"]);
            $addFeaturedSpec->setTitle($featuredSpec["title"]);
            $addFeaturedSpec->setSubTitle($featuredSpec["subTitle"]);
            $addFeaturedSpec->setPriority($featuredSpec["priority"]);

            $em->persist($addFeaturedSpec);

            foreach ($featuredSpec["image"] as $featuredSpecImage) {
                $imageTypeCode = $em->getRepository(ImageType::class)->find($featuredSpecImage["imageTypeId"]);
                if (!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }

                $replace = explode(',', $featuredSpecImage["imageFile"])[1];
                $replace = str_replace(' ', '+', $replace);
                $decode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $imageType = $split[1];
                $mimeFunction = $this->getMimeType($imageType);
                if ($mimeFunction == 'application/octet-stream') {
                    return new JsonResponse(array(
                        "message" => "FAILED"
                    ));
                }
                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                file_put_contents($file, $decode);
                $trimPath = ltrim($file, '.');

                $addFeaturedSpecImage = new FeaturedSpecImage();
                $addFeaturedSpecImage->setPriority($featuredSpecImage["priority"]);
                $addFeaturedSpecImage->setProduct($addFeaturedSpec);
                $addFeaturedSpecImage->setName($featuredSpecImage["name"]);
                $addFeaturedSpecImage->setType($imageTypeCode);
                $addFeaturedSpecImage->setPath($baseUrl . $trimPath);
                $addFeaturedSpecImage->setActive($featuredSpecImage["active"]);

                $em->persist($addFeaturedSpecImage);
                $em->flush();
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("featuredspec/{id}", name="updateFeaturedSpec", methods={"PUT"})
     */
    public function updateFeaturedSpec($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $spec = $em->getRepository(Specification::class)->find($data["specId"]);
        $lang = $em->getRepository(Language::class)->find($data["langId"]);
        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $updateFeatureSpec = $em->getRepository(FeaturedSpec::class)->find($id);
        $updateFeatureSpec->setSpec($spec);
        $updateFeatureSpec->setLang($lang);
        $updateFeatureSpec->setCountry($country);
        $updateFeatureSpec->setDescription($data["description"]);
        $updateFeatureSpec->setTitle($data["title"]);
        $updateFeatureSpec->setSubTitle($data["subTitle"]);
        $updateFeatureSpec->setPriority($data["priority"]);

        if (isset($data["image"]) && $data["image"] != null) {
            foreach ($data["image"] as $featuredSpecImage) {
                if(isset($featuredSpecImage["isDeleted"]) && $featuredSpecImage["isDeleted"] == true) {
                    $updateFeaturedSpecImage = $em->getRepository(FeaturedSpecImage::class)->findOneBy(array(
                        "featuredSpec" => $updateFeatureSpec,
                        "id" => $featuredSpecImage["id"]
                    ));
                    $em->remove($updateFeaturedSpecImage);
                    $em->flush();
                } else if (isset($featuredSpecImage["isOld"]) && $featuredSpecImage["isOld"] == true) {

                    $updateFeaturedSpecImage = $em->getRepository(FeaturedSpecImage::class)->findOneBy(array(
                        "featuredSpec" => $updateFeatureSpec,
                        "id" => $featuredSpecImage["id"]
                    ));
                    $imageTypeCode = $em->getRepository(ImageType::class)->find($featuredSpecImage["imageTypeId"]);
                    $updateFeaturedSpecImage->setType($imageTypeCode);
                    $updateFeaturedSpecImage->setActive($featuredSpecImage["active"]);
                    $updateFeaturedSpecImage->setName($featuredSpecImage["name"]);
                    $updateFeaturedSpecImage->setPriority($featuredSpecImage["priority"]);
                    $updateFeaturedSpecImage->setProduct($updateFeatureSpec);
                    $em->flush();
                } else {
                    $newFeatureSpecImage = new FeaturedSpecImage();
                    $imageTypeCode = $em->getRepository(ImageType::class)->find($featuredSpecImage["imageTypeId"]);
                    $newFeatureSpecImage->setType($imageTypeCode);
                    $newFeatureSpecImage->setActive($featuredSpecImage["active"]);
                    $newFeatureSpecImage->setName($featuredSpecImage["name"]);
                    $newFeatureSpecImage->setPriority($featuredSpecImage["priority"]);
                    $newFeatureSpecImage->setProduct($updateFeatureSpec);

                    if (isset($featuredSpecImage["imageFile"]) && $featuredSpecImage["imageFile"] != null) {
                        if (!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }

                        $replace = explode(',', $featuredSpecImage["imageFile"])[1];
                        $replace = str_replace(' ', '+', $replace);
                        $decode = base64_decode($replace);

                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $imageType = $split[1];

                        $mimeFunction = $this->getMimeType($imageType);
                        if ($mimeFunction == 'application/octet-stream') {
                            return new JsonResponse(array(
                                "message" => "Wrong file format!"
                            ));
                        }
                        $baseUrl = $this->container->getParameter('baseUrlHttp');

                        $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                        file_put_contents($file, $decode);
                        $trimPath = ltrim($file, '.');

                        $newFeatureSpecImage->setPath($baseUrl . $trimPath);
                    }
                    $em->persist($newFeatureSpecImage);
                    $em->flush();
                }
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuredspec/{id}", name="deleteFeaturedSpec", methods={"DELETE"})
     */
    public function deleteFeaturedSpec($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteFeaturedSpec = $em->getRepository(FeaturedSpec::class)->find($id);
        $deleteFeaturedSpec->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("featuredspec", name="getAllFeaturedSpec", methods={"GET"})
     */
    public function getAllFeaturedSpec() {
        $em = $this->getDoctrine()->getManager();

        $featuredSpec = $em->getRepository(FeaturedSpec::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($featuredSpec as $featured) {
            $tmp = array(
                "id" => $featured->getId(),
                "description" => $featured->getDescription(),
                "title" => $featured->getTitle(),
                "subTitle" => $featured->getSubTitle()
            );

            $spec = $featured->getSpec();
            $tmp["spec"][] = array(
                "id" => $spec->getId(),
                "name" => $spec->getName()
            );

            $country = $featured->getCountry();
            $tmp["country"] = array(
                "id" => $country->getId(),
                "code" => $country->getCode()
            );

            $language = $featured->getLang();
            $tmp["language"] = array(
                "id" => $language->getId(),
                "origName" => $language->getOrigName()
            );

            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("featuredspec/{id}", name="getFeaturedSpecById", methods={"GET"})
     * @return JsonResponse
     */
    public function getFeaturedSpecById($id) {
        $em = $this->getDoctrine()->getManager();

        $featuredSpec = $em->getRepository(FeaturedSpec::class)->getAllFeaturedSpecById($id);

        return new JsonResponse($featuredSpec[0]);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}