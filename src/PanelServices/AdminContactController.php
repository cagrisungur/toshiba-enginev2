<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 14:26
 */

namespace App\PanelServices;


use App\Entity\Contact;
use App\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminContactController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("contact", name="createContact", methods={"POST"})
     */
    public function createContact() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $contact) {
            $country = $em->getRepository(Country::class)->find($contact["countryId"]);

            $addContact = new Contact();
            $addContact->setCountry($country);
            $addContact->setName($contact["name"]);
            $addContact->setPhone($contact["phone"]);
            $addContact->setAddress($contact["address"]);
            $addContact->setEmail($contact["email"]);
            $addContact->setFax($contact["fax"]);
            $addContact->setLat($contact["lat"]);
            $addContact->setLng($contact["lng"]);
            $addContact->setWebsite($contact["website"]);

            $em->persist($addContact);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("contact/{id}", name="updateContact", methods={"PUT"})
     */
    public function updateContact($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $updateContact = $em->getRepository(Contact::class)->find($id);
        $updateContact->setCountry($country);
        $updateContact->setName($data["name"]);
        $updateContact->setPhone($data["phone"]);
        $updateContact->setAddress($data["address"]);
        $updateContact->setEmail($data["email"]);
        $updateContact->setFax($data["fax"]);
        $updateContact->setLat($data["lat"]);
        $updateContact->setLng($data["lng"]);
        $updateContact->setWebsite($data["website"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("contact", name="getAllContactForAdmin", methods={"GET"})
     */
    public function getAllContact() {
        $em = $this->getDoctrine()->getManager();

        $dataArray = array();

        $contacts = $em->getRepository(Contact::class)->findBy(array(
            "deletedAt" => null
        ));

        foreach ($contacts as $contact) {
            $tmp = array(
                "id" => $contact->getId(),
                "name" => $contact->getName(),
                "website" => $contact->getWebSite(),
                "phone" => $contact->getPhone(),
                "fax" => $contact->getFax(),
                "address" => $contact->getAddress(),
                "email" => $contact->getEmail(),
                "lat" => $contact->getLat(),
                "lng" => $contact->getLng(),
                "active" => $contact->getActive()
            );
            $country = $contact->getCountry();
            $tmp["country"] = array(
                "id" => $country->getId(),
                "name" => $country->getName(),
                "code" => $country->getCode()
            );
            $dataArray[] = $tmp;
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("contact/{id}", name="deleteContact", methods={"DELETE"})
     */
    public function deleteContact($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteContact = $em->getRepository(Contact::class)->find($id);
        $deleteContact->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("contact/{id}", name="getContactByIdAdmin", methods={"GET"})
     */
    public function getContactByIdAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $contact = $em->getRepository(Contact::class)->getContactByIdAdmin($id);

        return new JsonResponse($contact[0]);
    }
}