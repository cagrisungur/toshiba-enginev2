<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.11.2018
 * Time: 17:17
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\SalesPoint;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSalesPointController extends Controller
{
    /**
     * @Route("salespoint", name="createSalesPoint", methods={"POST"})
     */
    public function createSalesPoint(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $saleP) {
            $country = $em->getRepository(Country::class)->find($saleP["countryId"]);

            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }


            $replace = explode(',', $saleP["image"]);
            $replace = $replace[0];
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $baseUrl = $this->container->getParameter('baseUrlHttp');

            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $salesPoint =  new SalesPoint();

            $salesPoint->setName($saleP["name"]);
            $salesPoint->setWebsite($saleP["website"]);
            $salesPoint->setActive($saleP["active"]);
            $salesPoint->setAddress($saleP["address"]);
            $salesPoint->setCountry($country);
            $salesPoint->setPhone($saleP["phone"]);
            $salesPoint->setFax($saleP["fax"]);
            $salesPoint->setEmail($saleP["email"]);
            $salesPoint->setLat($saleP["lat"]);
            $salesPoint->setLng($saleP["lng"]);
            $salesPoint->setLogoPath($baseUrl . $trimPath);

            $em->persist($salesPoint);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("salespoint/{id}", name="updateSalesPoint", methods={"PUT"})
     */
    public function updateSalesPoint($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $salesPoint =  $em->getRepository(SalesPoint::class)->find($id);

        $salesPoint->setName($data["name"]);
        $salesPoint->setWebsite($data["website"]);
        $salesPoint->setActive($data["active"]);
        $salesPoint->setAddress($data["address"]);
        if (isset($data["image"]) && $data["image"] != null) {
            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }

            $replace = explode(',', $data["image"])[1];
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $baseUrl = $this->container->getParameter('baseUrlHttp');

            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $salesPoint->setLogoPath($baseUrl . $trimPath);
        }
        $salesPoint->setCountry($country);
        $salesPoint->setPhone($data["phone"]);
        $salesPoint->setFax($data["fax"]);
        $salesPoint->setEmail($data["email"]);
        $salesPoint->setLat($data["lat"]);
        $salesPoint->setLng($data["lng"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("salespoint/{id}", name="deleteSalesPoint", methods={"DELETE"})
     */
    public function deleteSalesPoint($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSalesPoint = $em->getRepository(SalesPoint::class)->find($id);
        $deleteSalesPoint->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("salespoint", name="getAllSalesPointForAdmin", methods={"GET"})
     */
    public function getAllSalesPoint() {
        $em = $this->getDoctrine()->getManager();

        $salesPoint = $em->getRepository(SalesPoint::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($salesPoint as $sPoint) {
            $tmp = array(
                "id" => $sPoint->getId(),
                "name" => $sPoint->getName(),
                "website" => $sPoint->getWebsite(),
                "phone"=> $sPoint->getPhone(),
                "fax" => $sPoint->getFax(),
                "priority" => $sPoint->getPriority(),
                "email" => $sPoint->getEmail(),
                "lat" => $sPoint->getLat(),
                "lng" => $sPoint->getLng(),
                "logoPath" => $sPoint->getLogoPath()
            );
            $salesPointCountry = $sPoint->getCountry();
                $tmp["country"] = array(
                  "id"  => $salesPointCountry->getId(),
                  "origName" => $salesPointCountry->getOrigName(),
                  "code" => $salesPointCountry->getCode()
                );
            $dataArray[] = $tmp;
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("salespoint/{id}", name="getSalesPointById", methods={"GET"})
     * @return JsonResponse
     */
    public function getSalesPointById($id) {
        $em = $this->getDoctrine()->getManager();

        $allSalesPoint = $em->getRepository(SalesPoint::class)->getAllSalesPointById($id);

        return new JsonResponse($allSalesPoint[0]);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}