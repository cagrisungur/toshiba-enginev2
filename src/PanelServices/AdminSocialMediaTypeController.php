<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 26.11.2018
 * Time: 10:18
 */

namespace App\PanelServices;


use App\Entity\SocialMediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSocialMediaTypeController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("socialmediatype", name="createSocialMediaTypeAdmin", methods={"POST"})
     */
    public function createSocialMediaTypeAdmin(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $baseUrl = $this->container->getParameter('baseUrlHttp');

        foreach ($data as $mediaType) {
            $socialMediaType = new SocialMediaType();
            $socialMediaType->setName($mediaType["name"]);
            $socialMediaType->setCode($mediaType["code"]);

            define('UPLOAD_DIR','./uploads/images/images');

            $replace = str_replace('data:image/png;base64,','', $mediaType["icon"]);
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $socialMediaType->setIcon($baseUrl . $trimPath);
            $em->persist($socialMediaType);
            $em->flush();
        }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @Route("socialmediatype/{id}", name="updateSocialMediaTypeAdmin", methods={"PUT"})
     */
    public function updateSocialMediaTypeAdmin($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $baseUrl = $this->container->getParameter('baseUrlHttp');

        $socialMediaType = $em->getRepository(SocialMediaType::class)->find($id);
        $socialMediaType->setName($data["name"]);
        $socialMediaType->setCode($data["code"]);

        if (isset($data["icon"]) && $data["icon"] != null) {
            define('UPLOAD_DIR','./uploads/images/images');

            $replace = str_replace('data:image/png;base64,','', $data["icon"]);
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "Image Not Supported!!"
                ));
            }
            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $socialMediaType->setIcon($baseUrl . $trimPath);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("socialmediatype/{id}", name="getSocialMediaTypeById", methods={"GET"})
     */
    public function getSocialMediaTypeById($id) {
        $em = $this->getDoctrine()->getManager();

        $mediaType = $em->getRepository(SocialMediaType::class)->getSocialMediaTypeAdminById($id);

        return new JsonResponse($mediaType[0]);
    }

    /**
     * @return JsonResponse
     * @Route("socialmediatype", name="getAllSocialMediaTypeAdmin", methods={"GET"})
     */
    public function getAllSocialMediaTypeAdmin() {
        $em = $this->getDoctrine()->getManager();

        $mediaType = $em->getRepository(SocialMediaType::class)->findAll();

        $dataArray = array();

        foreach ($mediaType as $socialType) {
            $dataArray[] = array(
                "id" => $socialType->getId(),
                "name" => $socialType->getName(),
                "code" => $socialType->getCode(),
                "iconPath" => $socialType->getIcon()
            );
        }
        return new JsonResponse($dataArray);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}