<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 14:08
 */

namespace App\PanelServices;


use App\Entity\Campaign;
use App\Entity\CampaignAction;
use App\Entity\CampaignText;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCampaignController extends Controller
{
    /**
     * @Route("campaign", name="createCampaign", methods={"POST"})
     */
    public function createCampaign() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $campaign) {
            $country = $em->getRepository(Country::class)->find($campaign["countryId"]);

            $date = new \DateTime();
            $campaignStartDate  = $date->setTimestamp($campaign["startDate"]);
            $campaignEndDate = $date->setTimestamp($campaign["endDate"]);

            $addCampaign = new Campaign();
            $addCampaign->setCountry($country);
            $addCampaign->setActive($campaign["active"]);
            $addCampaign->setStartDate($campaignStartDate);
            $addCampaign->setEndDate($campaignEndDate);

            $em->persist($addCampaign);

            foreach ($campaign["campaignAction"] as $campAction) {
                $campActionCronDate = $date->setTimestamp($campAction["cronDate"]);

                $campaignAction = new CampaignAction();
                $campaignAction->setCampaign($addCampaign);
                $campaignAction->setCronDate($campActionCronDate);
                $campaignAction->setCommand($campAction["command"]);
                $campaignAction->setStatus($campAction["status"]);

                $em->persist($campaignAction);
            }

            foreach ($campaign["text"] as $campText) {
                $lang = $em->getRepository(Language::class)->find($campText["languageId"]);

                $generateLink = AdminHelperController::getInstance()->buildSef($campText["name"]);

                $addCampText = new CampaignText();
                $addCampText->setCampaign($addCampaign);
                $addCampText->setLongDescription($campText["longDesc"]);
                $addCampText->setShortDescription($campText["shortDesc"]);
                $addCampText->setName($campText["name"]);
                $addCampText->setLang($lang);
                $addCampText->setLink($generateLink);

                $em->persist($addCampText);
            }
        }
        $em->flush();

        return new Response("success");
    }

    /**
     * @Route("campaign/{id}", name="updateCampaign", methods={"PUT"})
     */
    public function updateCampaign($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $campaign) {
            $country = $em->getRepository(Country::class)->find($campaign["countryId"]);

            $date = new \DateTime();
            $campaignStartDate  = $date->setTimestamp($campaign["startDate"]);
            $campaignEndDate = $date->setTimestamp($campaign["endDate"]);

            $updateCampaign = $em->getRepository(Campaign::class)->find($id);
            $updateCampaign->setCountry($country);
            $updateCampaign->setActive($campaign["active"]);
            $updateCampaign->setStartDate($campaignStartDate);
            $updateCampaign->setEndDate($campaignEndDate);

            foreach ($campaign["campaignAction"] as $campAction) {
                $campActionCronDate = $date->setTimestamp($campAction["cronDate"]);

                $campaignAction = $em->getRepository(CampaignAction::class)->findOneBy(array(
                   "campaign" => $updateCampaign
                ));
                $campaignAction->setCampaign($updateCampaign);
                $campaignAction->setCronDate($campActionCronDate);
                $campaignAction->setCommand($campAction["command"]);
                $campaignAction->setStatus($campAction["status"]);
            }

            foreach ($campaign["text"] as $campText) {
                $lang = $em->getRepository(Language::class)->find($campText["languageId"]);

                $addCampText = $em->getRepository(CampaignText::class)->findOneBy(array(
                   "campaign" => $updateCampaign,
                   "lang" => $lang
                ));

                $generateLink = AdminHelperController::getInstance()->buildSef($campText["name"]);

                $addCampText->setCampaign($updateCampaign);
                $addCampText->setLongDescription($campText["longDesc"]);
                $addCampText->setShortDescription($campText["shortDesc"]);
                $addCampText->setName($campText["name"]);
                $addCampText->setLang($lang);
                $addCampText->setLink($generateLink);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @Route("campaign/{id}", name="deleteCampaign", methods={"DELETE"})
     */
    public function deleteCampaign($id) {

        $em = $this->getDoctrine()->getManager();

        $deleteCampaign = $em->getRepository(Campaign::class)->find($id);

        $deleteCampaign->setDeletedAt(new \DateTime());

        $em->flush();

    }

    /**
     * @Route("campaign", name="getAllCampaignForAdmin", methods={"GET"})
     */
    public function getAllCampaign() {
        $em = $this->getDoctrine()->getManager();

        $campaign = $em->getRepository(Campaign::class)->getAllAdminCampaign();

        return new JsonResponse($campaign);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("campaign/{id}", name="getAllCampaignById", methods={"GET"})
     */
    public function getAllCampaignById($id) {
        $em = $this->getDoctrine()->getManager();

        $campaign = $em->getRepository(Campaign::class)->getAllAdminCampaignById($id);

        return new JsonResponse($campaign[0]);
    }
}