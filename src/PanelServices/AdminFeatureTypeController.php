<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.11.2018
 * Time: 16:39
 */

namespace App\PanelServices;


use App\Entity\Feature;
use App\Entity\FeatureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminFeatureTypeController extends Controller
{
    /**
     * @Route("featuretype", name="createFeatureType", methods={"POST"})
     */
    public function createFeatureType() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $featureT) {
            $featureType = new FeatureType();
            $featureType->setName($featureT["name"]);
            $featureType->setCode($featureT["code"]);

            $em->persist($featureType);
        }
        $em->flush();

        return new JsonResponse("success");
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuretype/{id}", name="updateFeatureType", methods={"PUT"})
     */
    public function updateFeatureType($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $featureType = $em->getRepository(FeatureType::class)->find($id);
        $featureType->setName($data["name"]);
        $featureType->setCode($data["code"]);

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuretype/{id}", name="deleteFeatureType", methods={"DELETE"})
     */
    public function deleteFeatureType($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteFeatureType = $em->getRepository(FeatureType::class)->find($id);
        $deleteFeatureType->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("featuretype", name="getAllFeatureTypeForAdmin", methods={"GET"})
     */
    public function getAllFeatureType() {
        $em = $this->getDoctrine()->getManager();

        $featureType = $em->getRepository(FeatureType::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();
        foreach ($featureType as $type) {
            $dataArray[] = array(
                "id" => $type->getId(),
                "name" => $type->getName(),
                "code" => $type->getCode()
            );
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuretype/{id}", name="getFeatureTypeById", methods={"GET"})
     */
    public function getFeatureTypeById($id) {
        $em = $this->getDoctrine()->getManager();

        $featureType = $em->getRepository(Feature::class)->getAllFeatureTypeById($id);

        return new JsonResponse($featureType[0]);
    }
}