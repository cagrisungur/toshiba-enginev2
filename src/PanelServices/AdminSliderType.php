<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 15:57
 */

namespace App\PanelServices;


use App\Entity\Slider;
use App\Entity\SliderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSliderType extends Controller
{
    /**
     * @return JsonResponse
     * @Route("slidertype", name="getAllSliderTypeForAdmin", methods={"GET"})
     */
    public function getAllSliderType() {
        $em = $this->getDoctrine()->getManager();

        $dataArray = array();

        $sliderType = $em->getRepository(SliderType::class)->findBy(array(
            "deletedAt" => null
        ));

        foreach ($sliderType as $type) {
            $dataArray[] = array(
                "id" => $type->getId(),
                "name" => $type->getName(),
                "code" => $type->getCode()
            );
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @return JsonResponse
     * @Route("slidertype", name="createSliderType", methods={"POST"})
     */
    public function createSliderType() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $sliderType) {
            $type = new SliderType();
            $type->setName($sliderType["name"]);
            $type->setCode($sliderType["code"]);

            $em->persist($type);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("slidertype/{id}", name="updateSliderType", methods={"PUT"})
     */
    public function updateSliderType($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $sliderType = $em->getRepository(SliderType::class)->find($id);
        $sliderType->setName($data["name"]);
        $sliderType->setCode($data["code"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("slidertype/{id}", name="deleteSliderType", methods={"DELETE"})
     */
    public function deleteSliderType($id) {
        $em = $this->getDoctrine()->getManager();

        $sliderType = $em->getRepository(SliderType::class)->find($id);
        $sliderType->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("slidertype/{id}", name="getSliderTypeById", methods={"GET"})
     */
    public function getSliderTypeById($id) {
        $em = $this->getDoctrine()->getManager();

        $sliderType = $em->getRepository(SliderType::class)->getSliderTypeById($id);

        return new JsonResponse($sliderType[0]);
    }
}