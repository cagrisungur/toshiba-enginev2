<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 12:02
 */

namespace App\PanelServices;


use App\Entity\Category;
use App\Entity\CategorySpec;
use App\Entity\Specification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategorySpecController extends Controller
{
    /**
     * @return Response
     * @Route("categoryspec", name="createCategorySpec", methods={"POST"})
     */
    public function createCategorySpec() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $catSpec) {
            $category = $em->getRepository(Category::class)->find($catSpec["categoryId"]);
            $specification = $em->getRepository(Specification::class)->find($catSpec["specId"]);

            $addCatSpec = new CategorySpec();
            $addCatSpec->setCategory($category);
            $addCatSpec->setSpec($specification);
            $addCatSpec->setValue($catSpec["value"]);
            $addCatSpec->setPriority($catSpec["priority"]);

            $em->persist($addCatSpec);
        }
        $em->flush();

        return new Response("success");
    }

    /**
     * @Route("categoryspec", name="getAllCategorySpecForAdmin", methods={"GET"})
     */
    public function getAllCategorySpec() {
        $em = $this->getDoctrine()->getManager();

        $catSpec = $em->getRepository(CategorySpec::class)->getAllCategorySpecById();

        return new JsonResponse($catSpec);
    }


}