<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 14:05
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\SmtpSetting;
use App\Entity\SmtpType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSmtpSettingController extends Controller
{
    /**
     * @Route("smtp", name="createSmtp", methods={"POST"})
     */
    public function createSmtp() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $smtp) {
            $country = $em->getRepository(Country::class)->find($smtp["countryId"]);
            $smtpType = $em->getRepository(SmtpType::class)->find($smtp["typeId"]);

            $addSmtp = new SmtpSetting();
            $addSmtp->setCountry($country);
            $addSmtp->setType($smtpType);
            $addSmtp->setUsername($smtp["username"]);
            $addSmtp->setMail($smtp["mail"]);
            $addSmtp->setHost($smtp["host"]);
            $addSmtp->setPort($smtp["port"]);
            $addSmtp->setUseSsl($smtp["ssl"]);
            $addSmtp->setPassword($smtp["password"]);

            $em->persist($addSmtp);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("smtp/{id}", name="updateSmtpSetting", methods={"PUT"})
     */
    public function updateSmtpSetting($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);
        $smtpType = $em->getRepository(SmtpType::class)->find($data["typeId"]);

        $updateSmtp = $em->getRepository(SmtpSetting::class)->find($id);
        $updateSmtp->setCountry($country);
        $updateSmtp->setType($smtpType);
        $updateSmtp->setUsername($data["username"]);
        $updateSmtp->setMail($data["mail"]);
        $updateSmtp->setHost($data["host"]);
        $updateSmtp->setPort($data["port"]);
        $updateSmtp->setUseSsl($data["ssl"]);
        $updateSmtp->setPassword($data["password"]);

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("smtp/{id}", name="deleteSmtpSetting", methods={"DELETE"})
     */
    public function deleteSmtpSetting($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSmtp = $em->getRepository(SmtpSetting::class)->find($id);
        $deleteSmtp->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("smtp", name="getAllSmtp", methods={"GET"})
     */
    public function getAllSmtp() {
        $em = $this->getDoctrine()->getManager();

        $smtp = $em->getRepository(SmtpSetting::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($smtp as $s) {
            $tmp = array(
              "id" => $s->getId(),
              "userName" => $s->getUserName(),
              "mail" => $s->getMail(),
              "host" => $s->getHost(),
              "port" => $s->getPort(),
              "useSsl" => $s->getUseSsl(),
            );
            $tmp["country"] = array(
              "id" => $s->getCountry()->getId(),
              "name" => $s->getCountry()->getName()
            );
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("smtp/{id}", name="getSmtpById", methods={"GET"})
     * @return JsonResponse
     */
    public function getSmtpById($id) {
        $em = $this->getDoctrine()->getManager();

        $smtp = $em->getRepository(SmtpSetting::class)->getAllSmtpById($id);

        return new JsonResponse($smtp[0]);
    }
}