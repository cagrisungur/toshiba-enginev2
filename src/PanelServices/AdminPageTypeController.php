<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 16:25
 */

namespace App\PanelServices;


use App\Entity\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminPageTypeController extends Controller
{
    /**
     * @Route("pagetype", name="createPageType", methods={"POST"})
     */
    public function createPageType() {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $pageType) {
            $addPageType = new PageType();
            $addPageType->setName($pageType["name"]);
            $addPageType->setCode($pageType["code"]);

            $em->persist($addPageType);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("pagetype/{id}", name="updatePageType", methods={"PUT"})
     */
    public function updatePageType($id) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $addPageType = $em->getRepository(PageType::class)->find($id);
        $addPageType->setName($data["name"]);
        $addPageType->setCode($data["code"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("pagetype/{id}", name="deletePageType", methods={"DELETE"})
     */
    public function deletePageType($id) {
        $em = $this->getDoctrine()->getManager();

        $deletePageType = $em->getRepository(PageType::class)->find($id);
        $deletePageType->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("pagetype", name="getAllPageTypeForAdmin", methods={"GET"})
     */
    public function getAllPageType() {
        $em = $this->getDoctrine()->getManager();

        $pageType = $em->getRepository(PageType::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($pageType as $type) {
            $dataArray[] = array(
              "id" => $type->getId(),
              "name" => $type->getName(),
              "code" => $type->getCode()
            );
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("pagetype/{id}", name="getPageTypeById", methods={"GET"})
     * @return JsonResponse
     */
    public function getPageTypeById($id) {
        $em = $this->getDoctrine()->getManager();

        $pageType = $em->getRepository(PageType::class)->getAllPageTypeById($id);

        return new JsonResponse($pageType[0]);
    }
}