<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 10:50
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\FeaturedProduct;
use App\Entity\Language;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminFeaturedProductController extends Controller
{
    /**
     * @Route("featuredproduct", name="createFeaturedProduct", methods={"POST"})
     */
    public function createFeaturedProduct() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $featuredProduct) {
            $product = $em->getRepository(Product::class)->find($featuredProduct["productId"]);
            $lang = $em->getRepository(Language::class)->find($featuredProduct["langId"]);
            $country = $em->getRepository(Country::class)->find($featuredProduct["countryId"]);

            $addFeaturedProduct = new FeaturedProduct();
            $addFeaturedProduct->setProduct($product);
            $addFeaturedProduct->setLang($lang);
            $addFeaturedProduct->setCountry($country);
            $addFeaturedProduct->setDescription($featuredProduct["description"]);
            $addFeaturedProduct->setTitle($featuredProduct["title"]);

            $em->persist($addFeaturedProduct);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuredproduct/{id}", name="updateFeaturedProduct", methods={"PUT"})
     */
    public function updateFeaturedProduct($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $product = $em->getRepository(Product::class)->find($data["productId"]);
        $lang = $em->getRepository(Language::class)->find($data["langId"]);
        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $updateFeaturedProduct = $em->getRepository(FeaturedProduct::class)->find($id);
        $updateFeaturedProduct->setProduct($product);
        $updateFeaturedProduct->setLang($lang);
        $updateFeaturedProduct->setCountry($country);
        $updateFeaturedProduct->setDescription($data["description"]);
        $updateFeaturedProduct->setTitle($data["title"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuredproduct/{id}", name="deleteFeaturedProduct", methods={"DELETE"})
     */
    public function deleteFeaturedProduct($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteFeaturedProduct = $em->getRepository(FeaturedProduct::class)->find($id);
        $deleteFeaturedProduct->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("featuredproduct", name="getAllFeaturedProductForAdmin", methods={"GET"})
     */
    public function getAllFeaturedProduct() {
        $em = $this->getDoctrine()->getManager();

        $featuredProduct = $em->getRepository(FeaturedProduct::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($featuredProduct as $featured) {
            $tmp = array(
              "id" => $featured->getId(),
              "description" => $featured->getDescription(),
              "title" => $featured->getTitle()
            );

            $product = $featured->getProduct();
            $tmp["product"] = array(
              "id" => $product->getId(),
              "name" => $product->getName()
            );

            $country = $featured->getCountry();
            $tmp["country"] = array(
              "id" => $country->getId(),
              "code" => $country->getCode()
            );

            $language = $featured->getLang();
            $tmp["language"] = array(
              "id" => $language->getId(),
              "origName" => $language->getOrigName()
            );

           $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("featuredproduct/{id}", name="getFeaturedProductById", methods={"GET"})
     * @return JsonResponse
     */
    public function getFeaturedProductById($id) {
        $em = $this->getDoctrine()->getManager();

        $featuredProduct = $em->getRepository(FeaturedProduct::class)->getAllFeaturedProductById($id);

        return new JsonResponse($featuredProduct[0]);
    }
}