<?php

namespace App\PanelServices;

use App\Entity\Continent;
use App\Entity\Country;
use App\Entity\CountryLang;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AdminCountryController extends Controller
{
    /**
     * @return Response
     * @Route("country", name="createCountry", methods={"POST"})
     */
    public function createCountry()
    {
        $json = file_get_contents('php://input');

        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $nCountry) {
            $country = new Country();
            $country->setName($nCountry["name"]);
            $country->setOrigName($nCountry["origName"]);
            $country->setCode($nCountry["code"]);
            $country->setActive($nCountry["active"]);
            $country->setMaintance($nCountry["maintance"]);
            $country->setPriority($nCountry["priority"]);
            $continentRepo = $em->getRepository(Continent::class)->find($nCountry["continentId"]);
            $country->setContinent($continentRepo);
            $em->persist($country);

            foreach ($nCountry["languages"] as $lang) {
                $language = $em->getRepository(Language::class)->find($lang["id"]);

                $countryLang = new CountryLang();
                $countryLang->setCountry($country);
                $countryLang->setLang($language);
                $countryLang->setMain($lang["main"]);
                $countryLang->setPriority($lang["priority"]);
                $em->persist($countryLang);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("country/{id}", name="updateCountry", methods={"PUT"})
     */
    public function updateCountry($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');

        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($id);
        $country->setName($data["name"]);
        $country->setOrigName($data["origName"]);
        $country->setCode($data["code"]);
        $country->setActive($data["active"]);
        $country->setMaintance($data["maintance"]);
        $country->setPriority($data["priority"]);
        $continentRepo = $em->getRepository(Continent::class)->find($data["continentId"]);
        $country->setContinent($continentRepo);

        foreach ($data["languages"] as $lang) {

            $language = $em->getRepository(Language::class)->find($lang["id"]);

            $countryLang = $em->getRepository(CountryLang::class)->findOneBy(array(
                "country" => $country,
                "lang" => $lang
            ));
            if ($countryLang) {
                $em->remove($countryLang);
                $em->flush();
            }
            $cLang = new CountryLang();
            $cLang->setCountry($country);
            $cLang->setLang($language);
            $cLang->setMain($lang["main"]);
            $cLang->setPriority($lang["priority"]);
            $em->persist($cLang);
            $em->flush();
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("country/{id}", name="deleteCountry", methods={"DELETE"})
     */
    public function deleteCountry($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteCountry = $em->getRepository(Country::class)->find($id);
        $deleteCountry->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("country", name="getAllCountryForAdmin", methods={"GET"})
     */
    public function getAllCountry() {
        $em = $this->getDoctrine()->getManager();

        $country = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($country as $ct) {
            $tmp = array(
              "id" => $ct->getId(),
              "name" => $ct->getName(),
              "origName" => $ct->getOrigName(),
              "code" => $ct->getCode()
            );

            $continent = $ct->getContinent();
                $tmp["continent"][] = array(
                    "id" => $continent->getId(),
                    "name" => $continent->getName(),
                    "code" => $continent->getCode()
                );
             foreach ($ct->getCountryLang() as $language) {
                 $tmp["languages"][] = array(
                    "id" => $language->getLang()->getId(),
                    "name" => $language->getLang()->getName(),
                    "code" => $language->getLang()->getCode()
                 );
             }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("country/{id}", name="getAllCountryById", methods={"GET"})
     */
    public function getAllCountryById($id) {
        $em = $this->getDoctrine()->getManager();

        $country = $em->getRepository(Country::class)->getAllCountryById($id);

        return new JsonResponse($country[0]);
    }
}