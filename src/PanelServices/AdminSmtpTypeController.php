<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 16:16
 */

namespace App\PanelServices;


use App\Entity\SmtpType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSmtpTypeController extends Controller
{
    /**
     * @Route("smtptype", name="createSmtpType", methods={"POST"})
     */
    public function createSmtpType() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $type) {
            $addType = new SmtpType();
            $addType->setName($type["name"]);
            $addType->setCode($type["code"]);

            $em->persist($addType);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("smtptype/{id}", name="updateSmtpType", methods={"PUT"})
     */
    public function updateSmtpType($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $updateType = $em->getRepository(SmtpType::class)->find($id);
        $updateType->setName($data["name"]);
        $updateType->setCode($data["code"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("smtptype/{id}", name="deleteSmtpType", methods={"DELETE"})
     */
    public function deleteSmtpType($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSmtpType = $em->getRepository(SmtpType::class)->find($id);
        $deleteSmtpType->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("smtptype", name="getAllSmtpType", methods={"GET"})
     */
    public function getAllSmtpType() {
        $em = $this->getDoctrine()->getManager();

        $smtpType = $em->getRepository(SmtpType::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($smtpType as $type) {
            $dataArray[] = array(
              "id" => $type->getId(),
              "name" => $type->getName()
            );
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("smtptype/{id}", name="getSmtpTypeById", methods={"GET"})
     */
    public function getSmtpTypeById($id) {
        $em = $this->getDoctrine()->getManager();

        $smtpType = $em->getRepository(SmtpType::class)->getAllSmtpTypeById($id);

        return new JsonResponse($smtpType[0]);
    }
}