<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 10:31
 */

namespace App\PanelServices;


use App\Entity\Connection;
use App\Entity\ConnectionImage;
use App\Entity\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminConnectionController extends Controller
{
    /**
     * @param $request
     * @Route("connection", name="createConnection", methods={"POST"})
     * @return JsonResponse
     */
    public function createConnection(Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $connection) {
            $addConnection = new Connection();
            $addConnection->setName($connection["name"]);
            $addConnection->setPriority($connection["priority"]);
            $addConnection->setActive($connection["active"]);

            $em->persist($addConnection);

            foreach ($connection["connectionImage"] as $conImage) {
                $imageTypeEntity = $em->getRepository(ImageType::class)->find($conImage["imageType"]);
                define('UPLOAD_DIR','./uploads/images/images');

                $replace = explode(',', $conImage["imageFile"])[1];
                $replace = str_replace(' ', '+', $replace);
                $decode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $imageType = $split[1];

                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                file_put_contents($file, $decode);
                $trimPath = ltrim($file, '.');

                $addConnectionImage = new ConnectionImage();
                $addConnectionImage->setConnection($addConnection);
                $addConnectionImage->setActive($conImage["active"]);
                $addConnectionImage->setType($imageTypeEntity);
                $addConnectionImage->setPath($baseUrl . $trimPath);
                $addConnectionImage->setName($conImage["name"]);
                $addConnectionImage->setPriority($conImage["priority"]);

                $em->persist($addConnectionImage);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("connection/{id}", name="updateConnection", methods={"PUT"})
     */
    public function updateConnection($id, Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

            $updateConnection = $em->getRepository(Connection::class)->find($id);
            $updateConnection->setName($data["name"]);
            $updateConnection->setPriority($data["priority"]);
            $updateConnection->setActive($data["active"]);

            foreach ($data["connectionImage"] as $conImage) {
                $imageTypeEntity = $em->getRepository(ImageType::class)->find($conImage["imageType"]);
                if(isset($conImage["isDeleted"]) && $conImage["isDeleted"] == true) {
                    $updateConnectionImage = $em->getRepository(ConnectionImage::class)->findOneBy(array(
                        "connection" => $updateConnection,
                        "id" => $conImage["id"]
                    ));
                    $em->remove($updateConnectionImage);
                    $em->flush();
                }  else if (isset($conImage["isOld"]) && $conImage["isOld"] == true) {
                    $updateConnectionImage = $em->getRepository(ConnectionImage::class)->findOneBy(array(
                        "connection" => $updateConnection,
                        "id" => $conImage["id"]
                    ));
                    $updateConnectionImage->setConnection($updateConnection);
                    $updateConnectionImage->setActive($conImage["active"]);
                    $updateConnectionImage->setType($imageTypeEntity);
                    $updateConnectionImage->setName($conImage["name"]);
                    $updateConnectionImage->setPriority($conImage["priority"]);
                    $em->flush();

                    if (isset($conImage["imageFile"]) && $conImage["imageFile"] != null) {
                        if (!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }
                            $replace = str_replace('data:image/png;base64,','', $conImage["imageFile"]);
                            $replace = str_replace(' ', '+', $replace);
                            $decode = base64_decode($replace);

                            $f = finfo_open();
                            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                            $split = explode( '/', $mimeType );
                            $imageType = $split[1];

                            $baseUrl = $this->container->getParameter('baseUrlHttp');

                            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                            file_put_contents($file, $decode);
                            $trimPath = ltrim($file, '.');

                            $updateConnectionImage->setPath($baseUrl . $trimPath);
                            $em->flush();
                    }
                } else {
                    $newConImage = new ConnectionImage();
                    $newConImage->setConnection($updateConnection);
                    $newConImage->setActive($conImage["active"]);
                    $newConImage->setType($imageTypeEntity);
                    $newConImage->setName($conImage["name"]);
                    $newConImage->setPriority($conImage["priority"]);
                    if (isset($conImage["imageFile"]) && $conImage["imageFile"] != null) {
                        if (!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }

                        $replace = str_replace('data:image/png;base64,','', $conImage["imageFile"]);
                        $replace = str_replace(' ', '+', $replace);
                        $decode = base64_decode($replace);

                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $imageType = $split[1];

                        $baseUrl = $this->container->getParameter('baseUrlHttp');

                        $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                        file_put_contents($file, $decode);
                        $trimPath = ltrim($file, '.');

                        $newConImage->setPath($baseUrl . $trimPath);
                    }
                    $em->persist($newConImage);
                    $em->flush();
                }
            }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("connection/{id}", name="deleteConnection", methods={"DELETE"})
     */
    public function deleteConnection($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteConnection = $em->getRepository(Connection::class)->find($id);

        $deleteConnection->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("connection", name="getAllConnectionForAdmin", methods={"GET"})
     */
    public function getAllConnection() {
        $em = $this->getDoctrine()->getManager();

        $connection = $em->getRepository(Connection::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($connection as $con) {
            $tmp = array(
                "id" => $con->getId(),
                "name" => $con->getName()
            );
            $conImage = $con->getImage();
            foreach ($conImage as $connectionImage) {
                $tmp["image"][] = array(
                  "path" => $connectionImage->getPath(),
                  "imageType" => $connectionImage->getType()->getCode()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("connection/{id}", name="getAllConnectionById", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllConnectionById($id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository(Connection::class)->getConnectionById($id);

        return new JsonResponse($category[0]);
    }
}