<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 13:26
 */

namespace App\PanelServices;


use App\Entity\Category;
use App\Entity\CategoryText;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategoryTextController extends Controller
{
    /**
     * @return Response
     * @Route("categorytext", name="createCategoryText", methods={"POST"})
     */
    public function createCategoryText() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $catText) {
            $category = $em->getRepository(Category::class)->find($catText["categoryId"]);
            $country = $em->getRepository(Country::class)->find($catText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($catText["languageId"]);

            $addCategoryText = new CategoryText();
            $addCategoryText->setCategory($category);
            $addCategoryText->setCountry($country);
            $addCategoryText->setLang($lang);
            $addCategoryText->setName($catText["name"]);
            $addCategoryText->setShortDescription($catText["shortDesc"]);
            $addCategoryText->setLongDescription($catText["longDesc"]);
            $addCategoryText->setLink($catText["link"]);
            $addCategoryText->setKeywords($catText["keywords"]);

            $em->persist($addCategoryText);
        }
        $em->flush();

        return new Response("success");
    }

    /**
     * @Route("categorytext", name="getAllCategoryTextForAdmin", methods={"GET"})
     */
    public function getAllCategoryText() {
        $em = $this->getDoctrine()->getManager();

        $categoryText = $em->getRepository(CategoryText::class)->getAllCategoryText();

        return new JsonResponse($categoryText);
    }
}