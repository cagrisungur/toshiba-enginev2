<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.03.2019
 * Time: 11:22
 */

namespace App\PanelServices;


use App\Entity\Logo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminLogoController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("logo", name="getLogoAdmin", methods={"GET"})
     */
    public function getLogoAdmin() {
        $em = $this->getDoctrine()->getManager();

        $logos = $em->getRepository(Logo::class)->findBy(array(
            "deletedAt" => null
        ));

        $logoArray = array();

        foreach ($logos as $logo) {
            $logoArray[] = array(
                "id" => $logo->getId(),
                "name" => $logo->getName(),
                "link" => $logo->getLink(),
                "thumbnail" => $logo->getThumbnail()
            );
        }

        return new JsonResponse($logoArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("logo/{id}", name="getLogoByIdForAdmin", methods={"GET"})
     */
    public function getLogoByIdForAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $logo = $em->getRepository(Logo::class)->getLogoById($id);

        return new JsonResponse($logo[0]);
    }

    /**
     * @return JsonResponse
     * @Route("logo", name="createLogoAdmin", methods={"POST"})
     */
    public function createLogoAdmin() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $logo) {

            $newLogo = new Logo();
            $newLogo->setPriority($logo["priority"]);
            $newLogo->setName($logo["name"]);
            $newLogo->setActive($logo["active"]);

            $baseUrl = $this->container->getParameter('baseUrlHttp');

            if (isset($logo["link"]) && !empty($logo["link"])) {

                define('file_upload','./uploads/files/file');
                $replace = explode(',', $logo["link"])[1];
                $replace = str_replace(' ', '+', $replace);
                $fileDecode = base64_decode($replace);
                $f = finfo_open();
                $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $fileMime = $split[1];

                $mimeFunction = $this->getFileMimeType($fileMime);

                if ($mimeFunction !== 'application/octet-stream') {
                    $split = explode( '/', $mimeType );
                    $fileExtension = $split[1];

                    $file = file_upload . uniqid(). '.' . $fileExtension;
                    file_put_contents($file, $fileDecode);
                    $trimPath = ltrim($file, '.');

                    $newLogo->setLink($baseUrl . $trimPath);
                    $newLogo->setSize($this->getBase64ImageSize($logo["link"]));
                }
            }

            if (isset($logo["thumbnail"]) && !empty($logo["thumbnail"])) {
                if(!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }

                $thumbnailReplace = explode(',', $logo["thumbnail"])[1];
                $thumbnailReplace = str_replace(' ', '+', $thumbnailReplace);
                $thumbDecode = base64_decode($thumbnailReplace);

                $f = finfo_open();
                $mimeTypeThumbnail = finfo_buffer($f, $thumbDecode, FILEINFO_MIME_TYPE);
                $thumpSplit = explode( '/', $mimeTypeThumbnail );
                $thumpImageType = $thumpSplit[1];

                $thumpMime = $this->getMimeType($thumpImageType);
                if ($thumpMime !== 'application/octet-stream') {
                    $file = UPLOAD_DIR . uniqid(). '.' . $thumpImageType;
                    file_put_contents($file, $thumbDecode);
                    $thumpTrim = ltrim($file, '.');

                    $newLogo->setThumbnail($baseUrl . $thumpTrim);
                }
            }

            $em->persist($newLogo);
            $em->flush();
        }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("logo/{id}", name="updateLogoAdmin", methods={"PUT"})
     */
    public function updateLogoAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $baseUrl = $this->container->getParameter('baseUrlHttp');

        $logo = $em->getRepository(Logo::class)->find($id);

        $logo->setName($data["name"]);
        $logo->setPriority($data["priority"]);
        $logo->setActive($data["active"]);

        if (isset($data["link"]) && !empty($data["link"])) {

            define('file_upload','./uploads/files/file');

            if (isset($data["isLinkOld"]) && $data["isLinkOld"] == false) {
                $replace = explode(',', $data["link"])[1];
                $replace = str_replace(' ', '+', $replace);
                $fileDecode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $fileMime = $split[1];

                $mimeFunction = $this->getFileMimeType($fileMime);

                if ($mimeFunction !== 'application/octet-stream') {
                    $split = explode( '/', $mimeType );
                    $fileExtension = $split[1];

                    $file = file_upload . uniqid(). '.' . $fileExtension;
                    file_put_contents($file, $fileDecode);
                    $trimPath = ltrim($file, '.');

                    $logo->setLink($baseUrl . $trimPath);
                    $logo->setSize($this->getBase64ImageSize($data["link"]));
                }
            }
        }

        if (isset($data["thumbnail"]) && !empty($data["thumbnail"])) {
            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }
            if (isset($data["isThumbnailOld"]) && $data["isThumbnailOld"] == false) {
                $thumbnailReplace = explode(',', $data["thumbnail"])[1];
                $thumbnailReplace = str_replace(' ', '+', $thumbnailReplace);
                $thumbDecode = base64_decode($thumbnailReplace);

                $f = finfo_open();
                $mimeTypeThumbnail = finfo_buffer($f, $thumbDecode, FILEINFO_MIME_TYPE);
                $thumpSplit = explode( '/', $mimeTypeThumbnail );
                $thumpImageType = $thumpSplit[1];

                $thumpMime = $this->getMimeType($thumpImageType);
                if ($thumpMime !== 'application/octet-stream') {
                    $file = UPLOAD_DIR . uniqid(). '.' . $thumpImageType;
                    file_put_contents($file, $thumbDecode);
                    $thumpTrim = ltrim($file, '.');

                    $logo->setThumbnail($baseUrl . $thumpTrim);
                }
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("logo/{id}", name="deleteLogo", methods={"DELETE"})
     */
    public function deleteLogo($id) {
        $em = $this->getDoctrine()->getManager();

        $logo = $em->getRepository(Logo::class)->find($id);
        $logo->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    public function getFileMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        try{
            $sizeInByte = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);

            return $sizeInByte;
        }
        catch(\Exception $e){
            return $e;
        }
    }
}