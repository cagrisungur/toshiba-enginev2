<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 11:32
 */

namespace App\PanelServices;


use App\Entity\Category;
use App\Entity\Country;
use App\Entity\FeaturedCategory;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminFeaturedCategoryController extends Controller
{
    /**
     * @Route("featuredcategory", name="createFeaturedCategory", methods={"POST"})
     */
    public function createFeaturedCategory() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $featuredCategory) {
            $category = $em->getRepository(Category::class)->find($featuredCategory["categoryId"]);
            $country = $em->getRepository(Country::class)->find($featuredCategory["countryId"]);
            $lang = $em->getRepository(Language::class)->find($featuredCategory["langId"]);

            $addFeaturedCategory = new FeaturedCategory();
            $addFeaturedCategory->setCategory($category);
            $addFeaturedCategory->setCountry($country);
            $addFeaturedCategory->setLang($lang);
            $addFeaturedCategory->setDescription($featuredCategory["description"]);
            $addFeaturedCategory->setTitle($featuredCategory["title"]);

            $em->persist($addFeaturedCategory);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuredcategory/{id}", name="updateFeaturedCategory", methods={"PUT"})
     */
    public function updateFeaturedCategory($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $category = $em->getRepository(Category::class)->find($data["categoryId"]);
        $country = $em->getRepository(Country::class)->find($data["countryId"]);
        $lang = $em->getRepository(Language::class)->find($data["langId"]);

        $updateFeaturedCategory = $em->getRepository(FeaturedCategory::class)->find($id);
        $updateFeaturedCategory->setCategory($category);
        $updateFeaturedCategory->setCountry($country);
        $updateFeaturedCategory->setLang($lang);
        $updateFeaturedCategory->setDescription($data["description"]);
        $updateFeaturedCategory->setTitle($data["title"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("featuredcategory/{id}", name="deleteFeaturedCategory", methods={"DELETE"})
     */
    public function deleteFeaturedCategory($id) {
        $em = $this->getDoctrine()->getManager();


        $deleteFeaturedCategory = $em->getRepository(FeaturedCategory::class)->find($id);
        $deleteFeaturedCategory->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
     }

    /**
     * @Route("featuredcategory", name="getAllFeaturedCategoryForAdmin", methods={"GET"})
     */
    public function getAllFeaturedCategory() {
        $em = $this->getDoctrine()->getManager();

        $featuredCategory = $em->getRepository(FeaturedCategory::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($featuredCategory as $featured) {
            $tmp = array(
                "id" => $featured->getId(),
                "description" => $featured->getDescription(),
                "title" => $featured->getTitle()
            );

            $category = $featured->getCategory();
            $tmp["category"] = array(
                "id" => $category->getId(),
                "name" => $category->getName()
            );

            $country = $featured->getCountry();
            $tmp["country"] = array(
                "id" => $country->getId(),
                "code" => $country->getCode()
            );

            $language = $featured->getLang();
            $tmp["language"] = array(
                "id" => $language->getId(),
                "origName" => $language->getOrigName()
            );

            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("featuredcategory/{id}", name="getAllFeaturedCategoryById", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllFeaturedCategoryById($id) {
        $em = $this->getDoctrine()->getManager();

        $featuredCategory = $em->getRepository(FeaturedCategory::class)->getAllFeaturedCategoryById($id);

        return new JsonResponse($featuredCategory);
    }
}