<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 13.11.2018
 * Time: 11:38
 */

namespace App\PanelServices;

use App\Entity\Country;
use App\Entity\Feature;
use App\Entity\FeatureImage;
use App\Entity\FeatureText;
use App\Entity\ImageType;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminFeatureController extends Controller
{
    /**
     * @param $request
     * @Route("feature", name="createFeature", methods={"POST"})
     * @return JsonResponse
     */
    public function createFeature(Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $feat) {
            $feature = new Feature();
            $feature->setName($feat["featureName"]);
            $feature->setActive($feat["active"]);
            
            $em->persist($feature);

            foreach ($feat["featureImage"] as $featureImage) {

                $findFeatureType = $em->getRepository(ImageType::class)->find($featureImage["imageType"]);

                $baseUrl = $this->container->getParameter('baseUrlHttp');
                
                if ($featureImage["isVideo"] == true) {
                    $addFeatureImage = new FeatureImage();
                    $addFeatureImage->setType($findFeatureType);
                    $addFeatureImage->setFeature($feature);
                    $addFeatureImage->setPath($featureImage["featureFile"]);
                    $em->persist($addFeatureImage);
                    $em->flush();
                }else if ($featureImage["featureFile"]){
                    if(!defined('UPLOAD_DIR')) {
                        define('UPLOAD_DIR','./uploads/images/images');
                    }

                    $replace = explode(',', $featureImage["featureFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);

                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];
                    // $mimeFunction = $this->getMimeType($imageType);
                    if ($mimeType == 'application/octet-stream') {
                        return new JsonResponse(array(
                            "message" => "FAILED"
                        ));
                    }

                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $addFeatureImage = new FeatureImage();
                    $addFeatureImage->setType($findFeatureType);
                    $addFeatureImage->setFeature($feature);
                    $addFeatureImage->setPath($baseUrl . $trimPath);

                    $em->persist($addFeatureImage);
                    $em->flush();
                } 
            }

            foreach ($feat["featureText"] as $featureText) {
                $findCountry = $em->getRepository(Country::class)->find($featureText["countryId"]);
                $findLang = $em->getRepository(Language::class)->find($featureText["langId"]);

                $addFeatureText = new FeatureText();
                $addFeatureText->setFeature($feature);
                $addFeatureText->setName($featureText["name"]);
                $addFeatureText->setLongDescription($featureText["longDescription"]);
                $addFeatureText->setShortDescription($featureText["shortDescription"]);
                $addFeatureText->setLang($findLang);
                $addFeatureText->setCountry($findCountry);

                $em->persist($addFeatureText);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("feature/{id}", name="updateFeature", methods={"PUT"})
     */
    public function updateFeature($id, Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $feature = $em->getRepository(Feature::class)->find($id);
        $feature->setName($data["featureName"]);
        $feature->setActive($data["active"]);

        foreach ($data["featureImage"] as $featureImage) {
            $findFeatureType = $em->getRepository(ImageType::class)->find($featureImage["imageType"]);

            if(isset($featureImage["isDeleted"]) && $featureImage["isDeleted"] == true) {
                $updateFeatureImage = $em->getRepository(FeatureImage::class)->findOneBy(array(
                    "feature" => $feature,
                    "path" => $featureImage["featureFile"]
                ));

                $em->remove($updateFeatureImage);
            }else if(isset($featureImage["isOld"]) && $featureImage["isOld"] == true) {
                $updateFeatureImage = $em->getRepository(FeatureImage::class)->findOneBy(array(
                    "feature" => $feature,
                    "path" => $featureImage["featureFile"]
                ));
                $updateFeatureImage->setType($findFeatureType);
                $updateFeatureImage->setFeature($feature);
            }else if ($featureImage["isVideo"] == true) {
                $addFeatureImage = new FeatureImage();
                $addFeatureImage->setType($findFeatureType);
                $addFeatureImage->setFeature($feature);
                $addFeatureImage->setPath($featureImage["featureFile"]);
                $em->persist($addFeatureImage);
            }else if (isset($featureImage["featureFile"]) && $featureImage["featureFile"] != null) {
                if(!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }

                if ( $featureImage["featureFile"]){
                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    $replace = explode(',', $featureImage["featureFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);

                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];
                    // $mimeFunction = $this->getMimeType($imageType);
                    if ($mimeType == 'application/octet-stream') {
                        return new JsonResponse(array(
                            "message" => "FAILED"
                        ));
                    }
                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $addFeatureImage = new FeatureImage();
                    $addFeatureImage->setType($findFeatureType);
                    $addFeatureImage->setFeature($feature);
                    $addFeatureImage->setPath($baseUrl . $trimPath);
                    $em->persist($addFeatureImage);
                }
            }
        }
        foreach ($data["featureText"] as $featureText) {
            $isPersist = false;
            $findCountry = $em->getRepository(Country::class)->find($featureText["countryId"]);
            $findLang = $em->getRepository(Language::class)->find($featureText["langId"]);

            $updateFeatureText = $em->getRepository(FeatureText::class)->findOneBy(array(
                "feature" => $feature,
                "country" => $findCountry,
                "lang" => $findLang
            ));

            if (!$updateFeatureText) {
                $updateFeatureText = new FeatureText();
                $isPersist = true;
            }

            $updateFeatureText->setFeature($feature);
            $updateFeatureText->setName($featureText["name"]);
            $updateFeatureText->setLongDescription($featureText["longDescription"]);
            $updateFeatureText->setShortDescription($featureText["shortDescription"]);
            $updateFeatureText->setLang($findLang);
            $updateFeatureText->setCountry($findCountry);
            if ($isPersist) {
                $em->persist($updateFeatureText);
            }
            $em->flush();
        }

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("feature/{id}", name="deleteFeature", methods={"DELETE"})
     */
    public function deleteFeature($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteFeature = $em->getRepository(Feature::class)->find($id);

        $deleteFeature->setDeletedAt(new \DateTime());

        $em->flush();
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("feature", name="getAllFeature", methods={"GET"})
     */
    public function getAllFeature() {
        $em = $this->getDoctrine()->getManager();

        $feature = $em->getRepository(Feature::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($feature as $f) {
            $dataArray[] = array(
                "id" => $f->getId(),
                "name" => $f->getName()
            );
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("feature/{id}", name="getFeatureById", methods={"GET"})
     * @return JsonResponse
     */
    public function getFeatureById($id) {
        $em = $this->getDoctrine()->getManager();

        $featureById = $em->getRepository(Feature::class)->getAllFeatureById($id);

        return new JsonResponse($featureById[0]);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}