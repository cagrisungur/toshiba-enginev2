<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 17:44
 */

namespace App\PanelServices;


use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Product;
use App\Entity\Slider;
use App\Entity\SliderCategory;
use App\Entity\SliderItem;
use App\Entity\SliderItemText;
use App\Entity\SliderProduct;
use App\Entity\SliderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminSliderController extends Controller
{
    /**
     * @param $request
     * @Route("slider", name="createSlider", methods={"POST"})
     * @return JsonResponse
     */
    public function createSlider(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $slider) {
            $country = $em->getRepository(Country::class)->find($slider["countryId"]);
            $sliderType = $em->getRepository(SliderType::class)->find($slider["sliderType"]);

            $addSlider = new Slider();
            $addSlider->setCountry($country);
            $addSlider->setActive($slider["active"]);
            $addSlider->setType($sliderType);

            $em->persist($addSlider);

            if (isset($slider["category"])) {
                foreach ($slider["category"] as $sliderCategory) {
                    $category = $em->getRepository(Category::class)->find($sliderCategory["categoryId"]);

                    $addSliderCategory = new SliderCategory();
                    $addSliderCategory->setActive($sliderCategory["active"]);
                    $addSliderCategory->setCategory($category);
                    $addSliderCategory->setPriority($sliderCategory["priority"]);
                    $addSliderCategory->setSlider($addSlider);

                    $em->persist($addSliderCategory);
                    $em->flush();
                }
            } 
            if (isset($slider["product"]))  {
                foreach ($slider["product"] as $sliderProduct) {
                    $product = $em->getRepository(Product::class)->find($sliderProduct["productId"]);

                    $addSliderProduct = new SliderProduct();
                    $addSliderProduct->setPriority($sliderProduct["priority"]);
                    $addSliderProduct->setActive($sliderProduct["active"]);
                    $addSliderProduct->setSlider($addSlider);
                    $addSliderProduct->setProduct($product);

                    $em->persist($addSliderProduct);
                }
            }

            if(isset($slider["item"])) {
                foreach ($slider["item"] as $sliderItem) {
                    if (!defined('UPLOAD_DIR')) {
                        define('UPLOAD_DIR','./uploads/images/images');
                    }

                    $replace = explode(',', $sliderItem["imageFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);
    
                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];
    
                    $mimeFunction = $this->getMimeTypeForImage($imageType);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse(array(
                            "message" => "FAILED"
                        ));
                    }
                    $baseUrl = $this->container->getParameter('baseUrlHttp');
    
                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');
    
                    $addSliderItem = new SliderItem();
                    $addSliderItem->setSlider($addSlider);
                    $addSliderItem->setActive($sliderItem["active"]);
                    $addSliderItem->setMimeType($sliderItem["mimeType"]);
                    $addSliderItem->setPriority($sliderItem["priority"]);
                    $addSliderItem->setPath($baseUrl . $trimPath);
                    $addSliderItem->setClass($sliderItem["class"]);
    
                    $em->persist($addSliderItem);
                    $em->flush();
    
                    foreach ($sliderItem["text"] as $sliderText) {
                        $lang = $em->getRepository(Language::class)->find($sliderText["langId"]);
    
                        $addSliderItemText = new SliderItemText();
                        $addSliderItemText->setSliderItem($addSliderItem);
                        $addSliderItemText->setLang($lang);
                        $addSliderItemText->setText($sliderText["sliderText"]);
                        $addSliderItemText->setLink($sliderText["link"]);
                        $addSliderItemText->setButtonText($sliderText["buttonText"]);
                        $addSliderItemText->setTitle($sliderText["title"]);
    
                        $em->persist($addSliderItemText);
                        $em->flush();
                    }
                }
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("slider/{id}", name="updateSlider", methods={"PUT"})
     */
    public function updateSlider($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);
        $sliderType = $em->getRepository(SliderType::class)->find($data["sliderType"]);

        $updateSlider = $em->getRepository(Slider::class)->find($id);
        $updateSlider->setCountry($country);
        $updateSlider->setActive($data["active"]);
        $updateSlider->setType($sliderType);

        if (isset($data["category"])) {
            $deleteSliderCategory = $em->getRepository(SliderCategory::class)->findBy(array(
                "slider" => $updateSlider
            ));
            foreach ($deleteSliderCategory as $deletedCat) {
                $em->remove($deletedCat);
                $em->flush();
            }
            foreach ($data["category"] as $sliderCategory) {
                $category = $em->getRepository(Category::class)->find($sliderCategory["categoryId"]);
                $updateSliderCategory = new SliderCategory();
                $updateSliderCategory->setActive($sliderCategory["active"]);
                $updateSliderCategory->setCategory($category);
                $updateSliderCategory->setPriority($sliderCategory["priority"]);
                $updateSliderCategory->setSlider($updateSlider);
                $em->persist($updateSliderCategory);
                $em->flush();
            }
        }
        
        if(isset($data["product"])) {
            $deleteSlider = $em->getRepository(SliderProduct::class)->findBy(array(
                "slider" => $updateSlider
            ));
            foreach ($deleteSlider as $deleted) {
                $em->remove($deleted);
                $em->flush();
            }
            foreach ($data["product"] as $sliderProduct) {
                $product = $em->getRepository(Product::class)->find($sliderProduct["productId"]);
                $updateSliderProduct = new SliderProduct();
                $updateSliderProduct->setPriority($sliderProduct["priority"]);
                $updateSliderProduct->setActive($sliderProduct["active"]);
                $updateSliderProduct->setSlider($updateSlider);
                $updateSliderProduct->setProduct($product);
                $em->persist($updateSliderProduct);
                $em->flush();
            }
        }

        if(isset($data["item"])) {
            foreach ($data["item"] as $sliderItem) {
                if(isset($sliderItem["isDeleted"]) && $sliderItem["isDeleted"] == true) {
                    $updateSliderItem = $em->getRepository(SliderItem::class)->findOneBy(array(
                        "slider" => $updateSlider,
                        "id" => $sliderItem["id"]
                    ));
                    if($updateSliderItem) {
                        $em->remove($updateSliderItem);
                        $em->flush();
                    }
                }else if (isset($sliderItem["isOld"]) && $sliderItem["isOld"] == true) {
                    $updateSliderItem = $em->getRepository(SliderItem::class)->findOneBy(array(
                        "slider" => $updateSlider,
                        "id" => $sliderItem["id"]
                    ));
    
                    $updateSliderItem->setActive($sliderItem["active"]);
                    $updateSliderItem->setPriority($sliderItem["priority"]);
                    $updateSliderItem->setClass($sliderItem["class"]);
                    $em->flush();

                    $removeSlider = $em->getRepository(SliderItemText::class)->findBy(array(
                        "sliderItem" => $updateSliderItem,
                    ));

                    foreach ($removeSlider as $deleteSliderText) {
                        $em->remove($deleteSliderText);
                        $em->flush();
                    }
                    foreach ($sliderItem["text"] as $sliderText) {

                        $lang = $em->getRepository(Language::class)->find($sliderText["langId"]);
        
                        $newSliderItemText = new SliderItemText();
                        $newSliderItemText->setSliderItem($updateSliderItem);
                        $newSliderItemText->setLang($lang);
                        $newSliderItemText->setText($sliderText["sliderText"]);
                        $newSliderItemText->setButtonText($sliderText["buttonText"]);
                        $newSliderItemText->setLink($sliderText["link"]);
                        $newSliderItemText->setTitle($sliderText["title"]);

                        $em->persist($newSliderItemText);
                        $em->flush();
                    }
                } else {
                    $updateSliderItem = new SliderItem();
    
                    $updateSliderItem->setSlider($updateSlider);
                    $updateSliderItem->setActive($sliderItem["active"]);
                    $updateSliderItem->setPriority($sliderItem["priority"]);
                    $updateSliderItem->setClass($sliderItem["class"]);
                    $updateSliderItem->setMimeType("");
    
                    if (isset($sliderItem["imageFile"]) && $sliderItem["imageFile"] != null) {
                        if (!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }

                        $replace = explode(',', $sliderItem["imageFile"])[1];
                        $replace = str_replace(' ', '+', $replace);
                        $decode = base64_decode($replace);
        
                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $imageType = $split[1];
        
                        $mimeFunction = $this->getMimeTypeForImage($imageType);
                        if ($mimeFunction == 'application/octet-stream') {
                            return new JsonResponse(array(
                                "message" => "FAILED"
                            ));
                        }
                        $baseUrl = $this->container->getParameter('baseUrlHttp');
        
                        $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                        file_put_contents($file, $decode);
                        $trimPath = ltrim($file, '.');
        
                        $updateSliderItem->setPath($baseUrl . $trimPath);
                    }
    
                    $em->persist($updateSliderItem);
                    $em->flush();
    
                    foreach ($sliderItem["text"] as $sliderText) {
                        $isPersist = false;
                        $lang = $em->getRepository(Language::class)->find($sliderText["langId"]);
        
                        $addSliderItemText = $em->getRepository(SliderItemText::class)->findOneBy(array(
                            "sliderItem" => $updateSliderItem,
                            "lang" => $lang
                        ));
                        if (!$addSliderItemText) {
                            $addSliderItemText = new SliderItemText();
                            $isPersist = true;
                        }
    
                        $addSliderItemText->setSliderItem($updateSliderItem);
                        $addSliderItemText->setLang($lang);
                        $addSliderItemText->setText($sliderText["sliderText"]);
                        $addSliderItemText->setLink($sliderText["link"]);
                        $addSliderItemText->setTitle($sliderText["title"]);
                        if ($isPersist) {
                            $em->persist($addSliderItemText);
                        }
                        $em->flush();
                    }
                }
            }
        }

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("slider/{id}", name="deleteSlider", methods={"DELETE"})
     */
    public function deleteSlider($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSlider = $em->getRepository(Slider::class)->find($id);
        $deleteSlider->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("slider", name="getAllSlider", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllSlider() {
        $em = $this->getDoctrine()->getManager();

        $slider = $em->getRepository(Slider::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($slider as $slide) {
            $tmp = array(
              "id" => $slide->getId(),
            );
            $tmp["country"] = array(
              "id" => $slide->getCountry()->getId(),
              "name" => $slide->getCountry()->getName()
            );

            foreach ($slide->getItem() as $item) {
                if ($item->getItemText()) {
                    $text = $item->getItemText();
                    foreach ($text as $itemText) {
                        $tmp["text"][] = array(
                            "title" => $itemText->getTitle(),
                            "text" => $itemText->getText()
                        );
                    }
                }
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("slider/{id}", name="getSliderById", methods={"GET"})
     * @return JsonResponse
     */
    public function getSliderById($id) {
        $em = $this->getDoctrine()->getManager();

        $slider = $em->getRepository(Slider::class)->getAllSliderById($id);

        return new JsonResponse($slider[0]);
    }

    public function getMimeTypeForImage($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}