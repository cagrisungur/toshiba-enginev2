<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 18:04
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\SocialMedia;
use App\Entity\SocialMediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSocialMediaController extends Controller
{
    /**
     * @Route("socialmedia", name="createSocialMedia", methods={"POST"})
     */
    public function createSocialMedia() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $socialMedia) {
            $country = $em->getRepository(Country::class)->find($socialMedia["countryId"]);
            $socialMediaType = $em->getRepository(SocialMediaType::class)->find($socialMedia["typeId"]);
            $findSocial = $em->getRepository(SocialMedia::class)->findOneBy(array(
                "country" => $country,
                "type" => $socialMediaType
            ));
            if ($findSocial) {
                    return new JsonResponse(array(
                        "message" => "Social Media Already Exist"
                    ));
            } else {
                $addSocialMedia = new SocialMedia();
                $addSocialMedia->setType($socialMediaType);
                $addSocialMedia->setCountry($country);
                $addSocialMedia->setUrl($socialMedia["url"]);
                $addSocialMedia->setActive($socialMedia["active"]);

                $em->persist($addSocialMedia);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("socialmedia/{type}/{country}", name="updateSocialMedia", methods={"PUT"})
     */
    public function updateSocialMedia($type, $country) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($country);
        $socialMediaType = $em->getRepository(SocialMediaType::class)->find($type);

        $addSocialMedia = $em->getRepository(SocialMedia::class)->findOneBy(array(
            "country" => $country,
            "type" => $socialMediaType
        ));

        $newCountry = $em->getRepository(Country::class)->find($data["countryId"]);
        $newType = $em->getRepository(SocialMediaType::class)->find($data["typeId"]);

        $addSocialMedia->setType($newType);
        $addSocialMedia->setCountry($newCountry);
        $addSocialMedia->setUrl($data["url"]);
        $addSocialMedia->setActive($data["active"]);
        $em->flush();

        return new JsonResponse(array(
           "status" => "200",
           "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("socialmedia", name="deleteSocialMedia", methods={"DELETE"})
     */
    public function deleteSocialMedia() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);
        $socialMediaType = $em->getRepository(SocialMediaType::class)->find($data["typeId"]);

        $deleteSocialMedia = $em->getRepository(SocialMedia::class)->findOneBy(array(
            "country" => $country,
            "type" => $socialMediaType
        ));
        $deleteSocialMedia->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("socialmedia", name="getAllSocialMediaForAdmin", methods={"GET"})
     */
    public function getAllSocialMedia() {
        $em = $this->getDoctrine()->getManager();

        $socialMedia = $em->getRepository(SocialMedia::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($socialMedia as $sMedia) {
            $tmp = array(
              "url" => $sMedia->getUrl()
            );

            $tmp["country"] = array(
              "id" => $sMedia->getCountry()->getId(),
              "name" => $sMedia->getCountry()->getName()
            );

            $tmp["type"] = array(
              "id" => $sMedia->getType()->getId(),
              "name" => $sMedia->getType()->getName(),
              "code" => $sMedia->getType()->getCode()
            );
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("socialmedia/{type}/{country}", name="getSocialMediaByCountryId", methods={"GET"})
     */
    public function getSocialMediaByCountryId($type, $country) {
        $em = $this->getDoctrine()->getManager();

        $socialMedia = $em->getRepository(SocialMedia::class)->getSocialMediaByCountryType($type, $country);

        return new JsonResponse($socialMedia[0]);
    }
}