<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 15:59
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\Faq;
use App\Entity\FaqText;
use App\Entity\FaqType;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminFaqController extends Controller
{
    /**
     * @Route("faq", name="createFaq", methods={"POST"})
     */
    public function createFaq() {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $faq) {
            $faqType = $em->getRepository(FaqType::class)->find($faq["faqTypeId"]);
            $addFaq = new Faq();
            $addFaq->setName($faq["name"]);
            $addFaq->setActive($faq["active"]);
            $addFaq->setPriority($faq["priority"]);
            $addFaq->setType($faqType);

            $em->persist($addFaq);
            $em->flush();
            foreach ($faq["text"] as $faqText) {
                $country = $em->getRepository(Country::class)->find($faqText["countryId"]);
                $lang = $em->getRepository(Language::class)->find($faqText["langId"]);

                $addFaqText = new FaqText();
                $addFaqText->setCountry($country);
                $addFaqText->setLang($lang);
                $addFaqText->setFaq($addFaq);
                $addFaqText->setQuestion($faqText["question"]);
                $addFaqText->setAnswer($faqText["answer"]);

                $em->persist($addFaqText);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("faq/{id}", name="updateFaq", methods={"PUT"})
     */
    public function updateFaq($id) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $faqType = $em->getRepository(FaqType::class)->find($data["faqTypeId"]);

        $updateFaq = $em->getRepository(Faq::class)->find($id);
        $updateFaq->setName($data["name"]);
        $updateFaq->setActive($data["active"]);
        $updateFaq->setPriority($data["priority"]);
        $updateFaq->setType($faqType);
        $em->flush();

        $updateFaqText =$em->getRepository(FaqText::class)->findBy(array(
            "faq" => $updateFaq,
        ));
        foreach ($updateFaqText as $fqText) {
            $em->remove($fqText);
            $em->flush();
        }

        foreach ($data["text"] as $faqText) {
            $country = $em->getRepository(Country::class)->find($faqText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($faqText["langId"]);

            $newFaqText = new FaqText();

            $newFaqText->setCountry($country);
            $newFaqText->setLang($lang);
            $newFaqText->setFaq($updateFaq);
            $newFaqText->setQuestion($faqText["question"]);
            $newFaqText->setAnswer($faqText["answer"]);
            $em->persist($newFaqText);
            $em->flush();
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("faq/{id}", name="deleteFaq", methods={"DELETE"})
     */
    public function deleteFaq($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteFaq = $em->getRepository(Faq::class)->find($id);

        $deleteFaq->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("faq", name="getAllFaqForAdmin", methods={"GET"})
     */
    public function getAllFaq() {
        $em = $this->getDoctrine()->getManager();

        $faq = $em->getRepository(Faq::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($faq as $f) {
            $tmp = array(
                "id" => $f->getId(),
                "name" => $f->getName(),
                "faqType" => $f->getType()->getName()
            );
            $faqText = $f->getAdminText();
            foreach ($faqText as $text) {
                $tmp["text"][] = array(
                    "question" => $text->getQuestion(),
                    "answer" => $text->getAnswer()
                );

                $tmp["country"] = array(
                  "id" => $text->getCountry()->getId(),
                  "name" => $text->getCountry()->getName()
                );

                $tmp["lang"] = array(
                  "id" => $text->getLang()->getId(),
                  "origName" => $text->getLang()->getOrigName()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("faq/{id}", name="getFaqById", methods={"GET"})
     * @return JsonResponse
     */
    public function getFaqById($id) {
        $em = $this->getDoctrine()->getManager();

        $faq = $em->getRepository(Faq::class)->getAllFaqById($id);

        return new JsonResponse($faq[0]);
    }
}