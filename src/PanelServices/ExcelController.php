<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 10.08.2018
 * Time: 14:55
 */

namespace App\PanelServices;

use App\Entity\Category;
use App\Entity\CategoryFeature;
use App\Entity\FeatureType;
use App\Entity\HighlightedText;
use App\Entity\HighlightedTextTranslate;
use App\Entity\Product;
use App\Entity\ProductCountry;
use App\Entity\ProductFeature;
use App\Entity\ProductSpec;
use App\Entity\ProductText;
use App\Entity\Series;
use App\Entity\Specification;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Feature;
use App\Entity\FeatureText;
use App\Entity\SpecificationCategory;
use App\Entity\SpecificationCategoryText;
use App\Entity\SpecificationText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

class ExcelController extends Controller
{
    public $inputFileType = 'Xlsx';

    /**
     * @SWG\Post(
     *   path="/{country}/{language}/excel",
     *   description="Import to Product Specs Related Excel",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *      name="uploaded_file",
     *      in="formData",
     *      required="true",
     *      type="file"
     *       )
     * )
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("excel", name="excelImport", methods={"POST"})
     */
    public function excelImport()
    {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        $product = null;
        $lastSpecCat = "";
        $lastProd = "";
        $lastCat = "";

        for ($row = 1; $row <= $highestRow; $row++) {
            for ($col = 'A'; $col != $highestColumn; ++$col) {
                if ($col == 'A'){
                    if ($row > 2)  {
                        $prodVal = $sheet2->getCell('A' . $row)->getValue();
                        if ($prodVal != null) {
                            if($prodVal) {
                                $lastProd = $prodVal;
                            }else {
                                $prodVal = $lastProd;
                            }

                            $product = new Product();
                            $product->setCode($prodVal);
                            $result = substr($prodVal, 0, 2);
                            $product->setSize($result);
                            $product->setName($prodVal);
                            $path = "/". $prodVal;
                            $product->setLink($path);
                            $product->setKeywords('asd');
                            $product->setPriority(1);
                            $product->setActive(1);
                            $catVal = $sheet2->getCell('B' . $row)->getValue();

                            if($catVal) {
                                $lastCat = $catVal;
                            }else {
                                $catVal = $lastCat;
                            }

                            $catCheck = $em->getRepository(Category::class)->findBy(array(
                                "name" => $catVal
                            ));

                            $seriesCheck = $em->getRepository(Series::class)->findBy(array(
                                "code" => $catVal
                            ));

                            if ($catCheck){
                                foreach ($catCheck as $cats){
                                    $product->setCategory($cats);
                                }
                            } else {
                                $cat = new Category();
                                $cat->setActive(1);
                                $cat->setCreatedAt(new \DateTime());
                                $cat->setName($catVal);
                                $path = "/". $catVal;
                                $cat->setLink($path);
                                $cat->setKeywords($catVal);
                                $cat->setParentId(1);
                                $em->persist($cat);
                                $product->setCategory($cat);
                            }

                            if ($seriesCheck){
                                foreach ($seriesCheck as $series){
                                    $product->setSeries($series);
                                }
                            } else {
                                $productSeries = new Series();
                                $productSeries->setCode($catVal);
                                $productSeries->setName($catVal);
                                $productSeries->setActive(1);
                                $productSeries->setCreatedAt(new \DateTime());
                                $em->persist($productSeries);
                                $product->setSeries($productSeries);

                            }
                            $em->persist($product);

                        }
                    }

                } else if ($col != 'A' && $col != 'B' && $col != 'C') {
                    $val = $sheet2->getCell($col . $row)->getValue();
                    if ($row == 1){
                        if ($val != null) {
                            $specCategory = new SpecificationCategory();
                            $specCategory->setName($val);
                            $specCategory->setPriority(1);
                            $specCategory->setCreatedAt(new \DateTime());
                            $em->persist($specCategory);
                            $em->flush();

                        }
                    }

                    if ($row == 2) {
                        if ($val != null) {
                            $catName = $sheet2->getCell($col . "1")->getValue();

                            $specCategory = $em->getRepository(SpecificationCategory::class)->findOneBy(array(
                                "name" => $catName
                            ));

                            $spec = new Specification();
                            $spec->setName($val);
                            $spec->setCreatedAt(new \DateTime());
                            $spec->setCode($val);
                            $spec->setCategory($specCategory);
                            $spec->setPriority(1);
                            $em->persist($spec);
                            $em->flush();

                        }
                    }
                    if($row > 2){
                        $productSpecValue = $sheet2->getCell($col . $row)->getValue();
                        $productSpecName = $sheet2->getCell($col . 2)->getValue();

                        if ($productSpecValue) {
                            $specs = $em->getRepository(Specification::class)->findOneBy(array(
                                "name" => $productSpecName
                            ));

                            $productSpec = new ProductSpec();
                            $productSpec->setProduct($product);
                            $productSpec->setValue($productSpecValue);
                            $productSpec->setPriority(1);
                            $productSpec->setCreatedAt(new \DateTime());
                            $productSpec->setSpec($specs);

                            $em->persist($productSpec);
                            $em->flush();
                        }
                    }
                }
            }
        }

        exit;
    }

    /**
     * @Route("/import/translations/{country}/{lang}", name="importTranslation", methods={"POST"})
     */
    public function importTranslation($country, $lang) {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $country = $this->getDoctrine()->getRepository(Country::class)->findOneBy(array(
            "id" => $country
        ));
        $lang = $this->getDoctrine()->getRepository(Language::class)->findOneBy(array(
            "id" => $lang
        ));

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);

        foreach($sheetNames as $sheetIndex => $sheetName) {
            $spreadSheet->setActiveSheetIndex($sheetIndex);
            $sheet = $spreadSheet->getActiveSheet();
            $rowCount = $sheet->getHighestRow();
            $textEntity = $this->createTextEntityName($sheetName);
            $entity = $this->createEntityName($sheetName);
            $fullEntity = "App\\Entity\\" . $entity;
            $fullTextEntity = "App\\Entity\\" . $textEntity;
            $columnCount = Coordinate::columnIndexFromString($sheet->getHighestColumn());

            for($i = 2; $i <= $rowCount; $i++) {
                $parentId = $sheet->getCellByColumnAndRow(1, $i)->getValue();

                $parent = $this->getDoctrine()->getRepository($fullEntity)->findOneBy(array(
                    "id" => $parentId
                ));

                if(!$parent) {
                    continue;
                }

                $item = new $fullTextEntity();
                $parentSetter = $this->createParrentSetterFunc($entity);
                $item->$parentSetter($parent);
                if($textEntity != "SliderItemText") {
                    $item->setCountry($country);
                }

                $item->setLang($lang);

                for($j = 2; $j <= $columnCount; $j++ ) {
                    $columnName = $sheet->getCellByColumnAndRow($j, 1)->getValue();
                    $value = $sheet->getCellByColumnAndRow($j, $i)->getValue();
                    $funcName = $this->createSetterFunc($columnName);
                    $item->$funcName($value);
                }
                try {
                    $em->persist($item);
                    $em->flush();
                }catch(\Exception $e) {

                }
            }

        }

        return new JsonResponse(array(
            "status" => 200,
            "message" => "Success"
        ));
    }

    private function createSetterFunc($name) {
        return "set" . implode("", array_map("ucwords", explode("_", $name)));
    }

    private function createParrentSetterFunc($name) {
        if($name == "SpecificationCategory") {
            return "setSpecCategory";
        }else if($name == "Specification") {
            return "setSpec";
        }else if($name == "SliderItem") {
            return "setSlider";
        }

        return "set" . $name;
    }

    private function createEntityName($name) {
        if($name == "slider_item") {
            return "Slider";
        }

        return implode("", array_map("ucwords", explode("_", $name)));
    }

    private function createTextEntityName($name) {
        if($name == "static_text") {
            return "StaticTextTranslate";
        }

        return implode("", array_map("ucwords", explode("_", $name))) . "Text";
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("sharpexcel", name="sharpExcelImportProduct", methods={"POST"})
     */
    public function sharpExcelImportProduct()
    {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $airPurifierSeries = $em->getRepository(Series::class)->find(1);
        $countryGlobal = $em->getRepository(Country::class)->find(1);
        $langGlobal = $em->getRepository(Language::class)->find(1);

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);
        $sheetName = $sheetNames[10];
        $product = null;
        $lastProd = "";
        $priority = 10;

        $spreadSheet->setActiveSheetIndex(1);
        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $category = new Category();
        $category->setName($sheetName);
        $category->setLink($sheetName);
        $category->setKeywords($sheetName);
        $category->setActive(1);
        $category->setPriority($priority);

        $em->persist($category);

        for ($row = 3; $row <= $highestRow; $row++) {
            $prodVal = $sheet->getCell('B' . $row)->getValue();

            if(!$prodVal) {
                continue;
            }
            $prodCode = $sheet->getCell('A' . $row)->getValue();
            $productShortDesc = $sheet->getCell('B' . $row)->getValue();
            $productDescription = $sheet->getCell('C' . $row)->getValue();

            $product = new Product();
            $product->setName($prodVal);
            $product->setCode($prodCode);
            $product->setLink($prodCode);
            $product->setKeywords($prodVal);
            $product->setSize("");
            $product->setCategory($category);
            $product->setSeries($airPurifierSeries);
            $product->setActive(1);
            $product->setPriority($priority);
            $product->setSmart(0);
            $product->setIsOffer(0);
            $product->setIsNew(0);
            $product->setIsBest(0);
            $priority++;

            $em->persist($product);
            $em->flush();

            $productText = new ProductText();
            $productText->setShortDescription($productShortDesc);
            $productText->setLongDescription($productDescription);
            $productText->setCountry($countryGlobal);
            $productText->setLang($langGlobal);
            $productText->setProduct($product);
            $productText->setLink("");

            $em->persist($productText);
            $em->flush();

            $translateArray = $sheet->rangeToArray('AE'.$row.':'.'AN'.$row);
            $translateArray = array_filter($translateArray[0]);

            foreach ($translateArray as $translate) {
                $highLight = new HighlightedText();
                $highLight->setProduct($product);
                $highLight->setPriority($priority);
                $priority++;

                $em->persist($highLight);
                $em->flush();

                $newTranslateText = new HighlightedTextTranslate();
                $newTranslateText->setText($translate);
                $newTranslateText->setLang($langGlobal);
                $newTranslateText->setCountry($countryGlobal);
                $newTranslateText->setHighlightedText($highLight);

                $em->persist($newTranslateText);
                $em->flush();
            }


            $featureArray = $sheet->rangeToArray('AO2:AU2')[0];
            $productFeatureArray = $sheet->rangeToArray('AO'.$row.':AU'.$row)[0];

            foreach ($productFeatureArray as $key => $productFeature) {
                $feature = $em->getRepository(Feature::class)->findOneBy(array(
                    "name" => $featureArray[$key]
                ));

                if(!$feature) {
                    $feature = new Feature();
                    $feature->setName($featureArray[$key]);
                    $feature->setActive(1);
                    $em->persist($feature);
                    $em->flush();
                }

                if(strtolower($productFeature) == 'yes') {
                    $featureType = $em->getRepository(FeatureType::class)->findOneBy(array(
                        "code" => "bigFeature"
                    ));

                    $productFeature = new ProductFeature();
                    $productFeature->setProduct($product);
                    $productFeature->setPriority($priority++);
                    $productFeature->setType($featureType);
                    $productFeature->setFeature($feature);
                    $em->persist($productFeature);
                    $em->flush();
                }
            }

            $specCategoryArray = $sheet->rangeToArray('D1:AD1')[0];
            $specArray = $sheet->rangeToArray('D2:AD2')[0];
            $specTextValue = $sheet->rangeToArray('D' . $row . ':' . 'AD' . $row);
            $this->extendArray($specCategoryArray);

            foreach ($specTextValue[0] as $key => $filterTextValue) {
                $findSpecCat = $em->getRepository(SpecificationCategory::class)->findOneBy(array(
                    "name" => $specCategoryArray[$key]
                ));
                if (!$findSpecCat) {
                    $findSpecCat = new SpecificationCategory();
                    $findSpecCat->setName($specCategoryArray[$key]);
                    $findSpecCat->setPriority($priority);
                    $priority++;

                    $em->persist($findSpecCat);
                    $em->flush();
                }

                $specCategoryText = new SpecificationCategoryText();
                $specCategoryText->setLang($langGlobal);
                $specCategoryText->setCountry($countryGlobal);
                $specCategoryText->setSpecCategory($findSpecCat);
                $specCategoryText->setText($specCategoryArray[$key]);

                $em->persist($specCategoryText);

                $findSpec = $em->getRepository(Specification::class)->findOneBy(array(
                    "name" => $specArray[$key],
                    "category" => $findSpecCat
                ));

                if (!$findSpec) {
                    $findSpec = new Specification();
                    $findSpec->setName($specArray[$key]);
                    $findSpec->setCreatedAt(new \DateTime());
                    $findSpec->setCode($specArray[$key]);
                    $findSpec->setCategory($findSpecCat);
                    $findSpec->setPriority($priority++);

                    $em->persist($findSpec);
                    $em->flush();
                }

                if($filterTextValue) {
                    $specText = new ProductSpec();
                    $specText->setProduct($product);
                    $specText->setSpec($findSpec);
                    $specText->setValue($filterTextValue);
                    $specText->setPriority($priority);
                    $priority++;

                    $em->persist($specText);
                    $em->flush();
                }
            }
        }

        return new JsonResponse("success");
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("translatesharp", name="translateSharp", methods={"POST"})
     */
    public function translateSharp()
    {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $country = $em->getRepository(Country::class)->find(21);
        $lang = $em->getRepository(Language::class)->find(19);

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);
        $product = null;
        $lastProd = "";
        $priority = 10;

        $spreadSheet->setActiveSheetIndex(8);
        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 5; $row <= $highestRow; $row++) {
            $prodVal = $sheet->getCell('B' . $row)->getValue();

            if(!$prodVal) {
                continue;
            }
            $prodCode = $sheet->getCell('A' . $row)->getValue();
            $modelName = $sheet->getCell('B' . $row)->getValue();
            $productDescription = $sheet->getCell('C' . $row)->getValue();
            $product = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $prodCode
            ));
            if (!$product) {
                continue;
            }
            $findProductText = $em->getRepository(ProductText::class)->findOneBy(array(
                "product" => $product,
                "country" => $country,
                "lang" => $lang
            ));
            if (!$findProductText) {
                $productText = new ProductText();
                $productText->setName($modelName);
                $productText->setLongDescription($productDescription);
                $productText->setLang($lang);
                $productText->setCountry($country);
                $productText->setProduct($product);
                $productText->setLink("");

                $em->persist($productText);
            }
            $findProductCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                "product" => $product,
                "country" => $country
            ));
            if (!$findProductCountry) {
                $productCountry = new ProductCountry();
                $productCountry->setCountry($country);
                $productCountry->setProduct($product);
                $productCountry->setActive(1);

                $em->persist($productCountry);
                $em->flush();
            }

            $translateArray = $sheet->rangeToArray('AU'.$row.':'.'BD'.$row);
            $translateArray = array_filter($translateArray[0]);
            $counter = 0;

            foreach ($translateArray as $translate) {

                $highLightArray = $em->getRepository(HighlightedText::class)->findBy(array(
                    "product" => $product
                ));

                if(!isset($highLightArray[$counter])) {
                    $highLightArray[$counter] = new HighlightedText();
                    $highLightArray[$counter]->setProduct($product);
                    $highLightArray[$counter]->setPriority($priority++);
                    $em->persist($highLightArray[$counter]);
                    $em->flush();
                }

                $newTranslateText = new HighlightedTextTranslate();
                $newTranslateText->setText($translate);
                $newTranslateText->setLang($lang);
                $newTranslateText->setCountry($country);
                $newTranslateText->setHighlightedText($highLightArray[$counter]);

                $em->persist($newTranslateText);
                $em->flush();

                $counter = $counter+1;
            }

            $featureArray = $sheet->rangeToArray('BE3:BK3')[0];
            $productFeatureArray = $sheet->rangeToArray('BE'.$row.':BK'.$row)[0];
            $featureTranslate = $sheet->rangeToArray('BE4:BK4')[0];

            foreach ($productFeatureArray as $key => $productFeature) {
                $feature = $em->getRepository(Feature::class)->findOneBy(array(
                    "name" => $featureArray[$key]
                ));

                if(!$feature) {
                    continue;
                }
                $findFeatureText = $em->getRepository(FeatureText::class)->findOneBy(array(
                    "feature" => $feature,
                    "country" => $country,
                    "lang" => $lang
                ));
                if (!$findFeatureText) {
                    $featureText = new FeatureText();
                    if ($featureTranslate[$key] == "") {
                        $featureTranslate[$key] = $featureArray[$key];
                    }
                    $featureText->setName($featureTranslate[$key]);
                    $featureText->setFeature($feature);
                    $featureText->setCountry($country);
                    $featureText->setLang($lang);
                    $em->persist($featureText);
                    $em->flush();
                }
            }

            $specCategoryArray = $sheet->rangeToArray('D1:AT1')[0];
            $specCategoryTranslateArray = $sheet->rangeToArray('D2:AT2')[0];
            $specArray = $sheet->rangeToArray('D3:AT3')[0];
            $specTextValue = $sheet->rangeToArray('D' . $row . ':' . 'AT' . $row);
            $specTranslateArray = $sheet->rangeToArray('D4:AT4')[0];
            $this->extendArray($specCategoryArray);

            foreach ($specTextValue[0] as $key => $filterTextValue) {
                $findSpecCat = $em->getRepository(SpecificationCategory::class)->findOneBy(array(
                    "name" => $specCategoryArray[$key]
                ));
                if (!$findSpecCat) {
                    $findSpecCat = new SpecificationCategory();
                    $findSpecCat->setName($specCategoryArray[$key]);
                    $findSpecCat->setPriority($priority);
                    $priority++;
                    $em->persist($findSpecCat);
                    $em->flush();
                }
                if (!$specCategoryTranslateArray[$key]) {
                    $specCategoryTranslateArray[$key] = "";
                }
                
                $specCategoryText = new SpecificationCategoryText();
                $specCategoryText->setLang($lang);
                $specCategoryText->setCountry($country);
                $specCategoryText->setSpecCategory($findSpecCat);
                $specCategoryText->setText($specCategoryTranslateArray[$key]);

                $em->persist($specCategoryText);

                $findSpec = $em->getRepository(Specification::class)->findOneBy(array(
                    "name" => $specArray[$key],
                    "category" => $findSpecCat
                ));

                if (!$findSpec) {
                    continue;
                }

                $translateText = $em->getRepository(SpecificationText::class)->findOneBy(array(
                    "spec" => $findSpec,
                    "country" => $country,
                    "lang" => $lang
                ));
                if (!$translateText) {
                    $translateSpecText = new SpecificationText();
                    $translateSpecText->setLang($lang);
                    $translateSpecText->setCountry($country);
                    $translateSpecText->setSpec($findSpec);
                    if($specTranslateArray[$key] == "") {
                        $specTranslateArray[$key] = $specArray[$key];
                    }
                    $translateSpecText->setName($specTranslateArray[$key]);

                    $em->persist($translateSpecText);
                    $em->flush();
                }

                if($filterTextValue) {
                    $findProdSpec = $em->getRepository(ProductSpec::class)->findOneBy(array(
                        "product" => $product,
                        "spec" => $findSpec,
                        "lang" => $lang
                    ));
                    if (!$findProdSpec) {
                        $specText = new ProductSpec();
                        $specText->setProduct($product);
                        $specText->setSpec($findSpec);
                        $specText->setValue($filterTextValue);
                        $specText->setPriority($priority);
                        $specText->setLang($lang);
                        $priority++;

                        $em->persist($specText);
                        $em->flush();
                    }
                }
            }
        }

        return new JsonResponse("success");
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("checkproduct", name="checkProductExist", methods={"POST"})
     */
    public function  checkProductExist() {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);
        $product = null;
        $lastProd = "";
        $priority = 10;

        $spreadSheet->setActiveSheetIndex(10);
        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();

        $prodArray = array();

        for ($row = 5; $row <= $highestRow; $row++) {
            $prodVal = $sheet->getCell('A' . $row)->getValue();
            $findProd = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $prodVal
            ));
            if (!$findProd) {
                array_push($prodArray, $prodVal);
            }
        }
        return new JsonResponse($prodArray);

    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("translatecheckproduct", name="translateNonExistProduct", methods={"POST"})
     */
    public function translateNonExistProduct(Request $request)
    {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $country = $em->getRepository(Country::class)->find(4);
        $lang = $em->getRepository(Language::class)->find(2);

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);

        $product = null;
        $lastProd = "";
        $priority = 10;

        $spreadSheet->setActiveSheetIndex(9);

        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $categoryName = "";


        $sheetName = $sheetNames[9];

        for ($row = 5; $row <= $highestRow; $row++) {
            $prodVal = $sheet->getCell('B' . $row)->getValue();
            if(!$prodVal) {
                continue;
            }
            $prodCode = $sheet->getCell('A' . $row)->getValue();
            $modelName = $sheet->getCell('B' . $row)->getValue();
            $productDescription = $sheet->getCell('C' . $row)->getValue();
            $product = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $prodCode
            ));

            if (!$product) {
                $product = new Product();
                $product->setName($modelName);
                $product->setCode($prodCode);
                $product->setPriority($priority);
                $product->setActive(1);
                $priority++;
                $findCategory = $em->getRepository(Category::class)->findOneBy(array(
                    "name" => $sheetName
                ));
                if (!$findCategory) {
                    $findCategory = new Category();
                    $findCategory->setKeywords($sheetName);
                    $findCategory->setLink($this->slug($sheetName));
                    $findCategory->setActive(1);
                    $findCategory->setName($sheetName);
                    $findCategory->setPriority($priority);
                    $priority++;

                    $em->persist($findCategory);
                    $em->flush();
                }
                $product->setCategory($findCategory);
                $findSeries = $em->getRepository(Series::class)->find(1);
                $product->setSeries($findSeries);
                $product->setLink($prodCode);
                $product->setKeywords($prodCode);

                $em->persist($product);
                $em->flush();

                $findProductText = $em->getRepository(ProductText::class)->findOneBy(array(
                    "product" => $product,
                    "country" => $country,
                    "lang" => $lang
                ));
                if (!$findProductText) {
                    $productText = new ProductText();
                    $productText->setName($modelName);
                    $productText->setLongDescription($productDescription);
                    $productText->setLang($lang);
                    $productText->setCountry($country);
                    $productText->setProduct($product);
                    $productText->setLink("");

                    $em->persist($productText);
                }
                $findProductCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                    "product" => $product,
                    "country" => $country
                ));
                if (!$findProductCountry) {
                    $productCountry = new ProductCountry();
                    $productCountry->setCountry($country);
                    $productCountry->setProduct($product);
                    $productCountry->setActive(1);

                    $em->persist($productCountry);
                    $em->flush();
                }

                $translateArray = $sheet->rangeToArray('AV'.$row.':'.'BE'.$row);
                $translateArray = array_filter($translateArray[0]);
                $counter = 0;

                foreach ($translateArray as $translate) {

                    $highLightArray = $em->getRepository(HighlightedText::class)->findBy(array(
                        "product" => $product
                    ));

                    if(!isset($highLightArray[$counter])) {
                        $highLightArray[$counter] = new HighlightedText();
                        $highLightArray[$counter]->setProduct($product);
                        $highLightArray[$counter]->setPriority($priority++);
                        $em->persist($highLightArray[$counter]);
                        $em->flush();
                    }

                    $newTranslateText = new HighlightedTextTranslate();
                    $newTranslateText->setText($translate);
                    $newTranslateText->setLang($lang);
                    $newTranslateText->setCountry($country);
                    $newTranslateText->setHighlightedText($highLightArray[$counter]);

                    $em->persist($newTranslateText);
                    $em->flush();

                    $counter = $counter+1;
                }

                $featureArray = $sheet->rangeToArray('BF3:BL3')[0];
                $productFeatureArray = $sheet->rangeToArray('BF'.$row.':BL'.$row)[0];
                $featureTranslate = $sheet->rangeToArray('BF4:BL4')[0];

                foreach ($productFeatureArray as $key => $productFeature) {
                    $feature = $em->getRepository(Feature::class)->findOneBy(array(
                        "name" => $featureArray[$key]
                    ));

                    if(!$feature) {
                        continue;
                    }
                    $findFeatureText = $em->getRepository(FeatureText::class)->findOneBy(array(
                        "feature" => $feature,
                        "country" => $country,
                        "lang" => $lang
                    ));
                    if (!$findFeatureText) {
                        $featureText = new FeatureText();
                        if ($featureTranslate[$key] == "") {
                            $featureTranslate[$key] = $featureArray[$key];
                        }
                        $featureText->setName($featureTranslate[$key]);
                        $featureText->setFeature($feature);
                        $featureText->setCountry($country);
                        $featureText->setLang($lang);
                        $em->persist($featureText);
                        $em->flush();
                    }
                }

                $specCategoryArray = $sheet->rangeToArray('D1:AU1')[0];
                $specCategoryTranslateArray = $sheet->rangeToArray('D2:AU2')[0];
                $specArray = $sheet->rangeToArray('D3:AU3')[0];
                $specTextValue = $sheet->rangeToArray('D' . $row . ':' . 'AU' . $row);
                $specTranslateArray = $sheet->rangeToArray('D4:AU4')[0];
                $this->extendArray($specCategoryArray);

                foreach ($specTextValue[0] as $key => $filterTextValue) {
                    $findSpecCat = $em->getRepository(SpecificationCategory::class)->findOneBy(array(
                        "name" => $specCategoryArray[$key]
                    ));
                    if (!$findSpecCat) {
                        $findSpecCat = new SpecificationCategory();
                        $findSpecCat->setName($specCategoryArray[$key]);
                        $findSpecCat->setPriority($priority);
                        $priority++;
                        $em->persist($findSpecCat);
                        $em->flush();
                    }
                    if (!$specCategoryTranslateArray[$key]) {
                        $specCategoryTranslateArray[$key] = "";
                    }

                    $specCategoryText = new SpecificationCategoryText();
                    $specCategoryText->setLang($lang);
                    $specCategoryText->setCountry($country);
                    $specCategoryText->setSpecCategory($findSpecCat);
                    $specCategoryText->setText($specCategoryTranslateArray[$key]);

                    $em->persist($specCategoryText);

                    $findSpec = $em->getRepository(Specification::class)->findOneBy(array(
                        "name" => $specArray[$key],
                        "category" => $findSpecCat
                    ));

                    if (!$findSpec) {
                        continue;
                    }

                    $translateText = $em->getRepository(SpecificationText::class)->findOneBy(array(
                        "spec" => $findSpec,
                        "country" => $country,
                        "lang" => $lang
                    ));
                    if (!$translateText) {
                        $translateSpecText = new SpecificationText();
                        $translateSpecText->setLang($lang);
                        $translateSpecText->setCountry($country);
                        $translateSpecText->setSpec($findSpec);
                        if($specTranslateArray[$key] == "") {
                            $specTranslateArray[$key] = $specArray[$key];
                        }
                        $translateSpecText->setName($specTranslateArray[$key]);

                        $em->persist($translateSpecText);
                        $em->flush();
                    }

                    if($filterTextValue) {
                        $findProdSpec = $em->getRepository(ProductSpec::class)->findOneBy(array(
                            "product" => $product,
                            "spec" => $findSpec,
                            "lang" => $lang
                        ));
                        if (!$findProdSpec) {
                            $specText = new ProductSpec();
                            $specText->setProduct($product);
                            $specText->setSpec($findSpec);
                            $specText->setValue($filterTextValue);
                            $specText->setPriority($priority);
                            $specText->setLang($lang);
                            $priority++;

                            $em->persist($specText);
                            $em->flush();
                        }
                    }
                }
            }
        }

        return new JsonResponse("success");
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("translatefeature", name="translateFeature", methods={"POST"})
     */
    public function translateFeature(){
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();



        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);

        $product = null;
        $lastProd = "";
        $priority = 10;

        $spreadSheet->setActiveSheetIndex(0);

        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $categoryName = "";

        $country = $em->getRepository(Country::class)->find(21);
        $lang = $em->getRepository(Language::class)->find(19);
        for ($row = 17; $row <= 47; $row++) {
            $featureName = $sheet->getCell('B' . $row)->getValue();

            $featureTranslate = $sheet->getCell('V'.$row)->getValue();

            $findFeature = $em->getRepository(Feature::class)->findOneBy(array(
                "name" => $featureName
            ));
            $isFeaturePersist = false;
            if (!$findFeature) {
                $findFeature = new Feature();
                $isFeaturePersist = true;
            }
            $findFeature->setName($featureName);
            $findFeature->setActive(1);
            if ($isFeaturePersist) {
                $em->persist($findFeature);
            }
            $findCategory = $em->getRepository(Category::class)->find(30);
            $type = $em->getRepository(FeatureType::class)->find(1);
            $findCategoryFeature = $em->getRepository(CategoryFeature::class)->findOneBy(array(
                "category" => $findCategory,
                "feature" => $findFeature,
                "type" => $type
            ));
            if (!$findCategoryFeature) {
                $findCategoryFeature = new CategoryFeature();
                $findCategoryFeature->setFeature($findFeature);
                $findCategoryFeature->setCategory($findCategory);
                $findCategoryFeature->setType($type);
                $findCategoryFeature->setPriority($priority++);

                $em->persist($findCategoryFeature);
                $em->flush();
            }

            $em->flush();

            $findFeatureText = $em->getRepository(FeatureText::class)->findOneBy(array(
                "feature" => $findFeature,
                "country" => $country,
                "lang" => $lang
            ));
            $isTextPersist = false;
            if (!$findFeatureText) {
                $findFeatureText = new FeatureText();
                $isTextPersist = true;
            }
            $findFeatureText->setName($featureTranslate);
            $findFeatureText->setLang($lang);
            $findFeatureText->setCountry($country);
            $findFeatureText->setFeature($findFeature);
            $findFeatureText->setLongDescription("");
            $findFeatureText->setShortDescription("");
            if ($isTextPersist) {
                $em->persist($findFeatureText);
            }
            $em->flush();

        }
        exit;

    }
    private function extendArray(&$array) {
        $lastItem = "";

        foreach ($array as &$item) {
            if($item != "") {
                $lastItem = $item;
            }else {
                $item = $lastItem;
            }
        }

        return $array;
    }

    function slug($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }
}
