<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 4.12.2018
 * Time: 11:59
 */

namespace App\PanelServices;


use App\Entity\Module;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Entity\UserModule;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends Controller
{
    /**
     * @Route("user", name="getAllUser", methods={"GET"})
     */
    public function getAllUser() {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($users as $user) {
            $tmp = array(
                "id" => $user->getId(),
                "name" => $user->getName(),
                "userName" => $user->getUserName(),
                "mail" => $user->getMail(),
                "phone" => $user->getPhone()
            );
            $userModule = $user->getModule();
            foreach ($userModule as $module) {
                $tmp["module"][] = array(
                    "id" => $module->getModule()->getId(),
                    "name" => $module->getModule()->getName(),
                    "code" => $module->getModule()->getCode()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("user/{id}", name="getUserById", methods={"GET"})
     */
    public function getUserById($id) {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->getUserById($id);

        if(count($users) === 0) {
            return new JsonResponse([]);
        }

        return new JsonResponse($users[0]);
    }

    /**
     * @return JsonResponse
     * @Route("user", name="createUser", methods={"POST"})
     */
    public function createUser() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');

        $data = json_decode($json,true);

        foreach ($data as $users) {

            $user = new User();
            $user->setActive($users["active"]);
            $user->setName($users["name"]);
            $user->setPassword(md5($users["password"]));
            $user->setMail($users["mail"]);
            $user->setUsername($users["userName"]);
            $user->setPhone($users["phone"]);

            $em->persist($user);

            foreach ($users["module"] as $module) {
                $findModule = $em->getRepository(Module::class)->find($module["moduleId"]);

                $addRank = new UserModule();

                $addRank->setUser($user);
                $addRank->setModule($findModule);

                $em->persist($addRank);
            }
            $em->flush();
        }


        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("user/{id}", name="updateUserAdmin", methods={"PUT"})
     */
    public function updateUserAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');

        $data = json_decode($json,true);

        $user = $em->getRepository(User::class)->find($id);
        $user->setActive($data["active"]);
        $user->setName($data["name"]);
        $user->setMail($data["mail"]);
        if ($data["password"] != "") {
            $user->setPassword(md5($data["password"]));
        }
        $user->setUsername($data["userName"]);
        $user->setPhone($data["phone"]);

        $em->persist($user);

        $removeModule = $em->getRepository(UserModule::class)->findBy(array(
            "user" => $user
        ));

        foreach ($removeModule as $mod) {
            $em->remove($mod);
            $em->flush();
        }
        foreach ($data["module"] as $module) {
            $findModule = $em->getRepository(Module::class)->find($module["moduleId"]);

            $newModule = new UserModule();
            $newModule->setModule($findModule);
            $newModule->setUser($user);

            $em->persist($newModule);
        }

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("user/{id}", name="deleteUserAdmin", methods={"DELETE"})
     */
    public function deleteUserAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($id);
        $user->setDeletedAt(new \DateTime());
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("modules", name="moduleList", methods={"GET"})
     */
    public function moduleList() {
        $em = $this->getDoctrine()->getManager();

        $findModules = $em->getRepository(Module::class)->findAll();
        $moduleArray = array();

        foreach ($findModules as $modules) {
            $moduleArray[] = array(
                "id" => $modules->getId(),
                "name" => $modules->getName(),
                "code" => $modules->getCode()
            );
        }

        return new JsonResponse($moduleArray);
    }
}