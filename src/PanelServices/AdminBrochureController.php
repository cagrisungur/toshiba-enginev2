<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.03.2019
 * Time: 10:54
 */

namespace App\PanelServices;


use App\Entity\Brochure;
use App\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminBrochureController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("brochure", name="getBrochureForAdmin", methods={"GET"})
     */
    public function getBrochureForAdmin() {
        $em = $this->getDoctrine()->getManager();

        $brochures = $em->getRepository(Brochure::class)->findBy(array(
            "deletedAt" => null
        ));

        $brochureArray = array();

        foreach ($brochures as $brochure) {
            $brochureArray[] = array(
                "id" => $brochure->getId(),
                "name" => $brochure->getName(),
                "text" => $brochure->getText(),
                "link" => $brochure->getLink(),
                "thumbnail" => $brochure->getThumbnail(),
                "country" => $brochure->getCountry()->getName(),
            );
        }

        return new JsonResponse($brochureArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("brochure/{id}", name="getBrochureByIdForAdmin", methods={"GET"})
     */
    public function getBrochureByIdForAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $brochures = $em->getRepository(Brochure::class)->getBrochureById($id);

        return new JsonResponse($brochures[0]);
    }

    /**
     * @return JsonResponse
     * @Route("brochure", name="createBrochureAdmin", methods={"POST"})
     */
    public function createBrochureAdmin() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $brochure) {
            $country = $em->getRepository(Country::class)->find($brochure["countryId"]);

            $newBrochure = new Brochure();
            $newBrochure->setPriority($brochure["priority"]);
            $newBrochure->setCountry($country);
            $newBrochure->setName($brochure["name"]);
            $newBrochure->setActive($brochure["active"]);
            $newBrochure->setText($brochure["text"]);

            $baseUrl = $this->container->getParameter('baseUrlHttp');
            if (isset($brochure["link"]) && !empty($brochure["link"])) {
                define('file_upload','./uploads/files/file');

                $replace = explode(',', $brochure["link"]);
                $replace = $replace[count($replace) - 1];
                $replace = str_replace(' ', '+', $replace);
                $fileDecode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $fileMime = $split[1];

                $mimeFunction = $this->getFileMimeType($fileMime);
                if ($mimeFunction !== 'application/octet-stream') {
                    $split = explode( '/', $mimeType );
                    $fileExtension = $split[1];

                    $file = file_upload . uniqid(). '.' . $fileExtension;
                    file_put_contents($file, $fileDecode);
                    $trimPath = ltrim($file, '.');
                    $newBrochure->setLink($baseUrl . $trimPath);
                    $newBrochure->setSize($this->getBase64ImageSize($brochure["link"]));
                }
            }
            if (isset($brochure["thumbnail"]) && !empty($brochure["thumbnail"])) {
                if(!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }

                $thumbnailReplace = explode(',', $brochure["thumbnail"]);
                $thumbnailReplace = $thumbnailReplace[count($thumbnailReplace) - 1];
                $thumbnailReplace = str_replace(' ', '+', $thumbnailReplace);
                $thumbDecode = base64_decode($thumbnailReplace);

                $f = finfo_open();
                $mimeTypeThumbnail = finfo_buffer($f, $thumbDecode, FILEINFO_MIME_TYPE);
                $thumpSplit = explode( '/', $mimeTypeThumbnail );
                $thumpImageType = $thumpSplit[1];
                $thumpMime = $this->getMimeType($thumpImageType);
                if ($thumpMime !== 'application/octet-stream') {
                    $file = UPLOAD_DIR . uniqid(). '.' . $thumpImageType;
                    file_put_contents($file, $thumbDecode);
                    $thumpTrim = ltrim($file, '.');

                    $newBrochure->setThumbnail($baseUrl . $thumpTrim);
                }
            }

            $em->persist($newBrochure);
            $em->flush();
        }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("brochure/{id}", name="updateBrochureAdmin", methods={"PUT"})
     */
    public function updateBrochureAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $baseUrl = $this->container->getParameter('baseUrlHttp');

        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $brochure = $em->getRepository(Brochure::class)->find($id);

        $brochure->setCountry($country);
        $brochure->setName($data["name"]);
        $brochure->setText($data["text"]);
        $brochure->setPriority($data["priority"]);
        $brochure->setActive($data["active"]);

        if (isset($data["link"]) && !empty($data["link"])) {
            if (isset($data["isLinkOld"]) && $data["isLinkOld"] == false) {
                define('file_upload','./uploads/files/file');

                // $replace = str_replace('data:file/base64,','', $prodFile["fileBase64"]);

                $replace = explode(',', $data["link"])[1];
                $replace = str_replace(' ', '+', $replace);
                $fileDecode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $fileMime = $split[1];

                $mimeFunction = $this->getFileMimeType($fileMime);

                if ($mimeFunction !== 'application/octet-stream') {
                    $split = explode( '/', $mimeType );
                    $fileExtension = $split[1];

                    $file = file_upload . uniqid(). '.' . $fileExtension;
                    file_put_contents($file, $fileDecode);
                    $trimPath = ltrim($file, '.');

                    $brochure->setLink($baseUrl . $trimPath);
                    $brochure->setSize($this->getBase64ImageSize($data["link"]));
                }
            }
        }

        if (isset($data["thumbnail"]) && !empty($data["thumbnail"])) {
            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }
            if (isset($data["isThumbnailOld"]) && $data["isThumbnailOld"] == false) {
                $thumbnailReplace = explode(',', $data["thumbnail"])[1];
                $thumbnailReplace = str_replace(' ', '+', $thumbnailReplace);
                $thumbDecode = base64_decode($thumbnailReplace);

                $f = finfo_open();
                $mimeTypeThumbnail = finfo_buffer($f, $thumbDecode, FILEINFO_MIME_TYPE);
                $thumpSplit = explode( '/', $mimeTypeThumbnail );
                $thumpImageType = $thumpSplit[1];

                $thumpMime = $this->getMimeType($thumpImageType);
                if ($thumpMime !== 'application/octet-stream') {
                    $file = UPLOAD_DIR . uniqid(). '.' . $thumpImageType;
                    file_put_contents($file, $thumbDecode);
                    $thumpTrim = ltrim($file, '.');

                    $brochure->setThumbnail($baseUrl . $thumpTrim);
                }
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("brochure/{id}", name="deleteBrochure", methods={"DELETE"})
     */
    public function deleteBrochure($id) {
        $em = $this->getDoctrine()->getManager();

        $brochure = $em->getRepository(Brochure::class)->find($id);
        $brochure->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    public function getFileMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'pdf' => 'application/pdf',
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        $base64Image = explode(',', $base64Image);
        $base64Image = $base64Image[count($base64Image) - 1];
        try{
            $sizeInByte = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);

            return $sizeInByte;
        }
        catch(\Exception $e){
            return $e;
        }
    }
}