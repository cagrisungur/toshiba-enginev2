<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 15:01
 */

namespace App\PanelServices;


use App\Entity\ControllerImage;
use App\Entity\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminControllerController extends Controller
{
    /**
     * @param $request
     * @Route("controller", name="createController", methods={"POST"})
     * @return JsonResponse
     */
    public function createController(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $controller) {
            $addController = new \App\Entity\Controller();
            $addController->setName($controller["name"]);
            $addController->setActive($controller["priority"]);
            $addController->setPriority($controller["active"]);

            $em->persist($addController);

            foreach ($controller["image"] as $controllerImage) {
                $controllerImageType = $em->getRepository(ImageType::class)->find($controllerImage["imageType"]);

                define('UPLOAD_DIR','./uploads/images/images');

                $replace = explode(',', $controllerImage["file"])[1];
                $replace = str_replace(' ', '+', $replace);
                $decode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $imageType = $split[1];

                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                file_put_contents($file, $decode);
                $trimPath = ltrim($file, '.');

                $addControllerImage = new ControllerImage();
                $addControllerImage->setController($addController);
                $addControllerImage->setType($controllerImageType);
                $addControllerImage->setPriority($controllerImage["priority"]);
                $addControllerImage->setPath($baseUrl . $trimPath);
                $addControllerImage->setActive($controllerImage["active"]);
                $addControllerImage->setName($controllerImage["name"]);

                $em->persist($addControllerImage);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("controller/{id}", name="updateController", methods={"PUT"})
     */
    public function updateController($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $updateController = $em->getRepository(\App\Entity\Controller::class)->find($id);
        $updateController->setName($data["name"]);
        $updateController->setActive($data["priority"]);
        $updateController->setPriority($data["active"]);

        foreach ($data["image"] as $controllerImage) {
            $controllerImageType = $em->getRepository(ImageType::class)->find($controllerImage["imageType"]);

            $updateControllerImage = $em->getRepository(ControllerImage::class)->findOneBy(array(
                "controller" => $updateController
            ));
            if (isset($controllerImage["imageFile"]) && $controllerImage["imageFile"] != null) {
                define('UPLOAD_DIR','./uploads/images/images');

                $replace = explode(',', $controllerImage["imageFile"])[1];
                $replace = str_replace(' ', '+', $replace);
                $decode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $imageType = $split[1];

                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                file_put_contents($file, $decode);
                $trimPath = ltrim($file, '.');

                $updateControllerImage->setPath($baseUrl . $trimPath);
            }
            $updateControllerImage->setController($updateController);
            $updateControllerImage->setType($controllerImageType);
            $updateControllerImage->setPriority($controllerImage["priority"]);
            $updateControllerImage->setActive($controllerImage["active"]);
            $updateControllerImage->setName($controllerImage["name"]);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("controller/{id}", name="deleteController", methods={"DELETE"})
     */
    public function deleteController($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteController = $em->getRepository(\App\Entity\Controller::class)->find($id);

        $deleteController->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @Route("controller/{id}", name="getControllerById", methods={"GET"})
     * @return JsonResponse
     */
    public function getControllerById($id) {
        $em = $this->getDoctrine()->getManager();

        $controller = $em->getRepository(\App\Entity\Controller::class)->getAllControllerById($id);

        return new JsonResponse($controller[0]);
    }

    /**
     * @Route("controller", name="getAllControllerForAdmin", methods={"GET"})
     */
    public function getAllController() {
        $em = $this->getDoctrine()->getManager();

        $controller = $em->getRepository(\App\Entity\Controller::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($controller as $cont) {
            $tmp = array(
              "name" => $cont->getName(),
              "id" => $cont->getId()
            );

            $image = $cont->getAdminImage();
            foreach ($image as $contImage) {
                $tmp["image"][$contImage->getType()->getCode()] = array(
                  "path" => $contImage->getPath()
                );
                $dataArray[] = $tmp;
            }
        }
        return new JsonResponse($dataArray);
    }
}