<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 16:38
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\File;
use App\Entity\FileType;
use App\Entity\Language;
use App\Entity\PressRelease;
use App\Entity\PressReleaseText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\PanelServices\AdminHelperController;

class AdminPressReleaseController extends Controller
{
    /**
     * @param $request
     * @Route("pressrelease", name="createPressRelease", methods={"POST"})
     * @return JsonResponse
     */
    public function createPressRelease(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $baseUrl = "";
        $trimPath = "";
        if (!defined('UPLOAD_DIR')) {
            define('file_upload','./uploads/files/file');
        }
        foreach ($data as $pressRelease) {
            if (!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }
            if (isset($pressRelease["image"]) && !empty($pressRelease["image"])) {
                $replace = explode(',', $pressRelease["image"]);
                $replace = $replace[0];
                $replace = str_replace(' ', '+', $replace);
                $decode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $imageType = $split[1];

                $mimeFunction = $this->getMimeTypeForImage($imageType);
                if ($mimeFunction == 'application/octet-stream') {
                    return new JsonResponse("Wrong image format! Please check your image type !");
                }
                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                file_put_contents($file, $decode);
                $trimPath = ltrim($file, '.');
            }

            $addPressRelease = new PressRelease();
            $addPressRelease->setImage($baseUrl . $trimPath);
            $addPressRelease->setPriority($pressRelease["priority"]);
            $addPressRelease->setActive($pressRelease["active"]);

            $em->persist($addPressRelease);

            if (isset($pressRelease["text"]) && !empty($pressRelease["text"])) {
                foreach ($pressRelease["text"] as $pressReleaseText) {
                    $country = $em->getRepository(Country::class)->find($pressReleaseText["countryId"]);
                    $lang = $em->getRepository(Language::class)->find($pressReleaseText["langId"]);

                    $baseUrl = $this->container->getParameter('baseUrlHttp');



                    $replace = str_replace('data:application/pdf;base64,','', $pressReleaseText["fileBase64"]);
                    $replace = str_replace(' ', '+', $replace);
                    $fileDecode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $fileMime = $split[1];

                    $mimeFunction = $this->getMimeType($fileMime);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse("dosya tipi hatalı");
                    }
                    else if ($mimeFunction !== 'application/octet-stream') {
                        $split = explode( '/', $mimeType );
                        $fileExtension = $split[1];

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $fileTrimPath = ltrim($file, '.');

                        $generateSef = AdminHelperController::getInstance()->buildSef($pressReleaseText["title"]);

                        $addPressReleaseText = new PressReleaseText();
                        $addPressReleaseText->setLang($lang);
                        $addPressReleaseText->setCountry($country);
                        $addPressReleaseText->setPressRelease($addPressRelease);
                        $addPressReleaseText->setDescription($pressReleaseText["description"]);
                        $addPressReleaseText->setTitle($pressReleaseText["title"]);
                        $addPressReleaseText->setSef($generateSef);
                        $addPressReleaseText->setPdf($baseUrl . $fileTrimPath);

                        $em->persist($addPressReleaseText);
                        $em->flush();
                    }
                }
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("pressrelease/{id}", name="updatePressRelease", methods={"PUT"})
     */
    public function updatePressRelease($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $updatePressRelease = $em->getRepository(PressRelease::class)->find($id);
        $updatePressRelease->setPriority($data["priority"]);
        $updatePressRelease->setActive($data["active"]);

        if (isset($data["image"]) && !empty($data["image"])) {
            if (!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }

            $replace = explode(',', $data["image"]);
            $replace = $replace[0];
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);

            $split = explode( '/', $mimeType );
            $imageType = $split[1];
            $mimeFunction = $this->getMimeTypeForImage($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse("WRONG IMAGE");
            }
            $baseUrl = $this->container->getParameter('baseUrlHttp');

            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $updatePressRelease->setImage($baseUrl . $trimPath);
        } else {
            return new JsonResponse("Please add correct image ");
        }

        foreach ($data["text"] as $pressText) {
            $country = $em->getRepository(Country::class)->find($pressText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($pressText["langId"]);
            $generateSef = AdminHelperController::getInstance()->buildSef($pressText["title"]);

            if(isset($pressText["isDeleted"]) && $pressText["isDeleted"] == true) {
                $addPressReleaseText = $em->getRepository(PressReleaseText::class)->findOneBy(array(
                    "pressRelease" => $updatePressRelease,
                    "country" => $country,
                    "lang" => $lang
                ));
                $em->remove($addPressReleaseText);
                $em->flush();
            } else if (isset($pressText["isOld"]) && $pressText["isOld"] == true) {
                $addPressReleaseText = $em->getRepository(PressReleaseText::class)->findOneBy(array(
                    "pressRelease" => $updatePressRelease,
                    "country" => $country,
                    "lang" => $lang
                ));

                $addPressReleaseText->setLang($lang);
                $addPressReleaseText->setCountry($country);
                $addPressReleaseText->setPressRelease($updatePressRelease);
                $addPressReleaseText->setDescription($pressText["description"]);
                $addPressReleaseText->setTitle($pressText["title"]);
                $addPressReleaseText->setSef($generateSef);
                $em->flush();

            } else {
                $newPressText = new PressReleaseText();
                $newPressText->setLang($lang);
                $newPressText->setCountry($country);
                $newPressText->setPressRelease($updatePressRelease);
                $newPressText->setDescription($pressText["description"]);
                $newPressText->setTitle($pressText["title"]);
                $newPressText->setSef($generateSef);

                $baseUrl = $this->container->getParameter('baseUrlHttp');

                $replace = str_replace('data:file/base64,','', $pressText["fileBase64"]);
                $replace = str_replace(' ', '+', $replace);
                $fileDecode = base64_decode($replace);

                $f = finfo_open();
                $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                $split = explode( '/', $mimeType );
                $fileMime = $split[1];

                $mimeFunction = $this->getMimeType($fileMime);
                if ($mimeFunction == 'application/octet-stream') {
                    return new JsonResponse("dosya tipi hatalı");
                }
                else if ($mimeFunction !== 'application/octet-stream') {
                    $split = explode( '/', $mimeType );
                    $fileExtension = $split[1];

                    $file = file_upload . uniqid(). '.' . $fileExtension;
                    file_put_contents($file, $fileDecode);
                    $fileTrimPath = ltrim($file, '.');

                    $newPressText->setPdf($baseUrl . $fileTrimPath);
                }
                $em->persist($newPressText);
                $em->flush();
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("pressrelease/{id}", name="deletePressRelease", methods={"DELETE"})
     */
    public function deletePressRelease($id) {
        $em = $this->getDoctrine()->getManager();

        $deletePressRelease = $em->getRepository(PressRelease::class)->find($id);
        $deletePressRelease->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("pressrelease", name="getAllPressReleaseForAdmin", methods={"GET"})
     */
    public function getAllPressRelease() {
        $em = $this->getDoctrine()->getManager();

        $pressRelease = $em->getRepository(PressRelease::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($pressRelease as $pRelease) {
            $tmp = array(
              "id" => $pRelease->getId(),
              "path" => $pRelease->getImage()
            );

            $text = $pRelease->getText();
            foreach ($text as $pressText) {
                $tmp["text"][] = array(
                    "description" => $pressText->getDescription(),
                    "title" => $pressText->getTitle(),
                    "sef" => $pressText->getSef(),
                    "pdf" => $pressText->getPdf()
                );

                $tmp["country"] = array(
                    "id" => $pressText->getCountry()->getId(),
                    "name" => $pressText->getCountry()->getName()
                );

                $tmp["lang"] = array(
                    "id" => $pressText->getLang()->getId(),
                    "origName" => $pressText->getLang()->getOrigName()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("pressrelease/{id}", name="getPressReleaseById", methods={"GET"})
     * @return JsonResponse
     */
    public function getPressReleaseById($id) {
        $em = $this->getDoctrine()->getManager();

        $pressReleaseById = $em->getRepository(PressRelease::class)->getAllPressReleaseById($id);

        return new JsonResponse($pressReleaseById[0]);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getMimeTypeForImage($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}