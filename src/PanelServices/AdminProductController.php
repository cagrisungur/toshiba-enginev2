<?php

namespace App\PanelServices;

use App\Entity\Category;
use App\Entity\Connection;
use App\Entity\Country;
use App\Entity\Feature;
use App\Entity\FeatureType;
use App\Entity\File;
use App\Entity\FileType;
use App\Entity\HighlightedText;
use App\Entity\HighlightedTextTranslate;
use App\Entity\ImageType;
use App\Entity\Language;
use App\Entity\Product;
use App\Entity\ProductConnection;
use App\Entity\ProductCountry;
use App\Entity\ProductFeature;
use App\Entity\ProductFile;
use App\Entity\ProductImage;
use App\Entity\ProductSize;
use App\Entity\ProductSpec;
use App\Entity\ProductText;
use App\Entity\RelatedProduct;
use App\Entity\Series;
use App\Entity\Specification;
use App\Library\Panel\AdminProductSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminProductController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{

    /**
     * @Route("/product/{id}", name="getAllProductById", methods={"GET"})
     */
    public function getAllProductById($id, AdminProductSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Product $product
         */
        $product = $em->getRepository(Product::class)->findBy(
            array(
                "deletedAt"=>null,
                "id" => $id
            )
        );

        $json = $serializer->serialize(
            $product[0],
            "json"
        );

        return new JsonResponse($json);
    }

    /**
     * @return JsonResponse
     * @Route("/product", name="getAllProductForAdmin", methods={"GET"})
     */
    public function getAllProduct() {
        $em = $this->getDoctrine()->getManager();

        $allProduct = $em->getRepository(Product::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($allProduct as $p) {
            if ($p->getCategory()) {
                $categoryName= $p->getCategory()->getName();
            } else {
                $categoryName = "-";
            }
            $tmp = array(
              "id" => $p->getId(),
              "name" => $p->getName(),
              "code" => $p->getCode(),
              "category" => $categoryName
            );
            $country = $p->getCountry();

            foreach ($country as $productCountry) {
                $tmp["country"] = array(
                  "id" => $productCountry->getCountry()->getId(),
                  "name" => $productCountry->getCountry()->getName()
                );
            }
            $dataArray[] = $tmp;
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $request
     * @Route("product", name="createProduct", methods={"POST"})
     * @return JsonResponse
     */
    public function createProduct(Request $request) {

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $prd) {
            $product = new Product();

            $product->setName($prd["name"]);
            $product->setCode($prd["code"]);
            $product->setLink($prd["link"]);
            $product->setKeywords($prd["keywords"]);
            $product->setActive($prd["active"]);
            $product->setSmart($prd["smart"]);
            $product->setIsBest($prd["isBest"]);
            $product->setIsNew($prd["isNew"]);
            $product->setIsOffer($prd["isOffer"]);
            $product->setPriority($prd["priority"]);

            $series = $em->getRepository(Series::class)->find($prd["seriesId"]);
            $product->setSeries($series);

            $cat = $em->getRepository(Category::class)->find($prd["categoryId"]);

            $product->setCategory($cat);

            $em->persist($product);


            foreach ($prd["productCountry"] as $prodCountry) {
                $country = $em->getRepository(Country::class)->find($prodCountry["countryId"]);

                $productCountry = new ProductCountry();

                $productCountry->setCountry($country);
                $productCountry->setProduct($product);
                $productCountry->setActive($prodCountry["active"]);
                $em->persist($productCountry);
            }
            if (isset($prd["productText"]) && !empty($prd["productText"])) {
                foreach ($prd["productText"] as $prodText) {
                    $lang = $em->getRepository(Language::class)->find($prodText["langId"]);
                    $newCountry = $em->getRepository(Country::class)->find($prodText["countryId"]);
                    $generateLink = AdminHelperController::getInstance()->buildSef($prodText["link"]);

                    $productText = new ProductText();
                    $productText->setProduct($product);
                    $productText->setName($prodText["name"]);
                    $productText->setCountry($newCountry);
                    $productText->setLang($lang);
                    $productText->setSubtitle($prodText["subtitle"]);
                    $productText->setShortDescription($prodText["shortDescription"]);
                    $productText->setLongDescription($prodText["longDescription"]);
                    $productText->setSize($prodText["size"]);
                    $productText->setLink($generateLink);
                    $productText->setKeywords($prodText["keywords"]);

                    $em->persist($productText);
                }
            }
            if (isset($prd["productSpec"]) && !empty($prd["productSpec"])) {
                foreach ($prd["productSpec"] as $prodSpec) {
                    $spec = $em->getRepository(Specification::class)->find($prodSpec["specId"]);
                    $specLang = $em->getRepository(Language::class)->find($prodSpec["langId"]);

                    $productSpec = new ProductSpec();
                    $productSpec->setProduct($product);
                    $productSpec->setLang($specLang);
                    $productSpec->setSpec($spec);
                    $productSpec->setValue($prodSpec["value"]);
                    $productSpec->setPriority($prodSpec["priority"]);

                    $em->persist($productSpec);
                }
            }
            if (isset($prd["productFeature"]) && !empty($prd["productFeature"])) {
                foreach ($prd["productFeature"] as $prodFeature) {
                    $findFeature = $em->getRepository(Feature::class)->find($prodFeature["featureId"]);
                    $findFeatureType = $em->getRepository(FeatureType::class)->find($prodFeature["featureTypeId"]);

                    $productFeature = new ProductFeature();
                    $productFeature->setProduct($product);
                    $productFeature->setFeature($findFeature);
                    $productFeature->setType($findFeatureType);
                    $productFeature->setPriority($prodFeature["priority"]);

                    $em->persist($productFeature);
                }
            }

            if (isset($prd["highLightedText"]) && !empty($prd["highLightedText"])) {
                foreach ($prd["highLightedText"] as $highText) {
                    $highLightedText = new HighlightedText();
                    $highLightedText->setProduct($product);
                    $highLightedText->setPriority($highText["priority"]);

                    $em->persist($highLightedText);
                    $em->flush();

                    foreach ($highText["translate"] as $translate) {
                        $textCountry = $em->getRepository(Country::class)->find($translate["countryId"]);
                        $lang = $em->getRepository(Language::class)->find($translate["langId"]);

                        $textTranslate = new HighlightedTextTranslate();
                        $textTranslate->setLang($lang);
                        $textTranslate->setCountry($textCountry);
                        $textTranslate->setHighlightedText($highLightedText);
                        $textTranslate->setText($translate["text"]);

                        $em->persist($textTranslate);
                        $em->flush();
                    }
                }
            }
            if (isset($prd["images"]) && !empty($prd["images"])) {
                foreach ($prd["images"] as $prodImages) {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $prodImages["imageCode"]));

                    if (!defined('UPLOAD_DIR')) {
                        define('UPLOAD_DIR','./uploads/images/images');
                    }

                    $replace = explode(',', $prodImages["prodImage"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];

                    $mimeFunction = $this->getMimeType($imageType);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse(array(
                            "message" => "IMAGE FAILED"
                        ));
                    }
                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $productImages = new ProductImage();
                    $productImages->setProduct($product);
                    $productImages->setName($prodImages["name"]);
                    $productImages->setActive($prodImages["active"]);
                    $productImages->setPath($baseUrl . $trimPath);
                    $productImages->setType($imageTypeCode);
                    $productImages->setPriority($prodImages["priority"]);

                    $em->persist($productImages);
                    $em->flush();
                }
            }

            if (isset($prd["files"]) && !empty($prd["files"])) {
                foreach ($prd["files"] as $prodFile) {
                    $findProductFile = $em->getRepository(FileType::class)->findOneBy(array("code" =>$prodFile["fileCode"]));

                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    define('file_upload','./uploads/files/file');
                    $replace = explode(',', $prodFile["fileBase64"])[1];
//                $replace = str_replace('data:file/base64,','', $prodFile["fileBase64"]);
                    $replace = str_replace(' ', '+', $replace);
                    $fileDecode = base64_decode($replace);
                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $fileMime = $split[1];

                    $mimeFunction = $this->getMimeType($fileMime);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse(array(
                            "message" => "FILE FAILED"
                        ));
                    }
                    if ($mimeFunction !== 'application/octet-stream') {
                        $split = explode( '/', $mimeType );
                        $fileExtension = $split[1];

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');

                    } else if ($mimeType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        $fileExtension = "xlsx";

                        $file = file_upload . uniqid(). '.' . $fileExtension;
                        file_put_contents($file, $fileDecode);
                        $trimPath = ltrim($file, '.');
                    }

                    $addFile = new File();
                    $addFile->setName($prodFile["fileName"]);
                    $addFile->setType($findProductFile);
                    $addFile->setPath($baseUrl . $trimPath);

                    $em->persist($addFile);

                    $pFile = new ProductFile();
                    $pFile->setProduct($product);
                    $pFile->setFile($addFile);
                    $pFile->setPriority($prodFile["priority"]);

                    $em->persist($pFile);
                    $em->flush();
                }
            }

            if (isset($prd["size"]) && !empty($prd["size"])) {
                $productSize = new ProductSize();
                $productSize->setProduct($product);
                $productSize->setName($prd["size"]);
                $em->persist($productSize);
            }

            if (isset($prd["relatedProduct"]) && !empty($prd["relatedProduct"])) {
                foreach ($prd["relatedProduct"] as $relatedProduct) {
                    $relatedOneProduct = $em->getRepository(Product::class)->find($relatedProduct["relatedId"]);

                    $addRelatedProduct = new RelatedProduct();
                    $addRelatedProduct->setPriority($relatedProduct["priority"]);
                    $addRelatedProduct->setHost($product);
                    $addRelatedProduct->setRelated($relatedOneProduct);

                    $em->persist($addRelatedProduct);
                }
            }

            if (isset($prd["productConnection"]) && !empty($prd["productConnection"])) {
                foreach ($prd["productConnection"] as $prodConnection) {
                    $findConnection = $em->getRepository(Connection::class)->find($prodConnection["connectionId"]);

                    $addProductConnection = new ProductConnection();
                    $addProductConnection->setProduct($product);
                    $addProductConnection->setConnection($findConnection);

                    $em->persist($addProductConnection);
                }
            }

            $em->persist($product);
            $em->flush();
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @Route("product/{id}", name="updateProduct", methods={"PUT"})
     * @return JsonResponse
     */
    public function updateProduct($id, Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($id);

        $product->setName($data["name"]);
        $product->setCode($data["code"]);
        $product->setLink($data["link"]);
        $product->setKeywords($data["keywords"]);
        $product->setActive($data["active"]);
        $product->setSmart($data["smart"]);
        $product->setIsBest($data["isBest"]);
        $product->setIsNew($data["isNew"]);
        $product->setIsOffer($data["isOffer"]);
        $product->setPriority($data["priority"]);

        $series = $em->getRepository(Series::class)->find($data["seriesId"]);
        $product->setSeries($series);

        $cat = $em->getRepository(Category::class)->find($data["categoryId"]);

        $product->setCategory($cat);

        $removeProductCountry = $em->getRepository(ProductCountry::class)->findBy(array(
            "deletedAt" => null,
            "product" => $product
        ));

        foreach ($removeProductCountry as $rem) {
            $rem->setActive(0);
            $em->flush();
        }
        foreach ($data["productCountry"] as $prodCountry) {
            $isPersist = false;
            $country = $em->getRepository(Country::class)->find($prodCountry["countryId"]);

            $productCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
               "country" => $country,
               "product" => $product
            ));
            if(!$productCountry) {
                $productCountry = new ProductCountry();
                $isPersist = true;
            }

            $productCountry->setCountry($country);
            $productCountry->setProduct($product);
            $productCountry->setActive($prodCountry["active"]);
            if($isPersist) {
                $em->persist($productCountry);
            }
            $em->flush();

        }

        if (isset($data["productText"]) && !empty($data["productText"])) {
            foreach ($data["productText"] as $prodText) {
                $isPersist = false;
                $lang = $em->getRepository(Language::class)->find($prodText["langId"]);
                $newCountry = $em->getRepository(Country::class)->find($prodText["countryId"]);

                $generateLink = AdminHelperController::getInstance()->buildSef($prodText["link"]);

                $productText = $em->getRepository(ProductText::class)->findOneBy(array(
                    "product" => $product,
                    "country" => $newCountry,
                    "lang" => $lang
                ));
                if (!$productText) {
                    $productText = new ProductText();
                    $isPersist = true;
                }
                $productText->setProduct($product);
                $productText->setCountry($newCountry);
                $productText->setName($prodText["name"]);
                $productText->setLang($lang);
                $productText->setSubtitle($prodText["subtitle"]);
                $productText->setShortDescription($prodText["shortDescription"]);
                $productText->setLongDescription($prodText["longDescription"]);
                $productText->setLink($generateLink);
                $productText->setSize($prodText["size"]);
                $productText->setKeywords($prodText["keywords"]);
                if ($isPersist) {
                    $em->persist($productText);
                }
                $em->flush();
            }
        }

//        if (isset($data["productSpec"]) && !empty($data["productSpec"])) {
//            $productSpec = $em->getRepository(ProductSpec::class)->findBy(array(
//                "product" => $product,
//            ));
//
//            foreach ($productSpec as $specP) {
//                $em->remove($specP);
//                $em->flush();
//            }
//            foreach ($data["productSpec"] as $prodSpec) {
//
//                $spec = $em->getRepository(Specification::class)->find($prodSpec["specId"]);
//                $specLang = $em->getRepository(Language::class)->find($prodSpec["langId"]);
//
//                $pSpec = new ProductSpec();
//                $pSpec->setProduct($product);
//                $pSpec->setLang($specLang);
//                $pSpec->setSpec($spec);
//                $pSpec->setValue($prodSpec["value"]);
//                $pSpec->setPriority($prodSpec["priority"]);
//
//                $em->persist($pSpec);
//                $em->flush();
//            }
//        }

        if (isset($data["productFeature"]) && !empty($data["productFeature"])) {
            $removeFeatures = $em->getRepository(ProductFeature::class)->findBy(array(
                "deletedAt" => null,
                "product" => $product
            ));

            foreach ($removeFeatures as $removeFeature) {
                $em->remove($removeFeature);
                $em->flush();
            }
            foreach ($data["productFeature"] as $prodFeature) {
                $findFeature = $em->getRepository(Feature::class)->find($prodFeature["featureId"]);
                $findFeatureType = $em->getRepository(FeatureType::class)->find($prodFeature["featureTypeId"]);

//                $pFeature = $em->getRepository(ProductFeature::class)->findOneBy(array(
//                    "product" => $product,
//                    "feature" => $findFeature
//                ));
//                if($pFeature) {
//                    $em->remove($pFeature);
//                    $em->flush();
//                }

                $productFeature = new ProductFeature();
                $productFeature->setProduct($product);
                $productFeature->setFeature($findFeature);
                $productFeature->setType($findFeatureType);
                $productFeature->setPriority($prodFeature["priority"]);

                $em->persist($productFeature);
                $em->flush();
            }
        }

        if (isset($data["highLightedText"]) && count($data["highLightedText"]) > 0) {
            $deleteHighLight = $em->getRepository(HighlightedText::class)->findBy(array(
                "product" => $product
            ));

            foreach($deleteHighLight as $item) {
                $em->remove($item);
                $em->flush();
            }

            foreach ($data["highLightedText"] as $highText) {
                $highLightedText = new HighlightedText();

                $highLightedText->setProduct($product);
                $highLightedText->setPriority($highText["priority"]);

                $em->persist($highLightedText);
                $em->flush();

                foreach ($highText["translate"] as $translate) {
                    if(isset($translate["text"]) && $translate["text"] != '') {
                        $textCountry = $em->getRepository(Country::class)->find($translate["countryId"]);
                        $lang = $em->getRepository(Language::class)->find($translate["langId"]);

                        $textTranslate = new HighlightedTextTranslate();
                        $textTranslate->setLang($lang);
                        $textTranslate->setCountry($textCountry);
                        $textTranslate->setHighlightedText($highLightedText);
                        $textTranslate->setText($translate["text"]);

                        $em->persist($textTranslate);
                        $em->flush();
                    }
                }
            }
        }

        if (isset($data["images"])) {
            foreach ($data["images"] as $prodImages) {
                if(isset($prodImages["isDeleted"]) && $prodImages["isDeleted"] == true) {
                    $pImage = $em->getRepository(ProductImage::class)->find($prodImages["id"]);
                    $em->remove($pImage);
                    $em->flush();
                } else if (isset($prodImages["isOld"]) && $prodImages["isOld"] == true) {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $prodImages["imageCode"]));

                    $pImage = $em->getRepository(ProductImage::class)->findOneBy(array(
                        "id" => $prodImages["id"],
                        "product" => $product
                    ));

                    $pImage->setProduct($product);
                    $pImage->setName($prodImages["name"]);
                    $pImage->setActive($prodImages["active"]);
                    $pImage->setType($imageTypeCode);
                    $pImage->setPriority($prodImages["priority"]);
                    $em->flush();
                } else {
                    $imageTypeCode = $em->getRepository(ImageType::class)->findOneBy(array("code" => $prodImages["imageCode"]));
                    $newPimage = new ProductImage();
                    $newPimage->setProduct($product);
                    $newPimage->setName($prodImages["name"]);
                    $newPimage->setActive($prodImages["active"]);
                    $newPimage->setType($imageTypeCode);
                    $newPimage->setPriority($prodImages["priority"]);

                    if (isset($prodImages["imageFile"]) && $prodImages["imageFile"] != null) {
                        if(!defined('UPLOAD_DIR')) {
                            define('UPLOAD_DIR','./uploads/images/images');
                        }

                        $replace = explode(',', $prodImages["imageFile"])[1];
                        $replace = str_replace(' ', '+', $replace);
                        $decode = base64_decode($replace);

                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $imageType = $split[1];

                        $baseUrl = $this->container->getParameter('baseUrlHttp');

                        $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                        file_put_contents($file, $decode);
                        $trimPath = ltrim($file, '.');

                        $newPimage->setPath($baseUrl . $trimPath);
                    }
                    $em->persist($newPimage);
                    $em->flush();
                }
            }
        }
        if (isset($data["files"])) {
            foreach ($data["files"] as $prodFile) {
                if(isset($prodFile["isDeleted"]) && $prodFile["isDeleted"] == true) {
                    $findFile = $em->getRepository(File::class)->find($prodFile["fileId"]);
                    $findProductFile = $em->getRepository(ProductFile::class)->findOneBy(array(
                        "product" => $product,
                        "file" => $findFile
                    ));

                    $em->remove($findProductFile);
                    $em->flush();
                } else if(isset($prodFile["isOld"]) && $prodFile["isOld"] == true) {

                    $findFile = $em->getRepository(File::class)->find($prodFile["fileId"]);

                    $findProductFile = $em->getRepository(ProductFile::class)->findOneBy(array(
                        "product" => $product,
                        "file" => $findFile
                    ));
                    if ($findProductFile) {
                        $findProductFile->setPriority($prodFile["priority"]);
                    }
                } else {
                    $findProductFileType = $em->getRepository(FileType::class)->findOneBy(array("code" => $prodFile["fileCode"]));
                    $newFile = new File();
                    $newFile->setName($prodFile["fileName"]);
                    $newFile->setType($findProductFileType);
                    if (isset($prodFile["fileBase64"]) && $prodFile["fileBase64"] != null) {
                        $baseUrl = $this->container->getParameter('baseUrlHttp');

                        define('file_upload','./uploads/files/file');

                        // $replace = str_replace('data:file/base64,','', $prodFile["fileBase64"]);
                        
                        $replace = explode(',', $prodFile["fileBase64"])[1];
                        $replace = str_replace(' ', '+', $replace);
                        $fileDecode = base64_decode($replace);

                        $f = finfo_open();
                        $mimeType = finfo_buffer($f, $fileDecode, FILEINFO_MIME_TYPE);
                        $split = explode( '/', $mimeType );
                        $fileMime = $split[1];
                        
                        $mimeFunction = $this->getMimeType($fileMime);
                        if ($mimeFunction == 'application/octet-stream') {
                            return new JsonResponse(array(
                                "message" => "FAILED"
                            ));
                        }
                        if ($mimeFunction !== 'application/octet-stream') {
                            $split = explode( '/', $mimeType );
                            $fileExtension = $split[1];

                            $file = file_upload . uniqid(). '.' . $fileExtension;
                            file_put_contents($file, $fileDecode);
                            $trimPath = ltrim($file, '.');

                        } else if ($mimeType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            $fileExtension = "xlsx";

                            $file = file_upload . uniqid(). '.' . $fileExtension;
                            file_put_contents($file, $fileDecode);
                            $trimPath = ltrim($file, '.');
                        }
                        $newFile->setPath($baseUrl . $trimPath);
                    }
                    $em->persist($newFile);
                    $em->flush();
                    $newProductFile = new ProductFile();
                    $newProductFile->setProduct($product);
                    $newProductFile->setFile($newFile);
                    $newProductFile->setPriority($prodFile["priority"]);
                    $em->persist($newProductFile);
                    $em->flush();
                }
            }
        }
        if (isset($data["relatedProduct"]) && !empty($data["relatedProduct"])) {
            foreach ($data["relatedProduct"] as $relatedProduct) {
                $relatedOneProduct = $em->getRepository(Product::class)->find($relatedProduct["relatedId"]);

                $addRelatedProduct = $em->getRepository(RelatedProduct::class)->findOneBy(array(
                    "host" => $product,
                    "related" => $relatedOneProduct
                ));
                if (!$addRelatedProduct){
                    $newRelated = new RelatedProduct();
                    $newRelated->setHost($product);
                    $newRelated->setRelated($relatedOneProduct);
                    $newRelated->setPriority($relatedProduct["priority"]);

                    $em->persist($newRelated);
                } else {
                    $addRelatedProduct->setPriority($relatedProduct["priority"]);
                }
            }
        }

        if (isset($data["productConnection"]) && !empty($data["productConnection"])) {
            $deleteConnection = $em->getRepository(ProductConnection::class)->findOneBy(array(
                "product" => $product
            ));

            foreach ($deleteConnection as $conn) {
                $em->remove($conn);
                $em->flush();
            }
            foreach ($data["productConnection"] as $prodConnection) {
                $findConnection = $em->getRepository(Connection::class)->find($prodConnection["connectionId"]);

                $prodConn = new ProductConnection();
                $prodConn->setProduct($product);
                $prodConn->setConnection($findConnection);

                $em->persist($prodConn);
                $em->flush();
            }
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("product/{id}", name="deleteProduct", methods={"DELETE"})
     */
    public function deleteProduct($id) {
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($id);

        $product->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $productId
     * @param $langId
     * @return JsonResponse
     * @Route("product-spec/{productId}/{langId}", name="productSpecList", methods={"GET"})
     */
    public function productSpecList($productId, $langId) {
        $em = $this->getDoctrine()->getManager();

        $findProduct = $em->getRepository(Product::class)->find($productId);

        $lang = $em->getRepository(Language::class)->find($langId);

        $productSpecs = $em->getRepository(ProductSpec::class)->findBy(array(
            "product" => $findProduct,
            "lang" => $lang,
            "deletedAt" => null
        ));
        $array = array();
        foreach ($productSpecs as $productSpec) {
            $array[] = array(
                "specName" => $productSpec->getSpec()->getName(),
                "langId" => $productSpec->getLang()->getId(),
                "specId" => $productSpec->getSpec()->getId(),
                "value" => $productSpec->getValue()
            );
        }

        return new JsonResponse($array);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'pdf' => 'application/pdf',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}