<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 4.12.2018
 * Time: 14:03
 */

namespace App\PanelServices;


use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminLanguageController extends Controller
{
    /**
     * @Route("language", name="getAllLanguageForAdmin", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllLanguage() {
        $em = $this->getDoctrine()->getManager();

        $languages = $em->getRepository(Language::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($languages as $language) {
            $dataArray[] = array(
                "id" => $language->getId(),
                "name" => $language->getName(),
                "origName" => $language->getOrigName(),
                "code" => $language->getCode()
            );
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("language/{id}", name="getLanguageById", methods={"GET"})
     */
    public function getLanguageById($id) {
        $em = $this->getDoctrine()->getManager();

        $languages = $em->getRepository(Language::class)->getLanguageById($id);

        return new JsonResponse($languages);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("language/{id}", name="updateLanguage", methods={"PUT"})
     */
    public function updateLanguage($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $language = $em->getRepository(Language::class)->find($id);
        $language->setName($data["name"]);
        $language->setOrigName($data["origName"]);
        $language->setCode($data["code"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("language", name="createLanguage", methods={"POST"})
     */
    public function createLanguage() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $language) {
            $lang = new Language();
            $lang->setName($language["name"]);
            $lang->setCode($language["code"]);
            $lang->setOrigName($language["origName"]);

            $em->persist($lang);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("language/{id}", name="deleteLanguage", methods={"DELETE"})
     */
    public function deleteLanguage($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteLang = $em->getRepository(Language::class)->find($id);
        $deleteLang->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => 200,
            "message" => "success"
        ));

    }
}