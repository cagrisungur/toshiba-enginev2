<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 11:09
 */

namespace App\PanelServices;


class AdminHelperController
{
    /**
     * @var AdminHelperController
     */
    private static $instance;

    public function __construct()
    {

    }

    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function buildSef($sef) {

        $cleanAscii = iconv('UTF-8','ASCII//TRANSLIT', $sef);
        $lowerCase = strtolower($cleanAscii);

        $trChars = array("ı", "ğ", "ü", "ş", "ö", "ç");
        $enChars = array("i", "g", "u", "s", "o", "c");

        $convertedSef = str_replace($trChars, $enChars, $lowerCase);
        $convertedSef = preg_replace('/\s+/', '-', $convertedSef);

        return $convertedSef;
    }
}