<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 13:31
 */

namespace App\PanelServices;


use App\Entity\Series;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSeriesController extends Controller
{
    /**
     * @Route("series", name="createSeries", methods={"POST"})
     */
    public function createSeries() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $series) {
            $addSeries = new Series();
            $addSeries->setCode($series["code"]);
            $addSeries->setActive($series["active"]);
            $addSeries->setName($series["name"]);

            $em->persist($addSeries);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("series/{id}", name="updateSeries", methods={"PUT"})
     */
    public function updateSeries($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $updateSeries = $em->getRepository(Series::class)->find($id);
        $updateSeries->setCode($data["code"]);
        $updateSeries->setActive($data["active"]);
        $updateSeries->setName($data["name"]);

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("series/{id}", name="deleteSeries", methods={"DELETE"})
     */
    public function deleteSeries($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSeries = $em->getRepository(Series::class)->find($id);
        $deleteSeries->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("series", name="getAllSeriesForAdmin", methods={"GET"})
     */
    public function getAllSeries() {
        $em = $this->getDoctrine()->getManager();

        $series = $em->getRepository(Series::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($series as $serie) {
            $dataArray[] = array(
              "id" => $serie->getId(),
              "name" => $serie->getName(),
              "code" => $serie->getCode()
            );
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("series/{id}", name="getSeriesById", methods={"GET"})
     */
    public function getSeriesById($id) {
        $em = $this->getDoctrine()->getManager();

        $series = $em->getRepository(Series::class)->getAllSeriesById($id);

        return new JsonResponse($series[0]);
    }
}