<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 14:07
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\CustomPage;
use App\Entity\CustomPageText;
use App\Entity\Language;
use App\Entity\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminCustomPageController extends Controller
{
    /**
     * @param $request
     * @Route("custompage", name="createCustomPage", methods={"POST"})
     * @return JsonResponse
     */
    public function createCustomPage(Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $customPage) {
            $pageType = $em->getRepository(PageType::class)->find($customPage["typeId"]);

            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }
            $replace = explode(',', $customPage["image"]);
            if (count($replace) > 1) {
                $replace = $replace[1];
            }else {
                $replace = $replace[0];
            }
            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }

            $parsedList = explode(',', $customPage["thumbImage"]);

            if(isset($parsedList[1])) {
                $replaceThumbnail = $parsedList[1];
            }else {
                $replaceThumbnail = $parsedList[0];
            }
            
            $replaceThumbnail = str_replace(' ','+', $replaceThumbnail);
            $decodeThumb = base64_decode($replaceThumbnail);

            $thumbMimeType = finfo_buffer($f, $decodeThumb, FILEINFO_MIME_TYPE);
            $thumbSplit = explode('/', $thumbMimeType);
            $thumbImageType = $thumbSplit[1];

            $mimeFunction = $this->getMimeType($thumbImageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $baseUrl = $this->container->getParameter('baseUrlHttp');

            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $thumbFile = UPLOAD_DIR . uniqid(). '.' . $thumbImageType;
            file_put_contents($thumbFile, $decodeThumb);
            $trimThumbPath = ltrim($thumbFile, '.');

            $addCustomPage = new CustomPage();
            $addCustomPage->setType($pageType);
            $addCustomPage->setPath($baseUrl . $trimPath);
            $addCustomPage->setThumbNailPath($baseUrl . $trimThumbPath);

            $em->persist($addCustomPage);
            $em->flush();
            foreach ($customPage["customPageText"] as $customPageText) {
                $country = $em->getRepository(Country::class)->find($customPageText["countryId"]);
                $lang = $em->getRepository(Language::class)->find($customPageText["langId"]);

                $addCustomPageText = new CustomPageText();
                $addCustomPageText->setCountry($country);
                $addCustomPageText->setLang($lang);
                $addCustomPageText->setPage($addCustomPage);
                $addCustomPageText->setData($customPageText["dataText"]);

                $em->persist($addCustomPageText);
                $em->flush();
            }
        }


        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param $request
     * @return JsonResponse
     * @Route("custompage/{id}", name="updateCustomPage", methods={"PUT"})
     */
    public function updateCustomPage($id, Request $request) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $baseUrl = $this->container->getParameter('baseUrlHttp');

        $pageType = $em->getRepository(PageType::class)->find($data["typeId"]);

        $updateCustomPage = $em->getRepository(CustomPage::class)->find($id);
        $updateCustomPage->setType($pageType);

        foreach ($data["customPageText"] as $customPageText) {
            $isPersist = false;
            $country = $em->getRepository(Country::class)->find($customPageText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($customPageText["langId"]);

            $addCustomPageText = $em->getRepository(CustomPageText::class)->findOneBy(array(
               "page" => $updateCustomPage,
               "country" => $country,
               "lang" => $lang
            ));
            if (!$addCustomPageText) {
                $addCustomPageText = new CustomPageText();
                $isPersist = true;
            }
            $addCustomPageText->setCountry($country);
            $addCustomPageText->setLang($lang);
            $addCustomPageText->setPage($updateCustomPage);
            $addCustomPageText->setData($customPageText["dataText"]);
            if ($isPersist) {
                $em->persist($addCustomPageText);
            }
            $em->flush();
        }
        if (isset($data["thumbImage"]) && $data["thumbImage"] != null) {
            $f = finfo_open();

            if(!defined('UPLOAD_DIR')) {
                define('UPLOAD_DIR','./uploads/images/images');
            }

            $parsedList = explode(',', $data["thumbImage"]);

            if(isset($parsedList[1])) {
                $replaceThumbnail = $parsedList[1];
            }else {
                $replaceThumbnail = $parsedList[0];
            }

            // $replaceThumbnail = explode(',', $data["thumbImage"])[1];
            $replaceThumbnail = str_replace(' ','+', $replaceThumbnail);
            $decodeThumb = base64_decode($replaceThumbnail);

            $thumbMimeType = finfo_buffer($f, $decodeThumb, FILEINFO_MIME_TYPE);
            $thumbSplit = explode('/', $thumbMimeType);
            $thumbImageType = $thumbSplit[1];

            $mimeFunction = $this->getMimeType($thumbImageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $thumbFile = UPLOAD_DIR . uniqid(). '.' . $thumbImageType;
            file_put_contents($thumbFile, $decodeThumb);
            $trimThumbPath = ltrim($thumbFile, '.');

            $updateCustomPage->setThumbNailPath($baseUrl . $trimThumbPath);

        }
        if (isset($data["image"]) && $data["image"] != null) {

            if (!defined('UPLOAD_DIR')) {
              define('UPLOAD_DIR','./uploads/images/images');
            }
            $parsedList = explode(',', $data["image"]);

            if(isset($parsedList[1])) {
                $replace = $parsedList[1];
            }else {
                $replace = $parsedList[0];
            }

            $replace = str_replace(' ', '+', $replace);
            $decode = base64_decode($replace);

            $f = finfo_open();
            $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
            $split = explode( '/', $mimeType );
            $imageType = $split[1];

            $mimeFunction = $this->getMimeType($imageType);
            if ($mimeFunction == 'application/octet-stream') {
                return new JsonResponse(array(
                    "message" => "FAILED"
                ));
            }
            $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
            file_put_contents($file, $decode);
            $trimPath = ltrim($file, '.');

            $updateCustomPage->setPath($baseUrl . $trimPath);
        }

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("custompage/{id}", name="deleteCustomPage", methods={"DELETE"})
     */
    public function deleteCustomPage($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteCustomPage = $em->getRepository(CustomPage::class)->find($id);

        $deleteCustomPage->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("custompage", name="getAllCustomPageForAdmin", methods={"GET"})
     */
    public function getAllCustomPage() {
        $em = $this->getDoctrine()->getManager();

        $customPage = $em->getRepository(CustomPage::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($customPage as $page) {
            $tmp = array(
              "id" => $page->getId(),
              "typeId" => $page->getType()->getId(),
              "path" => $page->getPath(),
              "thumbNailPath" => $page->getThumbNailPath(),
              "pageType" => $page->getType()->getName()
            );

            $customPageText = $page->getPageText();
            foreach ($customPageText as $pageText) {
                $tmp["text"][] = array(
                    "data" => $pageText->getData()
                );

                $tmp["country"] = array(
                    "id" => $pageText->getCountry()->getId(),
                    "name" => $pageText->getCountry()->getName()
                );

                $tmp["lang"] = array(
                    "id" => $pageText->getLang()->getId(),
                    "origName" => $pageText->getLang()->getOrigName()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @Route("custompage/{id}", name="getCustomPageById", methods={"GET"})
     * @return JsonResponse
     */
    public function getCustomPageById($id) {
        $em = $this->getDoctrine()->getManager();

        $customPage = $em->getRepository(CustomPage::class)->getAllCustomPageById($id);

        return new JsonResponse($customPage[0]);
    }

    public function getMimeType($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}