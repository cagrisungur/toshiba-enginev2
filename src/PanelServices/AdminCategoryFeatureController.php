<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 12:16
 */

namespace App\PanelServices;


use App\Entity\Category;
use App\Entity\CategoryFeature;
use App\Entity\Feature;
use App\Entity\FeatureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminCategoryFeatureController extends Controller
{
    /**
     * @Route("categoryfeature", name="createCategoryFeature", methods={"POST"})
     */
    public function createCategoryFeature() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $catFeature) {
            $category = $em->getRepository(Category::class)->find($catFeature["categoryId"]);
            $feature = $em->getRepository(Feature::class)->find($catFeature["featureId"]);
            $featureType = $em->getRepository(FeatureType::class)->find($catFeature["featureTypeId"]);

            $addCategoryFeature = new CategoryFeature();
            $addCategoryFeature->setCategory($category);
            $addCategoryFeature->setFeature($feature);
            $addCategoryFeature->setType($featureType);
            $addCategoryFeature->setPriority($catFeature["priority"]);

            $em->persist($addCategoryFeature);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("categoryfeature", name="getAllCategoryFeatureForAdmin", methods={"GET"})
     */
    public function getAllCategoryFeature() {
        $em = $this->getDoctrine()->getManager();

        $categoryFeature = $em->getRepository(CategoryFeature::class)->getAllCategoryFeature();

        return new JsonResponse($categoryFeature);
    }
}