<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 15.03.2019
 * Time: 11:00
 */

namespace App\PanelServices;


use App\Entity\ProductRegister;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminProductRegisterController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("productregister", name="getProductRegisterAdmin")
     */
    public function getProductRegisterAdmin() {
        $em = $this->getDoctrine()->getManager();

        $productRegister = $em->getRepository(ProductRegister::class)->findAll();
        $productArray = array();
        foreach ($productRegister as $register) {
            $productArray[] = array(
                "id" => $register->getId(),
                "product_id" => $register->getProduct()->getId(),
                "serialNumber" => $register->getSerialNumber(),
                "dateOfPurchase" => $register->getDateOfPurchase(),
                "retailerCountry" => $register->getRetailerCountry()->getName(),
                "retailerName" => $register->getRetailerName(),
                "retailerAddress" => $register->getRetailerAddress(),
                "retailerAddress2" => $register->getRetailerAddress2(),
                "retailerCity" => $register->getRetailerCity(),
                "retailerPostCode" => $register->getRetailerPostCode(),
                "title" => $register->getTitle(),
                "firstName" => $register->getFirstName(),
                "lastName" => $register->getLastName(),
                "country" => $register->getCountry()->getName(),
                "address" => $register->getAddress(),
                "address2" => $register->getAddress2(),
                "city" => $register->getCity(),
                "postCode" => $register->getPostCode(),
                "phone" => $register->getPhone(),
                "email" => $register->getEmail(),
                "contact" => $register->getContact(),
                "share" => $register->getShare()
            );
        }

        return new JsonResponse($productArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("productregister/{id}", name="getProductRegisterByIdAdmin", methods={"GET"})
     */
    public function getProductRegisterByIdAdmin($id) {
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(ProductRegister::class)->find($id);

        $productArray[] = array(
            "product_id" => $product->getProduct()->getId(),
            "serialNumber" => $product->getSerialNumber(),
            "dateOfPurchase" => $product->getDateOfPurchase(),
            "retailerCountry" => $product->getRetailerCountry()->getName(),
            "retailerName" => $product->getRetailerName(),
            "retailerAddress" => $product->getRetailerAddress(),
            "retailerAddress2" => $product->getRetailerAddress2(),
            "retailerCity" => $product->getRetailerCity(),
            "retailerPostCode" => $product->getRetailerPostCode(),
            "title" => $product->getTitle(),
            "firstName" => $product->getFirstName(),
            "lastName" => $product->getLastName(),
            "country" => $product->getCountry()->getName(),
            "address" => $product->getAddress(),
            "address2" => $product->getAddress2(),
            "city" => $product->getCity(),
            "postCode" => $product->getPostCode(),
            "phone" => $product->getPhone(),
            "email" => $product->getEmail(),
            "contact" => $product->getContact(),
            "share" => $product->getShare()
        );

        return new JsonResponse($productArray);
    }
}