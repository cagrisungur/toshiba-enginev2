<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.11.2018
 * Time: 10:05
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\Language;
use App\Entity\SpecificationCategory;
use App\Entity\SpecificationCategoryText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSpecificationCategoryController extends Controller
{
    /**
     * @Route("/specificationcategory", name="createSpecCategory", methods={"POST"})
     */
    public function createSpecCategory() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $specCat) {
            $specCategory = new SpecificationCategory();
            $specCategory->setName($specCat["specCatName"]);
            $specCategory->setPriority($specCat["priority"]);

            $em->persist($specCategory);
            $em->flush();

            if (isset($specCat["specCatText"]) && !empty($specCat["specCatText"])) {
                foreach ($specCat["specCatText"] as $specCatText) {
                    $country = $em->getRepository(Country::class)->find($specCatText["countryId"]);
                    $lang = $em->getRepository(Language::class)->find($specCatText["langId"]);

                    $addSpecCatText = new SpecificationCategoryText();
                    $addSpecCatText->setCountry($country);
                    $addSpecCatText->setLang($lang);
                    $addSpecCatText->setSpecCategory($specCategory);
                    $addSpecCatText->setText($specCatText["text"]);

                    $em->persist($addSpecCatText);
                    $em->flush();
                }

            }
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specificationcategory/{id}", name="updateSpecCategory", methods={"PUT"})
     */
    public function updateSpecCategory($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $specCategory = $em->getRepository(SpecificationCategory::class)->find($id);
        $specCategory->setName($data["specCatName"]);
        $specCategory->setPriority($data["priority"]);

        if(isset($data["specCatText"]) && !empty($data["specCatText"])) {
            foreach ($data["specCatText"] as $specCatText) {
                $isPersist = false;
                $country = $em->getRepository(Country::class)->find($specCatText["countryId"]);
                $lang = $em->getRepository(Language::class)->find($specCatText["langId"]);

                $addSpecCatText = $em->getRepository(SpecificationCategoryText::class)->findOneBy(array(
                    "specCategory" => $specCategory,
                    "lang" => $lang,
                    "country" => $country
                ));
                if (!$addSpecCatText) {
                    $addSpecCatText = new SpecificationCategoryText();
                    $isPersist = true;
                }
                $addSpecCatText->setCountry($country);
                $addSpecCatText->setLang($lang);
                $addSpecCatText->setSpecCategory($specCategory);
                $addSpecCatText->setText($specCatText["text"]);
                if ($isPersist) {
                    $em->persist($addSpecCatText);
                }
                $em->flush();
            }
        }

        $em->flush();
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specificationcategory/{id}", name="deleteSpecCategory", methods={"DELETE"})
     */
    public function deleteSpecCategory($id) {
        $em = $this->getDoctrine()->getManager();

        $specCat = $em->getRepository(SpecificationCategory::class)->find($id);
        $specCat->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("specificationcategory", name="getAllSpecCategoryForAdmin", methods={"GET"})
     */
    public function getAllSpecCategory() {
        $em = $this->getDoctrine()->getManager();

        $specCat = $em->getRepository(SpecificationCategory::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($specCat as $sCat) {
            $tmp = array(
                "id" => $sCat->getId(),
                "name" => $sCat->getName()
            );

            $specCatText = $sCat->getAdminText();
            foreach ($specCatText as $text) {
                $tmp["text"] = array(
                    "text" => $text->getText()
                );

                $tmp["country"] = array(
                    "id" => $text->getCountry()->getId(),
                    "name" => $text->getCountry()->getName(),
                    "code" => $text->getCountry()->getCode()
                );

                $tmp["language"] = array(
                    "id" => $text->getLang()->getId(),
                    "name" => $text->getLang()->getName(),
                    "code" => $text->getLang()->getCode()
                ) ;
            }
            $dataArray[] = $tmp;
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specificationcategory/{id}", name="getSpecCategoryById", methods={"GET"})
     */
    public function getSpecCategoryById($id) {
        $em = $this->getDoctrine()->getManager();

        $specCat = $em->getRepository(SpecificationCategory::class)->getAllSpecCatById($id);

        return new JsonResponse($specCat[0]);
    }
}