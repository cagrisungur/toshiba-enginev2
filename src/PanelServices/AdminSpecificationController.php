<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.11.2018
 * Time: 10:20
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Specification;
use App\Entity\SpecificationCategory;
use App\Entity\SpecificationText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminSpecificationController extends Controller
{
    /**
     * @Route("specification", name="createSpec", methods={"POST"})
     * @return JsonResponse
     */
    public function createSpec() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $sPecs) {
            $specCategory = $em->getRepository(SpecificationCategory::class)->find($sPecs["specCatId"]);

            $spec = new Specification();
            $spec->setCategory($specCategory);
            $spec->setName($sPecs["specName"]);
            $spec->setCode($sPecs["code"]);
            $spec->setPriority($sPecs["priority"]);

            $em->persist($spec);

            foreach ($sPecs["specText"] as $specText) {
                $country = $em->getRepository(Country::class)->find($specText["countryId"]);
                $lang = $em->getRepository(Language::class)->find($specText["langId"]);

                $specificationText = new SpecificationText();
                $specificationText->setName($specText["text"]);
                $specificationText->setCountry($country);
                $specificationText->setLang($lang);
                $specificationText->setSpec($spec);

                $em->persist($specificationText);
            }
        }

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specification/{id}", name="updateSpecification", methods={"PUT"})
     */
    public function updateSpecification($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $specCategory = $em->getRepository(SpecificationCategory::class)->find($data["specCatId"]);

        $spec = $em->getRepository(Specification::class)->find($id);
        $spec->setCategory($specCategory);
        $spec->setName($data["specName"]);
        $spec->setCode($data["code"]);
        $spec->setPriority($data["priority"]);

        foreach ($data["specText"] as $specText) {
            $isPersist = false;
            $country = $em->getRepository(Country::class)->find($specText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($specText["langId"]);

            $specificationText = $em->getRepository(SpecificationText::class)->findOneBy(array(
                "spec" => $spec,
                "country" => $country,
                "lang" => $lang
            ));
            if ($specificationText && empty($specText["text"])) {
                $em->remove($specificationText);
                $em->flush();
            }
            if (!$specificationText) {
                $specificationText = new SpecificationText();
                $isPersist = true;
            }
            $specificationText->setName($specText["text"]);
            $specificationText->setCountry($country);
            $specificationText->setLang($lang);
            $specificationText->setSpec($spec);
            if ($isPersist) {
                $em->persist($specificationText);
            }
            $em->flush();
        }

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specification/{id}", name="deleteSpecification", methods={"DELETE"})
     */
    public function deleteSpecification($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteSpec = $em->getRepository(Specification::class)->find($id);
        $deleteSpec->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("specification", name="getAllSpecification", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllSpecification() {
        $em = $this->getDoctrine()->getManager();

        $specification = $em->getRepository(Specification::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($specification as $spec) {
            if ($spec) {
                $tmp = array(
                    "id" => $spec->getId(),
                    "name" => $spec->getName(),
                    "code" => $spec->getCode()
                );

                if ($spec->getCategory()) {
                    $specCat = $spec->getCategory();
                    $tmp["specCategory"] = array(
                        "id" => $specCat->getId(),
                        "name" => $specCat->getName()
                    );

                    $text = $spec->getAdminText();
                    foreach ($text as $t) {
                        $tmp["text"] = array(
                            "name" => $t->getName(),
                        );
                        $tmp["country"] = array(
                            "id" => $t->getCountry()->getId(),
                            "name" => $t->getCountry()->getName(),
                            "code" => $t->getCountry()->getCode()
                        );
                        $tmp["lang"] = array(
                            "id" => $t->getLang()->getId(),
                            "name" => $t->getLang()->getName()
                        );
                    }
                    $dataArray[] = $tmp;
                }
            }
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("specification/{id}", name="getSpecificationByIdForAdmin", methods={"GET"})
     */
    public function getSpecificationById($id) {
        $em = $this->getDoctrine()->getManager();

        $specification = $em->getRepository(Specification::class)->getAllSpecificationById($id);

        return new JsonResponse($specification[0]);
    }
}