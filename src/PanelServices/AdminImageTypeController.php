<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 11:15
 */

namespace App\PanelServices;



use App\Entity\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminImageTypeController extends Controller
{
    /**
     * @return JsonResponse
     * @Route("imagetype", name="getAllImageType", methods={"GET"})
     */
    public function getAllImageType() {
        $em = $this->getDoctrine()->getManager();

        $imageType = $em->getRepository(ImageType::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();
        foreach ($imageType as $type) {
            $dataArray[] = array(
                "id" => $type->getId(),
                "name" => $type->getName(),
                "code" => $type->getCode()
            );
        }

        return new JsonResponse($dataArray);
    }
}