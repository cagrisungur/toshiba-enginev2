<?php
/**
 * Created by PhpStorm.
 * User: çağrı
 * Date: 19.11.2018
 * Time: 17:30
 */

namespace App\PanelServices;

use App\Entity\Continent;
use App\Entity\ContinentText;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminContinentText extends Controller
{
    /**
     * @Route("continenttext", name="createContinentText", methods={"POST"})
     */
    public function createContinentText() {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $d) {
            $continent = $em->getRepository(Continent::class)->find($d["continentId"]);
            $country = $em->getRepository(Country::class)->find($d["countryId"]);
            $lang = $em->getRepository(Language::class)->find($d["langId"]);

            $continentText = new ContinentText();
            $continentText->setText($d["text"]);
            $continentText->setContinent($continent);
            $continentText->setLang($lang);
            $continentText->setCountry($country);

            $em->persist($continentText);
        }
        $em->flush();

        return new Response("success");
    }
}