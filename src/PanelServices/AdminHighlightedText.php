<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.11.2018
 * Time: 15:52
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\HighlightedText;
use App\Entity\HighlightedTextTranslate;
use App\Entity\Language;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminHighlightedText extends Controller
{
    /**
     * @Route("highlightedtext", name="createHighlightedText", methods={"POST"})
     */
    public function createHighlightedText() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $product = $em->getRepository(Product::class)->find($data["productId"]);

        $highLightedText = new HighlightedText();
        $highLightedText->setProduct($product);
        $highLightedText->setPriority($data["priority"]);

        $em->persist($highLightedText);

        foreach ($data["highLightedTextTranslate"] as $highLightedTextTranslate) {
            $country = $em->getRepository(Country::class)->find($highLightedTextTranslate["countryId"]);
            $lang = $em->getRepository(Language::class)->find($highLightedTextTranslate["langId"]);

            $highTranslate = new HighlightedTextTranslate();
            $highTranslate->setLang($lang);
            $highTranslate->setCountry($country);
            $highTranslate->setHighlightedText($highLightedText);
            $highTranslate->setText($highLightedTextTranslate["text"]);

            $em->persist($highTranslate);
        }
        $em->flush();

        return new Response("success");
    }
}