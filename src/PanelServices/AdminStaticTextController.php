<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 4.12.2018
 * Time: 16:13
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\Language;
use App\Entity\StaticText;
use App\Entity\StaticTextTranslate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminStaticTextController extends Controller
{
    /**
     * @Route("statictext", name="getAllStaticTextForAdmin", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllStaticText() {
        $em = $this->getDoctrine()->getManager();

        $dataArray = array();

        $staticTexts = $em->getRepository(StaticText::class)->findBy(array(
            "deletedAt" => null
        ));

        foreach ($staticTexts as $staticText) {
            $tmp = array(
                "id" => $staticText->getId(),
                "code" => $staticText->getCode(),
                "name" => $staticText->getName()
            );

            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("statictext/{id}", name="updateStaticText", methods={"PUT"})
     */
    public function updateStaticText($id) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $findStaticText = $em->getRepository(StaticText::class)->find($id);
        $findStaticText->setCode($data["code"]);
        $findStaticText->setName($data["name"]);

        foreach ($data["translate"] as $translate) {
            $isPersist = false;
            $country = $em->getRepository(Country::class)->find($translate["countryId"]);
            $lang = $em->getRepository(Language::class)->find($translate["langId"]);

            $staticTextTranslate = $em->getRepository(StaticTextTranslate::class)->findOneBy(array(
                "staticText" => $findStaticText,
                "country" => $country,
                "lang" => $lang
            ));
            if (!$staticTextTranslate) {
                $staticTextTranslate = new StaticTextTranslate();
                $staticTextTranslate->setCountry($country);
                $staticTextTranslate->setLang($lang);
                $staticTextTranslate->setStaticText($findStaticText);
                $isPersist = true;
            }
            $staticTextTranslate->setData($translate["data"]);
            if ($isPersist) {
                $em->persist($staticTextTranslate);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("statictext/{id}", name="deleteStaticText", methods={"DELETE"})
     */
    public function deleteStaticText($id) {
        $em = $this->getDoctrine()->getManager();

        $findStaticText = $em->getRepository(StaticText::class)->find($id);
        $findStaticText->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @return JsonResponse
     * @Route("statictext", name="createStaticText", methods={"POST"})
     */
    public function createStaticText() {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $staticText) {
            $addStaticText = new StaticText();
            $addStaticText->setCode($staticText["code"]);
            $addStaticText->setName($staticText["name"]);

            $em->persist($addStaticText);

            foreach ($staticText["translate"] as $translate) {
                $country = $em->getRepository(Country::class)->find($translate["countryId"]);
                $lang = $em->getRepository(Language::class)->find($translate["langId"]);

                $staticTextTranslate = new StaticTextTranslate();
                $staticTextTranslate->setData($translate["data"]);
                $staticTextTranslate->setCountry($country);
                $staticTextTranslate->setLang($lang);
                $staticTextTranslate->setStaticText($addStaticText);

                $em->persist($staticTextTranslate);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("statictext/{id}", name="getStaticTextById", methods={"GET"})
     */
    public function getStaticTextById($id) {
        $em = $this->getDoctrine()->getManager();

        $staticText = $em->getRepository(StaticText::class)->getStaticTextById($id);

        return new JsonResponse($staticText[0]);
    }
}