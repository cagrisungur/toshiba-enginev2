<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 8.12.2018
 * Time: 17:38
 */

namespace App\PanelServices;


use App\Entity\FileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AdminFileType extends Controller
{
    /**
     * @return JsonResponse
     * @Route("filetype", name="getFileType", methods={"GET"})
     */
    public function getFileType() {
        $em = $this->getDoctrine()->getManager();

        $dataArray = array();

        $fileTypes = $em->getRepository(FileType::class)->findAll();

        foreach ($fileTypes as $fileType) {
            $dataArray[] = array(
                "id" => $fileType->getId(),
                "name" => $fileType->getName(),
                "code" => $fileType->getCode()
            );
        }

        return new JsonResponse($dataArray);
    }
}