<?php
/**
 * Created by PhpStorm.
 * User: çağrı
 * Date: 19.11.2018
 * Time: 17:22
 */

namespace App\PanelServices;

use App\Entity\Continent;
use App\Entity\ContinentText;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminContinentController extends Controller
{
    /**
     * @Route("continent", name="createContinent", methods={"POST"})
     */
    public function createContinent() {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $cont) {
            $continent = new Continent();
            $continent->setActive($cont["active"]);
            $continent->setName($cont["name"]);
            $continent->setCode($cont["code"]);
            $continent->setPriority($cont["priority"]);
            $em->persist($continent);

            foreach ($cont["continentText"] as $continentText) {
                $country = $em->getRepository(Country::class)->find($continentText["countryId"]);
                $lang = $em->getRepository(Language::class)->find($continentText["langId"]);

                $addContinentText = new ContinentText();
                $addContinentText->setCountry($country);
                $addContinentText->setLang($lang);
                $addContinentText->setContinent($continent);
                $addContinentText->setText($continentText["text"]);

                $em->persist($addContinentText);
            }
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("continent/{id}", name="updateContinent", methods={"PUT"})
     */
    public function updateContinent($id) {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $em = $this->getDoctrine()->getManager();

        $continent = $em->getRepository(Continent::class)->find($id);
        $continent->setActive($data["active"]);
        $continent->setName($data["name"]);
        $continent->setCode($data["code"]);
        $continent->setPriority($data["priority"]);

        foreach ($data["continentText"] as $continentText) {

            $country = $em->getRepository(Country::class)->find($continentText["countryId"]);
            $lang = $em->getRepository(Language::class)->find($continentText["langId"]);

            $updateContinentText = $em->getRepository(ContinentText::class)->findOneBy(array(
                "continent"=> $continent
            ));
            $updateContinentText->setCountry($country);
            $updateContinentText->setLang($lang);
            $updateContinentText->setContinent($continent);
            $updateContinentText->setText($continentText["text"]);
        }
        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("continent/{id}", name="deleteContinent", methods={"DELETE"})
     */
    public function deleteContinent($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteContinent = $em->getRepository(Continent::class)->find($id);

        $deleteContinent->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @Route("continent", name="getAllContinentForAdmin", methods={"GET"})
     */
    public function getAllContinent() {
        $em = $this->getDoctrine()->getManager();

        $continent = $em->getRepository(Continent::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($continent as $allContinent) {
            $tmp = array(
                "id" => $allContinent->getId(),
                "name" => $allContinent->getName(),
                "code" => $allContinent->getCode()
            );
            $text = $allContinent->getContinentText();
            foreach ($text as $contText) {
                $tmp["continentText"] = array(
                    "text" => $contText->getText()
                );
            }
            $dataArray[] = $tmp;
        }

        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("continent/{id}", name="getContinentById", methods={"GET"})
     */
    public function getContinentById($id) {
        $em = $this->getDoctrine()->getManager();

        $continent = $em->getRepository(Continent::class)->getContinentById($id);

        return new JsonResponse($continent[0]);
    }
}