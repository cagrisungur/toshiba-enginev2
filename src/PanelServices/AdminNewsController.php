<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.01.2019
 * Time: 13:43
 */

namespace App\PanelServices;


use App\Entity\Country;
use App\Entity\ImageType;
use App\Entity\Language;
use App\Entity\News;
use App\Entity\NewsImage;
use App\Entity\NewsText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminNewsController extends Controller
{
    /**
     * @Route("/news", name="getAllNewsAdmin", methods={"GET"})
     */
    public function getAllNewsAdmin() {
        $em = $this->getDoctrine()->getManager();

        $news = $em->getRepository(News::class)->findBy(array(
            "deletedAt" => null
        ));

        $dataArray = array();

        foreach ($news as $new) {
            $tmp = array(
                "id" => $new->getId(),
                "title" => $new->getTitle(),
                "priority" => $new->getPriority()
            );

            $newText = $new->getAdminText();
            foreach ($newText as $text) {
                $tmp["text"][] = array(
                    "title" => $text->getTitle(),
                    "subtitle" => $text->getSubtitle(),
                    "description" => $text->getDescription(),
                    "sef" => $text->getSef(),
                );
            }

            $images = $new->getAdminImages();
            foreach ($images as $image) {
                $tmp["images"][] = array(
                    "id" => $image->getId(),
                    "name" => $image->getName(),
                    "path" => $image->getPath(),
                    "priority" => $image->getPriority()
                );
            }
            $dataArray[] = $tmp;
        }
        return new JsonResponse($dataArray);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("/news/{id}", name="getAdminNewsById", methods={"GET"})
     */
    public function getAdminNewsById($id) {
        $em = $this->getDoctrine()->getManager();

        $news = $em->getRepository(News::class)->getAdminNewsById($id);

        return new JsonResponse($news[0]);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("/news/{id}", name="deleteAdminNews", methods={"DELETE"})
     */
    public function deleteAdminNews($id) {
        $em = $this->getDoctrine()->getManager();

        $deleteNews = $em->getRepository(News::class)->find($id);
        $deleteNews->setDeletedAt(new \DateTime());

        $em->flush();

        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/news", name="createAdminNews", methods={"POST"})
     */
    public function createAdminNews(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        foreach ($data as $news) {
            $country = $em->getRepository(Country::class)->find($news["countryId"]);

            $addNews = new News();
            $addNews->setCountry($country);
            $addNews->setTitle($news["title"]);
            $addNews->setActive($news["active"]);
            $addNews->setPriority($news["priority"]);
            $em->persist($addNews);

            foreach ($news["image"] as $image) {
                $imageType = $em->getRepository(ImageType::class)->find($image["typeId"]);

                $addNewsImage = new NewsImage();
                $addNewsImage->setNews($addNews);
                $addNewsImage->setType($imageType);
                $addNewsImage->setName($image["name"]);
                $addNewsImage->setActive($image["active"]);
                $addNewsImage->setPriority($image["priority"]);
                if (!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }
                if (isset($image["imageFile"]) && !empty($image["imageFile"])) {
                    $replace = explode(',', $image["imageFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];
                    $mimeFunction = $this->getMimeTypeForImage($imageType);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse("Wrong image format!");
                    }
                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $addNewsImage->setPath($baseUrl . $trimPath);
                }
                $em->persist($addNewsImage);
                $em->flush();
            }
            foreach ($news["text"] as $text) {
                $lang = $em->getRepository(Language::class)->find($text["langId"]);
                $generateSef = AdminHelperController::getInstance()->buildSef($text["title"]);

                $addNewsText = new NewsText();
                $addNewsText->setNews($addNews);
                $addNewsText->setLang($lang);
                $addNewsText->setTitle($text["title"]);
                $addNewsText->setSubtitle($text["subtitle"]);
                $addNewsText->setDescription($text["description"]);
                $addNewsText->setSef($generateSef);

                $em->persist($addNewsText);
                $em->flush();
            }
        }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @Route("/news/{id}", name="updateAdminNews", methods={"PUT"})
     */
    public function updateAdminNews($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $country = $em->getRepository(Country::class)->find($data["countryId"]);

        $updateNews = $em->getRepository(News::class)->find($id);
        $updateNews->setCountry($country);
        $updateNews->setTitle($data["title"]);
        $updateNews->setActive($data["active"]);
        $updateNews->setPriority($data["priority"]);

        $em->flush();
        foreach ($data["image"] as $image) {
            if(isset($image["isDeleted"]) && $image["isDeleted"] == true) {
                $deleteNewsImage = $em->getRepository(NewsImage::class)->findOneBy(array(
                    "id" => $image["id"],
                    "news" => $updateNews
                ));

                $em->remove($deleteNewsImage);
                $em->flush();
            } else if(isset($image["isOld"]) && $image["isOld"] == true) {
                $updateNewsImage = $em->getRepository(NewsImage::class)->findOneBy(array(
                    "id" => $image["id"],
                    "news" => $updateNews
                ));
                $imageType = $em->getRepository(ImageType::class)->find($image["typeId"]);

                $updateNewsImage->setNews($updateNews);
                $updateNewsImage->setType($imageType);
                $updateNewsImage->setName($image["name"]);
                $updateNewsImage->setActive($image["active"]);
                $updateNewsImage->setPriority($image["priority"]);
                $em->flush();
            } else {
                $imageType = $em->getRepository(ImageType::class)->find($image["typeId"]);

                $addNewsImage = new NewsImage();
                $addNewsImage->setNews($updateNews);
                $addNewsImage->setType($imageType);
                $addNewsImage->setName($image["name"]);
                $addNewsImage->setActive($image["active"]);
                $addNewsImage->setPriority($image["priority"]);
                if (!defined('UPLOAD_DIR')) {
                    define('UPLOAD_DIR','./uploads/images/images');
                }
                if (isset($image["imageFile"]) && !empty($image["imageFile"])) {
                    $replace = explode(',', $image["imageFile"])[1];
                    $replace = str_replace(' ', '+', $replace);
                    $decode = base64_decode($replace);

                    $f = finfo_open();
                    $mimeType = finfo_buffer($f, $decode, FILEINFO_MIME_TYPE);
                    $split = explode( '/', $mimeType );
                    $imageType = $split[1];
                    $mimeFunction = $this->getMimeTypeForImage($imageType);
                    if ($mimeFunction == 'application/octet-stream') {
                        return new JsonResponse("Wrong image format! Please check your image type !");
                    }
                    $baseUrl = $this->container->getParameter('baseUrlHttp');

                    $file = UPLOAD_DIR . uniqid(). '.' . $imageType;
                    file_put_contents($file, $decode);
                    $trimPath = ltrim($file, '.');

                    $addNewsImage->setPath($baseUrl . $trimPath);
                }
                $em->persist($addNewsImage);
                $em->flush();
            }
        }
        foreach ($data["text"] as $text) {
            $lang = $em->getRepository(Language::class)->find($text["langId"]);
            $generateSef = AdminHelperController::getInstance()->buildSef($text["title"]);

            $isPersist = false;

            $addNewsText = $em->getRepository(NewsText::class)->findOneBy(array(
                "news" => $updateNews,
                "lang" => $lang
            ));

            if (!$addNewsText) {
                $addNewsText = new NewsText();
                $isPersist = true;
            }

            $addNewsText->setNews($updateNews);
            $addNewsText->setLang($lang);
            $addNewsText->setTitle($text["title"]);
            $addNewsText->setSubtitle($text["subtitle"]);
            $addNewsText->setDescription($text["description"]);
            $addNewsText->setSef($generateSef);
            if ($isPersist) {
                $em->persist($addNewsText);
            }

            $em->flush();
        }
        return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
    }

    public function getMimeTypeForImage($fileName) {
        $idx = explode( '.', $fileName );
        $countExplode = count($idx);
        $idx = strtolower($idx[$countExplode-1]);

        $mime = array(
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
        );

        if (isset( $mime[$idx] )) {
            return $mime[$idx];
        } else {
            return 'application/octet-stream';
        }
    }
}