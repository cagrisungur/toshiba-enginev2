<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 3.07.2018
 * Time: 09:25
 */

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Logs
 * @MongoDB\Document(db="logs", collection="auth_token", repositoryClass="App\Repository\AuthenticationUserTokensRepository")
 */
class AuthenticationUserTokens
{
    /**
     * @MongoDB\Id()
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $token;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }


}