<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 28.06.2018
 * Time: 14:50
 */
namespace App\EventListener;

use App\Document\Logs;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class AuthenticationListener extends Controller
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $headerRequest = $this->container->get("request_stack")->getCurrentRequest();
        $tokenString = str_replace("Bearer ", "", $headerRequest->headers->get("authorization"));
        $jwtManager = $this->container->get("lexik_jwt_authentication.jwt_manager");
        $tokenObj = new JWTUserToken(array(), null, $tokenString, null);
        $token = $jwtManager->decode($tokenObj);

        $clientIp = $this->container->get("request_stack")->getCurrentRequest()->getClientIp();

        $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );

        $logs = new Logs();

        $response = new Response();
        $response->setContent($message);

        if ($exception instanceof HttpExceptionInterface) {

            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());

            if (!$token["username"]) {
                $logs->setName("");
            } else {
                $logs->setName($token["username"]);
            }

            if (!$headerRequest->headers->get("authorization")) {
                $logs->setToken("");
            } else {
                $logs->setToken($tokenString);
            }

            $logs->setCreatedAt(new \DateTime());
            $logs->setIp($clientIp);
            $logs->setResponse($response->send()->getStatusCode());
            $logs->setPathInfo($headerRequest->getPathInfo());
            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($logs);
            $em->flush();

        }  else {

            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

            if (!$token["username"]) {
                $logs->setName("");
            } else {
                $logs->setName($token["username"]);
            }

            if (!$headerRequest->headers->get("authorization")) {
                $logs->setToken("");
            } else {
                $logs->setToken($tokenString);
            }

            $logs->setCreatedAt(new \DateTime());
            $logs->setIp($clientIp);
            $logs->setResponse($response->send()->getStatusCode());
            $logs->setPathInfo($headerRequest->getPathInfo());

            $em = $this->get('doctrine_mongodb')->getManager();
            $em->persist($logs);
            $em->flush();
        }

        $event->setResponse($response);
    }
}