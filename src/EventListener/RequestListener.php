<?php

namespace App\EventListener;

use App\Entity\OauthSessionAccessTokens;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class RequestListener extends Controller
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $this->container->get('router')->getContext()->getPathInfo();
        $str =  strpos($route,"admin");
        if($str){
            $em = $this->getDoctrine()->getManager();
            $headerRequest = $this->container->get("request_stack")->getCurrentRequest();
            $tokenString = str_replace("Bearer ", "", $headerRequest->headers->get("authorization"));
            $isSessionAccessToken = $em->getRepository(OauthSessionAccessTokens::class)->findOneBy(array("accessToken" => $tokenString));
            if(!$isSessionAccessToken) {
//                $array = array('error' => 'Not Authorized');
//                $response = new Response(json_encode($array), 401);
//                $response->headers->set('Content-Type', 'application/json');

                throw new HttpException(401,"Unauthorized");
            }
        }

    }
}