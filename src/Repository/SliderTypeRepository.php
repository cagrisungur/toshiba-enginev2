<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 16:07
 */

namespace App\Repository;


use App\Entity\SliderType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SliderTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SliderType::class);
    }

    public function getSliderTypeById($id) {
        return $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.deletedAt is NULL")
            ->andWhere("s.id =:sliderTypeId")
            ->setParameter("sliderTypeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}