<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CountryLangRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function getAllLocations()
    {
        return $this->createQueryBuilder("c")
            ->select("c", "countryLang", "lang")
            ->leftJoin("c.countryLang", "countryLang", "WITH", "countryLang.deletedAt is null")
            ->leftJoin("countryLang.lang", "lang")
            ->where("c.active = :isActive")
            ->andWhere("c.deletedAt is null")
            ->setParameter("isActive", 1)
            ->orderBy("c.priority", "ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllCountryById($id) {
        return $this->createQueryBuilder("c")
            ->select("c", "countryLang", "lang", "continent")
            ->leftJoin("c.countryLang", "countryLang")
            ->leftJoin("countryLang.lang", "lang")
            ->leftJoin("c.continent","continent")
            ->where("c.id = :countryId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("countryId", $id)
            ->getQuery()
            ->getArrayResult();
    }

}