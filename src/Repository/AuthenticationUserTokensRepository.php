<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 3.07.2018
 * Time: 09:30
 */

namespace App\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;


class AuthenticationUserTokensRepository extends DocumentRepository
{
    public function getUserByToken($token)
    {
        return $this->createQueryBuilder()
            ->select("auth_token")
            ->field("token")->equals($token)
            ->hydrate(false)
            ->getQuery()
            ->toArray();
    }
}