<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 12:07
 */

namespace App\Repository;


use App\Entity\CategorySpec;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


class CategorySpecRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorySpec::class);
    }

    public function getAllCategorySpecById() {
        return $this->createQueryBuilder("cs")
            ->select("cs", "category", "spec")
            ->leftJoin("cs.category","category")
            ->leftJoin("cs.spec","spec")
            ->getQuery()
            ->getArrayResult();
    }
}
