<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.11.2018
 * Time: 18:11
 */

namespace App\Repository;


use App\Entity\SpecificationCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SpecificationCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecificationCategory::class);
    }

    public function getAllSpecCatById($id) {
        return $this->createQueryBuilder("s")
            ->select("s", "text", "country", "lang")
            ->leftJoin("s.text","text")
            ->leftJoin("text.country", "country")
            ->leftJoin("text.lang", "lang")
            ->where("s.deletedAt is NULL")
            ->andWhere("s.id = :specCatId")
            ->setParameter("specCatId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}