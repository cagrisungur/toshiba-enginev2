<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 11:50
 */

namespace App\Repository;


use App\Entity\ProductFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ProductFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFile::class);
    }

    public function getProductImage($id) {
        //print_r($id);
        return $this->createQueryBuilder("p")
            ->select("p","product")
            ->leftJoin("p.product","product")
            ->leftJoin("p.type", "type")
            ->where("product.id= :productId")
            ->andWhere("type.id =:typeId")
            ->andWhere("p.fileName not like :query")
            ->andWhere("p.fileName not like :query1")
            ->andWhere("p.deletedAt is null")
            ->setParameter("productId", $id)
            ->setParameter('query', '%blob%')
            ->setParameter('query1', '%image.png%')
            ->setParameter("typeId", 20)
            ->addOrderBy("p.priority", "ASC")
            ->addOrderBy("p.id", "ASC")
            ->getQuery()
            ->getResult();
    }
}