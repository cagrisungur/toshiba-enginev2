<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 12.07.2018
 * Time: 09:44
 */

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getAllCategory($langId, $countryId)
    {
        return $this->createQueryBuilder("c")
            ->select("c", "file", "feature", "image", "spec", "text", "feat", "filed", "type", "specid")
            ->leftJoin("c.file","file","WITH","file.deletedAt is null")
            ->leftJoin("c.feature","feature","WITH","feature.deletedAt is null")
            ->leftJoin("c.image","image","WITH","image.deletedAt is null and image.active=1")
            ->leftJoin("c.spec","spec","WITH","spec.deletedAt is null")
            ->leftJoin("c.text","text","WITH","text.deletedAt is null")
            ->leftJoin("feature.feature","feat","WITH","feat.deletedAt is null and feat.active=1")
            ->leftJoin("file.file","filed","WITH","filed.deletedAt is null")
            ->leftJoin("feature.type","type","WITH","type.deletedAt is null")
            ->leftJoin("spec.spec","specid","WITH","specid.deletedAt is null")
            ->where("text.country = :countryId")
            ->andWhere("text.lang= :langId")
            ->andWhere("c.deletedAt is NULL")
            ->andWhere("c.active= :isActive")
            ->setParameter("isActive",1)
            ->setParameter("countryId", $countryId)
            ->setParameter("langId", $langId)
            ->orderBy("c.priority","ASC")
            ->orderBy("file.priority","ASC")
            ->addOrderBy("feature.priority","ASC")
            ->addOrderBy("image.priority","ASC")
            ->addOrderBy("spec.priority","ASC")
            ->addOrderBy("specid.priority","ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getCategoryByParentId($langId, $countryId, $parentId)
    {
        return $this->createQueryBuilder("c")
            ->select("c", "file", "feature", "image", "spec", "text", "feat", "filed", "type", "specid","product")
            ->leftJoin("c.file","file","WITH","file.deletedAt is null")
            ->leftJoin('c.product',"product")
            ->leftJoin("c.feature","feature","WITH","feature.deletedAt is null")
            ->leftJoin("c.image","image","WITH","image.deletedAt is null and image.active=1")
            ->leftJoin("c.spec","spec","WITH","spec.deletedAt is null")
            ->leftJoin("c.text","text","WITH","text.deletedAt is null")
            ->leftJoin("feature.feature","feat","WITH","feat.deletedAt is null and feat.active=1")
            ->leftJoin("file.file","filed","WITH","filed.deletedAt is null")
            ->leftJoin("feature.type","type","WITH","type.deletedAt is null")
            ->leftJoin("spec.spec","specid","WITH","specid.deletedAt is null")
            ->where("text.country = :countryId")
            ->andWhere("text.lang= :langId")
            ->andWhere("c.deletedAt is NULL")
            ->andWhere("c.active= :isActive")
            ->andWhere("c.parentId= :parentId")
            ->setParameter("parentId",$parentId)
            ->setParameter("isActive",1)
            ->setParameter("countryId", $countryId)
            ->setParameter("langId", $langId)
            ->orderBy("c.priority","ASC")
            ->orderBy("file.priority","ASC")
            ->addOrderBy("feature.priority","ASC")
            ->addOrderBy("image.priority","ASC")
            ->addOrderBy("spec.priority","ASC")
            ->addOrderBy("specid.priority","ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllCategoryAdmin($categoryId) {
        return $this->createQueryBuilder("c")
            ->select(
                "c",
                "categoryFeature",
                "feature",
                "categoryFeatureType",
                "categoryFile",
                "file",
                "fileType",
                "categoryImage",
                "imageType",
                "categorySpec",
                "categoryText",
                "categoryTextCountry",
                "categoryTextLang"

            )
            ->leftJoin("c.feature","categoryFeature")
            ->leftJoin("categoryFeature.feature","feature")
            ->leftJoin("categoryFeature.type","categoryFeatureType")
            ->leftJoin("feature.text","catFeatureText")
            ->leftJoin("catFeatureText.country","catFeatureTextCountry")
            ->leftJoin("catFeatureText.lang","catFeatureTextLang")
            ->leftJoin("c.file","categoryFile")
            ->leftJoin("categoryFile.file","file")
            ->leftJoin("file.type","fileType")
            ->leftJoin("c.image","categoryImage")
            ->leftJoin("categoryImage.type","imageType")
            ->leftJoin("c.spec","categorySpec")
            ->leftJoin("categorySpec.spec","spec")
            ->leftJoin("c.text","categoryText")
            ->leftJoin("categoryText.country","categoryTextCountry")
            ->leftJoin("categoryText.lang","categoryTextLang")
            ->where("c.id = :categoryId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("categoryId",$categoryId)
            ->getQuery()
            ->getArrayResult();
    }

    public function getCategoryById($langId, $countryId, $id)
    {
        return $this->createQueryBuilder("c")
            ->select("c", "file", "feature", "image", "spec", "text", "feat", "filed", "type", "specid","product")
            ->leftJoin("c.file","file","WITH","file.deletedAt is null")
            ->leftJoin('c.product',"product")
            ->leftJoin("c.feature","feature","WITH","feature.deletedAt is null")
            ->leftJoin("c.image","image","WITH","image.deletedAt is null and image.active=1")
            ->leftJoin("c.spec","spec","WITH","spec.deletedAt is null")
            ->leftJoin("c.text","text","WITH","text.deletedAt is null")
            ->leftJoin("feature.feature","feat","WITH","feat.deletedAt is null and feat.active=1")
            ->leftJoin("file.file","filed","WITH","filed.deletedAt is null")
            ->leftJoin("feature.type","type","WITH","type.deletedAt is null")
            ->leftJoin("spec.spec","specid","WITH","specid.deletedAt is null")
            ->where("text.country = :countryId")
            ->andWhere("text.lang= :langId")
            ->andWhere("c.deletedAt is NULL")
            ->andWhere("c.active= :isActive")
            ->andWhere("c.id= :catId")
            ->setParameter("isActive",1)
            ->setParameter("catId", $id)
            ->setParameter("countryId", $countryId)
            ->setParameter("langId", $langId)
            ->orderBy("c.priority","ASC")
            ->orderBy("file.priority","ASC")
            ->addOrderBy("feature.priority","ASC")
            ->addOrderBy("image.priority","ASC")
            ->addOrderBy("spec.priority","ASC")
            ->addOrderBy("specid.priority","ASC")
            ->getQuery()
            ->getArrayResult();
    }


    public function getNewCategoryById($langId, $sef)
    {
        return $this->createQueryBuilder("c")
            ->select("c", "file", "feature", "spec", "text", "feat", "filed", "type", "specid","product", "productText")
            ->leftJoin("c.file","file","WITH","file.deletedAt is null")
            ->leftJoin('c.product',"product")
            ->leftJoin("c.feature","feature","WITH","feature.deletedAt is null")
            ->leftJoin("c.spec","spec","WITH","spec.deletedAt is null")
            ->leftJoin("c.text","text","WITH","text.deletedAt is null")
            ->leftJoin("feature.feature","feat","WITH","feat.deletedAt is null and feat.active=1")
            ->leftJoin("file.file","filed","WITH","filed.deletedAt is null")
            ->leftJoin("feature.type","type","WITH","type.deletedAt is null")
            ->leftJoin("spec.spec","specid","WITH","specid.deletedAt is null")
            ->leftJoin("product.text", "productText")
            ->andWhere("text.lang= :langId")
            ->andWhere("c.deletedAt is NULL")
            ->andWhere("c.active= :isActive")
            ->andWhere("c.link= :catLink")
            ->andWhere("productText.lang= :textLang")
            ->setParameter("isActive",1)
            ->setParameter("catLink", $sef)
            ->setParameter("langId", $langId)
            ->setParameter("textLang", $langId)
            ->orderBy("c.priority","ASC")
            ->orderBy("file.priority","ASC")
            ->addOrderBy("feature.priority","ASC")
            ->addOrderBy("spec.priority","ASC")
            ->addOrderBy("specid.priority","ASC")
            ->getQuery()
            ->getArrayResult();
    }
}
