<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Software
 * Date: 18.06.2018
 * Time: 13:52
 */

namespace App\Repository;


use App\Entity\Slider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class SliderRepository
 * @package App\Repository
 */
class SliderRepository extends ServiceEntityRepository
{
    /**
     * SliderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Slider::class);
    }

    public function getAllSliders($langId, $countryId, $code)
    {
        return $this->createQueryBuilder("s")
            ->select("s", "pp", "ppCat", "category", "item", "product", "itemText","pid", "type", "cat")
            ->leftJoin("s.item","item","WITH","item.deletedAt is null and item.active=1")
            ->leftJoin("s.country", "country")
            ->leftJoin("s.category","category","WITH","category.deletedAt is null and category.active=1")
            ->leftJoin("s.product", "product", "WITH","product.deletedAt is null and product.active=1")
            ->leftJoin("product.product","pp")
            ->leftJoin("pp.category","ppCat")
            ->leftJoin("item.itemText","itemText","WITH","itemText.deletedAt is null and itemText.lang =:langId")
            ->leftJoin("category.category","cat","WITH","cat.deletedAt is null and cat.active=1")
            ->leftJoin("product.product", "pid","WITH","pid.deletedAt is null and pid.active=1")
            ->leftJoin("s.type", "type")
            ->where("country.id = :countryId")
            ->andWhere("type.code = :code")
            ->andWhere("s.deletedAt is NULL")
            ->andWhere("s.active = :isActive")
            ->setParameter("isActive", 1)
            ->setParameter("langId", $langId)
            ->setParameter("countryId", $countryId)
            ->setParameter("code", $code)
            ->orderBy("item.priority", "ASC")
            // ->orderBy("product.priority", "ASC")
            ->addOrderBy("cat.priority","ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllSliderById($id) {
        return $this->createQueryBuilder("s")
            ->select("s", "item", "country", "category", "product", "prod", "ppCat", "itemText", "cat", "type")
            ->leftJoin("s.item","item")
            ->leftJoin("item.itemText", "itemText")
            ->leftJoin("s.country","country")
            ->leftJoin("s.category","category")
            ->leftJoin("s.product","product")
            ->leftJoin("product.product","prod")
            ->leftJoin("prod.category","ppCat")
            ->leftJoin("category.category","cat")
            ->leftJoin("s.type","type")
            ->where("s.id = :sliderId")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("sliderId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}