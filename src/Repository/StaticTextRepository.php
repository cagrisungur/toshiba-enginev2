<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 5.12.2018
 * Time: 13:20
 */

namespace App\Repository;


use App\Entity\StaticText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class StaticTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StaticText::class);
    }

    public function getStaticTextById($id) {
        return $this->createQueryBuilder("s")
            ->select("s"," translate", "language", "country")
            ->leftJoin("s.staticTextTranslate","translate")
            ->leftJoin("translate.country","country")
            ->leftJoin("translate.lang","language")
            ->where("s.id =:staticId")
            ->setParameter("staticId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}