<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 15:31
 */

namespace App\Repository;


use App\Entity\CustomPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CustomPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomPage::class);
    }

    public function getAllCustomPageById($id) {
        return $this->createQueryBuilder("c")
            ->select("c", "pageType", "text", "country", "language")
            ->leftJoin("c.type","pageType")
            ->leftJoin("c.pageText","text")
            ->leftJoin("text.country","country")
            ->leftJoin("text.lang","language")
            ->where("c.id =:customPageId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("customPageId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}