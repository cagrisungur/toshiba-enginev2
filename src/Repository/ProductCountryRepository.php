<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Software
 * Date: 26.06.2018
 * Time: 14:23
 */

namespace App\Repository;


use App\Entity\ProductCountry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Persistence\ManagerRegistry;

class ProductCountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCountry::class);
    }

    public function getProductBySearched($countryId, $query){
        return $this->createQueryBuilder("p")
            ->select("p","product","file","productText","productFile", "image", "feature", "productFeature", "imageType", "productType", "catFile", "category", "categoryFile", "catFileType")
            ->leftJoin("p.country","country")
            ->leftJoin("p.product","product")
            ->leftJoin("product.category", "category")
            ->leftJoin("category.file","catFile")
            ->leftJoin("catFile.file","categoryFile")
            ->leftJoin("categoryFile.type","catFileType")
            ->leftJoin("product.file", "file")
            ->leftJoin("file.file","productFile")
            ->leftJoin("productFile.type","productType")
            ->leftJoin("product.feature","productFeature")
            ->leftJoin("productFeature.feature", "feature")
            ->leftJoin("product.images", "image")
            ->leftJoin("image.type","imageType")
            ->leftJoin("product.text","productText")
            ->where("product.active = 1")
            ->andWhere("country.id = :countryId")
            ->andWhere("product.name LIKE :name")
            ->orderBy("image.priority","ASC")
            ->setParameter("countryId",$countryId)
            ->setParameter("name",'%'.$query.'%')
            ->getQuery()
            ->getArrayResult();
    }


    function isExistChild($id){
            $conn = $this->getEntityManager()
                ->getConnection();
            $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
        }



    function getChilds($id){
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = 'SELECT id FROM category where parent_id='.intval($id);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $arr = $stmt->fetchAll();

        $all = array();
        foreach($arr as $row){
            $all[] = $row['id'];
            if($this->isExistChild($row['id'])){
                $res = $this->getChilds($row['id']);
                foreach( $res as $inRow){
                    $all[] = $inRow;
                }

            }

        }
        return $all;//
    }




    function getProductByFeature($country, $lang, $categoryLink, $filterList, $feature = null)
    {
echo 'hii';
        die("test");
    }

        public function getProductByFilter($country, $lang, $categoryLink, $filterList, $feature = null) {



            if($feature != null) {
              //  $this->getProductByFeature($country, $lang, $categoryLink, $filterList, $feature);
            }



        // can 180719
        // tüm alt kategorileri dahil ederek arama yapar.
        // searches with all child categories
        // begin
        $sql = 'SELECT id FROM category where link=?';
        $conn = $this->getEntityManager()
            ->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute(array($categoryLink));
        $arr = $stmt->fetchAll();

        if(isset($arr[0])){
        $catID = $arr[0]['id'];
        }else{
            die("unknown category");
        }


        $cats = $this->getChilds($catID);
        $cats[] = intval($catID);
        $cats = '('.implode(',',$cats).')';
        //end




        $filterIDs = array();
        if(is_array($filterList)){
            if(count($filterList) > 0){
                foreach($filterList as $row){
                    $filterIDs[] = intval($row);
                }
            }
        }


        $filterExp = '(0)';
        if(count($filterIDs) > 0){
            $filterExp = '('.implode(",",$filterIDs).')';
        }





                                  $conn = $this->getEntityManager()->getConnection();
                                  $sql = '


        SELECT


                                ps.product_id

                                FROM filters_cats fc

                                left join filters fi on fi.filtercat = fc.id
                                left join filters_specids sids on sids.filter = fi.id
                                left join filters_specvalues svals on svals.parent = fi.id
                                left join specification s on s.id = sids.spec
                                left join product_spec ps on ps.spec_id = sids.spec and svals.val = ps.value


                                where
                                fi.id IN '.$filterExp.' and
                                ps.lang_id = 1 and

                                fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN '.$cats.' group by pcr.filtercat)


                                ';



                                $sql = '(


        SELECT


                                ps.product_id

                                FROM filters_cats fc

                                left join filters fi on fi.filtercat = fc.id
                                left join filters_specids sids on sids.filter = fi.id
                                left join filters_specvalues svals on svals.parent = fi.id
                                left join specification s on s.id = sids.spec
                                left join product_spec ps on ps.spec_id = sids.spec and svals.val = ps.value


                                where
                                fi.id IN '.$filterExp.' and
                                ps.lang_id = 1 and

                                fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN ('.$cats.') group by pcr.filtercat)


                                )
                                          union all (

SELECT


                                 pf.product_id



                                 from filters_featureids fids





                                 left join product_feature pf on pf.feature_id = fids.feature



                                 left join filters fi on fi.id = fids.filter
                                 left join filters_cats fc on fc.id=fi.filtercat
                                 left join feature f on f.id = fids.feature

                                 where
                                 fi.id IN '.$filterExp.' and
                                      fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN ('.$cats.') group by pcr.filtercat)

  group by pf.product_id


                                )';


                                             $stmt = $conn->prepare($sql);
                                             $stmt->execute();
                                             $arr = $stmt->fetchAll();
                                             $filtered = array();
                                foreach($arr as $row){
                                    $filtered[] = $row['product_id'];
                                }
                                $filtered = array_unique($filtered);







                                $filterExp = '(0)';
                                if(count($filtered) > 0){
                                    $filterExp = '('.implode(",",$filtered).')';
                                }








        $qb = $this->createQueryBuilder("p")
            ->select("p", "product","category")
            ->leftJoin("p.product", "product")
            ->innerJoin("product.spec", "productSpec")
            ->innerJoin("product.category", "category")
            ->where("p.country = :country")
          //  ->andWhere("category.link = :categoryLink")
            ->andWhere("category.id IN".$cats)
            ->andWhere("product.id IN".$filterExp)
            ->andWhere("productSpec.lang = :lang")
            ->andWhere("product.deletedAt is null")
            ->setParameter("country", $country)
            //->setParameter("categoryLink", $categoryLink)
            ->setParameter("lang", $lang);



        if($feature != null) {
			$qb->innerJoin("product.feature", "productFeature")
				->andWhere("productFeature.feature = :feature")
				->setParameter("feature", $feature);
		}

        /*$wherePart = array();
        foreach($filterList as $key => $filters) {
            foreach($filters as $subKey => $filter) {
                $filterPlaceholder = "filterId".$key.$subKey;

                if($filter->getOperator() == "between") {
                    $values = explode("-", $filter->getSpecValue());

                    $wherePart[] = "productSpec.value between :"
                        .$filterPlaceholder."Val1 and :".$filterPlaceholder."Val2";
                    $qb->setParameter($filterPlaceholder."Val1", $values[0]);
                    $qb->setParameter($filterPlaceholder."Val2", $values[1]);
                }else {
                    $wherePart[] = "productSpec.value ".
                        $filter->getOperator()." :".$filterPlaceholder;
                    $qb->setParameter($filterPlaceholder, $filter->getSpecValue());
                }
            }

            $qb->andWhere("(".implode(" OR ", $wherePart).
                ") and productSpec.spec = :specId".$key);
                $qb->setParameter("specId".$key, $filter->getSpec());
		}

		if(count($filterList) > 0) {
			$qb->having("count(product.id) = :filterSize")
            	->setParameter("filterSize", count($filterList));
		}*/

        return $qb->groupBy("product.id")
            ->getQuery()
            ->getResult();
    }

    public function supportSearch($countryId, $query, $catTextLang) {
        return $this->createQueryBuilder("p")
            ->select("p", "product", "productText", "image", "categoryText", "category", "lang", "imageType", "productStatus")
            ->leftJoin("p.country","country")
            ->leftJoin("p.product","product")
            ->leftJoin("product.status", "productStatus")
            ->leftJoin("product.category", "category")
            ->leftJoin("category.text", "categoryText")
            ->leftJoin("categoryText.lang", "lang")
            ->leftJoin("product.file", "file")
            ->leftJoin("product.images", "image")
            ->leftJoin("image.type","imageType")
            ->leftJoin("product.text","productText")
            ->where("product.active = 1")
            ->andWhere("product.deletedAt is null")
            ->andWhere("productStatus.id = 1 OR productStatus.id = 2")
            ->andWhere("country.id = :countryId")
            ->andWhere("product.name LIKE :name")
            ->andWhere("lang.id = :langId")
            ->orderBy("image.priority","ASC")
            ->setParameter("countryId",$countryId)
            ->setParameter("name",'%'.$query.'%')
            ->setParameter("langId", $catTextLang)
            ->getQuery()
            ->getArrayResult();
    }

    public function globalSearch($countryId, $query, $catTextLang) {
        return $this->createQueryBuilder("p")
            ->select("p", "product", "productText", "image", "categoryText", "category", "lang", "imageType", "productStatus")
            ->leftJoin("p.country","country")
            ->leftJoin("p.product","product")
            ->leftJoin("product.status", "productStatus")
            ->leftJoin("product.category", "category")
            ->leftJoin("category.text", "categoryText")
            ->leftJoin("categoryText.lang", "lang")
            ->leftJoin("product.file", "file")
            ->leftJoin("product.images", "image")
            ->leftJoin("image.type","imageType")
            ->leftJoin("product.text","productText")
            ->where("product.active = 1")
            ->andWhere("product.deletedAt is null")
            ->andWhere("productStatus.id = 1 OR productStatus.id = 2")
            ->andWhere("country.id = :countryId")
            ->andWhere("product.name LIKE :name")
            ->andWhere("lang.id = :langId")
            ->orderBy("image.priority","ASC")
            ->setParameter("countryId",$countryId)
            ->setParameter("name",'%'.$query.'%')
            ->setParameter("langId", $catTextLang)
            ->getQuery()
            ->getResult();
    }

}
