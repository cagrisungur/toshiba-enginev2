<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.11.2018
 * Time: 14:03
 */

namespace App\Repository;


use App\Entity\FeatureType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FeatureTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeatureType::class);
    }

    public function getAllFeatureTypeById($id) {
        return $this->createQueryBuilder("f")
            ->select("f")
            ->where("f.id = :featureTypeId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("featureTypeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}