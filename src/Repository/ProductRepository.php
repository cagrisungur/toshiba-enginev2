<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 3.10.2018
 * Time: 17:03
 */

namespace App\Repository;


use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getProductByCategory($limit, $offSet, $country, $lang) {
        $em = $this->createQueryBuilder("p")
            ->select("p", "images", "category", "buy")
            ->innerJoin('p.country', 'country')
            ->innerJoin("country.country", 'c')
            ->innerJoin('p.category',"category")
            ->innerJoin('p.images', "images")
            ->leftJoin('p.productBuy', 'buy')
            ->where("p.active= :isActive")
            ->andWhere("p.deletedAt is NULL")
            ->andWhere('images.priority = 0 or images.id is null')
            ->andWhere('buy.country = :country or buy.country is null')
            ->andWhere('buy.active = 1 or buy.active is null')
            ->andWhere('country.country = :country')
            ->groupBy('p.id')
            ->setParameter("isActive",1)
            ->setParameter("country", $country)
            ->setFirstResult($offSet)
            ->setMaxResults($limit);
        if(isset($_POST["categoryId"])) {
            $em->andWhere("category.id = :id")
                ->setParameter("id", $_POST["categoryId"]);
        }

        if (isset($_POST["smart"])) {
            $em->andWhere("p.smart = :smart")
                ->setParameter("smart", $_POST["smart"]);
        }

        if (isset($_POST["minSize"])) {
            if(isset($_POST['maxSize'])) {
                $em->andWhere("p.size <=:maxSize")
                    ->andWhere("p.size >=:minSize")
                    ->setParameter('maxSize', $_POST["maxSize"])
                    ->setParameter('minSize', $_POST["minSize"]);
            } else {
                $em->andWhere("p.size >=:minSize")
                    ->setParameter('minSize', $_POST["minSize"]);
            }
        }

        if (isset($_POST["minFt"])) {
            if(isset($_POST['maxFt'])) {
                $em->andWhere("p.minFt <=:minFt")
                    ->andWhere("p.maxFt >=:maxFt")
                    ->setParameter("maxFt", $_POST["maxFt"])
                    ->setParameter("minFt", $_POST["minFt"]);
            } else{
                $em->andWhere("p.minFt >=:minFt")
                    ->orWhere("p.maxFt >=:minFt")
                    ->setParameter("minFt", $_POST["minFt"]);
            }
        }

        return $em->getQuery()->getArrayResult();
    }

    public function getAllProductAdmin($productId) {
        $em = $this->createQueryBuilder("p")
            ->select(
                "p",
                "category",
                "series",
                "productCountry",
                "productText",
                "sliderProduct",
                "relatedProduct",
                "productFeature",
                "feature",
                "featureText",
                "featureImage",
                "imageType",
                "featureType",
                "productFile",
                "file",
                "fileType",
                "country",
                "textCountry",
                "textLang",
                "related",
                "productSpec",
                "specification",
                "specText",
                "specTextCountry",
                "specTextLang",
                "specCategory",
                "specCategoryText",
                "specCategoryTextCountry",
                "specCategoryTextLang",
                "productConnection",
                "connection",
                "productController",
                "controller",
                "controllerImage",
                "controllerImageType",
                "connectionImage",
                "connectionImageType",
                "slider",
                "sliderItem",
                "sliderItemText",
                "sliderItemTextLang"

            )
            ->leftJoin("p.country","productCountry")
            ->leftJoin("productCountry.country","country")
            ->leftJoin("p.category", "category")
            ->leftJoin("p.series", "series")
            ->leftJoin("p.connection","productConnection")
            ->leftJoin("productConnection.connection","connection")
            ->leftJoin("connection.image","connectionImage")
            ->leftJoin("connectionImage.type","connectionImageType")
            ->leftJoin("p.controller","productController")
            ->leftJoin("productController.controller","controller")
            ->leftJoin("controller.image","controllerImage")
            ->leftJoin("controllerImage.type","controllerImageType")
            ->leftJoin("p.spec","productSpec")
            ->leftJoin("productSpec.spec","specification")
            ->leftJoin("specification.category","specCategory")
            ->leftJoin("specCategory.text","specCategoryText")
            ->leftJoin("specCategoryText.country","specCategoryTextCountry")
            ->leftJoin("specCategoryText.lang","specCategoryTextLang")
            ->leftJoin("specification.text","specText")
            ->leftJoin("specText.country","specTextCountry")
            ->leftJoin("specText.lang","specTextLang")
            ->leftJoin("p.file","productFile")
            ->leftJoin("productFile.file","file")
            ->leftJoin("file.type","fileType")
            ->leftJoin("p.text", "productText")
            ->leftJoin("productText.country","textCountry")
            ->leftJoin("productText.lang","textLang")
            ->leftJoin("p.slider", "sliderProduct")
            ->leftJoin("sliderProduct.slider","slider")
            ->leftJoin("slider.item","sliderItem")
            ->leftJoin("slider.itemText","sliderItemText")
            ->leftJoin("sliderItemText.lang","sliderItemTextLang")
            ->leftJoin("p.related", "relatedProduct")
            ->leftJoin("relatedProduct.related","related")
            ->leftJoin("p.feature","productFeature")
            ->leftJoin("productFeature.type","featureType")
            ->leftJoin("productFeature.feature", "feature")
            ->leftJoin("feature.image","featureImage")
            ->leftJoin("featureImage.type","imageType")
            ->leftJoin("feature.text", "featureText")
            ->where("p.deletedAt is NULL");

            if($productId != null) {
                    $em->andWhere("p.id = :productId")
                        ->setParameter("productId",$productId);
            }

            return $em->getQuery()->getArrayResult();
    }




    public function getProductByIdArray($em,$data) {
        $catID = intval($data[0]);

        if($catID<1){
            die("unknown");
        }

        $cats = $this->getChilds($em, $catID);
        $cats[] = intval($catID);

        if(count($cats)>0){
            $cats = '('.implode(',',$cats).')';
        }

        $productId="1";
        $em = $this->createQueryBuilder("p")
            ->select("p", "images", "category", "buy")
        ->innerJoin('p.country', 'country')
        ->innerJoin("country.country", 'c')
        ->innerJoin('p.category',"category")
        ->innerJoin('p.images', "images")
        ->leftJoin('p.productBuy', 'buy')
        ->where("p.active= :isActive")
        ->andWhere("p.deletedAt is NULL")
        ->andWhere("p.category IN ".$cats)
        ->andWhere('images.priority = 0 or images.id is null')
        ->andWhere('buy.country = :country or buy.country is null')
        ->andWhere('buy.active = 1 or buy.active is null')
        ->andWhere('country.country = :country')
        ->groupBy('p.id')
        ->setParameter("isActive",1)
        ->setParameter("country", 1);


            return $em->getQuery()->getResult();
    }





        function isExistChild($em,$id){
            $conn = $em->getConnection();
            $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
        }



        function getChilds($em, $id){
            $conn = $em->getConnection();
            $sql = 'SELECT id FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            $all = array();
            foreach($arr as $row){
                $all[] = $row['id'];
                if($this->isExistChild($em,$row['id'])){
                    $res = $this->getChilds($em,$row['id']);
                    foreach( $res as $inRow){
                        $all[] = $inRow;
                    }

                }

            }
            return $all;//
        }


        public function seriesGroupProduct($seriesId, $lang, $status)
        {
            $em = $this->createQueryBuilder("p")
                ->select("p", "series", "text", "textLang", "productImage", "category")
                ->leftJoin("p.series", "series")
                ->leftJoin("p.status", "productStatus")
                ->leftJoin("p.text", "text")
                ->leftJoin("text.lang", "textLang")
                ->leftJoin("p.images", "productImage")
                ->leftJoin("productImage.type", "imageType")
                ->leftJoin("p.category", "category")
                // ->leftJoin("series.text", "seriesText")
                ->andWhere("productStatus = :statusId")
                ->andWhere("p.active = :isActive")
                ->andWhere("p.deletedAt is NULL")
                ->andWhere("p.id = :productId")
                ->andWhere("category.deletedAt is NULL")
                // ->andWhere("textLang = :langId")
                // ->andWhere("imageType.code = :imageCode")
                // ->andWhere("productImage.name LIKE :frontParameter")
                ->orderBy("p.name", "DESC")
                ->orderBy("series.priority", "DESC")
                ->setParameter("productId", $seriesId)
                ->setParameter("statusId", $status)
                // ->setParameter("langId", $lang)
                ->setParameter("isActive", 1);
                // ->setParameter("imageCode", "desktop")
                // ->setParameter("frontParameter", "%front%");
                
                return $em->getQuery()->getResult();   

        }

        public function searchForTruPicture($product) {
            $em = $this->createQueryBuilder("p")
            ->select("p")
            ->leftJoin("p.feature", "productFeature")
            ->leftJoin("productFeature.feature", "feature")
            ->where("p.id = :product")
            ->andWhere("feature.name LIKE :truPicture")
            ->setParameter("product", $product)
            ->setParameter("truPicture", "TRU Picture Engine");

            return $em->getQuery()->getResult(); 
        }


        public function getSupportFile($product, $country) {
            $em = $this->createQueryBuilder("p")
            ->select("p", "file", "fileType")
            ->leftJoin("p.file", "file")
            ->leftJoin("file.country", "fileCountry")
            ->leftJoin("file.type", "fileType")
            ->where("file.fileName NOT LIKE :parameter")
            ->andWhere("file.fileName NOT LIKE :parameter1")
            ->andWhere("fileCountry = :fileC")
            ->andWhere("p.id = :productId")
            ->andWhere("fileType.id = :typeId")
            ->setParameter("parameter", "%blob%")
            ->setParameter("parameter1", "%image.png%")
            ->setParameter("fileC", $country)
            ->setParameter("productId", $product)
            ->setParameter("typeId", 10);

            return $em->getQuery()->getArrayResult(); 
        }


}
