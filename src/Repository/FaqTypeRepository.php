<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 14.01.2019
 * Time: 14:09
 */

namespace App\Repository;


use App\Entity\FaqType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FaqTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FaqType::class);
    }

    public function getFaqTypeById($id) {
        return $this->createQueryBuilder("f")
            ->select("f")
            ->where("f.id =:faqTypeId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("faqTypeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}