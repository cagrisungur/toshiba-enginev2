<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.01.2019
 * Time: 13:50
 */

namespace App\Repository;


use App\Entity\NewsText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class NewsTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsText::class);
    }

    public function getNews($lang) {
        return $this->createQueryBuilder("n")
            ->select("n" ,"news", "image")
            ->leftJoin("n.lang", "lang")
            ->leftJoin("n.news", "news")
            ->leftJoin("news.images", "image")
            ->where("lang.id =:langId")
            ->setParameter("langId", $lang)
            ->setMaxResults(3)
            ->getQuery()
            ->getArrayResult();
    }
}    