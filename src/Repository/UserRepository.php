<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Software
 * Date: 12.06.2018
 * Time: 10:42
 */

namespace App\Repository;


use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;


class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUser()
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->getQuery()
            ->getArrayResult();
    }

    public function getUserById($id) {
        return $this->createQueryBuilder("u")
            ->select("u", "userModule", "userLang", "module")
            ->innerJoin("u.module","userModule")
            ->innerJoin("userModule.module","module")
            ->leftJoin("u.lang","userLang")
            ->where("u.id =:userId")
            ->andWhere("u.deletedAt is NULL")
            ->setParameter("userId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}