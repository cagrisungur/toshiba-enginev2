<?php
/**
 * Created by PhpStorm.
 * User: Burcu
 * Date: 3.06.2018
 * Time: 22:49
 */

namespace App\Repository;

use App\Entity\UserGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserGroup::class);
    }

    public function getUserGroup($id)
    {
        return $this->createQueryBuilder('g')
            ->select('g')
            ->where('g.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getArrayResult();
    }
}