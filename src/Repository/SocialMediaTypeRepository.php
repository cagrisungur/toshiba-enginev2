<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 8.01.2019
 * Time: 11:08
 */

namespace App\Repository;


use App\Entity\SocialMediaType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SocialMediaTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialMediaType::class);
    }

    public function getSocialMediaTypeAdminById($id) {
        return $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.id = :typeId")
            ->setParameter("typeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}