<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 24.10.2018
 * Time: 10:06
 */

namespace App\Repository;


use App\Entity\SalesPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SalesPointRepository extends ServiceEntityRepository
{
    /**
     * SalesPointRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesPoint::class);
    }

    public function getSalesPoint() {
        return $this->createQueryBuilder("s")
            ->select("s", "country", "continent")
            ->leftJoin("s.country", "country")
            ->leftJoin("country.continent","continent")
            ->andWhere("s.deletedAt is NULL")
            ->andWhere("s.active = :isActive")
            ->orderBy("country.priority","ASC")
            ->addOrderBy("s.priority", "asc")
            ->setParameter("isActive", 1)
            ->getQuery()
            ->getResult();
    }

    public function getAllSalesPointById($id) {
        return $this->createQueryBuilder("s")
            ->select("s", "country", "continent")
            ->leftJoin("s.country", "country")
            ->leftJoin("country.continent","continent")
            ->orderBy("country.priority","ASC")
            ->andWhere("s.deletedAt is NULL")
            ->andWhere("s.id = :salesPointId")
            ->setParameter("salesPointId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}