<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 16:17
 */

namespace App\Repository;


use App\Entity\Faq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FaqRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faq::class);
    }

    public function faqSearch($lang, $query) {
        return $this->createQueryBuilder("f")
        ->select("f", "faqText","faqText.answer", "faqText.question", "faqCat","catText")
        ->leftJoin("f.text", "faqText")
        ->leftJoin("faqText.lang", "lang")
        ->leftJoin("f.faqCategory", "faqCat")
        ->leftJoin("faqCat.faqText", "catText")
        ->leftJoin("catText.lang", "faqCatLang")
        ->where("f.deletedAt is null")
        ->andWhere("lang.id = :langId")
        ->andWhere("faqCatLang.id = :catLangId")
        ->andWhere('faqText.question LIKE :term OR faqText.answer LIKE :term')
        ->setParameter("term", '%'.$query.'%')
        ->setParameter("langId", $lang)
        ->setParameter("catLangId", $lang)
        ->getQuery()
        ->getResult();
    }
}