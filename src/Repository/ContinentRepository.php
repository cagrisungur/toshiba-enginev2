<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 30.11.2018
 * Time: 15:35
 */

namespace App\Repository;


use App\Entity\Continent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ContinentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Continent::class);
    }

    public function getContinentById($id) {
        return $this->createQueryBuilder("c")
            ->select("c", "text")
            ->leftJoin("c.continentText","text")
            ->where("c.id =:continentId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("continentId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}