<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 11:27
 */

namespace App\Repository;


use App\Entity\FeaturedProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FeaturedProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeaturedProduct::class);
    }

    public function getAllFeaturedProductById($id) {
        return $this->createQueryBuilder("f")
            ->select("f", "product", "country", "language")
            ->leftJoin("f.product","product")
            ->leftJoin("f.country","country")
            ->leftJoin("f.lang","language")
            ->where("f.id = :featuredProductId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("featuredProductId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}