<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.11.2018
 * Time: 15:10
 */

namespace App\Repository;


use App\Entity\Series;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SeriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Series::class);
    }

    public function getAllSeriesById($id) {
        return $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.id =:seriesId")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("seriesId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}