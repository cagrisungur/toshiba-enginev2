<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.03.2019
 * Time: 10:57
 */

namespace App\Repository;


use App\Entity\Brochure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use  Doctrine\Common\Persistence\ManagerRegistry;

class BrochuresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brochure::class);
    }

    public function getBrochureById($id) {
        return $this->createQueryBuilder("b")
            ->select("b", "country")
            ->leftJoin("b.country","country")
            ->where("b.deletedAt is null")
            ->andWhere("b.id =:brochureId")
            ->setParameter("brochureId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}