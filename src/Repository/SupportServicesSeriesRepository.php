<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 5.12.2018
 * Time: 13:20
 */

namespace App\Repository;


use App\Entity\SupportServices;
use App\Entity\SupportServicesSeries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SupportServicesSeriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupportServicesSeries::class);
    }

    public function getSupportServices($serialNumber, $modelNo) {
     
        $query = $this->createQueryBuilder("a")
        ->select("b")
        ->innerJoin("App\Entity\SupportServices", "b", \Doctrine\ORM\Query\Expr\Join::WITH, "b.VBELN_VL = a.VBELN_VL")
        ->andWhere("a.SERINO = :seriNo")
        ->andWhere("b.MODELNO = :modelNo")
        ->setParameter("seriNo", $serialNumber)
        ->setParameter("modelNo", $modelNo)
        ->getQuery()
        ->getArrayResult();
       
        return $query;
    }

    public function serialExist($serialNumber) {
     
        $query = $this->createQueryBuilder("a")
        ->select("b")
        ->innerJoin("App\Entity\SupportServices", "b", \Doctrine\ORM\Query\Expr\Join::WITH, "b.VBELN_VL = a.VBELN_VL")
        ->where("a.SERINO = :seriNo")
        ->setParameter("seriNo", $serialNumber)
        ->getQuery()
        ->getArrayResult();
       
        return $query;
    }
}