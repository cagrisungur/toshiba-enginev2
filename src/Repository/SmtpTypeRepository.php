<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.11.2018
 * Time: 16:43
 */

namespace App\Repository;


use App\Entity\SmtpType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SmtpTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmtpType::class);
    }

    public function getAllSmtpTypeById($id) {
        return $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.deletedAt is NULL")
            ->andWhere("s.id = :smtpTypeId")
            ->setParameter("smtpTypeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}