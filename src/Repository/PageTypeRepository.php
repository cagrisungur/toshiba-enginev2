<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 16:33
 */

namespace App\Repository;


use App\Entity\PageType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class PageTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PageType::class);
    }

    public function getAllPageTypeById($id) {
        return $this->createQueryBuilder("p")
            ->select("p")
            ->where("p.id =:pageTypeId")
            ->andWhere("p.deletedAt is NULL")
            ->setParameter("pageTypeId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}