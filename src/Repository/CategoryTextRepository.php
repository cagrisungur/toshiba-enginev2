<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 13:33
 */

namespace App\Repository;


use App\Entity\CategoryText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CategoryTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryText::class);
    }

    public function getAllCategoryText() {
        return $this->createQueryBuilder("ct")
            ->select("ct")
            ->leftJoin("ct.category","category")
            ->leftJoin("ct.country","country")
            ->leftJoin("ct.lang","lang")
            ->getQuery()
            ->getArrayResult();
    }
}