<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 12:14
 */

namespace App\Repository;


use App\Entity\Feature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FeatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Feature::class);
    }

    public function getAllFeatureById($id) {
        return $this->createQueryBuilder("f")
            ->select("f", "featureImage", "featureText", "featureType", "country", "lang")
            ->leftJoin("f.image","featureImage")
            ->leftJoin("f.text","featureText")
            ->leftJoin("featureImage.type","featureType")
            ->leftJoin("featureText.country","country")
            ->leftJoin("featureText.lang","lang")
            ->where("f.id =:featureId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("featureId", $id)
            ->getQuery()
            ->getArrayResult();

    }
}