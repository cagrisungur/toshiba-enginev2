<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 16:08
 */

namespace App\Repository;


use App\Entity\SmtpSetting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SmtpSettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SmtpSetting::class);
    }

    public function getAllSmtpById($id) {
        return $this->createQueryBuilder("s")
            ->select("s", "country", "type")
            ->leftJoin("s.country","country")
            ->leftJoin("s.type","type")
            ->where("s.id =:smtpId")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("smtpId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}