<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 13:49
 */

namespace App\Repository;


use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function getContact($country) {
        return $this->createQueryBuilder("c")
            ->select("c")
            ->leftJoin("c.country","country")
            ->where("c.country = :country")
            ->andWhere("c.deletedAt is NULL")
            ->andWhere("c.active = :active")
            ->setParameter("active", 1)
            ->setParameter("country", $country)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getContactByIdAdmin($id) {
        return $this->createQueryBuilder("c")
            ->select("c", "country")
            ->leftJoin("c.country","country")
            ->where("c.id =:contactId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("contactId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}