<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 22.11.2018
 * Time: 12:05
 */

namespace App\Repository;


use App\Entity\FeaturedCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FeaturedCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeaturedCategory::class);
    }

    public function getAllFeaturedCategoryById($id) {
        return $this->createQueryBuilder("f")
            ->select("f", "category", "country", "language")
            ->leftJoin("f.category","category")
            ->leftJoin("f.country","country")
            ->leftJoin("f.lang","language")
            ->where("f.id = :featuredCategoryId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("featuredCategoryId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}