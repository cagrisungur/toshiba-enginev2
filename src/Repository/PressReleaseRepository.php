<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 11:50
 */

namespace App\Repository;


use App\Entity\PressRelease;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class PressReleaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PressRelease::class);
    }

    public function getAllPressReleaseById($id) {
        return $this->createQueryBuilder("p")
            ->select("p", "country", "lang", "text")
            ->leftJoin("p.pressReleaseText","text")
            ->leftJoin("text.country","country")
            ->leftJoin("text.lang","lang")
            ->where("p.id =:pressReleaseId")
            ->andWhere("p.deletedAt is NULL")
            ->setParameter("pressReleaseId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}