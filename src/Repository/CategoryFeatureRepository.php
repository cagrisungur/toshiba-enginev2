<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 12:28
 */

namespace App\Repository;


use App\Entity\CategoryFeature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


class CategoryFeatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryFeature::class);
    }

    public function getAllCategoryFeature() {
        return $this->createQueryBuilder("cf")
            ->select("cf")
            ->leftJoin("cf.category", "category")
            ->leftJoin("cf.type","type")
            ->leftJoin("cf.feature","feature")
            ->getQuery()
            ->getArrayResult();
    }
}