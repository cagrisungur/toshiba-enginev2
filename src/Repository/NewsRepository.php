<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.01.2019
 * Time: 13:50
 */

namespace App\Repository;


use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getAdminNewsById($id) {
        return $this->createQueryBuilder("n")
            ->select("n", "text", "type", "image", "country")
            ->leftJoin("n.text","text")
            ->leftJoin("n.images","image")
            ->leftJoin("image.type","type")
            ->leftJoin("n.country","country")
            ->where("n.id =:newsId")
            ->andWhere("n.deletedAt is null")
            ->andWhere("image.deletedAt is null")
            ->andWhere("text.deletedAt is null")
            ->setParameter("newsId", $id)
            ->getQuery()
            ->getArrayResult();
    }

    public function getGlobalNewsById($id, $country, $lang) {
        return $this->createQueryBuilder("n")
            ->select("n", "text", "type", "image", "country")
            ->leftJoin("n.text","text")
            ->leftJoin("n.images","image")
            ->leftJoin("text.lang","lang")
            ->leftJoin("image.type","type")
            ->leftJoin("n.country","country")
            ->where("n.id =:newsId")
            ->andWhere("n.deletedAt is null")
            ->andWhere("country.id =:countryId")
            ->andWhere("image.deletedAt is null")
            ->andWhere("text.deletedAt is null")
            ->andWhere("lang.id =:langId")
            ->setParameter("newsId", $id)
            ->setParameter("countryId", $country)
            ->setParameter("langId", $lang)
            ->getQuery()
            ->getArrayResult();
    }
}