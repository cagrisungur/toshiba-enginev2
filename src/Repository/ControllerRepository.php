<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 21.11.2018
 * Time: 15:17
 */

namespace App\Repository;


use App\Entity\Controller;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ControllerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Controller::class);
    }

    public function getAllControllerById($id) {
        return $this->createQueryBuilder("c")
            ->select("c", "image", "type")
            ->leftJoin("c.image","image")
            ->leftJoin("image.type","type")
            ->where("c.id = :controllerId")
            ->andWhere("c.deletedAt is NULL")
            ->setParameter("controllerId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}