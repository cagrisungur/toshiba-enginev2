<?php

namespace App\Repository;

use App\Entity\FeaturedSpec;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FeaturedSpecRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeaturedSpec::class);
    }

    public function getAllFeaturedSpec($country,$lang)
    {
        return $this->createQueryBuilder("f")
            ->select("f", "spec", "image", "type")
            ->leftJoin("f.spec", "spec")
            ->leftJoin("f.image", "image")
            ->leftJoin("image.type", "type")
            ->where("f.country = :country")
            ->andWhere("f.lang = :lang")
            ->andWhere("f.deletedAt is null")
            ->setParameter("country", $country)
            ->setParameter("lang", $lang)
            ->orderBy("f.priority", "ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllFeaturedSpecById($id)
    {
        return $this->createQueryBuilder("f")
            ->select("f", "spec", "image", "type", "country", "lang")
            ->leftJoin("f.country","country")
            ->leftJoin("f.lang","lang")
            ->leftJoin("f.spec", "spec")
            ->leftJoin("f.image", "image")
            ->leftJoin("image.type", "type")
            ->where("f.id =:featuredSpecId")
            ->andWhere("f.deletedAt is NULL")
            ->setParameter("featuredSpecId", $id)
            ->orderBy("f.priority", "ASC")
            ->getQuery()
            ->getArrayResult();
    }
}