<?php

namespace App\Repository;

use App\Entity\Specification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SpecificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specification::class);
    }

    public function getSpecificationById($id,$lang,$country)
    {
        return $this->createQueryBuilder("s")
            ->select("s", "category", "text")
            ->leftJoin("s.category", "category", "WITH", "category.deletedAt is NULL")
            ->leftJoin("s.text", "text", "WITH", "text.lang = :lang and text.country = :country and text.deletedAt is NULL")
            ->where("s.id = :id")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("id", $id)
            ->setParameter("country", $country)
            ->setParameter("lang", $lang)
            ->orderBy("s.priority", "ASC")
            ->orderBy("category.priority", "ASC")
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllSpecificationById($id)
    {
        return $this->createQueryBuilder("s")
            ->select("s", "category", "text")
            ->leftJoin("s.category", "category")
            ->leftJoin("s.text", "text")
            ->where("s.id = :id")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("id", $id)
            ->orderBy("s.priority", "ASC")
            ->getQuery()
            ->getArrayResult();
    }
}