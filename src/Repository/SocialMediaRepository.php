<?php

namespace App\Repository;

use App\Entity\SocialMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SocialMediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialMedia::class);
    }

    public function getSocialMedia($country)
    {
        return $this->createQueryBuilder("s")
            ->select("s", "type")
            ->leftJoin("s.type", "type")
            ->where("s.country = :country")
            ->andWhere("s.deletedAt is null")
            ->andWhere("s.active = :active")
            ->setParameter("active", 1)
            ->setParameter("country", $country)
            ->getQuery()
            ->getArrayResult();
    }

    public function getSocialMediaByCountryType($type, $country)
    {
        return $this->createQueryBuilder("s")
            ->select("s")
            ->where("s.country = :country")
            ->andWhere("s.type = :type")
            ->andWhere("s.deletedAt is NULL")
            ->setParameter("type", $type)
            ->setParameter("country", $country)
            ->getQuery()
            ->getArrayResult();
    }
}