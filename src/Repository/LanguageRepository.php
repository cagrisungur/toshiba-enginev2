<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 4.12.2018
 * Time: 14:12
 */

namespace App\Repository;


use App\Entity\Language;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class LanguageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Language::class);
    }

    public function getLanguageById($id) {
        return $this->createQueryBuilder("l")
            ->select("l")
            ->where("l.id =:languageId")
            ->andWhere("l.deletedAt is NULL")
            ->setParameter("languageId", $id)
            ->getQuery()
            ->getArrayResult();
    }
}