<?php

namespace App\Controller;

use App\Entity\SocialMedia;
use App\Entity\SocialMediaType;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SocialMediaController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/socialmedia",
     *     description="Get All Social Media",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     * @Route("/socialmedia", name="socialMedia", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getSocialMedia(SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var SocialMedia $socialMedia
         */
        $socialMedia = $em->getRepository(SocialMedia::class)->getSocialMedia($this->country);

        $socialMediaArray = [];
        foreach ($socialMedia as $social)
        {
            $socialArray = $social;
            $socialArray["typeIcon"] = $social['type']['icon'];
            $socialArray["typeName"] = $social['type']['name'];
            unset($socialArray['type']);

            $socialMediaArray [] = $socialArray;
        }

        $json = $serializer->serialize(
            $socialMediaArray,
            "json"
        );

        return new Response($json);
    }

}