<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 25.07.2018
 * Time: 15:27
 */

namespace App\Controller;
use App\Entity\FeaturedProduct;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class FeaturedProductController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/featuredProduct",
     *     description="Get Featured Product",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("featuredProduct", name="getFeaturedProduct", methods={"GET"})
     * @return Response
     * @param SerializerInterface $serializer
     */
    public function getFeaturedProduct(SerializerInterface $serializer)
    {
        $dataArray = [];
        $countryId = $this->country;
        $langId = $this->lang;

        $em = $this->getDoctrine()->getManager();

        /**
         * @var FeaturedProduct $featuredProduct
         */
        $featuredProduct = $em->getRepository(FeaturedProduct::class)->findBy(array(
            "deleted_at" => null,
            "country" => $countryId,
            "lang" => $langId
        ));
        foreach ($featuredProduct as $prod){
            $tmp = array(
                "productName" => $prod->getProduct()->getName(),
                "title" => $prod->getTitle(),
                "Description" => $prod->getDescription()
            );

                $image = $prod->getProduct()->getFeature();

                foreach ($image as $img) {
                   $new = $img->getFeature()->getImage();
                    foreach ($new as $news) {
                        $tmp["images"][$news->getType()->getCode()][] = array(
                            "name" => $news->getType()->getName(),
                            "path" => $news->getPath()
                        );

                    }

                }
            $dataArray[] = $tmp;
        }

        $json = $serializer->serialize(
            $dataArray,
            "json"
        );

        return new Response($json);
    }
}