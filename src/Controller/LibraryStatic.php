<?php

namespace App\Controller;

use App\Library\CustomController;
use App\Entity\StaticText;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LibraryStatic extends Controller 
{

    protected $container;

    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function globStat() {
       
        $em = $this->container->get('doctrine')->getManager();
        
        $staticTextObj = $this->getDoctrine()->getManager()->getRepository(StaticText::class)->findBy(array(
            "deletedAt" => null
        ));

        $headerRequest = $this->container->get("request_stack")->getCurrentRequest();
        $countryCode = $headerRequest->get('country');
        $langCode = $headerRequest->get('language');
        
        $countryObj = $em->getRepository(Country::class)->findOneBy(array(
            "code" => $countryCode,
            "deletedAt" => null
        ));
        
        $langObj = $em->getRepository(Language::class)->findOneBy(array(
            "code" => $langCode,
            "deletedAt" => null
        ));
        
        $dataArray = array();
        $tmp = [];
        /**
         * @var StaticText $statictext
         */
        foreach ($staticTextObj as $statictext){
            $translate = $statictext->getStaticTextTranslate($langObj);
            foreach ($translate as $translated) {
                $tmp[$statictext->getCode()] = $translated->getData();
            }
        }

        $dataArray = $tmp;
        $upperCaseArr = array_change_key_case($dataArray, CASE_UPPER);
        
        return $upperCaseArr;
    }
}