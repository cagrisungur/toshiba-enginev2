<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Entity\Product;
use App\Entity\ProductFile;
use App\Entity\Category;
use App\Entity\ProductCountry;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ProductStatus;
use App\Entity\Feature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class NewCustomPageController extends CustomController
{

    /**
     * @Route("/tru-pictureeng", name="truPictureEng")
     */
    public function truPictureEng() {

        $em = $this->getDoctrine()->getManager();

        $productArray = array();

        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));

        $products = $em->getRepository(Product::class)->findBy(array(
            "status" => $productStatus,
            "isShowTru" => 1,
            "active" => 1,
            "deletedAt" => null
        )
    );

        $truFeature = $em->getRepository(Feature::class)->find(75);

        foreach($products as $product) {
            $checkCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                "product" => $product,
                "country" => $this->country
            ));
            if($checkCountry) {

                $productInc = str_split($product->getLink(), 2);
                $productInc = $productInc[0];
                $text = $product->getLangText($this->lang);

                if(!isset($text[0])) {
                    $text = $product->getLangText($this->globalLang)[0]->getName();
                } else {
                    $text = $product->getName();
                }

                $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                //$newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                $fullPath = "";
                foreach($newImage as $image) {
                    if(strpos($image->getFileName(), "front") !== false) {
                        $fullPath = "http://63.35.162.212/uploads/files/files/".$image->getPath();
                    }
                    
                }

                $productArray[$product->getSeries()->getName()][] = array(
                    "id" => $product->getId(),
                    "link" => $product->getLink(),
                    "path" => $fullPath,
                    "serie" => $product->getSeries()->getName(),
                    "name" => $text,
                    "inc" => $productInc
                );
            }
        }


        $arr = array();

        // foreach ($productArray as $key => $item) {
        //     $arr[$item['serie']][$key] = $item;
        // }
        //print_r($productArray);exit;
        return $this->render("truPictureEngine.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "truProducts" => $productArray,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/landing-page", name="landingPage")
     */
    public function landingPage() {

        $em = $this->getDoctrine()->getManager();

        $productArray = array();

        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));

        $category = $em->getRepository(Category::class)->findOneBy(array(
            "link" => "ultra-hd-tv"
        ));

        $products = $em->getRepository(Product::class)->findBy(array(
            "status" => $productStatus,
            "active" => 1,
            "isLandingPage" => 1,
            "category" => $category,
            "deletedAt" => null
             )
        );

        //$truFeature = $em->getRepository(Feature::class)->find(75);

        foreach($products as $product) {
            $checkCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                "product" => $product,
                "country" => $this->country
            ));
            if($checkCountry) {

                $productInc = str_split($product->getLink(), 2);
                $productInc = $productInc[0];

                if($product->getText()[0] == "") {
                    $textName = $product->getLangText($this->globalLang)[0]->getName();
                } else {
                    $textName = $product->getText()[0]->getName();
                }
                $image = "";
                foreach($product->getImages() as $images) {
                    if($images->getType()->getCode() !== "video" && \strtolower($images->getName()) == "front" ) {
                        $image = $images->getPath();
                    }
                }
                $productArray[] = array(
                    "id" => $product->getId(),
                    "link" => $product->getLink(),
                    "path" => $image,
                    "serie" => $product->getSeries()->getName(),
                    "name" => $textName,
                    "code" => $product->getCode(),
                    "inc" => $productInc
                );
            }
        }

        $arr = array();

        foreach ($productArray as $key => $item) {
            $arr[$item['serie']][$key] = $item;
        }

        foreach ($arr as $key => $item) {
            $arr[$key] = array_values($item);
        }
        //print_r($arr);
        //print_r($arr);exit;
        return $this->render("landingPage.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "uhdProducts" => $arr,
            "lang" => $this->lang
        ));
    }
}
