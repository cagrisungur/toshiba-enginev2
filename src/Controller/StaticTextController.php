<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 26.07.2018
 * Time: 11:24
 */

namespace App\Controller;


use App\Entity\Language;
use App\Entity\StaticText;
use Swagger\Annotations as SWG;
use App\Library\CustomController;
use App\Library\StaticTextSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StaticTextController extends CustomController
{

    /**
     * @SWG\Get(
     *   path="/{country}/{language}/statictext",
     *   description="Get Static Text Translations",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     )
     * )
     * @Route("/statictext", name="getStaticTextTranslation", methods={"GET"})
     * @return Response
     * @param StaticTextSerializer $serializer
     */
    public function getStaticTextTranslation() {
        $em = $this->getDoctrine()->getManager();
        $text = $em->getRepository(StaticText::class)->findBy(array(
            "deletedAt" => null
        ));
        $dataArray = array();


        $tmp = array();

        // first of all, get global language translations
        foreach ($text as $statictext){
            $lang = $em->getRepository(Language::class)->find(2);

            $translate = $statictext->getStaticTextTranslate($lang); // 2-en lang
            foreach ($translate as $translated) {
                $tmp[$statictext->getCode()] = $translated->getData();
            }
        }

        // secondly, get local translations
        foreach ($text as $statictext){
            $translate = $statictext->getStaticTextTranslate($this->lang);
            foreach ($translate as $translated) {
                $tmp[$statictext->getCode()] = $translated->getData();
            }
        }
        $dataArray = $tmp;


        $upperCaseArr = array_change_key_case($dataArray, CASE_UPPER);

        return new JsonResponse($upperCaseArr);
    }
}
