<?php
namespace App\Controller;

use App\Library\CustomController;
use App\Entity\Product;
use App\Entity\Country;
use App\Entity\ProductRegister;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ProductRegisterController extends CustomController {
	
	/**
	 * @Route("/product-register", name="registerProduct")
	 */
	public function registerProduct() {
		$json = file_get_contents('php://input');
		$data = json_decode($json, true);
		$em = $this->getDoctrine()->getManager();

		$product = $em->getRepository(Product::class)->findOneBy(array(
			"id" => $data["product"]
		));
		$retailerCountry = $em->getRepository(Country::class)->findOneBy(array(
			"id" => $data["retailerCountry"]
		));
		$country = $em->getRepository(Country::class)->findOneBy(array(
			"id" => $data["country"]
		));

		$date = \DateTime::createFromFormat('Y-m-d', $data["dateOfPurchase"]);

		$register = new ProductRegister();
		$register->setProduct($product);
		$register->setRetailerCountry($retailerCountry);
		$register->setSerialNumber($data["serialNumber"]);
		$register->setTitle($data["title"]);
		$register->setFirstName($data["firstName"]);
		$register->setLastName($data["lastName"]);
		$register->setCountry($country);
		$register->setAddress($data["address"]);
		$register->setCity($data["city"]);
		$register->setPostcode($data["postcode"]);
		$register->setEmail($data["email"]);
		$register->setContact($data["contact"]);
		$register->setShare($data["share"]);
    $register->setDateOfPurchase($date);
    
    if(isset($data["retailerPostcode"])) {
      $register->setRetailerPostcode($data["retailerPostcode"]);
    }

    if(isset($data["retailerCity"])) {
      $register->setRetailerCity($data["retailerCity"]);
    }

    if(isset($data["retailerName"])) {
      $register->setRetailerName($data["retailerName"]);
    }

    if(isset($data["retailerAddress"])) {
      $register->setRetailerAddress($data["retailerAddress"]);
    }

    if(isset($data["retailerAddress2"])) {
      $register->setRetailerAddress2($data["retailerAddress2"]);
    }

    if(isset($data["retailerAddress2"])) {
      $register->setRetailerAddress2($data["retailerAddress2"]);
    }

    if(isset($data["address2"])) {
      $register->setAddress2($data["address2"]);
    }

    if(isset($data["phone"])) {
      $register->setPhone($data["phone"]);
    }

		$em->persist($register);
		$em->flush();

		return new JsonResponse(array(
            "status" => "200",
            "message" => "success"
        ));
	}
}
?>