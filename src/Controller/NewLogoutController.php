<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class NewLogoutController extends CustomController
{
    /**
    * @Route("/logout", name="logout")
    */
    public function logout(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        $session->clear();
        $em = $this->getDoctrine()->getManager();

        return $this->redirectToRoute("register", array(
            "country" => $this->country->getCode(),
            "language" => $this->lang->getCode(),
        ));
    }
}