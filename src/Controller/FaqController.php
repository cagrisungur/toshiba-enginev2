<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 16.10.2018
 * Time: 10:22
 */

namespace App\Controller;


use App\Entity\Faq;
use App\Library\CustomController;
use App\Library\FaqSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\SerializerInterface;

class FaqController extends CustomController
{
    /**
     * @SWG\Get(
     *   path="/{country}/{language}/faq",
     *   description="Faq",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="eu"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="en"
     *     )
     * )
     * @Route("/faq", name="getFaqText", methods={"GET"})
     */
    public function getFaqText(FaqSerializer $serializer) {
        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository(Faq::class)->findAll();
        $serializer->setLocale($this->country, $this->lang);
        $json = $serializer->serialize(
            $faqs,
            'json'
        );

        return new Response($json);
    }
}