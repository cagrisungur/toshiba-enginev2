<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;

use App\Library\CategorySerializer;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends CustomController
{





    function isExistChild($id){
        $conn = $this->getDoctrine()->getConnection();
        $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
    }



    function getChilds($id){
        $conn = $this->getDoctrine()->getConnection();
        $sql = 'SELECT id FROM category where parent_id='.intval($id);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $arr = $stmt->fetchAll();

        $all = array();
        foreach($arr as $row){
            $all[] = $row['id'];
            if($this->isExistChild($row['id'])){
                $res = $this->getChilds($row['id']);
                foreach( $res as $inRow){
                    $all[] = $inRow;
                }

            }

        }
        return $all;//
    }





    /**
     * @SWG\Get(
     *     path="/{country}/{language}/category/{parentId}",
     *     description="Get All Category By ParentId",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="parentId",
     *          in="path",
     *          required=true,
     *          type="integer"
     *      )
     * )
     *
     * @SWG\Get(
     *     path="/{country}/{language}/category",
     *     description="Get All Category",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("/category/{parentId}", name="getCategoryAndProductByParentId", methods={"GET"})
     * @Route("/category", name="getAllCategory", methods={"GET"})
     * @param CategorySerializer $serializer
     * @param $parentId
     * @return Response
     */
    public function getCategoryAndProductByParentId(CategorySerializer $serializer, $parentId = null)
    {
        $em = $this->getDoctrine()->getManager();

        if($parentId) {

            $products = array();
            for($x=0;$x<30;$x++){
                $products[] = $x;
            }


            $catParent = $em->getRepository(Category::class)->findBy(
                array(
                    "link" => $parentId,
                    "active" => 1,
                    "deletedAt" => null
                ),
                array("priority" => "ASC")
            );


/*
$catParent = $em->getRepository(Product::class)->getProductByIdArray();



            print_r(get_class_methods($catParent[0]));
            die();*/

            /*    $catParent = $em->getRepository(Category::class)->findBy(
                    array(
                        "link" => $parentId,
                        "active" => 1,
                        "deletedAt" => null
                    ),
                    array("priority" => "ASC")
                );*/







        }else {
            $catParent = $em->getRepository(Category::class)->findBy(
                array(
                    "parentId" => null,
                    "active" => 1,
                    "deletedAt" => null
                ),
                array("priority" => "ASC")
            );
            $secondData=array();
        }


        $serializer->setLocale($this->country, $this->lang);

//        $json = $serializer->serialize(
//            $catParent,
//            "json",array(),array('em'=>$this->getDoctrine(),'data'=>array($catParent[0]->getID()))
//        );
//
//        $json = json_decode($json);

        $json = $serializer->serialize(
            $catParent,
            'json'
        );

        $json = json_decode($json);

        //print_r($json);

       // die();

       $excepts =array();
       foreach($json->categories as $row){
           foreach($row->child as $inRow){
               $excepts[$inRow->id] = array();
               foreach($inRow->child as $ininRow){
                   $excepts[$ininRow->id] = array();
               }
           }
       }


        $conn = $this->getDoctrine()->getConnection();
        $sql = 'SELECT * from cat_feature_except';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $rawExcepts = $stmt->fetchAll();

        foreach($rawExcepts as $row){
            $excepts[$row["cat"]][] = $row["feature"];

        }
        $json->excepts = $excepts;


      //  print_r($excepts);


    $json = json_encode($json);
        return new Response($json);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @Route("categorybyid/{id}", name="getCategoryById", methods={"GET"})
     */
    public function getCategoryById($id) {
        $em = $this->getDoctrine()->getManager();
        $country = $this->country;
        $lang = $this->lang;

        $category = $em->getRepository(Category::class)->getCategoryById($lang, $country, $id);

        return new JsonResponse($category);
    }
}

      /*
        for($x=0;$x<count($json->categories);$x++){
          //$json->categories[$x]->feature

          $features = $json->categories[$x]->feature;
          $safeFeatures = [];
          for($y=0;$y<count($features);$y++){
            if(isset($excepts[$json->categories[$x]->id])){
              if(!in_array($features[$y]->id,$excepts[$json->categories[$x]->id])){
                  $safeFeatures[] = $features[$y];
              }
            }else{
              $safeFeatures[] = $features[$y];
            }

          }
          $json->categories[$x]->feature = $safeFeatures;
            //print_r($json->categories[$x]->feature);
        }
      //  print_r($json->categories[0]->feature);

        //$json = $json->categories;
      //  print_r($json);
        //die();

  $json = json_encode($json);


  */






  /*
              $sql = 'SELECT id FROM category where link=?';

              $conn = $this->getDoctrine()->getConnection();
              $stmt = $conn->prepare($sql);
              $stmt->execute(array($parentId));
              $arr = $stmt->fetchAll();

              if (isset($arr[0])) {
                  $catID = $arr[0]['id'];
              } else {
                  die("unknown category");
              }


              $cats = $this->getChilds($catID);
              $cats[] = intval($catID);

              $secondData = array();
              foreach($cats as $inRow){
                  //echo '1,';
                  //echo $inRow.',';
                  $tmp = $em->getRepository(Category::class)->findBy(
                          array(
                              "parentId" => $inRow,
                              "active" => 1,
                              "deletedAt" => null
                          ),
                          array("priority" => "ASC")
                  );
                  $secondData[] = $tmp;
              }


  */
