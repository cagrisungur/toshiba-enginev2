<?php

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EnvironmentalPolicyController extends CustomController
{
    /**
     * @Route("/enviromental-policy", name="environmentalPolicy")
     */
    public function environmentalPolicy() {

        return $this->render("enviromentalPolicy.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}