<?php

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Product;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyProduct;
use App\Entity\FeaturedCategory;
use App\Entity\SupportServicesSeries;
use App\Entity\CustomPage;
use App\Entity\StaticText;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NewWarrantyProductController extends CustomController
{
 /**
     * @Route("/add-newProduct", name="addNewProduct")
     */
    public function addNewProduct(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if(!$session->get("userInfo")["id"]) {
            return $this->redirectToRoute("register", array(
                "country" => $this->country->getCode(),
                "language" => $this->lang->getCode(),
            ));
        }
        $message = "";
        $em = $this->getDoctrine()->getManager();

        if(isset($_POST["submit"])) {
            if(isset($_POST["serial"]) && isset($_POST["model"])) {
                $serialNumber = $_POST["serial"];
                $modelNo = $_POST["model"];
        
                $checkProductExistBefore = $em->getRepository(WarrantyProduct::class)->findBy(array(
                    "serialNo" => $serialNumber
                ));
        
                $isSerialExist = $em->getRepository(SupportServicesSeries::class)->serialExist($serialNumber);
                $isModelExist = $em->getRepository(SupportServicesSeries::class)->getSupportServices($serialNumber, $modelNo);
        
                if(count($checkProductExistBefore) > 0) {
                    $message = $this->staticText["SUPPORTSERIALNUMBERAVAILABLE"];
        
                } else if(!$isSerialExist) {
                    $message = array(
                        "error" => $this->staticText["SUPPORTSERIALNUMBERNOTEXIST"]
                    );
                
                } else if(!$isModelExist) {
                    $message = array(
                        "error" => "Serial number and model number do not match each other. You can click ”i” for help."
                    );
        
                } else {
                    $date = $_POST['date'];
                    $dealer = $_POST['place'];
                    $model = $_POST['model'];
                    $serialNumber = $_POST["serial"];
                    $file = $_FILES['proof']['tmp_name'];
                    $fileName = $_FILES['proof']['name'];
                    $ext = pathinfo($fileName);
                    $ext = '.' . $ext['extension'];
                    $date = new \Datetime($date);
                    if (! in_array($ext, array('.jpg','.jpeg','.pdf','.png'))) {
                        $fileError = 'Wrong file format!';
                        $message = $fileError;
                    }
        
                    $imageLink = md5(time().$fileName).$ext;
        
                    $isUploaded = move_uploaded_file($file,'./uploads/files/'.$imageLink);
                    if($isUploaded) {
                        $user = $em->getRepository(WarrantyUser::class)->find($session->get("userInfo")["id"]);
                        
                        $userProduct = new WarrantyProduct();
                        $userProduct->setUser($user);
                        $userProduct->setSerialNo($serialNumber);
                        $userProduct->setModelNo($modelNo);
                        $userProduct->setSalesDate($date);
                        $userProduct->setDealer($dealer);
                        $userProduct->setSalesDoc($imageLink);
                        $userProduct->setStatus(0);

                        $em->persist($userProduct);
                        $em->flush();

                        $message = "Product added successfuly";
                        
                    }
                }
            }
        }
        

        $em = $this->getDoctrine()->getManager();

        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));

       return $this->render("addNewProduct.html.twig", array(
           "staticText" =>  $this->staticText,
           "categories" => $this->category,
           "country" => $this->country,
           "lang" => $this->lang ,
           "countries" => $countries,
           "message" => $message
       ));
    }
}