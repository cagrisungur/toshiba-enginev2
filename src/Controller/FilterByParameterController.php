<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.09.2018
 * Time: 16:35
 */

namespace App\Controller;


use App\Entity\Product;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class FilterByParameterController extends CustomController
{
    /**
     * @SWG\Post(
     *   path="/{country}/{language}/filteredproduct",
     *   description="filter products",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *      name="categoryId",
     *      in="formData",
     *      required=false,
     *      type="integer"
     *
     *     ),
     *   @SWG\Parameter(
     *      name="minSize",
     *      in="formData",
     *      required=false,
     *      type="integer"
     *     ),
     *   @SWG\Parameter(
     *      name="maxSize",
     *      in="formData",
     *      required=false,
     *      type="integer"
     *     ),
     *   @SWG\Parameter(
     *      name="limit",
     *      in="formData",
     *      required="true",
     *      type="integer"
     *     ),
     *   @SWG\Parameter(
     *      name="offset",
     *      in="formData",
     *      required="true",
     *      type="integer"
     *     ),
     *   @SWG\Parameter(
     *      name="smart",
     *      in="formData",
     *      required=false,
     *      description="0=false, 1=true",
     *      type="integer"
     *     ),
     *  @SWG\Parameter(
     *      name="minFt",
     *      in="formData",
     *      required=false,
     *      type="integer"
     *     ),
     *  @SWG\Parameter(
     *      name="maxFt",
     *      in="formData",
     *      required=false,
     *      type="integer"
     *     )
     * )
     * @Route("/filteredproduct", name="filterByParameter", methods={"POST"})
     */
    public function filterByParameter() {

        $limit = $_POST["limit"];
        $offSet = $_POST["offset"];

        $catParent = $this->getDoctrine()->getRepository(Product::class)
            ->getProductByCategory($limit, $offSet, $this->country, $this->lang);

        return new JsonResponse($catParent);
    }
}