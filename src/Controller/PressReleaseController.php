<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 31.07.2018
 * Time: 11:54
 */

namespace App\Controller;


use App\Entity\PressRelease;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PressReleaseController extends CustomController
{
    /**
     * @SWG\Get(
     *   path="/{country}/{language}/pressRelease",
     *   description="Get Press Release Texts",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     )
     * )
     * @Route("pressRelease", name="getPressRelease", methods={"GET"})
     */
    public function getPressRelease() {

        $tmp = array();
        $em = $this->getDoctrine()->getManager();

        /**
         * @var PressRelease $pressText
         */
        $pressText = $em->getRepository(PressRelease::class)->findBy(array(
            "active" => 1,
            "deletedAt" => null),
            array("priority" => "ASC")
        );

        /**
         * @var PressRelease $text
         */
        foreach ($pressText as $text) {
            $press = $text->getPressReleaseText($this->country, $this->lang);
            $tmp[] = array(
                "id" => $text->getId(),
                "image" => $text->getImage(),
                "title" => $press[0]->getTitle(),
                "description" => $press[0]->getDescription(),
                "pdf" => $press[0]->getPdf(),
                "sef" => $press[0]->getSef(),
                "createdAt" => ($press[0]->getCreatedAt())->format('Y-m-d h:i:s')
            );
        }

        return new JsonResponse($tmp);
     }
}