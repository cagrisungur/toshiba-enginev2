<?php
namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Country;
use App\Entity\ProductCountry;
use App\Entity\FiltersValue;
use App\Entity\FiltersValueProduct;
use App\Entity\FiltersCategory;
use App\Entity\ProductFile;
use App\Entity\ProductStatus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;

class NewCategoryController extends CustomController
{

    public $inputFileType = 'Xlsx';

    /**
     * @Route("/newCategory/{sef}", name="getCategoryBySef")
     */
    public function getCategoryBySef($sef) {
        $em = $this->getDoctrine()->getManager();

        $dataArray = array();
        $productArray = array();
        $catProduct = array();
        //print_r($sef);exit;
        $category = $em->getRepository(Category::class)->findOneBy(array(
            "link" => $sef,
            "deletedAt" => null
        ));


        //print_r($category->getName());exit;
        //print_r($category->getName());exit;
        $filterArray = array();

        $findFilters = $em->getRepository(FiltersCategory::class)->findBy(array(
            "category" => $category
        ));

        foreach($findFilters as $filters) {
            $filterCategory = $filters->getFilters();
            if($filterCategory->getActive() == 1) {
                // print_r($filterCategory->getId());
                $filterValues = $em->getRepository(FiltersValue::class)->findBy(array(
                    "filtersCategory" => $filters,
                    "category" => $category
                ));

                $tmp = array();
                foreach($filterValues as $value) {
                    //print_r($value->getName());
                    $tmp[] = array(
                        "id" => $value->getId(),
                        "name" => $value->getName()
                    );
                }

                $filterArray[] = array(
                    "filterCategoryId" =>  $filterCategory->getId(),
                    "filterCategoryName" =>  $filterCategory->getName(),
                    "values" => $tmp
                );
            }
        }
        //print_r($filterArray);exit;
        $products = $category->getProduct();

        $catArray = array();

        $catArray = array(
            "categoryName" => $category->getCatText($this->lang)[0]->getName(),
            "categoryLink" => $category->getLink()
        );

        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));

        foreach($products as $product) {
            $series = $product->getSeries();
            $checkProductCountry = $em->getRepository(ProductCountry::class)->findBy(array(
                "product" => $product,
                "country" => $this->country
            ));

            if($checkProductCountry) {
                if($series) {
                    $findSerieProducts = $em->getRepository(Product::class)->seriesGroupProduct($product->getId(), $this->lang, $productStatus);

                    // print_r($findSerieProducts);exit;
                    foreach($findSerieProducts as $serieProduct) {
                        
                        $productInc = str_split($serieProduct->getName(), 2);
                        $productInc = $productInc[0];
                        if($serieProduct->getSeries() != null) {
                            $checkSerieProdCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                                "country" => $this->country,
                                "product" => $serieProduct
                            ));
                            if($checkSerieProdCountry) {
                                if($serieProduct->getText()[0] == "") {
                                    $textName = $product->getLangText($this->globalLang)[0]->getName();
                                } else {
                                    $textName = $serieProduct->getText()[0]->getName();
                                }
    
                                if($serieProduct->getSeries()->getLangText($this->lang)[0] == null) {
                                    $serieDesc = "";
                                } else {
                                    $serieDesc = $serieProduct->getSeries()->getLangText($this->lang)[0]->getDescription();
                                }
    
                                // $serieImg = $serieProduct->getImages()[0]->getPath();
                                // $pImage = $em->getRepository(ProductFile::class)->getProductImage($serieProduct->getId());
    
                                // foreach($pImage as $image) {
                                //     if(strpos($image->getName(), 'front') == true){
                                //         $serieImg = $image->getPath();
                                //     }
                                // }
    
                                //print_r($productArray["images"]);exit;
                                $ourImg = "";
                                // if(strpos($serieProduct->getImages()[0]->getName(), "front") == true && $serieProduct->getImages()[0]->getType()->getCode() !== "video") {
                                //     $ourImg = $serieProduct->getImages()[0]->getPath();
                                // }
    
                                $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
    
                                foreach($newImage as $newImg) {
                                    if(strpos($newImg->getFileName(), "front") !== false && $newImg->getType()->getCode() !== "video") {
                                        $ourImg = $newImg->getPath();
                                    }
                                }
    
    
    
                               if($checkSerieProdCountry) {
                                   if($serieProduct->getText() != null) {
                                    $productArray[$serieProduct->getSeries()->getCode()]["priority"] = $serieProduct->getSeries()->getPriority();
    
                                    $productArray[$serieProduct->getSeries()->getCode()][] = array(
                                        "productId" => $serieProduct->getId(),
                                        "name" => $serieProduct->getName(),
                                        "textName" => $textName,
                                        "link" => $serieProduct->getLink(),
                                        "seriesDescription" => $serieDesc,
                                        "serie" => $serieProduct->getSeries()->getName(),
                                        "image" => $ourImg,
                                        "seriePrio" => $serieProduct->getSeries()->getPriority(),
                                        "inc" => $productInc
                                    );
                                   }
                                   $this->arraySortByColumn($productArray[$serieProduct->getSeries()->getCode()], 'inc');
                               }
                            }
                            
                        }

                    }
                }
            }
        }
        $this->arraySortByColumn($productArray, 'priority');
        //print_r($productArray);exit;
        return $this->render("premium4KSmart.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "category" => $category,
            "catInfo" => $catArray,
            "productCat" => $productArray,
            "filters" => $filterArray
        ));
    }

    /**
     * @Route("/filtered-products", name="getFilteredProducts")
     */
    public function getFilteredProducts() {
        $data = json_decode(stripslashes($_POST['products']));
        $em = $this->getDoctrine()->getManager();

        $categoryId = intval(json_decode(stripslashes($_POST["category"])));

        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));
        $productArray = array();
        $data = array_filter($data);

        $newFilterArray = array();

        foreach ($data as $d) {
            foreach($d as $filterValue) {
                $newFilterArray[] = $filterValue;
            }
        }
        $counted = count($newFilterArray);

        $filterValueIdString = '('.implode(",",$newFilterArray).')';
        //print_r($filterValueIdString);exit;
        $conn = $em->getConnection();
        $sql = 'SELECT filters_value_product.product_id FROM filters_value_product
                left join filters_value as fval on fval.id = filters_value_product.filter_value_id
                left join product on product.id = filters_value_product.product_id
                where product.category_id = '.$categoryId;//.' and  fval.id IN '.$filterValueIdString.'';
                if($counted>0){$sql .= " and fval.id IN ".$filterValueIdString;}

            $stmt = $conn->prepare($sql);

            $stmt->execute();
            $arr = $stmt->fetchAll();
            $filtered = array();
            $newArray = array();

            foreach($arr as $row) {
                $filtered[] = $row['product_id'];
            }

            if($counted > 1) {
                $countVal = array_count_values($filtered);
                foreach($filtered as $key => $value) {
                    //echo '<pre>';
                    //print_r($counted."--".$countVal[$value]."-->".$value);
                    if($countVal[$value] == $counted) {
                        //echo '^gg';
                        // echo '<pre>';
                        // print_r($countVal[$value]);
                        // echo '<pre>';
                        // print_r($value);
                        $newArray[] = $value;

                    }

                }

                $filtered = $newArray;
                $filtered = array_unique($filtered);
            }else {
                $filtered = array_unique($filtered);
            }

        $productArray = array();
        //print_r($filtered);exit;
        foreach($filtered as $productId) {
            $product = $em->getRepository(Product::class)->find($productId);
            $series = $product->getSeries();

            $productInc = str_split($product->getName(), 2);
            $productInc = $productInc[0];

            // $productArray[$serieProduct->getSeries()->getCode()][] = array(
            //     "productId" => $serieProduct->getId(),
            //     "name" => $serieProduct->getName(),
            //     "textName" => $textName,
            //     "link" => $serieProduct->getLink(),
            //     "serie" => $serieProduct->getSeries()->getName(),
            //     "image" => $serieProduct->getImages()[0]->getPath(),
            //     "inc" => $productInc
            // );

            if($product->getText()[0] == "") {
                $textName = $product->getLangText($this->globalLang)[0]->getName();
            } else {
                $textName = $product->getText()[0]->getName();
            }
            $serieName = "";
            $serie = $product->getSeries();
            if($serie) {
                $serieName = $serie->getName();
            }

            $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                //$newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
            $fullPath = "";
            foreach($newImage as $image) {
                if(strpos($image->getFileName(), "front") !== false) {
                    $fullPath = "http://63.35.162.212/uploads/files/files/".$image->getPath();
                }

            }

            $productArray[] = array(
                "productId" => $product->getId(),
                "name" => $product->getName(),
                "textName" => $textName,
                "link" => $product->getLink(),
                "serie" => $serie,
                "inc" => $productInc,
                "image" => $fullPath

            );
        }
        if(count($productArray) > 0) {
            return new JsonResponse($productArray);
        }  else {
            return new JsonResponse(array("error" => "NOMATCH"));
        }

    }


    function dummy() {
        exit;
        $em = $this->getDoctrine()->getManager();

        $conn = $em->getConnection();

        // $sql = "SELECT product.name,product.id FROM product LEFT JOIN category ON category.id = product.category_id LEFT JOIN product_country ON product_country.product_id = product.id  where product.active = 1 and product.status_id = 1 and product.deleted_at is null and product.category_id = 3 and product_country.country_id = 39";

        $sql = 'SELECT product.id, product_spec.value
        FROM product
        left JOIN product_spec on  product_spec.product_id = product.id
        left JOIN product_country ON product_country.product_id = product.id
        where product.category_id = 6
        and product.active = 1
        and product.status_id = 1
        and product.deleted_at is null
        and product_spec.spec_id = 121
        and product_country.country_id = 39
        and product_spec.deleted_at is null
        and product_spec.value like "%yes%"
        group by product.id';

        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $arr = $stmt->fetchAll();
        //print_r($arr);exit;
        $category = $em->getRepository(Category::class)->find(6);
        foreach($arr as $row) {
            // $productInc = str_split($row["name"], 2);
            // $productInc = $productInc[0];
            //print_r($row["name"]);
            $filterCategory = $em->getRepository(FiltersCategory::class)->find(18);

            $findInc = $em->getRepository(FiltersValue::class)->findOneBy(array(
                "filtersCategory" => $filterCategory,
                "category" => $category,
                "code" => "soundByOnkyo"
            ));

            $findProduct = $em->getRepository(Product::class)->find($row["id"]);

            if($findInc) {
                $newFilterProducts = new FiltersValueProduct;
                $newFilterProducts->setProduct($findProduct);
                $newFilterProducts->setFiltersValue($findInc);

                $em->persist($newFilterProducts);
                $em->flush();

            }
        }
        exit;
    }

    function arraySortByColumn(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        return array_multisort($sort_col, $dir, $arr);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("filter-import-excel", name="filterNewExcel", methods={"POST"})
     */
    public function filterNewExcel() {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();

        $filterValue  = $em->getRepository(FiltersValue::class)->find(97);
        $status = $em->getRepository(ProductStatus::class)->find(1);
        $country = $em->getRepository(Country::class)->find(39);

        for ($row = 2; $row <= $highestRow; $row++) {

                $prodVal = $sheet2->getCell('A' . $row)->getValue();

                $findProd = $em->getRepository(Product::class)->findOneBy(array(
                    "code" => $prodVal,
                    "active" => 1,
                    "status" => $status,
                    "deletedAt" => null,
                ));

                if($findProd) {
                    $checkCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                        "product" => $findProd,
                        "country" => $country
                    ));
                    if($checkCountry) {
                        $category = $findProd->getCategory();
                        $val = $sheet2->getCell('E' . $row)->getValue();
                        if($category->getId() == 3) {
                            if($val == "yes") {
                                // echo $prodVal;exit;
                                $newValProd = new FiltersValueProduct();
                                $newValProd->setFiltersValue($filterValue);
                                $newValProd->setProduct($findProd);

                                $em->persist($newValProd);
                                $em->flush();
                            }
                        }
                    }
                }
        }
        exit;
    }
}
