<?php

namespace App\Controller;

use App\Library\CustomController;
use App\Entity\ProductFile;
use App\Library\ProductSerializer;
use App\Entity\Product;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MediaLibraryController extends CustomController
{
  /**
   * @Route("/media-library", name="mediaLibrary")
   */
  public function mediaLibrary() {

      $em = $this->getDoctrine()->getManager();

      $products = $em->getRepository(Product::class)->findBy(array(
          "deletedAt" => null
      ));

      $productArray = array();
      $count = 0;

      foreach($products as $product) {
          //if($product->getIfCountryExist($this->country) !== null) {
              /*$productImages = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
              $imgArray = array();
              foreach($productImages as $image) {
                  $imgArray[] = array(
                      "id" => $image->getId(),
                      "path" => "http://63.35.162.212/uploads/files/files/".$image->getPath()
                  );
              }*/
              $imgArray[]=array(array("id"=>"","path"=>""));
              $productArray[] = array(
                  "name" => $product->getText()[0]->getName(),
                  "code" => $product->getName(),
                  "link" => $product->getLink(),
                  "images" => $imgArray
              );
          //}
          $count++;

      }


      return $this->render("mediaLibrary.html.twig", array(
          "staticText" =>  $this->staticText,
          "categories" => $this->category,
          "country" => $this->country,
          "lang" => $this->lang,
          "products" => $productArray
      ));
  }


  /**
   * @Route("/media-library-ajax/{code}", name="mediaLibraryAjax")
   */
  public function mediaLibraryAjax($code) {

      $em = $this->getDoctrine()->getManager();

      $products = $em->getRepository(Product::class)->findBy(array(
          "deletedAt" => null,
          "code" => $code
      ));

      $productArray = array();
      $count = 0;

      foreach($products as $product) {
          //if($product->getIfCountryExist($this->country) !== null) {
              $productImages = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
              $imgArray = array();
              foreach($productImages as $image) {
                  
                  $imgArray[] = array(
                      "id" => $image->getId(),
                      "path" => "http://63.35.162.212/uploads/files/files/".$image->getPath()
                  );
              }
              $imgArray[]=array(array("id"=>"","path"=>""));
              $productArray[] = array(
                  "name" => $product->getText()[0]->getName(),
                  "code" => $product->getName(),
                  "link" => $product->getLink(),
                  "images" => $imgArray
              );
          //}
          $count++;

      }

      $productArray = $productArray[0];
      $response = new Response(json_encode($productArray));
      $response->headers->set('Content-Type', 'application/json');

      return $response;


  }



}
