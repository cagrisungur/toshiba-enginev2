<?php

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Product;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyProduct;
use App\Entity\WarrantyHash;
use App\Entity\FeaturedCategory;
use App\Entity\CustomPage;
use App\Entity\StaticText;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NewWarrantyUserController extends CustomController
{
    /**
     * @Route("/edit-profile", name="editProfile")
     */
    public function editProfile(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if(!$session->get("userInfo")["id"]) {
            return $this->redirectToRoute("register", array(
                "country" => $this->country->getCode(),
                "language" => $this->lang->getCode(),
            ));
        }
        $message = "";
        $user = $em->getRepository(WarrantyUser::class)->find($session->get("userInfo")["id"]);

        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));

        if(isset($_POST["submit"])) {
            $user->setName($_POST["name"]);
            $user->setEmail($_POST["email"]);
            $user->setPhone($_POST["phone"]);
            $date = new \Datetime($_POST["birthdate"]);
            $user->setBirthDate($date);

            $em->flush();

            $message = "Changes saved successfully";
        }

       return $this->render("editProfile.html.twig", array(
           "staticText" =>  $this->staticText,
           "categories" => $this->category,
           "country" => $this->country,
           "lang" => $this->lang,
           "countries" => $countries,
           "user" => $user,
           "message" => $message
       ));
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        //$session->clear();
        //print_r($session->get("userInfo")["id"]);exit;
        if(!$session->get("userInfo")["id"]) {
            return $this->redirectToRoute("register", array(
                "country" => $this->country->getCode(),
                "language" => $this->lang->getCode(),
            ));
        }

        $productArray = array();

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(WarrantyUser::class)->find($session->get("userInfo")["id"]);
        $warrantyProduct = $em->getRepository(WarrantyProduct::class)->findBy(array(
            "user" => $user
        ));
        foreach($warrantyProduct as $userProduct) {
            $findProductData = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $userProduct->getModelNo()
            ));
            if($findProductData) {
                $productArray[] = array(
                    "id" => $findProductData->getId(),
                    "code" => $findProductData->getCode(),
                    "image" => $findProductData->getImages()[0]->getPath(),
                    "series" => $findProductData->getSeries()->getName(),
                    "status" => $userProduct->getStatus(),
                );
            }
        }
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));
       
       return $this->render("dashboard.html.twig", array(
           "staticText" =>  $this->staticText,
           "categories" => $this->category,
           "country" => $this->country,
           "lang" => $this->lang,
           "countries" => $countries,
           "productArray" => $productArray,
           "user" => $user
       ));
    }
    /**
     * @Route("forgot-password", name="forgotPassword")
     */
    public function forgotPassword() {
        $em = $this->getDoctrine()->getManager();
        $message = "";
        if(isset($_POST["submit"])) {
            if(isset($_POST["email"])) {
                $mail = $_POST["email"];
                $hash = md5(rand(0, 1000));    
                $findUser = $em->getRepository(WarrantyUser::class)->findOneBy(array(
                    "email" => $mail
                ));
                if($findUser) {
                    $warrantyHash = new WarrantyHash();
                    $warrantyHash->setWarrantyUser($findUser);
                    $warrantyHash->setHash($hash);
                    $warrantyHash->setIsForgetPassword(1);

                    $em->persist($warrantyHash);
                    $em->flush();

                    $this->sendForgotMail($findUser->getEmail(), $hash, $findUser->getName());
                    $message = "Mail send successfully if your mail is correct.";        
                } 
            }
        }

        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));

        return $this->render("registerEmail.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "countries" => $countries,
            "message" => $message
        ));
    }

    /**
     * @Route("reset-password", name="passwordReset")
     */
    public function passwordReset(Request $request) {
        if(isset($_GET["email"]) && isset($_GET["hash"])) {
            $em = $this->getDoctrine()->getManager();

            $email = $_GET["email"];
            $hash = $_GET["hash"];
            $message = "";
            $session = $request->getSession();
            if(!$session) {
                session_start();
            }

            $findUser = $em->getRepository(Warrantyuser::class)->findOneBy(array(
                "email" => $email
            ));

            $warrantyHashCheck = $em->getRepository(WarrantyHash::class)->findOneBy(array(
                "warrantyUser" => $findUser,
                "hash" => $hash,
                "isForgetPassword" => 1
            ));
            
            // if($warrantyHashCheck) {
            //     echo 'gg';exit;
            // } else {
            //     echo 'cc';exit;
            // }
            if($warrantyHashCheck) {
                if($warrantyHashCheck->getStatus() == 1) {
                    $expiredMessage = "This link is used or expired, please get a new link for action.";

                    return $this->render("supportRegisterPassword/resetPassword.html.twig", array(
                        "staticText" =>  $this->staticText,
                        "categories" => $this->category,
                        "country" => $this->country,
                        "lang" => $this->lang,
                        "message" => $message,
                        "expiredMessage" => $expiredMessage
                    ));
                }
                if(isset($_POST["submit"])) {
                    // print_r($email);
                    // exit;
                    $password = isset($_POST["password"]);
                    $rePassword = isset($_POST["re-password"]);

                    if($password && $rePassword) {
                        $password = $_POST["password"];
                        $rePassword = $_POST["re-password"];

                        
                        if($password == $rePassword) {
                            $findUser->setPassword(md5($password));
                            $warrantyHashCheck->setStatus(1);
                            $em->flush();

                            return $this->render("registerHome.html.twig", array(
                                "staticText" =>  $this->staticText,
                                "categories" => $this->category,
                                "country" => $this->country,
                                "lang" => $this->lang,
                                "message" => $message,
                            ));
                        } else {
                            $message = "Please enter the same password values";

                            return $this->render("supportRegisterPassword/resetPassword.html.twig", array(
                                "staticText" =>  $this->staticText,
                                "categories" => $this->category,
                                "country" => $this->country,
                                "lang" => $this->lang,
                                "message" => $message
                            ));
                        }
                    } else {
                        $message = "Please fill the passwords";

                        return $this->render("supportRegisterPassword/resetPassword.html.twig", array(
                            "staticText" =>  $this->staticText,
                            "categories" => $this->category,
                            "country" => $this->country,
                            "lang" => $this->lang,
                            "message" => $message
                        ));
                    }
                    

                }
            }
            
            return $this->render("supportRegisterPassword/resetPassword.html.twig", array(
                "staticText" =>  $this->staticText,
                "categories" => $this->category,
                "country" => $this->country,
                "lang" => $this->lang,
                "message" => $message
            ));
        }
    }

    public function sendForgotMail($to, $hash = null, $name = null) {

        $transport = (new \Swift_SmtpTransport("smtp.office365.com", "587", "SSL"))
                        ->setUsername("no-reply@toshiba-tv.com")
                        ->setPassword("T05h1Ba.!");

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message("Toshiba"))
            ->setFrom("no-reply@toshiba-tv.com")
            ->setTo($to)
            ->setBody(
                $this->renderView(
                    "mailTemplates/forgotPassword.html.twig",
                    array("hash" => $hash, "name" => $name, "email" => $to, "country" => $this->country, "lang" => $this->lang)
                ),
                "text/html"
            );

        return $mailer->send($message);

    }
}