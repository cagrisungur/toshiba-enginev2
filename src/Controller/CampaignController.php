<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CampaignController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/campaign",
     *     description="Get All Campaign",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("/campaign", name="getAllCampaign", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getAllCampaign(SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $countryId = $this->country;
        $langId = $this->lang;

        /**
         * @var Campaign $campaign
         */
        $campaign = $em->getRepository(Campaign::class)->getAllCampaign($countryId,$langId);

        $json = $serializer->serialize(
            $campaign,
            "json"
        );

        return new Response($json);
    }

}