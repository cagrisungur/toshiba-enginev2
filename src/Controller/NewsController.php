<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.01.2019
 * Time: 11:16
 */

namespace App\Controller;


use App\Entity\News;
use App\Entity\NewsImage;
use App\Entity\Country;
use App\Entity\ImageType;
use App\Entity\Language;
use App\Entity\NewsText;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class NewsController extends CustomController
{
    /**
     * @Route("/news", name="listNews")
     */
    public function listNews() {
        $em = $this->getDoctrine()->getManager();

        $news = $em->getRepository(News::class)->findBy(
            array(
            "deletedAt" => null,
            "active" => 1,
            "country" => $this->country
        ),
            array(
                "priority" => "ASC",
                "createdAt" => "ASC"
            )
        );

        $dataArray = array();

        foreach ($news as $new) {
            $date = date_format($new->getCreatedAt(),"Y-m-d");
            $str = strtotime($date);
            $newDate = date('d-M-Y', $str);
            $tmp = array(
                "id" => $new->getId(),
                "title" => $new->getTitle(),
                "priority" => $new->getPriority(),
                "isFeatured" => $new->getIsFeatured(),
                "createdAt" => $newDate
            );

            $newText = $new->getText($this->lang);
            foreach ($newText as $text) {
                $tmp["text"][] = array(
                    "title" => htmlspecialchars($text->getTitle(), ENT_QUOTES, 'UTF-8'),
                    "subtitle" => htmlspecialchars($text->getSubtitle(), ENT_QUOTES, 'UTF-8'),
                    "description" => htmlspecialchars($text->getDescription(), ENT_QUOTES, 'UTF-8'),
                    "sef" => $text->getSef(),
                );
            }

            $images = $new->getImages();
            foreach ($images as $image) {
                $tmp["images"][$image->getType()->getCode()][] = array(
                    "name" => $image->getName(),
                    "path" => $image->getPath(),
                    "priority" => $image->getPriority()
            );
            }
            $dataArray[] = $tmp;
        }
        $stds = array();
        $featured = array();
        foreach ($dataArray as $row) {
            if($row["isFeatured"] == 1 ){
                $featured = $row;
            }else{
              $stds[] = $row;
            }
        }



        //print_r($featured);exit;
        return $this->render("news.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "news" => $stds,
            "featured" => $featured
        ));
    }

    /**
     * @Route("/news-detail/{sef}", name="newsDetail")
     */
    public function newsDetail($sef) {
        $em = $this->getDoctrine()->getManager();

        $getNewsText = $em->getRepository(NewsText::class)->findOneBy(array(
            "sef" => $sef,
            "lang" => $this->lang
        ));

        $newsArray = array();
        $images = $getNewsText->getNews()->getImages();
        $imageArray = array();

        $moreNewsArray = array();

        $morewNews = $em->getRepository(NewsText::class)->getNews($this->lang->getId());
        foreach($morewNews as $moreN) {
            $moreNewsArray[] = array(
                "title" => $moreN["title"],
                "sef" => $moreN["sef"],
                "path" => $moreN["news"]["images"][0]["path"],

            );
        }
        foreach($images as $image) {
            $imageArray[$image->getType()->getCode()][] = array(
                "path" => $image->getPath()
            );
        }
        $newsArray = array(
            "title" => $getNewsText->getTitle(),
            "description" => \htmlspecialchars_decode($getNewsText->getDescription()),
            "banner" => $getNewsText->getBannerPath(),
            "createdAt" => $getNewsText->getCreatedAt(),
            "images" => $imageArray
        );

        //print_r($newsArray);exit;
        return $this->render("newsDetail.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "data" => $newsArray,
            "moreNews" => $moreNewsArray

        ));
    }

    // /**
    //  * @Route("/old-toshiba-import", name="customNewsImport")
    //  */
    // public function customNewsImport(Request $request) {
    //     $servername = "localhost";
    //     $username = "root";
    //     $password = "";

    //     $em = $this->getDoctrine()->getManager();

    //     $conn = new \PDO("mysql:host=$servername;dbname=old_toshiba;charset=utf8", $username, $password);
    //     $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    //     $sql = "SELECT * FROM press where deleted = 0";

    //     $result = $conn->query($sql);
    //     $country = $em->getRepository(Country::class)->find(39);
    //     $lang = $em->getRepository(Language::class)->find(35);
    //     $thumbType = $em->getRepository(ImageType::class)->find(2);
    //     $desktopType = $em->getRepository(ImageType::class)->find(1);

    //     $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
    //     //print_r($baseUrl);exit;
    //     foreach($result as $row) {
    //         $news = new News();
    //         $news->setCountry($country);
    //         $news->setTitle($row["name"]);
    //         $news->setActive(1);
    //         $news->setIsFeatured(0);
    //         $news->setPriority(100);
            
    //         $em->persist($news);
    //         $em->flush();

    //         $newsText = new NewsText();
    //         $newsText->setNews($news);
    //         $newsText->setLang($lang);
    //         $newsText->setSubtitle($row["name"]);
    //         $newsText->setDescription($row["content"]);
    //         $newsText->setSef($row["sef"]);
    //         $newsText->setTitle($row["name"]);

    //         $em->persist($newsText);
    //         $em->flush();

    //         if($row["thumbnail"]) {
    //             if($row["thumbnail"] !== "") {
    //                 $newsImage = new NewsImage();
    //                 $newsImage->setNews($news);
    //                 $newsImage->setType($thumbType);
    //                 $newsImage->setName("thumb");
    //                 $newsImage->setActive(1);

    //                 $path = substr($row["thumbnail"], 2);
    //                 $path = "http://63.35.162.212/uploads/images".$path;
    //                 $newsImage->setPath($path);
    //                 $newsImage->setPriority(10);
                    
    //                 $em->persist($newsImage);
    //                 $em->flush();
    //             }
    //         }

    //         if($row["banner"]) {
    //             if($row["banner"] !== "") {
    //                 $newsBanner = new NewsImage();
    //                 $newsBanner->setNews($news);
    //                 $newsBanner->setType($desktopType);
    //                 $newsBanner->setName("banner");
    //                 $newsBanner->setActive(1);

    //                 $newPath = substr($row["banner"], 2);
    //                 $newPath = "http://63.35.162.212/uploads/images".$newPath;
    //                 $newsBanner->setPath($newPath);
    //                 $newsBanner->setPriority(10);
                    
    //                 $em->persist($newsBanner);
    //                 $em->flush();
    //             }
    //         }
            

    //     }exit;
    // }
}
