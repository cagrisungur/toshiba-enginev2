<?php

namespace App\Controller;

use App\Entity\Warranty;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WarrantyController extends CustomController
{
    /**
     * @Route("/warranty", name="warranty")
     */
    public function warranty() {


          $em = $this->getDoctrine()->getManager();

          $war = $em->getRepository(Warranty::class)->find(2);
          $warranty = $war->getWarranty();

          $war = $em->getRepository(Warranty::class)->findBy(array("lang"=>$this->lang));
          if($war){
            $warranty = $war[0]->getWarranty();
          }



        return $this->render("warranty.html.twig", array(
            "staticText" =>  $this->staticText,
            "warranty" => $warranty,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}
