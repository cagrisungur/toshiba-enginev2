<?php

namespace App\Controller;

use App\Document\Logs;
use App\Entity\User;
use App\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        $user = new User($username);
        $group = $em->getRepository(UserGroup::class)->find(1);

        $user->setPassword($encoder->encodePassword($user, $password));

        $user->setName($username);
        $user->setMail($username);
        $user->setPhone($username);
        $user->setGroup($group);

        $em->persist($user);
        $em->flush();


        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function api(Request $request)
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }

    /**
     * @return Response
     */
    public function getAllUser()
    {
        $em = $this->getDoctrine()->getManager()->getRepository(User::class)->getUser();
        
        return new Response(json_encode($em));
    }
}
