<?php
namespace App\Controller;

use App\Entity\SupportServicesSeries;
use App\Library\CustomController;
use App\Entity\WarrantyProduct;
use Swagger\Annotations as SWG;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyHash;
use App\Entity\Product;
use App\Entity\Country;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\Request;

class SupportProductRegisterController extends CustomController
{
    public $redirectUrl;

    /**
     * @Route("support-services", name="supportRegister")
     */
    public function supportRegister(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        $serialNumber = "27710198600019";
        $modelNo = "40L1657DB";
        // serial number basına 2 koymayı unutma.
        $em = $this->getDoctrine()->getManager();
        $error = "";
        $fileError = "";
        $action = "";
        //upload state denemek icin true yapıldı daha sonra false cekilecek.
        $uploadState = true;
        $productArray = array();

        if(isset($_POST["action"])) {
             $action = $_POST["action"];
        }
        $this->redirectUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()."/".$this->country->getCode(). "/".$this->lang->getCode()."/register-next";
        switch ($action) {
            case "upload":
                if(isset($_POST["action"]) == "upload") {
                    
                    $serialNumber = $_POST["serial"];
                    $modelNo = $_POST["model"];

                    $checkProductExistBefore = $em->getRepository(WarrantyProduct::class)->findBy(array(
                        "serialNo" => $serialNumber
                    ));

                    $isSerialExist = $em->getRepository(SupportServicesSeries::class)->serialExist($serialNumber);
                    $isModelExist = $em->getRepository(SupportServicesSeries::class)->getSupportServices($serialNumber, $modelNo);

                    if(count($checkProductExistBefore) > 0) {
                        $error = array(
                            "error" => $this->staticText["SUPPORTSERIALNUMBERAVAILABLE"]
                        );

                        return new JsonResponse($error);
                    } else if(!$isSerialExist) {
                        $error = array(
                            "error" => $this->staticText["SUPPORTSERIALNUMBERNOTEXIST"]
                        );

                        return new JsonResponse($error);
                    } else if(!$isModelExist) {
                        $error = "Serial number and model number do not match each other. You can click ”i” for help.";
                        $error = array(
                            "error" => "Serial number and model number do not match each other. You can click ”i” for help."
                        );

                        return new JsonResponse($error);
                    } else {
                        $date = $_POST['date'];
                        $dealer = $_POST['place'];
                        $model = $_POST['model'];
                        $serialNumber = $_POST["serial"];
                        $file = $_FILES['proof']['tmp_name'];
                        $fileName = $_FILES['proof']['name'];
                        $ext = pathinfo($fileName);
                        $ext = '.' . $ext['extension'];

                        if (! in_array($ext, array('.jpg','.jpeg','.pdf','.png'))) {
                            $fileError = 'Wrong file format!';
                            return new JsonResponse($fileError);
                        }

                        $imageLink = md5(time().$fileName).$ext;

                        $isUploaded = move_uploaded_file($file,'./uploads/files/'.$imageLink);

                        if($isUploaded) {
                            $productArray["date"] = $date;
                            $productArray["dealer"] = $dealer;
                            $productArray["serialNumber"] = $serialNumber;
                            $productArray["imageLink"] = $imageLink;
                            $productArray["modelNumber"] = $model;
                            $session->set("SUPPORTREGISTER", $productArray);
                            $uploadState = true;
                            return new JsonResponse($session->get("SUPPORTREGISTER"));
                        } else {
                            return new JsonResponse("ERROR");
                        }
                    }
                }
            case "continue":
                    if(!isset($session->get("SUPPORTREGISTER")["serialNumber"])) {
                        return new JsonResponse("GOFIRSTSTEP");
                    }

                    $formDate = $session->get("SUPPORTREGISTER")['date'];
                    $serialNumber = $session->get("SUPPORTREGISTER")['serialNumber'];
                    $dealer = $session->get("SUPPORTREGISTER")['dealer'];
                    $model = $session->get("SUPPORTREGISTER")['modelNumber'];
                    $imageLink = $session->get("SUPPORTREGISTER")['imageLink'];
                    $email = $_POST["email"];

                    $checkMail = $em->getRepository(WarrantyUser::class)->findOneBy(array(
                        "email" => $email
                    ));

                    if($checkMail) {
                        $error = array(
                            "error" => "Email address is existing in records. Please try again with another email address."
                        );

                        return new JsonResponse($error);
                    } else {
                        if(isset($_POST['pwd'])) {
                            $pattern = "/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";
                            if(!preg_match($pattern, $_POST['pwd'])) {
                                $error = array(
                                    "error" => $this->staticText["SUPPORTPASSWORDERROR"]
                                );
                                return new JsonResponse($error);
                            } else {
                                $newUser = new WarrantyUser();
                                $newUser->setName($_POST["name"]);
                                $newUser->setEmail($email);
                                $newUser->setPhone($_POST["phone"]);

                                $country = $_POST['selectCountry'];
                                $countryObj = $em->getRepository(Country::class)->find($country);

                                $newUser->setCountry($countryObj);
                                $newUser->setZipCode($_POST["postcode"]);
                                $newUser->setPassword(md5($_POST["pwd"]));
                                $newUser->setAddress($_POST["address"]);
                                $newUser->setActive(0);
                                $em->persist($newUser);

                                $date = new \Datetime($formDate);

                                $userProduct = new WarrantyProduct();
                                $userProduct->setUser($newUser);
                                $userProduct->setSerialNo($serialNumber);
                                $userProduct->setModelNo($model);
                                $userProduct->setSalesDate($date);
                                $userProduct->setDealer($dealer);
                                $userProduct->setSalesDoc($imageLink);
                                $userProduct->setStatus(0);
                                $em->persist($userProduct);

                                $em->flush();

                                $session->clear();
                                $hash = md5(rand(0, 1000));
                                $sendMail = $this->sendSupportMail($email, $hash, $_POST["name"]);
                                if($sendMail) {
                                    $warrantyHash = new WarrantyHash();
                                    $warrantyHash->setHash($hash);
                                    $warrantyHash->setWarrantyUser($newUser);
                                    $warrantyHash->setStatus(0);

                                    $em->persist($warrantyHash);
                                    $em->flush();
                                    $userAndProduct = array(
                                        "id" => $newUser->getId(),
                                        "product" => $userProduct->getModelNo()
                                    );
                                    $session->set("userInfo", $userAndProduct);
                                    if($session->get("userInfo")["id"]) {
                                        return new JsonResponse(array(
                                            "succcess" => "SUCCESS",
                                            "redirectUrl" => $this->redirectUrl
                                        ));
                                    }else {
                                        return new JsonResponse(array(
                                            "error" => "ERROR"
                                        ));
                                    }
                                }
                            }
                        }
                    }
        }

        exit;
    }

    /**
     * @Route("support-activation", name="activationMailHash")
     */
    public function activationMailHash(Request $request) {
        if(isset($_GET["email"]) && isset($_GET["hash"])) {
            $em = $this->getDoctrine()->getManager();

            $email = $_GET["email"];
            $hash = $_GET["hash"];

            $session = $request->getSession();
            if(!$session) {
                session_start();
            }
            //print_r($session->get("userInfo"));exit;
            if(!$session->get("userInfo")) {
                return new JsonResponse("ERROR");
            } else {
                $userId = $session->get("userInfo")["id"];
                $findUser = $em->getRepository(Warrantyuser::class)->find($userId);
                $warrantyHashCheck = $em->getRepository(WarrantyHash::class)->findOneBy(array(
                    "warrantyUser" => $findUser,
                    "hash" => $hash,
                    "isForgetPassword" => 0
                ));
                //print_r($$session->get("userInfo")["product"]);exit;
                $findUserProduct = $em->getRepository(Product::class)->findOneBy(array(
                    "code" => $session->get("userInfo")["product"]
                ));
                //print_r($findUserProduct->getId());exit;
                if($warrantyHashCheck) {
                    $warrantyHashCheck->setStatus(1);
                    $em->flush();

                    $session->remove("userInfo");
                    $userAndProduct = array(
                        "id" => $findUser->getId(),
                        "product" => $findUserProduct->getId()
                    );

                    $session->set("userInfo", $userAndProduct);
                    return new JsonResponse(array(
                        "success" => "EMAILSUCCESS",
                        "redirectUrl" => $this->redirectUrl
                    ));
                } else {
                    return new JsonResponse(array(
                        "error" => "CANTSAVEDATA",
                    ));
                }
            }
        }
    }

    

    public function sendSupportMail($to, $hash = null, $name = null) {

        $transport = (new \Swift_SmtpTransport("smtp.office365.com", "587", "TLS"))
                        ->setUsername("no-reply@toshiba-tv.com")
                        ->setPassword("T05h1Ba.!");

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message("Toshiba"))
            ->setFrom("no-reply@toshiba-tv.com")
            ->setTo($to)
            ->setBody(
                $this->renderView(
                    "emailActivation.html.twig",
                    array("hash" => $hash, "name" => $name, "email" => $to, "country" => $this->country, "lang" => $this->lang)
                ),
                "text/html"
            );

        return $mailer->send($message);

    }
}