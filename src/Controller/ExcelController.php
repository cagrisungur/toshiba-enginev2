<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 10.08.2018
 * Time: 14:55
 */

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductSpec;
use App\Entity\Series;
use App\Entity\StaticText;
use App\Entity\StaticTextTranslate;
use App\Entity\Language;
use App\Entity\SeriesText;
use App\Entity\Specification;
use App\Entity\SpecificationCategory;
use App\Library\CustomController;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;


class ExcelController extends CustomController
{
    public $inputFileType = 'Xlsx';

    /**
     * @SWG\Post(
     *   path="/{country}/{language}/excel",
     *   description="Import to Product Specs Related Excel",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *      name="uploaded_file",
     *      in="formData",
     *      required="true",
     *      type="file"
     *       )
     * )
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("excel", name="excelImport", methods={"POST"})
     */
    public function excelImport()
    {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        $product = null;
        $lastSpecCat = "";
        $lastProd = "";
        $lastCat = "";

        for ($row = 1; $row <= $highestRow; $row++) {
            for ($col = 'A'; $col != $highestColumn; ++$col) {
                if ($col == 'A'){
                    if ($row > 2)  {
                        $prodVal = $sheet2->getCell('A' . $row)->getValue();
                        if ($prodVal != null) {
                            if($prodVal) {
                                $lastProd = $prodVal;
                            }else {
                                $prodVal = $lastProd;
                            }

                            $product = new Product();
                            $product->setCode($prodVal);
                            $result = substr($prodVal, 0, 2);
                            $product->setSize($result);
                            $product->setName($prodVal);
                            $path = "/". $prodVal;
                            $product->setLink($path);
                            $product->setKeywords('asd');
                            $product->setPriority(1);
                            $product->setActive(1);
                            $catVal = $sheet2->getCell('B' . $row)->getValue();

                            if($catVal) {
                                $lastCat = $catVal;
                            }else {
                                $catVal = $lastCat;
                            }

                            $catCheck = $em->getRepository(Category::class)->findBy(array(
                                "name" => $catVal
                            ));

                            $seriesCheck = $em->getRepository(Series::class)->findBy(array(
                                "code" => $catVal
                            ));

                            if ($catCheck){
                                foreach ($catCheck as $cats){
                                    $product->setCategory($cats);
                                }
                            } else {
                                $cat = new Category();
                                $cat->setActive(1);
                                $cat->setCreatedAt(new \DateTime());
                                $cat->setName($catVal);
                                $path = "/". $catVal;
                                $cat->setLink($path);
                                $cat->setKeywords($catVal);
                                $cat->setParentId(1);
                                $em->persist($cat);
                                $product->setCategory($cat);
                            }

                            if ($seriesCheck){
                                foreach ($seriesCheck as $series){
                                    $product->setSeries($series);
                                }
                            } else {
                                $productSeries = new Series();
                                $productSeries->setCode($catVal);
                                $productSeries->setName($catVal);
                                $productSeries->setActive(1);
                                $productSeries->setCreatedAt(new \DateTime());
                                $em->persist($productSeries);
                                $product->setSeries($productSeries);

                            }
                            $em->persist($product);

                        }
                    }

                } else if ($col != 'A' && $col != 'B' && $col != 'C') {
                    $val = $sheet2->getCell($col . $row)->getValue();
                    if ($row == 1){
                        if ($val != null) {
                            $specCategory = new SpecificationCategory();
                            $specCategory->setName($val);
                            $specCategory->setPriority(1);
                            $specCategory->setCreatedAt(new \DateTime());
                            $em->persist($specCategory);
                            $em->flush();

                        }
                    }

                    if ($row == 2) {
                        if ($val != null) {
                            $catName = $sheet2->getCell($col . "1")->getValue();
                            if($catName) {
                                $lastSpecCat = $catName;
                            }else {
                                $catName = $lastSpecCat;
                            }
                            $specCategory = $em->getRepository(SpecificationCategory::class)->findOneBy(array(
                                "name" => $catName
                            ));

                            $spec = new Specification();
                            $spec->setName($val);
                            $spec->setCreatedAt(new \DateTime());
                            $spec->setCode($val);
                            $spec->setCategory($specCategory);
                            $spec->setPriority(1);
                            $em->persist($spec);
                            $em->flush();

                        }
                    }
                    if($row > 2){
                        $productSpecValue = $sheet2->getCell($col . $row)->getValue();
                        $productSpecName = $sheet2->getCell($col . 2)->getValue();

                        if ($productSpecValue) {
                            $specs = $em->getRepository(Specification::class)->findOneBy(array(
                                "name" => $productSpecName
                            ));

                            $productSpec = new ProductSpec();
                            $productSpec->setProduct($product);
                            $productSpec->setValue($productSpecValue);
                            $productSpec->setPriority(1);
                            $productSpec->setCreatedAt(new \DateTime());
                            $productSpec->setSpec($specs);

                            $em->persist($productSpec);
                            $em->flush();
                        }
                    }
                }
            }
        }exit;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("find-name-excel", name="findName", methods={"POST"})
     */
    public function findName() {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++) {
                $prodVal = $sheet2->getCell('BO' . $row)->getValue();
                $findProd = $em->getRepository(Product::class)->findOneBy(array(
                    "code" => $prodVal
                ), array(
                    "priority" => "ASC"
                    )
                );
                if (!$findProd) {
                    continue;
                }
                echo '<br>';
                print_r($findProd->getName());
        }
        exit;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("series-export", name="seriesExport", methods={"POST"})
     */
    public function seriesExport() {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();

        for ($row = 3; $row <= $highestRow; $row++) {
                $isPersist = 0;
                $seriesCode = $sheet2->getCell('A' . $row)->getValue();
                $serieDesc = $sheet2->getCell('B' . $row)->getValue();
                $seriesCode = $seriesCode." Series";
                $findSeries = $em->getRepository(Series::class)->findOneBy(array(
                    "code" => $seriesCode
                )
                );
                if(!$findSeries) {
                    print_r($seriesCode);
                } else {
                    $text = $em->getRepository(SeriesText::class)->findOneBy(array(
                        "lang" => $this->lang,
                        "series" => $findSeries
                    ));
                    if(!$text) {
                        $text = new SeriesText();   
                        $isPersist = 1;   
                    }
                    $text->setDescription($serieDesc);

                    if($isPersist == 1) {
                        $text->setSeries($findSeries);
                        $text->setLang($this->lang);
                        $em->persist($text);
                    }

                    $em->flush();
                }
                //echo '<br>';
                
        }
        exit;
    }

     /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("importStatic", name="staticImport", methods={"POST"})
     */
    public function staticImport() {
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $lang = $em->getRepository(Language::class)->find(1);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();

        for ($row = 3; $row <= $highestRow; $row++) {
            $staticCode = $sheet2->getCell('B' . $row)->getValue();
            // print_r($staticCode);
            // exit;    
            if($sheet2->getCell('A' . $row)->getValue() == "") {
                continue;
            }
            $checkText = $em->getRepository(StaticText::class)->findOneBy(array(
                "code" => $staticCode
            ));

            $translate = new StaticTextTranslate();
            $translate->setStaticText($checkText);
            $translate->setLang($lang);
            $translate->setData($sheet2->getCell('Y' . $row)->getValue());
            
            $em->persist($translate);
            $em->flush();
    
        }
        exit;
    }
}
