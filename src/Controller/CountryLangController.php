<?php

namespace App\Controller;

use App\Entity\Country;
use App\Library\CountryLangSerializer;
use App\Library\CustomController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class CountryLangController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/locations",
     *     description="Get All Locations",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("/locations", name="locations", methods={"GET"})
     * @param CountryLangSerializer $serializer
     * @return Response
     */
    public function getAllLocations(CountryLangSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var Country $countryLang
         */
        $countryLang = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 1,
            ),
        array("priority" => "ASC")
        );

        $json = $serializer->serialize(
            $countryLang,
            'json'
        );

        return new Response($json);
    }

	/**
	 * @Route("/registercountries", name="registercountries", methods={"GET"})
     * @param CountryLangSerializer $serializer
     * @return Response
	 */
    public function getRegisterCountries(CountryLangSerializer $serializer) {
      $em = $this->getDoctrine()->getManager();
        /**
         * @var Country $countryLang
         */
        $countryLang = $em->getRepository(Country::class)->findBy(
			array(
				"deletedAt" => null,
				"active" => 1,
				"register" => 1
			),
        	array("priority" => "ASC")
        );

        $json = $serializer->serialize(
            $countryLang,
            'json'
        );

        return new Response($json);
    }
}