<?php

namespace App\Controller;

use App\Entity\SalesPoint;
use App\Library\CustomController;
use App\Library\SalesPointSerializer;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SalesPointController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/salespoint",
     *     description="Get Sales Point By Country",
     *     @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     )
     * )
     *
     * @Route("/salespoint", name="getSalesPoint", methods={"GET"})
     * @param SalesPointSerializer $serializer
     * @return Response
     */
    public function getSalesPoint(SalesPointSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        $salesPoint = $em->getRepository(SalesPoint::class)->getSalesPoint();

        $json = $serializer->serialize(
            $salesPoint,
            "json"
        );

        return new Response($json);
    }
}