<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 26.07.2018
 * Time: 14:29
 */

namespace App\Controller;


use App\Entity\CustomPage;
use App\Library\CustomController;
use App\Library\CustomPageSerializer;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomPageController extends CustomController
{
    /**
     *
     * @SWG\Get(
     *   path="/{country}/{language}/custompage",
     *   description="Custom Page Translations",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     )
     * )
     * @Route("/custompage", name="getCustomPage", methods={"GET"})
     * @param CustomPageSerializer $serializer
     * @return Response
     */
    public function getCustomPage(CustomPageSerializer $serializer){
        $em = $this->getDoctrine()->getManager();

        $customPage = $em->getRepository(CustomPage::class)->findBy(array(
            "deletedAt" => null,
        ));

        $serializer->setLocale($this->country, $this->lang);

        $json = $serializer->serialize(
            $customPage,
            "json"
        );

        //$escapedJson = htmlspecialchars($json, ENT_QUOTES, 'UTF-8');
        return new JsonResponse($json);
    }
}