<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Entity\Product;
use App\Entity\ProductText;
use App\Entity\ProductCountry;
use App\Entity\Faq;
use App\Entity\FaqCategory;
use App\Entity\ProductFile;
use App\Library\CustomController;
use App\Library\ProductSerializer;
use App\Entity\SupportVideos;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

class NewSupportController extends CustomController {

    /**
     * @Route("/product/support/{productLink}", name="supportModel")
     */
    public function supportModel($productLink, Request $request) {
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $uploadUrl = $baseUrl."/uploads/files/files/";
        $productArray = array();


        $em = $this->getDoctrine()->getManager();

        $faqArray = array();
        $faqList = $em->getRepository(Faq::class)->findBy(array(
            "deletedAt" => null
        ));
        //country id si 0 olanlar 53 yapıldı.Global
        $newGlobalCountry = $em->getRepository(Country::class)->find(53);

        foreach($faqList as $faq) {
            $faqText = $faq->getText($this->lang);
            if(!$faqText) {
                $faqText = $faq->getText($this->globalLang);
            }
            foreach($faqText as $text) {
                if($text->getQuestion() == "") {
                    $question = $faq->getName();
                } else {
                    $question = $text->getQuestion();
                }
                $faqArray[] = array(
                    "id" => $faq->getId(),
                    "question" => $text->getQuestion(),
                    "answer" => $text->getAnswer()
                );
            }
        }

        $product = $em->getRepository(Product::class)->findOneBy(array(
            "link" => $productLink,
            "deletedAt" => null
        ));

        $productArray = array(
            "id" => $product->getId(),
            "link" => $product->getLink(),
            "name" => $product->getName()
        );
        if($product) {
            $productArray["text"] = array();
            $textList = $product->getLangText($this->lang);
            if(!$textList) {
                $textList = $product->getLangText($this->globalLang);
            }
            foreach ($textList as $text) {
                if (!$text->getName() && $text->getName() == "") {
                    $prodTextName = $product->getName();
                } else {
                    $prodTextName = $text->getName();
                }
                $productArray["text"][] = array(
                    "title" => $text->getSubtitle(),
                    "name" => $prodTextName,
                    "shortDescription" => $text->getShortDescription(),
                    "longDescription" => $text->getLongDescription(),
                    "link" => $text->getLink(),
                    "keywords" => $text->getKeywords(),
                    "video" => $text->getYtVideo(),
                    "videoThumbnail" => $text->getVideoThumbnail()
                );
            }

        $productArray["images"] = array();
        $imageList = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
            // $imageList = $product->getImages();
        $fullPath = "";
        foreach ($imageList as $image) {
            if(strpos($image->getFileName(), "front") !== false) {
                $fullPath = "http://63.35.162.212/uploads/files/files/".$image->getPath();
            }
            if($image->getType() != null) {
                $productArray["images"] = $fullPath;
            }
        }

        $productArray["series"] = array();
            $series = $product->getSeries();
            $currentProductLink = str_split($product->getLink(), 2);
            $currentProductLink = $currentProductLink[0];
            if($series != null) {
                $array = array(
                    "name" => $series->getName(),
                    "code" => $series->getCode(),
                    "mainSize" => $currentProductLink
                );
                $array["products"] = array();
                if($series->getProduct($product->getId()) != null) {
                    $productList = $series->getProduct($product->getId());
                    foreach ($productList as $pro) {
                        $findSeriesCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                            "product" => $pro->getId(),
                            "country" => $this->country
                        ));


                        if($findSeriesCountry) {
                            if($product->getCategory()->getCode() == $pro->getCategory()->getCode()) {
                                $sizeSplit = str_split($pro->getLink(), 2);
                                //  $sizeSplit = $sizeSplit[0];
      
                                  $array["products"][] = array(
                                      "id" => $pro->getId(),
                                      "size" => trim($sizeSplit[0]),
                                      "link" => $pro->getLink(),
                                      "name" => $pro->getName(),
                                      "inc" => $sizeSplit,
                                      "baseUrl" => $baseUrl."/".$pro->getLink()
                                  );
                            }
                            
                        }
                    }
                }

                $productArray["series"] = $array;
            }

            $videoList = $em->getRepository(SupportVideos::class)->findBy(array(
                "deletedAt" => null
            ));

            $videoArray = array();

            foreach($videoList as $video) {
                $videoText = $video->getVideoText($this->lang);

                foreach($videoText as $text) {
                    $videoArray[$video->getVideoFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                        "title" => $text->getTitle(),
                        "video" => $text->getVideo(),
                        "thumbnail" => $baseUrl."/uploads/support-admin/images/".$text->getThumbnail(),
                        "id" => $video->getId()
                    );
                }
            }
            $productArray["videos"] = $videoArray;

            $productArray["file"] = array();
            $productFiles = $em->getRepository(Product::class)->getSupportFile($product->getId(), $this->country);

            if(!isset($productFiles[0])) {
                $productFiles = $em->getRepository(Product::class)->getSupportFile($product->getId(), $newGlobalCountry);
            }

            if(isset($productFiles[0])) {
                foreach($productFiles[0]["file"] as $file) {
                    $productArray["file"][$file["type"]["code"]][] = array(
                        "id" => $file["id"],
                        "path" => $file["path"],
                        "type" => $file["type"]["name"],
                        "fileName" => $file["fileName"]
                    );
                }
            }
            //print_r($productFiles);exit;
            //print_r($productArray);exit;

        }
        //print_r($productArray);exit;    
        //print_r($productArray["series"]);exit; => burası product a ait aynı seride ki diğer ürünleri basıyor. Inc'leri bu arraydeki products indexini forda dönerek basabiliriz.
        //print_r($productArray);exit; // hmtl yüklenmesi icin burayı commentle
        return $this->render("supportModel.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "product" =>  $productArray,
            "faqs" => $faqArray,
            "uploadUrl" => $uploadUrl
        ));
    }

}
