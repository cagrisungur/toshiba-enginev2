<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyProduct;
use App\Entity\FeaturedCategory;
use App\Entity\Product;
use App\Entity\SupportServicesSeries;
use App\Entity\CustomPage;
use App\Entity\StaticText;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class IndexController extends CustomController
{
    /**
     * @Route("/", name="index")
     */
    public function index() {
        //echo 'gg';exit;
        // echo 'gg';exit;
        // if($this->country == null && $this->lang == null) {
        //     return $this->redirect('http://127.0.0.1:8000/uk/uk');
        // }
        $em = $this->getDoctrine()->getManager();

        $featureCat = array();

        $featuredCategory = $em->getRepository(FeaturedCategory::class)->findBy(array(
            "deletedAt" => null,
            "country" => $this->country,
            "lang" => $this->lang
        ));

        foreach ($featuredCategory as $cat){
            if($cat->getCategory()->getCatText($this->lang)[0] == "") {
                $catName = $cat->getCategory()->getCatText($this->globalLang)[0]->getName();
            } else {
                $catName = $cat->getCategory()->getCatText($this->lang)[0]->getName();
            }
            $tmp = array(
                "categoryName" => $catName,
                "title" => $cat->getTitle(),
                "description" => $cat->getDescription(),
                "categoryId" => $cat->getCategory()->getId(),
                "categoryLink" => $cat->getCategory()->getLink(),
                "path" => $cat->getFeaturedImage(),
                "divClass" => $cat->getDivClass()
            );
            $featureCat[] = $tmp;
        }


        $customPages = $em->getRepository(CustomPage::class)->findBy(array(
            "deletedAt" => null
        ));
        $customPageArray = array();
        $customTmp = array();

        $text = "";


        foreach ($customPages as $customPage){
            $pageText =$customPage->getNewPageText($this->country, $this->lang);
            if (!$pageText) {
                $pageText =$customPage->getNewPageText($this->globalCountry, $this->globalLang);
            }
            foreach($pageText as $text) {
                $customTmp[$customPage->getType()->getCode()] = array(
                    "data" => $text->getData(),
                    "path"  => $customPage->getPath(),
                    "thumbNailPath"  => $customPage->getThumbNailPath(),
                    "title" => $text->getTitle()
                );
            }

        }

        $customPageArray[] = $customTmp;
        return $this->render("index.html.twig", array(
            "staticText" => $this->staticText,
            "country" => $this->country,
            "lang" => $this->lang,
            "isHomePage" => 1,
            "categories" => $this->category,
            "featuredCategories" => $featureCat,
            "customPages" => $customPageArray

        ));
    }

    /**
     * @Route("/tv", name="tv")
     */
    public function tv() {
        return $this->render("tv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }


    /**
     * @Route("/hd-tvs", name="hdTvs")
     */
    public function hdTvs() {
        return $this->render("hdTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/premium-tvs", name="premiumTv")
     */
    public function premiumTv() {
        return $this->render("premiumTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/premium4k-smart-tvs", name="premium4kSmartTv")
     */
    public function premium4kSmartTv() {
        return $this->render("premium4KSmart.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/ultra-hd-tvs", name="ultraHdTv")
     */
    public function ultraHdTv() {
        return $this->render("ultraHdTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/press-release-article", name="pressReleaseArticle")
     */
    public function pressReleaseArticle() {
        return $this->render("pressReleaseArticle.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/press-release", name="pressRelease")
     */
    public function pressRelease() {
        return $this->render("pressReleaseList.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    


    /**
     * @Route("/support-search", name="supportSearch")
     */
    public function supportSearch() {
        return $this->render("supportSearch.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/support-nothing-found", name="supportNothingFound")
     */
    public function supportNothingFound() {
        return $this->render("supportNothingFound.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/support-quick-link", name="supportQuickLink")
     */
    public function supportQuickLink() {
        return $this->render("supportQuickLink.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
    
    /**
     * @Route("/forum-home", name="forumHome")
     */
    public function forumHome() {
        return $this->render("forumHome.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/forum-thread", name="forumThread")
     */
    public function forumThread() {
        return $this->render("forumThread.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }


   

    /**
     * @Route("/buying-guide", name="buyingGuide")
     */
    public function buyingGuide() {
        return $this->render("buyingGuide.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

     /**
     * @Route("/soundbars", name="sound")
     */
    public function soundBars() {
        return $this->render("sound.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

     /**
     * @Route("/smart-tv", name="smartTv")
     */
    public function smartTv() {
        return $this->render("smartTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

     /**
     * @Route("/android-tv", name="androidTv")
     */
    public function androidTv() {
        return $this->render("androidTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

     /**
     * @Route("/connect-tv", name="connectTv")
     */
    public function connectTv() {
        return $this->render("connectTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }


     /**
     * @Route("/roku-tv", name="rokuTv")
     */
    public function rokuTV() {
        return $this->render("rokuTv.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

}