<?php

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends CustomController
{
    /**
     * @Route("/about-us", name="aboutUs")
     */
    public function aboutUs() {

        return $this->render("aboutUs.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}