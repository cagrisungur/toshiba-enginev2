<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 25.07.2018
 * Time: 15:27
 */

namespace App\Controller;
use App\Entity\FeaturedCategory;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Swagger\Annotations as SWG;


class FeaturedCategoryController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/featuredCategory",
     *     description="Get Featured Category",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("featuredCategory", name="getFeaturedCategory", methods={"GET"})
     * @return Response
     * @param SerializerInterface $serializer
     */
    public function getFeaturedCategory(SerializerInterface $serializer)
    {
        $dataArray = [];

        $countryId = $this->country;
        $langId = $this->lang;

        $em = $this->getDoctrine()->getManager();

        $featuredCategory = $em->getRepository(FeaturedCategory::class)->findBy(array(
            "deletedAt" => null,
            "country" => $countryId,
            "lang" => $langId
        ));
        foreach ($featuredCategory as $cat){
            $tmp = array(
                "categoryName" => $cat->getCategory()->getName(),
                "title" => $cat->getTitle(),
                "description" => $cat->getDescription(),
                "categoryId" => $cat->getCategory()->getId(),
                "categoryLink" => $cat->getCategory()->getLink()
            );
            foreach ($cat->getCategory()->getImage() as $catt) {
                $tmp["image"][$catt->getType()->getCode()][] = array(
                    "path" => $catt->getPath()
                );
            }
            $dataArray[] = $tmp;
        }

        $json = $serializer->serialize(
            $dataArray,
            "json"
        );

        return new Response($json);
    }
}