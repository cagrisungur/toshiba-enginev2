<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 7.12.2018
 * Time: 13:43
 */

namespace App\Controller;


use App\Entity\Contact;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class ContactController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/contact",
     *     description="Get Contact",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="eu"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="en"
     *      )
     * )
     *
     * @Route("contact", name="contact", methods={"GET"})
     */
    public function contact() {
        $em = $this->getDoctrine()->getManager();

        $country = $this->country;

        $dataArray = array();

        $contacts = $em->getRepository(Contact::class)->getContact($country);
        if (isset($contacts)) {
            $dataArray = array(
                "id" => $contacts->getId(),
                "name" => $contacts->getName(),
                "website" => $contacts->getWebSite(),
                "phone" => $contacts->getPhone(),
                "fax" => $contacts->getFax(),
                "address" => $contacts->getAddress(),
                "email" => $contacts->getEmail(),
                "lat" => $contacts->getLat(),
                "lng" => $contacts->getLng(),
                "active" => $contacts->getActive()
            );
        }

        return new JsonResponse($dataArray);
    }
}