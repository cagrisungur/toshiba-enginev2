<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyProduct;
use App\Entity\Product;
use App\Entity\SupportServicesSeries;
use App\Entity\CustomPage;
use App\Entity\StaticText;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ipinfo\ipinfo\IPinfo;


class NewSupportRegisterController extends CustomController
{
     /**
     * @Route("/register", name="register")
     */
    public function register(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $ip = $request->getClientIp();

        $accessToken = "7140d65f627b36";
        $client = new IPinfo($accessToken);
        $details = $client->getDetails($ip);
        
        if($ip != "127.0.0.1") {
            $ipCountry = $details->country;
            $ipCountry = strtolower($ipCountry);
        } else {
            $ipCountry = "uk";
        }
        
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        $productArray = array();
        if(isset($session->get("userInfo")["id"])) {
            $user = $em->getRepository(WarrantyUser::class)->find($session->get("userInfo")["id"]);
            $userProducts = $em->getRepository(WarrantyProduct::class)->findBy(array(
                "user" => $user
            ));

            foreach($userProducts as $userProduct) {
                $findProductData = $em->getRepository(Product::class)->findOneBy(array(
                    "code" => $userProduct->getModelNo()
                ));
                if($findProductData) {
                    $productArray = array(
                        "id" => $findProductData->getId(),
                        "code" => $findProductData->getCode(),
                        "image" => $findProductData->getImages()[0]->getPath(),
                        "series" => $findProductData->getSeries()->getName()
                    );
                }
            }
            $countries = $em->getRepository(Country::class)->findBy(array(
                "deletedAt" => null,
                "active" => 7
            ));

            return $this->render("dashboard.html.twig", array(
                "staticText" =>  $this->staticText,
                "categories" => $this->category,
                "country" => $this->country,
                "lang" => $this->lang,
                "user" => $user,
                "countries" => $countries,
                "productArray" => $productArray
            ));
        }
         $em = $this->getDoctrine()->getManager();
         $countries = $em->getRepository(Country::class)->findBy(array(
             "deletedAt" => null,
             "active" => 7
         ));

        return $this->render("register.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang ,
            "countries" => $countries,
            "ipCountry" => $ipCountry
        ));
    }
}