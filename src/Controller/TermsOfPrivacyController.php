<?php

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TermsOfPrivacyController extends CustomController
{
    /**
     * @Route("/terms-of-privacy", name="termsOfPrivacy")
     */
    public function termsOfPrivacy() {

        return $this->render("termsOfPrivacy.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}