<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\pfeature;
use App\Entity\ProductCountry;
use App\Library\CustomController;
use App\Library\ProductSerializer;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends CustomController
{
    /**
     * @SWG\Get(
     *   path="/{country}/{language}/product",
     *   description="Get Product",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     )
     * )
     * @Route("/product", name="getProduct", methods={"GET"})
     * @param ProductSerializer $serializer
     * @return Response
     */
    public function getProduct(ProductSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var ProductCountry $product
         */
        $product = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "active"=>1,
            "deletedAt"=>null)
        );




        $product = $em->getRepository(pfeature::class)->findBy(array(
          "deletedAt"=>0)
        );

        // print_r($product);


/*        print_r($product->getActive());
        exit;*/
        $serializer->setLocale($this->country, $this->lang, $this->globalLang, $this->globalCountry);
        $json = $serializer->serialize(
            $product,
            "json"
        );


        return new Response($json);
    }

    /**
     * @SWG\Get(
     *   path="/{country}/{language}/product/{id}",
     *   description="Get Product By Id",
     *   @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer"
     *     ),
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     )
     * )
     * @Route("/product/{id}", name="getProductById", methods={"GET"})
     * @param ProductSerializer $serializer
     * @param $id
     * @return Response
     *
     */
    public function getProductById($id, ProductSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->findOneBy(array(
            "link" => $id,
            "deletedAt" => null
        ));

        /**
         * @var ProductCountry[] $product
         */
        $product = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "product" => $product,
            "active"=>1,
            "deletedAt"=>null)
        );

        $serializer->setLocale($this->country, $this->lang, $this->globalLang, $this->globalCountry);
        $json = $serializer->serialize(
            $product,
            "json"
        );

        return new Response($json);
    }

    /**
     * @SWG\Get(
     *   path="/{country}/{language}/allproduct",
     *   description="Get All Product",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     )
     *)
     * @Route("/allproduct", name="getAllProduct", methods={"GET"})
     * @param ProductSerializer $serializer
     * @return Response
     */
    public function getAllProduct(ProductSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var ProductCountry $product
         */
        $product = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "deletedAt" => null
        ));

        $serializer->setLocale($this->country,$this->lang, $this->globalLang, $this->globalCountry);
        $json = $serializer->serialize(
            $product,
            'json'
        );

        return new Response($json);
    }
}
