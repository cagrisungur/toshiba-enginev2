<?php

namespace App\Controller;

use App\Entity\Specification;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SpecificationController extends CustomController
{
    /**
     * @Route("/specification/{id}", name="getSpecificationById", methods={"GET"})
     * @param $id
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getSpecificationById($id, SerializerInterface $serializer)
    {
        $country = $this->country;
        $lang = $this->lang;
        $em = $this->getDoctrine()->getManager();

        $spec =  $em->getRepository(Specification::class)->getSpecificationById($id,$lang,$country);

        $json = $serializer->serialize(
            $spec,
            'json'
        );

        return new Response($json);
    }
}