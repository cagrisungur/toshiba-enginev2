<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 26.09.2018
 * Time: 17:09
 */

namespace App\Controller;


use App\Entity\ContactMail;
use App\Entity\FormMessage;
use App\Entity\FormMessageType;
use App\Entity\SmtpSetting;
use App\Library\CustomController;
use http\Env\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class ContactFormController extends CustomController
{
    /**
     * @SWG\Post(
     *   path="/{country}/{language}/contactform",
     *   description="Contact Form",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="tr"
     *     ),
     *   @SWG\Parameter(
     *      name="name",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     ),
     *   @SWG\Parameter(
     *      name="surname",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     ),
     *   @SWG\Parameter(
     *      name="mail",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     ),
     *   @SWG\Parameter(
     *      name="subject",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     ),
     *   @SWG\Parameter(
     *      name="message",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     ),
     *   @SWG\Parameter(
     *      name="code",
     *      description="contactForm",
     *      in="formData",
     *      required=true,
     *      type="string"
     *     )
     * )
     * @Route("contactform", name="contactForm", methods={"POST"})
     */
    public function contactForm(\Swift_Mailer $mailer) {
        $em = $this->getDoctrine()->getManager();
        $contactMail = $em->getRepository(ContactMail::class)->findBy(array(
            "country" => $this->country,
            "active" => 1,
            "deletedAt" => null
        ));
        $smtpSettings = $em->getRepository(SmtpSetting::class)->findBy(array(
            "country" => $this->country,
            "deletedAt" => null,
        ));

        $formMessageTypeByCode = $em->getRepository(FormMessageType::class)->findOneBy(["code" => $_POST["code"]]);
        if (isset($_POST["message"]) && isset($_POST["subject"]) && isset($_POST["mail"]) && isset($_POST["mail"]) && isset($_POST["name"]) && isset($_POST["surname"])) {
            foreach ($smtpSettings as $smtpSetting){
                foreach ($contactMail as $mails)  {

                    $transport = (new \Swift_SmtpTransport($smtpSetting->getHost(),$smtpSetting->getPort(),'SSL'))
                        ->setUsername($smtpSetting->getUserName())
                        ->setPassword($smtpSetting->getPassword());

                    $mailer = new \Swift_Mailer($transport);

                    $message = (new \Swift_Message($_POST["subject"]))
                        ->setFrom($smtpSetting->getMail())
                        ->setTo($mails->getMail())
                        ->setBody($_POST["message"]);

                    $mailer->send($message);

                    $formRegister = new FormMessage();

                    $formRegister->setMessage($_POST["message"]);
                    $formRegister->setMail($_POST["mail"]);
                    $formRegister->setName($_POST["name"]);
                    $formRegister->setSurname($_POST["surname"]);
                    $formRegister->setSubject($_POST["subject"]);
                    $formRegister->setType($formMessageTypeByCode);

                    $em->persist($formRegister);
                    $em->flush();

                }
            }

        }
        exit;
    }
}