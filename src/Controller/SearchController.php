<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 30.07.2018
 * Time: 10:26
 */

namespace App\Controller;


use App\Entity\Product;
use App\Entity\ProductCountry;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends CustomController
{
    /**
     * @SWG\Get(
     *   path="/{country}/{language}/search/{search}",
     *   description="Get Products By Search Value",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *     @SWG\Parameter(
     *          name="search",
     *          in="path",
     *          required=true,
     *          type="string"
     *      )
     * )
     * @Route("/search/{search}", name="getProductBySearch", methods={"GET"})
     * @param $search
     * @return JsonResponse
     */
    public function getProductBySearch($search){

        $countryId = $this->country;
        $em = $this->getDoctrine()->getManager();

        $searchProduct =  $em->getRepository(ProductCountry::class)->getProductBySearched($countryId, $search);
        $data = array();

        foreach ($searchProduct as $value) {
            $productBuy = "";
            if (isset($value["product"]["productBuy"])) {
                $productBuy = $value["product"]["productBuy"];
            }
            $array  = array(
                "id" => $value["product"]["id"],
                "name" => $value["product"]["name"],
                "link" => $value["product"]["link"],
                "buyNow" => $productBuy
            );

            $tmpImg = array();
            foreach ($value["product"]["images"] as $image) {
                $tmpImg[$image["type"]["code"]][] = array(
                  "name" => $image["name"],
                  "path" => $image["path"]
                );
                $array["images"] = $tmpImg;
            }

            $tmpText = array();
            foreach ($value["product"]["text"] as $text) {
                $tmpText[] = array(
                    "subtitle" => $text["subtitle"],
                    "shortDescription" => $text["shortDescription"],
                    "longDescription" => $text["longDescription"]
                );
                $array["text"] = $tmpText;
            }

            $tmpFile = array();
            foreach ($value["product"]["file"] as $file) {
                    $tmpFile[$file["file"]["type"]["code"]][] = array(
                        "name" => $file["file"]["name"],
                        "path" => $file["file"]["path"]
                    );
                    $array["files"] = $tmpFile;
            }

            foreach ($value["product"]["category"]["file"] as $catFile) {
                    $code = $catFile["file"]["type"]["code"];
                    $tmpFile[$code][] = array(
                        "name" => $catFile["file"]["name"],
                        "path" => $catFile["file"]["path"]
                    );
                    $array["files"] = $tmpFile;
            }
            $data[] = $array;
        }
        return new JsonResponse($data);
    }
}