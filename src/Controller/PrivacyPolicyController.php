<?php

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrivacyPolicyController extends CustomController
{
    /**
     * @Route("/privacy-policy", name="privacyPolicy")
     */
    public function privacyPolicy() {

        return $this->render("privacyPolicy.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}