<?php
namespace App\Controller;

use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\ProductCountry;
use App\Entity\FilterCategory;
use App\Entity\Filter;
use App\Library\CustomController;
use App\Library\FilterSerializer;
use App\Library\ProductSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use \SimpleExcel;
//use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

class FiltersController extends CustomController {

    public function getCat($id){


                                            $conn = $this->getDoctrine()->getConnection();
                                            $sql = '



                                                         SELECT
         *

                                                         FROM category where parent_id='.intval($id).'  and deleted_at is null



                               ';
                               //and
                               // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


                                            $stmt = $conn->prepare($sql);
                                            $stmt->execute();
                                            $arr = $stmt->fetchAll();
                                            return $arr;
    }

    public function topCat(){


                                            $conn = $this->getDoctrine()->getConnection();
                                            $sql = '



                                                         SELECT
         *

                                                         FROM category where parent_id is null and deleted_at is null



                               ';
                               //and
                               // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


                                            $stmt = $conn->prepare($sql);
                                            $stmt->execute();
                                            $arr = $stmt->fetchAll();
                                            return $arr;
    }

    /**
     * @Route("download-ib/{url}", name="downloadIB")
     */
    public function downloadIB($url) {

//        $em = $this->getDoctrine()->getManager();
//
//        $productCodes = $em->getRepository(Product::class)->findBy(array(
//            "deletedAt" => null
//        ));
//
//        foreach ($productCodes as $code) {
//            echo '<pre>';
//            print_r("http://34.244.173.118:6060/eu/en/download-ib/".$code->getLink());
//        }exit;

        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT product.link,file.path FROM product_file
        INNER JOIN product ON product.id = product_file.product_id  
        INNER JOIN file ON file.id = product_file.file_id 
        WHERE file.type_id = 2 AND product.link='".$url."'";



                               ;
        //and
        // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $arr = $stmt->fetchAll();
            $fileurl = $arr[0]["path"];
            header("Content-type:application/pdf");
            header('Content-Disposition: attachment; filename=' . $arr[0]["link"].'.pdf');
            readfile( $fileurl );

exit;


    }


        public function getProducts($id){

                                            $conn = $this->getDoctrine()->getConnection();
                                            $sql = '



                                                         SELECT
         *

                                                         FROM product where


                                                         category_id = '.intval($id).' and deleted_at is null
                                                          and active=1



                               ';
                               //and
                               // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


                                            $stmt = $conn->prepare($sql);
                                            $stmt->execute();
                                            $arr = $stmt->fetchAll();
                                            return $arr;
    }


    /**
     * @param $url
     * @Route("filt/{url}")
     */
                                         public function xlsx($url) {



                                             $em = $this->getDoctrine()->getManager();

                                                             if(strlen($url) > 15 || strlen($url) < 3){
                                                                 die("unknown");
                                                             }


                                                             $product = $em->getRepository(Product::class)->findOneBy(array(
                                                                 "link" => $url,
                                                                 "deletedAt" => null
                                                             ));


                                                             if($product == null or $product == ''){
                                                                 die("unknown");
                                                             }


                                                               $lang = $em->getRepository(Language::class)->findOneBy(array(
                                                                   "code" => $this->lang->getCode(),
                                                                   "deletedAt" => null
                                                               ));
                                                             $country = $em->getRepository(Country::class)->findOneBy(array(
                                                                 "code" => $this->country->getCode(),
                                                                 "deletedAt" => null
                                                             ));

                                                             $data = array();
                                                             $data['id'] = $product->getId();
                                                             $data['name'] = $product->getName();
                                                             $data['code'] = $product->getCode();
                                                             $data['link'] = $product->getLink();
                                                             $data['keyw'] = $product->getKeywords();
                                                             $data['active'] = $product->getActive();
                                                             $data['created'] = $product->getCreatedAt();

                                                             $data['cat'] = $product->getCategory()->getName();
                                                             $data['catID'] = $product->getCategory()->getID();


                                                             $data['series'] = array();
                                                             $data['series']['name'] = $product->getSeries()->getName();
                                                             $data['series']['code'] = $product->getSeries()->getCode();

                                                            foreach($product->getImages()->toArray() as $row){
                                                                 $tmp = array();
                                                                 if($row->getActive()){
                                                                     $tmp['name'] = $row->getName();
                                                                     $tmp['path'] = $row->getPath();
                                                                 }
                                                                 $data['images'][] = $tmp;

                                                            }


                                                           $tmpSpecs = array();
                                                           $tmpCats = array();
                                                         if ($product->getSpec($lang)->toArray() !== array()) {
                                                             foreach($product->getSpec($lang)->toArray() as $row){
                                                                 $tmp = array();

                                                                 $catID   = $row->getSpec()->getCategory()->getId();
                                                                 $catName = $row->getSpec()->getCategory()->getName();
                                                                 $tmp['specID']    = $row->getSpec()->getID();
                                                                 $tmp['spec']    = $row->getSpec()->getName();
                                                                 $tmp['val']     = $row->getValue();


                                                                 if(!isset($data['specs'][$catID])){
                                                                     $tmpCats = array();
                                                                     $tmpCats['catid'] = $catID;
                                                                     $tmpCats['catname'] = $catName;
                                                                     $data['specs'][$catID] = $tmpCats;
                                                                 }

                                                                 $data['specs'][$catID]['specs'][] = $tmp;


                                                                 $data['specs'] = array_values($data['specs']);
                                                             }
                                                         } else {
                                                             $ln = $em->getRepository(Language::class)->findAll();
                                                             foreach ($ln as $l) {
                                                                 if ($product->getSpec($l)->toArray() !== array()) {
                                                                     foreach($product->getSpec($l)->toArray() as $row){
                                                                         $tmp = array();
                                                                         $catID   = $row->getSpec()->getCategory()->getId();
                                                                         $catName = $row->getSpec()->getCategory()->getName();
                                                                         $tmp['specID']    = $row->getSpec()->getID();
                                                                         $tmp['spec']    = $row->getSpec()->getName();
                                                                         $tmp['val']     = $row->getValue();



                                                                         if(!isset($data['specs'][$catID])){
                                                                             $tmpCats = array();
                                                                             $tmpCats['catid'] = $catID;
                                                                             $tmpCats['catname'] = $catName;
                                                                             $data['specs'][$catID] = $tmpCats;
                                                                         }

                                                                         $data['specs'][$catID]['specs'][] = $tmp;


                                                                         $data['specs'] = array_values($data['specs']);
                                                                     }
                                                                 }
                                                             }
                                                         }

                                                        if ($product->getFeature()) {
                                                            foreach($product->getFeature()->toArray() as $row){
                                                                $tmp = array();
                                                                $tmp['id'] = $row->getFeature()->getId();
                                                                $tmp['active'] = $row->getFeature()->getActive();
                                                                $tmp['name'] = $row->getFeature()->getName();
                                                                $tmp['long'] = $row->getFeature()->getText($country,$lang)->toArray()[0]->getShortDescription();
                                                                $tmp['long'] = $row->getFeature()->getText($country,$lang)->toArray()[0]->getLongDescription();
                                                                $tmp['img'] = $row->getFeature()->getImage()->toArray()[0]->getPath();
                                                                $data['features'][] = $tmp;


                                                            }
                                                        }



                                                         $data['highligted'] = array();
                                                         foreach($product->getHighlightedText()->toArray() as $row){

                                                             if ($row->getTranslate($country,$lang)->toArray()) {
                                                                 $data['highligted'][] = $row->getTranslate($country,$lang)->toArray()[0]->getText();
                                                             }
                                                         }

                                                         /*
                                                         *   div highligeteds to 2 part
                                                         */
                                                         $dividedHL = array();
                                                         $divHLcols = intval(count($data['highligted'])/2);
                                                         $divHLnonEq = $divHLcols*2 != count($data['highligted']);
                                                         for($x=0;$x<2;$x++){
                                                             if($divHLnonEq && $x==0){
                                                                 for($y=0;$y<=$divHLcols;$y++){
                                                                     $dividedHL[$x][] = $data['highligted'][$y];
                                                                 }
                                                             }else if($divHLnonEq && $x==1){
                                                                 for($y=1;$y<=$divHLcols;$y++){
                                                                     $dividedHL[$x][] = $data['highligted'][$x*$divHLcols+$y];
                                                                 }
                                                             }else{
                                                                 for($y=0;$y<$divHLcols;$y++){
                                                                     $dividedHL[$x][] = $data['highligted'][$x*$divHLcols+$y];
                                                                 }
                                                             }

                                                         }
                                                         $data['dividedHL'] = $dividedHL;
                                                         $data['pp'] = $data["images"][0]["path"];


                                                         //echo $
                                                         if (isset($data["features"])) {
                                                             if(count($data['features'])>=6){
                                                                 for($x=0;$x<6;$x++){
                                                                     $data['topfeatures'][] = $data['features'][$x];
                                                                 }
                                                                 for($x=6;$x<count($data['features']);$x++){
                                                                     $data['othfeatures'][] = $data['features'][$x]['name'];
                                                                 }

                                                             }

                                                         }



                                                         $cats = $this->getParent($data["catID"]).' > '.$data['cat'];







                                                                                                      require_once (__DIR__.'/../../vendor/excel/xlsxwriter.class.php');
                                                                                                     // require_once(__DIR__.'/SimpleExcel/SimpleExcel.php'); // load the main class file (if you're not using autoloader)

                                                                                                     $otdata = array(
                                                                                                         array('CategoryID','Category','specID','spec','value')

                                                                                                     );

                                                                                                        if (isset($data['specs'])) {
                                                                                                            foreach($data['specs'] as $row){
                                                                                                                foreach($row['specs'] as $inRow){
                                                                                                                    $otdata [] =  array($row['catid'],$row["catname"],$inRow['specID'],$inRow['spec'],$inRow['val']);

                                                                                                                }
                                                                                                            }
                                                                                                        }



                                                                                                    // print_r($data);
                                                                                                    // die();

                                                                                                     $ot1data = array(
                                                                                                         array($cats),
                                                                                                         array($data["name"]),
                                                                                                         array($data["code"]),
                                                                                                         array($data["code"]),

                                                                                                     );

                                                                                                      $ot1data[] = array('');
                                                                                                      $ot1data[] = array('');
                                                                                                      $ot1data[] = array('');

                                                                                                     foreach($data['highligted'] as $aarow){
                                                                                                         $ot1data[] = array($aarow);
                                                                                                     }


                                                                                                     $fdata = array();
                                                                                                     if (isset($data['features']))
                                                                                                     foreach($data['features'] as $aarow){
                                                                                                         $fdata[] = array($aarow['name']);
                                                                                                     }



                                                                                                     $writer = new \XLSXWriter();
                                                                                                     $writer->writeSheet($ot1data,"Meta");

                                                                                                     $writer->writeSheet($otdata,"Specs");

                                                                                                     $writer->writeSheet($fdata,"Features");


                                                                                                    // $writer->writeSheet($data['highligted'],"Highligteds");

                                                                                                     $writer->writeToFile('/tmp/output.xlsx');
                                                                                                     $read = file_get_contents('/tmp/output.xlsx');
                                                                                                     header('Content-Type:file');
                                                                                                     header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
                                                         header("Content-Disposition: attachment; filename=".$url.".xls");  //File name extension was wrong
                                                         header("Expires: 0");
                                                         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                                                         header("Cache-Control: private",false);

                                                                                                     header('Content-Length: ' . filesize('/tmp/output.xlsx'));
                                                                                                     readfile('/tmp/output.xlsx');

                                                                                                     echo $read;






                                                         //print_r($data['topfeatures']);
                                                     //    print_r($data['othfeatures']);




                                                         /*echo $header = $this->render('pdfGen.html.twig', array(
                                                             "product"=>$data,
                                                             "rootDir"=>"",
                                                         ));*/

                                             /*
                                                         $snappyPdf = $this->get("knp_snappy.pdf");
                                                         $fileName =  json_decode($json)[0]->name;
                                                         $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

                                                         $html =  $this->render("jvcPdfGenerator.html.twig", array(
                                                             "product" => json_decode($json)[0],
                                                             'rootDir' => $this->get('kernel')->getRootDir().'/..',
                                                             'baseUrl' => $baseUrl
                                                         ));

                                                         return $html;*/

                                                         //include __DIR__.'/../../vendor/tcpdf/tcpdf_include.php';
                                                         require_once (__DIR__.'/../../vendor/tcpdf/tcpdf.php');


                                                         // create new PDF document
                                                         $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);




                                                     // set document information
                                                     $pdf->SetCreator(PDF_CREATOR);
                                                     $pdf->SetAuthor('SHARP');
                                                     $pdf->SetTitle($url.'');
                                                     $pdf->SetSubject($url.'');
                                                     $pdf->SetKeywords($url.'');

                                                     // remove default header/footer
                                                     $pdf->setPrintHeader(false);
                                                     $pdf->setPrintFooter(false);

                                                     // set default monospaced font
                                                     $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                                                     // set margins
                                                     $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

                                                     // set auto page breaks
                                                     $pdf->SetAutoPageBreak(TRUE, 0);

                                                     // set image scale factor
                                                     $pdf->setImageScale(0);

                                                     // set some language-dependent strings (optional)
                                                     if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                                                         require_once(dirname(__FILE__).'/lang/eng.php');
                                                         $pdf->setLanguageArray($l);
                                                     }

                                                         // ---------------------------------------------------------

                                                         // set default font subsetting mode
                                                         $pdf->setFontSubsetting(true);

                                                         // Set font
                                                         // dejavusans is a UTF-8 Unicode font, if you only need to
                                                         // print standard ASCII chars, you can use core fonts like
                                                         // helvetica or times to reduce file size.
                                                         $pdf->SetFont('courier', '', 14, '', true);

                                                         // Add a page
                                                         // This method has several options, check the source code documentation for more information.

                                                         $pdf->SetMargins(0, 0, 0, 0);

                                             $pdf->setImageScale(1);


                                                         //$pdf->Image('http://34.244.173.118:6858/assets/img/demoFrontbg.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);




                                                                     $pdf->setListIndentWidth(8);
                                                                     $pdf->setListIndentWidth(8);




                                                                     $pdf->AddPage();
                                                                     $pdf->Image('http://34.244.173.118:6858/assets/img/pdfbgfront.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);


                                                                     $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n'
                                                                     => 0)));
                                                                     $pdf->setHtmlVSpace($tagvs);
                                                                     $pdf->SetFont('courier', '', 14, '', true);
                                                                     $html = '<p><font color="white" size="9">'.$data['code'].'</font></p>';
                                                                     $pdf->writeHTMLCell(0, 0, 19, 19, $html, 0, 1, 0, false, '', true);


                                                                     $pdf->SetFont('courier', '', 14, '', true);
                                                                     $html = '<p><font color="white" size="27">'.$data['name'].'</font></p>';
                                                                     $pdf->writeHTMLCell(0, 0, 19, 24, $html, 0, 1, 0, false, '', true);
                                                                     $pdf->SetFont('courier', '', 14, '', true);

                                                                     $html = '<p><font color="white" size="17">'.$data['keyw'].'</font></p>';
                                                                     $pdf->writeHTMLCell(0, 0, 19, 38, $html, 0, 1, 0, false, '', true);


                                                                     $html = '

                                                                     <br>
                                                                     <br>

                                                                     <div align="center">
                                                                     <center><img align="center" height="400" src="'.$data["pp"].'"></center></div>
                                                                     <br>';
                                                                     $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                                                                                 $pdf->SetFont('courier', '', 20, '', true);
                                                                     $html ='<font size="18" color="#e6000d">Highligths</font><br>';

                                                                     $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);


                                                                     $pdf->SetFont('courier', '', 14, '', true);

                                                                     $html = '
                                                                     <font size="10" family="courier">
                                                                     <table width="450">
                                                                         <tr>
                                                                             <td><ul>';
                                                                             foreach($data['dividedHL'][0] as $row){
                                                                                 $html .= '<li>'.$row.'</li>';
                                                                             }

                                                                             $html .= '</ul></td>
                                                                             <td><ul>';
                                                                             foreach($data['dividedHL'][1] as $row){
                                                                                 $html .= '<li>'.$row.'</li>';
                                                                             }

                                                                             $html .= '</ul></td>
                                                                         </tr>
                                                                     </table>
                                                                     </font>';

                                                                     $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);












                                             $features=array();

                                             //$data["features"] = array_merge($data["features"],$data["features"]);
                                             //$data["features"] = array_merge($data["features"],$data["features"]);



                                             if(count($data["features"])<=8){
                                                 $featuresRaw = array_chunk($data["features"], ceil(count($data["features"])/2));

                                                 $features = '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<table width="530">';
                                                 for($x=0;$x<count($featuresRaw[0]);$x++){

                                                     $features .= '<tr><td height="30" colspan="2"><b><font face="courier" align="left" color="#e20e16" size="18">'.$featuresRaw[0][$x]["name"].'</font></b></td><td width="10"></td><td colspan="2"><b><font size="20" face="courier" color="#e20e16">'.(isset($featuresRaw[1][$x]["name"]) ? $featuresRaw[1][$x]["name"]:'').'</font></b></td></tr>';
                                                     $features .= '<tr><td width="110" height="100">';
                                                     $features .= '<img src="'.$featuresRaw[0][$x]["img"].'" width="100">';

                                                     $features .= '</td><td>';
                                                     $features .= '<p><font face="courier">'.$featuresRaw[0][$x]["long"].'</font></p>';
                                                     $features .= '</td><td width="10">';
                                                     $features .= '</td><td width="110">';
                                                     if(isset($featuresRaw[1][$x])){
                                                         $features .= '<img src="'.$featuresRaw[1][$x]["img"].'" width="100">';
                                                     }
                                                     $features .= '</td><td>';
                                                     if(isset($featuresRaw[1][$x])){
                                                         $features .= '<p><font face="courier">'.$featuresRaw[1][$x]["long"].'</font></p>';
                                                     }
                                                     $features .= '</td></tr>';




                                                 }
                                                 $features .= '</table>';
                                                 //    print_r($features);
                                             }else{

                                                 $featuresRaw = array();
                                                 for($x=0;$x<6;$x++){
                                                     $featuresRaw[] =$data["features"][$x];
                                                 }

                                                 $featuresText = array();
                                                 for($x=6;$x<count($data["features"]);$x++){
                                                     $featuresText[] =$data["features"][$x];
                                                 }


                                                 $featuresRaw = array_chunk($featuresRaw, ceil(count($featuresRaw)/2));

                                                 $features = '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<table width="530">';
                                                 for($x=0;$x<count($featuresRaw[0]);$x++){

                                                     $features .= '<tr><td height="30" colspan="2"><b><font face="courier" align="left" color="#e20e16" size="18">'.$featuresRaw[0][$x]["name"].'</font>';
                                                     $features .= '</b></td><td width="30"></td><td colspan="2"><b><font size="20" face="courier" color="#e20e16">'.(isset($featuresRaw[1][$x]["name"]) ? $featuresRaw[1][$x]["name"]:'').'</font></b></td></tr>';
                                                     $features .= '<tr><td width="110" height="100">';
                                                     $features .= '<img src="'.$featuresRaw[0][$x]["img"].'" width="100">';

                                                     $features .= '</td><td>';
                                                     $features .= '<p><font face="courier">'.$featuresRaw[0][$x]["long"].'</font></p>';
                                                     $features .= '</td><td width="30">';
                                                     $features .= '</td><td width="110">';
                                                     if(isset($featuresRaw[1][$x])){
                                                         $features .= '<img src="'.$featuresRaw[1][$x]["img"].'" width="100">';
                                                     }
                                                     $features .= '</td><td>';
                                                     if(isset($featuresRaw[1][$x])){
                                                         $features .= '<p><font face="courier">'.$featuresRaw[1][$x]["long"].'</font></p>';
                                                     }
                                                     $features .= '</td></tr>';




                                                 }
                                                 $features .= '</table>';



                                                 $featuresText = array_chunk($featuresText, ceil(count($featuresText)/3));

                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<br>';
                                                 $features .= '<table width="500">';
                                                 $pdf->SetFont('courier', '', '', 'li', true);

                                                 $features .= '<tr>';
                                                 $features .= '<td>'.'<font face="courier" align="left" color="#e20e16" size="18">More Features</font>'.'</td>';
                                                 $features .= '<td><ul>';
                                                 foreach($featuresText[0] as $row){
                                                     $features .= '<li><font face="courier" size="9">'.$row['name'].'</font><br></li>';
                                                 }
                                                 $features .= '</ul></td>';


                                                 $features .= '<td><ul>';
                                                 foreach($featuresText[1] as $row){
                                                     $features .= '<li><font face="courier" size="9">'.$row['name'].'</font><br></li>';
                                                 }
                                                 $features .= '</ul></td>';


                                                 $features .= '<td><ul>';
                                                 foreach($featuresText[2] as $row){
                                                     $features .= '<li><font face="courier" size="9">'.$row['name'].'</font><br></li>';
                                                 }
                                                 $features .= '</ul></td>';



                                                 $features .= '</tr>';



                                                 /*
                                                 for($x=0;$x<count($featuresText[0]);$x++){
                                                     if($x==0){
                                                         $features .= '<tr>';
                                                         $features .= '<td colspan="4">'.'<font face="courier" align="left" color="#e20e16" size="18">More Features</font>'.'</td>';
                                                         $features .= '</tr>';
                                                     }else{
                                                         $features .= '<tr>';
                                                         $features .= '<td> </td>';
                                                         $features .= '<td>'.(isset($featuresText[0][$x]) ? $featuresText[0][$x]['name']:'').'</td>';
                                                         $features .= '<td>'.(isset($featuresText[1][$x]) ? $featuresText[1][$x]['name']:'').'</td>';
                                                         $features .= '<td>'.(isset($featuresText[2][$x]) ? $featuresText[2][$x]['name']:'').'</td>';
                                                         $features .= '</tr>';
                                                     }



                                                 }*/

                                                 $features .= '</table>';

                                                 //    print_r($features);

                                             }




                                                 $pdf->AddPage();
                                                 $pdf->Image('http://34.244.173.118:6858/assets/img/pdfbgsec.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
                                                 $pdf->SetFont('courier', '', 9, 'td', true);
                                                 $pdf->SetFont('courier', '', 9, 'b', true);
                                                 //$pdf->SetFont('courier', '', 10, 'td', true);

                                                 $html = $features;

                                                 $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);











                                             $tmp = array();
                                             foreach($data["specs"] as $row){
                                                 $tmp[] = '<tr><td colspan="2" height="18"><font size="9" color="#f00" face="courier">'.$row['catname'].'</font></td></tr>';
                                                 $x=0;
                                                 foreach($row["specs"] as $inRow){
                                                     $x++;
                                                     $tmp[] = '<tr><td height="18"><font size="9" face="courier">'.$inRow['spec'].'</font></td><td><font size="9" face="courier">'.$inRow["val"].'</font></td></tr>';
                                                     if($x == count($row["specs"])){
                                                         $tmp[] = '<tr><td height="18" colspan="2"><br></td></tr>';
                                                         $tmp[] = '<tr><td height="18" colspan="2"><br></td></tr>';
                                                     }
                                                 }
                                             }
                                             /*
                                             $tmp = array();
                                             foreach($data["specs"] as $row){
                                                 $tmp[] = $this->specHead($row);
                                                 foreach($row["specs"] as $inRow){
                                                     $tmp[] = $this->specRow($inRow);
                                                 }
                                             }
                                             */




                                             $specParts = array_chunk($tmp, 32);






                                                 $pdf->AddPage();
                                                 $pdf->Image('http://34.244.173.118:6858/assets/img/pdfbgsec.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
                                                 $pdf->SetFont('courier', '', 9, 'td', true);
                                                 $pdf->SetFont('courier', '', 9, 'b', true);
                                                 //$pdf->SetFont('courier', '', 10, 'td', true);

                                                 $html = '
                                                 <br><br><br><br><br><br>
                                                 <table width="500">
                                                     <tr>
                                                         <td>
                                                         <table width="100%">
                                                         ';
                                                         if(isset($specParts[0])){
                                                             foreach($specParts[0] as $row){
                                                                 $html .= $row;
                                                             }
                                                         }

                                                         $html .= '</table></td><td><table width="100%">';
                                                         if(isset($specParts[1])){
                                                             foreach($specParts[1] as $row){
                                                                 $html .= $row;
                                                             }
                                                         }

                                                         $html .= '
                                                         </table>
                                                         </td></tr>
                                                 </font>';

                                                 $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);





                                                 $pdf->AddPage();
                                                 $pdf->Image('http://34.244.173.118:6858/assets/img/pdfbgsec.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
                                                             $pdf->SetFont('courier', '', 14, 'B', true);
                                                             $pdf->SetFont('courier', '', 10, 'td', true);

                                                             $html = '
                                                             <br><br><br><br><br><br>
                                                             <table width="500">
                                                                 <tr>
                                                                     <td>
                                                                     <table width="100%">
                                                                     ';
                                                                     if(isset($specParts[2])){
                                                                         foreach($specParts[2] as $row){
                                                                             $html .= $row;
                                                                         }
                                                                     }


                                                                     $html .= '</table></td><td><table width="100%">';
                                                                     if(isset($specParts[3])){
                                                                         foreach($specParts[3] as $row){
                                                                             $html .= $row;
                                                                         }
                                                                     }

                                                                     $html .= '
                                                                     </table>
                                                                     </td></tr>
                                                             </font>';

                                                             $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);





                                                         // ---------------------------------------------------------

                                                         // Close and output PDF document
                                                         // This method has several options, check the source code documentation for more information.
                                                         $pdf->Output($url.'.pdf', 'I');




                                                    die();


                                                         //'path' => $path
                                                 }











































    /**
    * @Route("filters/fcats", name="listfiltercats", methods={"GET"})
    */
                                     public function listfiltercats($country,$language) {






//$arr = $this->getCat(0);
$arr = $this->topCat();

                                    $fcatids = array();
                                    foreach($arr as $row){
                                        echo '<li><b>'.$row["name"].'</b>';
                                        echo '<ul>';


                                        ///
                                        echo '<ul>';
                                        $otrarr = $this->getProducts($row["id"]);
                                        foreach($otrarr as $otr){
                                            echo '<li>  '.$row["name"].' - '.$inRow["name"].' - '.$ininrow["name"].'   ['.$inininrow["code"].'] '.$inininrow["name"].'</li>';
                                            echo '<li>['.$otr["code"].'] '.$otr["name"].'</li>';
                                        }
                                        echo '</ul>';
                                        /////



                                        $inArr = $this->getCat($row["id"]);
                                        foreach($inArr as $inRow){
                                            echo '<li>'.$row["name"];

                                            ///
                                            echo '<ul>';
                                            $otrarr = $this->getProducts($inRow["id"]);
                                            foreach($otrarr as $otr){
                                            //    echo '<li>  '.$row["name"].' - '.$inRow["name"].' - '.$ininrow["name"].'   ['.$inininrow["code"].'] '.$inininrow["name"].'</li>';
                                                echo '<li>['.$otr["code"].'] '.$otr["name"].'</li>';
                                                //    echo '"'.$row["name"].'";"'.$inRow["name"].'";" - ";"'.$otr["code"].'";"'.$otr["name"].'"<br>';
                                            }
                                        echo '</ul>';
                                            /////



                                            echo '<ul>';
                                            $ininarr = $this->getCat($inRow["id"]);
                                            foreach($ininarr as $ininrow){
                                                echo '<li>'.$ininrow["name"];




                                                ///
                                                echo '<ul>';
                                                $inininarr = $this->getProducts($ininrow["id"]);
                                                foreach($inininarr as $inininrow){
                                                    //echo '"'.$row["name"].'";"'.$inRow["name"].'";"'.$ininrow["name"].'";"'.$inininrow["code"].'";"'.$inininrow["name"].'"<br>';
                                                    echo '<li>['.$inininrow["code"].'] '.$inininrow["name"].'</li>';
                                                }
                                                echo '</ul>';
                                                /////

                                                echo '</li>';


                                            }
                                            echo '</ul>';
                                            echo '</li>';
                                        }
                                        echo '</ul>';
                                        echo '</li>';
                                    }







                                         die("");




                                                                   $conn = $this->getDoctrine()->getConnection();
                                                                   $sql = '



                                                                                SELECT

                                                                                fi.id,
                                                                                fi.name,
                                                                                fc.name as fname,
                                                                                fc.id as fcatid

                                                                                FROM filters_cats fc

                                                                                left join filters fi on fi.filtercat = fc.id
                                                                                left join filters_specids sids on sids.filter = fi.id
                                                                                left join filters_specvalues svals on svals.parent = fi.id
                                                                                left join specification s on s.id = sids.spec





                                                                                group by fi.id
                                                      ';
                                                      //and
                                                      // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


                                                                   $stmt = $conn->prepare($sql);
                                                                   $stmt->execute();
                                                                   $arr = $stmt->fetchAll();


                                                                   $fcatids = array();
                                                                   foreach($arr as $row){
                                                                       $fcatids[$row['fcatid']] = $row['fname'];
                                                                   }


                                                                   $filterPool = array();
                                                                   foreach($arr as $row){
                                                                       $filterPool[$row['fcatid']]['filters'][] = array('name'=>$row['name'],'matched'=>0,'id'=>$row["id"]);
                                                                   }

                                                                   foreach($fcatids as $key=>$val){
                                                                       $filterPool[$key]['name'] = $val;
                                                                       //$filterPool[$key]['id'] = $key;
                                                                   }
                                                                   $filterPool = array_values($filterPool);
                                                                  // print_r($filterPool);

                                                                   //die();


                                                                   foreach($filterPool as $row){
                                                                       echo '<b>'.$row["name"].'</b>';
                                                                   foreach($row['filters'] as $inRow){
                                                                          echo '<li>['.$inRow["id"].']'.$inRow["name"].'</li>';//.' <em style="color:#f00">'.$inRow["matched"].' adet bulundu</em>'

                                                                          $this->getRelatedValues($inRow["id"]);

                                                                   }
                                                                   }




                                                                   echo '

                                                                       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
                                                                       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap-theme.min.css">
                                                                       <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

                                                                       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                                                                       <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
                                                                          <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
                                                                       <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput/bootstrap-tagsinput-angular.min.js"></script>


                                                                   <script>

                                                                   $("input").tagsinput({

                                                                   });

                                                                   $("button").click(function(){
                                                                       var id = $(this).attr("data-id");
                                                                       $.post( "/eu/en/filters/ajax/"+id, { name: $(".vals"+id).val(), time: "2pm" })
                                                                               .done(function( data ) {

                                                                         });
                                                                   })

                                                                   </script>

                                                                       ';



            die(123);


            }



        function isExistChild($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
        }

        function getParent($id){
            $text='';

            $data = array();
            $text = array();
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT * FROM category where id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            for($x=0;$x<10;$x++){



            if(count($arr) > 0){
                $tmpData = array();
                $tmpData['name'] = $arr[0]['name'];
                $tmpData['parent'] = $arr[0]['parent_id'];
                $nextParent = $tmpData['parent'];
                $data[] = $tmpData;
                $text[] = $arr[0]['name'];
                //print_r($data);
            }

            if($nextParent==0){
                $x=11;
            }

            $sql = 'SELECT * FROM category where id='.intval($nextParent);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            }

            return implode(' > ',$text);



        }




        function getChilds($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT id FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            $all = array();
            foreach($arr as $row){
                $all[] = $row['id'];
                if($this->isExistChild($row['id'])){
                    $res = $this->getChilds($row['id']);
                    foreach( $res as $inRow){
                        $all[] = $inRow;
                    }

                }

            }
            return $all;//
        }



        public function getCats($id){
            $conn = $this->getDoctrine()->getConnection();

            $sql = '
        SELECT * FROM `category` WHERE `parent_id` ='.$id.'
            ';


            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $tmp = $stmt->fetchAll();
            return $tmp;
        }






                public function getRelatedValues($id){
                    if(intval($id)<1){
                        return;
                    }
                    $conn = $this->getDoctrine()->getConnection();

                    $sql = '
                SELECT * FROM `filters_specvalues` WHERE `parent` ='.$id.'
                    ';



                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $ootmp = $stmt->fetchAll();

                        echo '<li style="argin-left:50px;color:#0f1f1f">';
                        echo '<button data-id="'.$id.'">UP</button><input class="vals'.$id.'" type="text" value="';
                        if(count($ootmp)>0){
                        foreach($ootmp as $oorow){
                           echo ''.$oorow['val'].',';
                        }
                    }
                        echo '">';

                        /*echo '<textarea>';
                        foreach($ootmp as $oorow){
                           echo ''.$oorow['val'].',';
                        }
                        echo '</textarea>';*/
                        echo '</li>';



                /*    if(count($ootmp)>0){
                        echo '<ul style="font-size:10pt;color:#f0f0f0;">';
                        foreach($ootmp as $oorow){
                           echo '<li><a>['.$oorow['id'].']'.$oorow['val'].'</a></li>';
                        }
                        echo '</ul>';
                    }*/

                }



        public function getRelatedSpecs($id){
            if(intval($id)<1){
                return;
            }
            $conn = $this->getDoctrine()->getConnection();

            $sql = '
        SELECT s.name,s.id FROM filters_specids sids
        left join specification s on s.id=sids.spec

         WHERE sids.filter ='.$id.'
            ';



            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $ootmp = $stmt->fetchAll();
            if(count($ootmp)>0){
                echo '<li style=" margin-left:50px;font-size:10pt;color:#f0f;">';
                foreach($ootmp as $oorow){
                   echo '<a>['.$oorow['id'].']'.$oorow['name'].'</a></li>';

                }
                $this->getRelatedValues($id);
                echo '</li>';
            }

        }





        public function getRelatedFilters($id){
            if(intval($id)<1){
                return;
            }
            $conn = $this->getDoctrine()->getConnection();

            $sql = '
        SELECT * FROM filters
         WHERE filtercat ='.$id.'
            ';


            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $ootmp = $stmt->fetchAll();
            if(count($ootmp)>0){
                echo '<ul style="margin-left:25px;font-size:10pt;color:#00f;">';
                foreach($ootmp as $oorow){
                   echo '<li><a>['.$oorow['id'].']'.$oorow['name'].'</a></li>';
                   $this->getRelatedSpecs($oorow["id"]);
                }
                echo '</ul>';
            }

        }


        public function getRelatedCats($id){
            $conn = $this->getDoctrine()->getConnection();

            $sql = '
        SELECT fc.name,fc.id FROM filters_productcatrel fpcr
        left join filters_cats fc on fc.id=fpcr.filtercat

         WHERE fpcr.cats ='.$id.'
            ';


            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $ootmp = $stmt->fetchAll();
            if(count($ootmp)>0){
                echo '<ul style="margin-left:25px;font-size:10pt;color:#f00;">';
                foreach($ootmp as $oorow){
                   echo '<li>['.$oorow['id'].']'.$oorow['name'];
                   $this->getRelatedFilters($oorow["id"]);
                   echo '</li>';
                }
                echo '</ul>';
            }

            //return $tmp;
        }





            /**
             * @SWG\Get(
             *   path="/{country}/{language}/filters",
             *   description="filters",
             *   @SWG\Parameter(
             *         name="country",
             *         in="path",
             *         required=true,
             *         type="string",
             *         default="eu"
             *     ),
             *   @SWG\Parameter(
             *         name="language",
             *         in="path",
             *         required=true,
             *         type="string",
             *         default="en"
             *     )
             * )
             * @Route("/filters/dd", name="allCategoryCombos", methods={"GET"})
             */
                 public function allCategoryCombos($country,$language) {


                                               $conn = $this->getDoctrine()->getConnection();

                                               $sql = '
                                          SELECT * FROM `category` WHERE `parent_id` is null
                                               ';


                                               $stmt = $conn->prepare($sql);
                                               $stmt->execute();
                                               $tmp = $stmt->fetchAll();



                     foreach($tmp as $row){
                         echo '<li><a>'.$row['name'].'</a></li>';
                         $this->getRelatedCats($row['id']);
                         $inTmp = $this->getCats($row["id"]);
                         if(count($inTmp)){
                             echo '<ul>';
                             foreach($inTmp as $inRow){
                                echo '<li>'.$inRow['name'];
                                $this->getRelatedCats($inRow['id']);
                                $ininTmp = $this->getCats($inRow["id"]);
                                if(count($ininTmp)){
                                    echo '<ul>';
                                    foreach($ininTmp as $ininRow){
                                       echo '<li>'.$ininRow['name'].'';
                                       $this->getRelatedCats($ininRow['id']);
                                       echo '</li>';
                                    }
                                    echo '</ul>';
                                }
                                echo '</li>';
                             }
                             echo '</ul>';
                         }

                         //print_r($row);
                         /*foreach($arr as $inRow){

                         }*/
                     }





echo '

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
       <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput/bootstrap-tagsinput-angular.min.js"></script>


<script>

$("input").tagsinput({

});

$("button").click(function(){
    var id = $(this).attr("data-id");
    $.post( "/eu/en/filters/ajax/"+id, { name: $(".vals"+id).val(), time: "2pm" })
            .done(function( data ) {

      });
})

</script>

    ';


die();

                 }



    /**
     * @SWG\Get(
     *   path="/{country}/{language}/filters",
     *   description="filters",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="eu"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="en"
     *     )
     * )
     * @Route("/filters/{url}", name="getAllFilters", methods={"GET"})
     */
         public function getFile($url,$country,$language) {


                          $conn = $this->getDoctrine()->getConnection();

                          $sql = '
                     SELECT * FROM `filters_specids` WHERE `spec` = 12
                          ';


                          $stmt = $conn->prepare($sql);
                          $stmt->execute();
                          $tmp = $stmt->fetchAll();
                          //print_r($tmp);
                          $arr = array("Beige","Black / Red","Black / White","Black Glass Door","Charcoal gray","dark silver","grey","Pearl White","Red/White","Silver Glass Door","silver grey","Silver Steel","stainless","steel","White / Green","White / Red");
                          foreach($tmp as $row){
                              foreach($arr as $inRow){
                                  echo "INSERT INTO `filters_specvalues` (`id`, `parent`, `val`) VALUES (NULL, ".$row["id"].",\"".$inRow."\");".PHP_EOL;
                              }
                          }





die();
for($x=346;$x<362;$x++){
    echo "INSERT INTO `filters_specids` (`id`, `spec`, `filter`) VALUES (NULL, '12', ".$x.");".PHP_EOL;

}

$subs = $this->getChilds(31);

//print_r($subs);




             $conn = $this->getDoctrine()->getConnection();

             $sql = '
        SELECT * FROM `filters_specids` WHERE `spec` = 12
             ';


             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $tmp = $stmt->fetchAll();
             //print_r($tmp);
             $arr = array("Beige","Black / Red","Black / White","Black Glass Door","Charcoal gray","dark silver","grey","Pearl White","Red/White","Silver Glass Door","silver grey","Silver Steel","stainless","steel","White / Green","White / Red");
             foreach($subs as $row){
                 foreach($arr as $inRow){
                     echo "INSERT INTO `filters_specvalues` (`id`, `parent`, `val`) VALUES (NULL, ".$row.",\"".$inRow."\");".PHP_EOL;
                 }
             }



die();


             $conn = $this->getDoctrine()->getConnection();

             $sql = '
        SELECT * FROM `product_spec` WHERE `spec_id` = 12 group by refval
             ';


             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $tmp = $stmt->fetchAll();
             foreach($subs as $row){
                 echo "INSERT INTO `filters_productcatrel` (`id`, `filtercat`, `cats`) VALUES (NULL, '31', '".$row."');";
             }


             foreach($tmp as $row){
                echo $row['refval'].PHP_EOL;
             }

             print_r($tmp);







die();


             $conn = $this->getDoctrine()->getConnection();

             $sql = '
             SELECT c.name, fpcr.filtercat FROM filters_productcatrel fpcr
         left join category c on c.id = fpcr.cats

             ';
//and
// fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $catsRAW = $stmt->fetchAll();
            // print_r($specsRAW);
             $cats = array();
             foreach($catsRAW as $row){
                 $cats[$row["filtercat"]][] = array($row["name"],$row["name"]);
             }
             //print_r($specs);






             $sql = '
             SELECT spec,filter,s.name,sc.name as scat FROM filters_specids sp
 left join specification s on sp.spec = s.id
  left join specification_category sc on sc.id = s.category_id
             ';
//and
// fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $specsRAW = $stmt->fetchAll();
            // print_r($specsRAW);
             $specs = array();
             foreach($specsRAW as $row){
                 $specs[$row["filter"]][] = array($row["spec"],$row["name"],$row["scat"]);
             }
             //print_r($specs);


















             $sql = '
                    SELECT
                    fi.id,
                    fi.name,
                    fc.name as fname,
                    fc.id as fcatid
                    FROM filters_cats fc
                    left join filters fi on fi.filtercat = fc.id
                    group by fi.id
             ';
//and
// fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $arr = $stmt->fetchAll();


             $fcatids = array();
             foreach($arr as $row){
                 $fcatids[$row['fcatid']] = $row['fname'];
             }


                                       $filterPool = array();
                                       foreach($arr as $row){
                                           $filterPool[$row['fcatid']]['filters'][] = array('name'=>$row['name'],'id'=>$row["id"]);
                                       }

                                       foreach($fcatids as $key=>$val){
                                           $filterPool[$key]['name'] = $val;
                                           $filterPool[$key]['id'] = $key;
                                       }
                                       $filterPool = array_values($filterPool);
                                      // print_r($filterPool);

                                       //die();




                                       foreach($filterPool as $row){
                                          // print_r($row);
                                           if(!isset($row["name"])){
                                               continue;
                                           }
                                           echo '<b>'.$row["name"].'</b>';

                                           if(isset($cats[$row["id"]])){
                                               echo '<ul style="color:#00f">';
                                               echo '<b>';
                                               echo 'Related Categories';
                                               echo '</b>';
                                           foreach($cats[$row["id"]] as $orow){
                                               echo '<li>'.$orow[0].'</li>';
                                           }
                                            echo '</ul>';
                                        }
                                           echo '<ul>';
                                       foreach($row['filters'] as $inRow){
                                              echo '<li>['.$inRow["id"].']'.$inRow["name"].' <em style="color:#f00">'.' adet bulundu</em>';
                                              if(isset($specs[$inRow["id"]])){
                                                  echo '<ul>';
                                                  foreach($specs[$inRow["id"]] as $ininRow){
                                                      echo '<li style="color:#999">['.$ininRow[0].']related spec: '.$ininRow[2].' > '.$ininRow[1].'</li>';
                                                  }
                                                  echo '</ul>';
                                              }
                                              echo '</li>';

                                       }
                                       echo '</ul>';
                                       echo '<div style="width:100%;background-color:#000;height:10px;margin-bottom:10px;"></div>';
                                       }






die();

                          $conn = $this->getDoctrine()->getConnection();
                          $sql = '



                                       SELECT

                                       fi.id,
                                       fi.name,
                                       fc.name as fname,
                                       fc.id as fcatid,
                                      count(ps.product_id) as matched

                                       FROM filters_cats fc

                                       left join filters fi on fi.filtercat = fc.id
                                       left join filters_specids sids on sids.filter = fi.id
                                       left join filters_specvalues svals on svals.parent = fi.id
                                       left join specification s on s.id = sids.spec
                                       left join product_spec ps on ps.spec_id = sids.spec and svals.val = ps.value


                                       where
                                       ps.lang_id = 1



                                       group by fi.id
             ';
             //and
             // fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)


                          $stmt = $conn->prepare($sql);
                          $stmt->execute();
                          $arr = $stmt->fetchAll();


                          $fcatids = array();
                          foreach($arr as $row){
                              $fcatids[$row['fcatid']] = $row['fname'];
                          }


                          $filterPool = array();
                          foreach($arr as $row){
                              $filterPool[$row['fcatid']]['filters'][] = array('name'=>$row['name'],'matched'=>$row["matched"],'id'=>$row["id"]);
                          }

                          foreach($fcatids as $key=>$val){
                              $filterPool[$key]['name'] = $val;
                              //$filterPool[$key]['id'] = $key;
                          }
                          $filterPool = array_values($filterPool);
                         // print_r($filterPool);

                          //die();


                          foreach($filterPool as $row){
                              echo '<b>'.$row["name"].'</b>';
                          foreach($row['filters'] as $inRow){
                                 echo '<li>['.$inRow["id"].']'.$inRow["name"].' <em style="color:#f00">'.$inRow["matched"].' adet bulundu</em>'.'</li>';

                          }
                          }













die();



























             $conn = $this->getDoctrine()->getConnection();
             $sql = '



                          SELECT

                          fi.id,
                          fi.name,
                          fc.name as fname,
                          fc.id as fcatid,
                         count(ps.product_id) as matched

                          FROM filters_cats fc

                          left join filters fi on fi.filtercat = fc.id
                          left join filters_specids sids on sids.filter = fi.id
                          left join filters_specvalues svals on svals.parent = fi.id
                          left join specification s on s.id = sids.spec
                          left join product_spec ps on ps.spec_id = sids.spec and svals.val = ps.value


                          where
                          ps.lang_id = 1 and

                          fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN (43,1,2,30,48,19) group by pcr.filtercat)

                          group by fi.id
';
             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $arr = $stmt->fetchAll();


             $fcatids = array();
             foreach($arr as $row){
                 $fcatids[$row['fcatid']] = $row['fname'];
             }


             $filterPool = array();
             foreach($arr as $row){
                 $filterPool[$row['fcatid']]['filters'][] = array('name'=>$row['name'],'matched'=>$row["matched"],'id'=>$row["id"]);
             }

             foreach($fcatids as $key=>$val){
                 $filterPool[$key]['name'] = $val;
                 $filterPool[$key]['id'] = $key;
             }


             foreach($filterPool as $row){
                 echo '<b>'.$row["name"].'</b>';
             foreach($row['filters'] as $inRow){
                    echo '<li>['.$inRow["id"].']'.$inRow["name"].' <em style="color:#f00">'.$inRow["matched"].' adet bulundu</em>'.'</li>';

             }
             }


             //print_r($filterPool);

             //print_r($arr);




echo 'hi';


die();
                if(strlen($url) > 15 || strlen($url) < 3){
                    die("unknown");
                }



                $em = $this->getDoctrine()->getManager();
                $product = $em->getRepository(Category::class)->findOneBy(array(
                    "link" => $url,
                    "deletedAt" => null
                ));


                if($product == null or $product == ''){
                    die("unknown");
                }



            //    $id = $product->getId();
                //echo $id;
                $cats = $this->getChilds($id);
                $cats[] = $id;

                print_r($cats);


                  $lang = $em->getRepository(Language::class)->findOneBy(array(
                      "code" => $language,
                      "deletedAt" => null
                  ));
                $country = $em->getRepository(Country::class)->findOneBy(array(
                    "code" => $country,
                    "deletedAt" => null
                ));

        $json = json_encode($data);
        return new Response($json);
    }


/**
* @Route("filters/ajax/{id}", name="upcatvals", methods={"POST","GET"})
*/
                                 public function upcatvals($id,$country,$language) {
                                     $datas = explode(",",$_POST["name"]);


                                     $conn = $this->getDoctrine()->getConnection();

                                     $stmt = $conn->prepare("delete from filters_specvalues where parent=".intval($id));
                                     $stmt->execute();

                                     foreach($datas as $row){
                                         $sql = "INSERT INTO `filters_specvalues` (`id`, `parent`, `val`) VALUES (NULL, $id, '".$row."');";

                                                      $conn = $this->getDoctrine()->getConnection();

                                                      $stmt = $conn->prepare($sql);
                                                      $stmt->execute();
                                     }

        die($id);


        }




}
