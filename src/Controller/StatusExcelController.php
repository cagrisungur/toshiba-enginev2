<?php
namespace App\Controller;

use App\Entity\SocialMedia;
use App\Entity\SocialMediaType;
use App\Entity\Product;
use App\Entity\ProductFeature;
use App\Entity\Feature;
use App\Entity\FeatureType;
use App\Entity\ProductStatus;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;

class StatusExcelController extends CustomController
{

    public $inputFileType = 'Xlsx';

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("status-import", name="statusImport", methods={"POST"})
     */
    public function statusImport() {

        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($this->inputFileType);
        $reader->setReadDataOnly(true);
        $spreadSheet = $reader->load($tmpFile);
        $sheetNames = $reader->listWorksheetNames($tmpFile);
        $sheetName = $sheetNames[0];

        $spreadSheet->setActiveSheetIndex(0);
        $sheet = $spreadSheet->getActiveSheet();
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();


        for ($row = 0; $row <= $highestRow; $row++) {
            $prodCode = $sheet->getCell('A' . $row)->getValue();
            $status = $sheet->getCell('B' . $row)->getValue();
            if($status == 0) {
                $status = 4;
            }
            $findStatus = $em->getRepository(ProductStatus::class)->find($status);

            $product = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $prodCode,
                "deletedAt" => null
            ));
            if($product) {
                $product->setStatus($findStatus);
            }
            $em->flush();
        }exit;

    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @Route("feature-priority", name="featurePriority", methods={"POST"})
     */
    public function featurePriority() {
      ini_set('max_execution_time', 3000);
      ini_set('default_socket_timeout', 6000);

      $em = $this->getDoctrine()->getManager();
      $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

      $reader = IOFactory::createReader($this->inputFileType);
      $reader->setReadDataOnly(true);
      $spreadSheet = $reader->load($tmpFile);
      $sheetNames = $reader->listWorksheetNames($tmpFile);
      $sheetName = $sheetNames[0];

      $spreadSheet->setActiveSheetIndex(0);
      $sheet = $spreadSheet->getActiveSheet();
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();



        for ($row = 2; $row <= $highestRow; $row++) {
          $featureName = $sheet->getCell('A' . $row)->getValue();
          $priority = $sheet->getCell('C' . $row)->getValue();
          $findFeature = $em->getRepository(Feature::class)->findOneBy(array(
            "name" => $featureName,
            "deletedAt" => null
          ));
          echo '<pre>';
          if($findFeature) {
            //\print_r($featureName."found");
            $findType = $em->getRepository(FeatureType::class)->find(1);
            $productFeatures = $em->getRepository(ProductFeature::class)->findBy(array(
              "feature" => $findFeature,
              "type" => $findType,
              "deletedAt" => null
            ));
            if($productFeatures) {
              foreach ($productFeatures as $productFeature) {
                $productFeature->setPriority($priority);
                $em->flush();
              }
            }
          } else {
            \print_r($featureName."not-found");
          }
          //exit;
      }
      return new Response("success");
    }
}
