<?php

namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PromotionsController extends CustomController
{
    /**
     * @Route("/promotions", name="promotions")
     */
    public function promotions() {

        return $this->render("promotions.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }
}