<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 27.09.2018
 * Time: 15:19
 */

namespace App\Controller;


use App\Entity\ProductCountry;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

class FilterProductController extends CustomController
{
    /**
     * @SWG\Get(
     *   path="/{country}/{language}/checkproduct",
     *   description="Filter Product",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="tr"
     *     )
     * )
     * @Route("checkproduct", name="checkProduct", methods={"GET"})
     */
    public function checkProduct() {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var ProductCountry $product
         */
        $product = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "active" => 1,
            "deletedAt" => null
        ));

        $arr = array(
            "offers" => [],
            "bestSeller" => [],
            "news" => [],
        );

        foreach ($product as $p) {
            if ($p->getProduct()->getisOffer() == 1) {
                if ($p->getProduct()->getImages()[0] == null) {
                    $images = "";
                } else {
                    $images = $p->getProduct()->getImages()[0]->getPath();
                }

                if ($p->getProduct()->getText()[0] == null) {
                    $shortDesc = "";
                } else {
                    $shortDesc = $p->getProduct()->getText()[0]->getShortDescription();
                }
                $arr["offers"][] = array(
                    "id" => $p->getProduct()->getId(),
                    "name" => $p->getProduct()->getName(),
                    "size" => $p->getProduct()->getSize(),
                    "code" => $p->getProduct()->getCode(),
                    "link" => $p->getProduct()->getLink(),
                    "image" => $images,
                    "shortDescription" => $shortDesc

                );
            }

            if ($p->getProduct()->getisNew() == 1) {
                if ($p->getProduct()->getText()[0] == null) {
                    $shortDesc = "";
                } else {
                    $shortDesc = $p->getProduct()->getText()[0]->getShortDescription();
                }

                if ($p->getProduct()->getImages()[0] == null) {
                    $images = "";
                } else {
                    $images = $p->getProduct()->getImages()[0]->getPath();
                }

                $arr["news"][] =array(
                    "id" => $p->getProduct()->getId(),
                    "name" => $p->getProduct()->getName(),
                    "size" => $p->getProduct()->getSize(),
                    "code" => $p->getProduct()->getCode(),
                    "link" => $p->getProduct()->getLink(),
                    "image" => $images,
                    "shortDescription" => $shortDesc

                );

            }

            if ($p->getProduct()->getisBest() == 1) {
                if ($p->getProduct()->getText()[0] == null) {
                    $shortDesc = "";
                } else {
                    $shortDesc = $p->getProduct()->getText()[0]->getShortDescription();
                }

                if ($p->getProduct()->getImages()[0] == null) {
                    $images = "";
                } else {
                    $images = $p->getProduct()->getImages()[0]->getPath();
                }

                $arr["bestSeller"][] = array(
                    "id" => $p->getProduct()->getId(),
                    "name" => $p->getProduct()->getName(),
                    "size" => $p->getProduct()->getSize(),
                    "code" => $p->getProduct()->getCode(),
                    "link" => $p->getProduct()->getLink(),
                    "image" => $images,
                    "shortDescription" => $shortDesc

                );
            }
        }
        return new JsonResponse($arr);
    }
}