<?php


namespace App\Controller;

use App\Library\CustomController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Country;
use App\Entity\Contact;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ipinfo\ipinfo\IPinfo;

class NewContactController extends CustomController
{

/**
     * @Route("/contact-new", name="supportContact")
     */
    public function supportContact(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $message = "";
        $errorMessage = "";

        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null
        ));

        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $ip = $request->getClientIp();

        $accessToken = "7140d65f627b36";
        $client = new IPinfo($accessToken);
        $details = $client->getDetails($ip);
        
        if($ip != "127.0.0.1") {
            $ipCountry = $details->country;
            $ipCountry = strtolower($ipCountry);
        } else {
            $ipCountry = "uk";
        }
        $isUpload = false;
        if(isset($_POST["submit"])) {
            if(isset($_POST["message"]) && isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["model"])) {
                $name = $_POST["name"];
                $email = $_POST["email"];
                $phone = $_POST["phone"];
                $modelNo = $_POST["model"];
                $serialNo = $_POST["serial"];
                $tvWhere = $_POST["tvWhere"];
                $subject = $_POST["subject"];
                $formMessage = $_POST["message"];
                $country = $_POST["selectCountry"];

                if(isset($_FILES["myfile"]) && $_FILES["myfile"]["name"] != "") {
                    // print_r($_FILES["myfile"]);exit;
                    $target = "uploads/form-files/";
                    $fileExt = explode('.', $_FILES['myfile']['name']);
                    $fileExt = strtolower($fileExt[1]);
                    $fileTmp = $_FILES['myfile']['tmp_name'];
                    $fileName = $_FILES['myfile']['name'];

                    $extensions= array("jpeg","jpg","png");

                    if(in_array($fileExt,$extensions)=== false){
                        $errorMessage = "Extension not allowed, please choose a JPEG or PNG file.";
                        return $this->render("contact.html.twig", array(
                            "staticText" =>  $this->staticText,
                            "categories" => $this->category,
                            "country" => $this->country,
                            "countries" => $countries,
                            "lang" => $this->lang,
                            "message" => $message,
                            "errorMessage" => $errorMessage,
                            "ipCountry" => $ipCountry
                        ));
                     } else {
                        if(move_uploaded_file($fileTmp, $target . $this->generateMd5FileName($fileName).".".$fileExt)) {
                            $isUpload = true;
                        }
                     }

                }

                $findCountry = $em->getRepository(Country::class)->find($country);

                $contactForm = new Contact();
                $contactForm->setName($name);
                $contactForm->setEmail($email);
                $contactForm->setPhone($phone);
                $contactForm->setModelNumber($modelNo);
                $contactForm->setSerialNumber($serialNo);
                $contactForm->setWhereBuy($tvWhere);

                
                $filePath = "";

                if($isUpload == true) {
                    $fileName = $this->generateMd5FileName($fileName).".".$fileExt;
                    $contactForm->setPath($baseUrl."/uploads/form-files/".$fileName);
                    $filePath = $baseUrl."/uploads/form-files/".$fileName;
                }
                $contactForm->setSubject($subject);
                $contactForm->setMessage($formMessage);
                
                $em->persist($contactForm);
                $em->flush();
                if($contactForm->getId()) {
                    $sendMail = $this->sendSupportMail($email, $name, $subject, $formMessage, $filePath);
                    if($sendMail) {
                        $message = "Your e-mail is sent. Thank you for contacting us";
                    }
                }
            } else {
                $errorMessage = "Please fill the required fields.";
            }
            
        }

        
        return $this->render("contact.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "countries" => $countries,
            "lang" => $this->lang,
            "message" => $message,
            "errorMessage" => $errorMessage,
            "ipCountry" => $ipCountry
        ));
    }

    public function sendSupportMail($sender, $name, $subject, $message, $file = null) {

        $transport = (new \Swift_SmtpTransport("SMTP.office365.com", "587", "TLS"))
                        ->setUsername("no-reply@toshiba-tv.com")
                        ->setPassword("T05h1Ba.!");

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message($subject))
            ->setFrom("no-reply@toshiba-tv.com")
            ->setTo("registration@toshiba-tv.com")
            ->setBody(
                $this->renderView(
                    "contactEmail/contactForm.html.twig",
                    array("sender" => $sender, "name" => $name, "message" => $message, "file" => $file )
                ),
                "text/html"
            );

        return $mailer->send($message);

    }

    public function generateMd5FileName($file) {
        return md5($file.md5(time()));
    }
}    