<?php
namespace App\Controller;

use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\ProductCountry;
use App\Entity\FilterCategory;
use App\Entity\Filter;
use App\Library\CustomController;
use App\Library\FilterSerializer;
use App\Library\ProductSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
//use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
// use TCPDF\TCPDF;

class pdfGenController extends CustomController {


        /**
         * @SWG\Get(
         *     path="/{country}/{language}/pdf/{parentId}",
         *     description="Get All Category By ParentId",
         *     @SWG\Parameter(
         *          name="country",
         *          in="path",
         *          required=true,
         *          type="string",
         *          default="tr"
         *      ),
         *     @SWG\Parameter(
         *          name="language",
         *          in="path",
         *          required=true,
         *          type="string",
         *          default="tr"
         *      ),
         *     @SWG\Parameter(
         *          name="parentId",
         *          in="path",
         *          required=true,
         *          type="integer"
         *      )
         * )
         *
         * @return Response
         */












        /**
         * @Route("/pdf/{url}", name="getFile", methods={"GET"})

         */

            public function getFile($url,$country,$language) {


                if(strlen($url) > 15 || strlen($url) < 3){
                    die("unknown");
                }



                $em = $this->getDoctrine()->getManager();
                $product = $em->getRepository(Product::class)->findOneBy(array(
                    "link" => $url,
                    "deletedAt" => null
                ));


                if($product == null or $product == ''){
                    die("unknown");
                }


                $lang = $em->getRepository(Language::class)->findOneBy(array(
                    "code" => $language,
                    "deletedAt" => null
                ));
                $country = $em->getRepository(Country::class)->findOneBy(array(
                    "code" => $country,
                    "deletedAt" => null
                ));

                $data = array();
                $data['id'] = $product->getId();
                $data['name'] = $product->getName();
                $data['code'] = $product->getCode();
                $data['link'] = $product->getLink();
                $data['keyw'] = $product->getKeywords();
                $data['active'] = $product->getActive();
                $data['created'] = $product->getCreatedAt();

                $data['cat'] = $product->getCategory()->getName();


                $data['series'] = array();
                $data['series']['name'] = $product->getSeries()->getName();
                $data['series']['code'] = $product->getSeries()->getCode();

               foreach($product->getImages()->toArray() as $row){
                    $tmp = array();
                    if($row->getActive()){
                        $tmp['name'] = $row->getName();
                        $tmp['path'] = $row->getPath();
                    }
                    $data['images'][] = $tmp;

               }


              $tmpSpecs = array();
              $tmpCats = array();
              $newSpec = $product->getSpec($this->globalLang)->toArray();
              if(!$newSpec) {
                $newSpec = $product->getSpec($lang)->toArray();
              }
            foreach($newSpec as $row){
                $tmp = array();
                $catID   = $row->getSpec()->getCategory()->getId();
                $catName = $row->getSpec()->getCategory()->getName();
                $tmp['spec']    = $row->getSpec()->getName();
                $tmp['val']     = $row->getValue();


                if(!isset($data['specs'][$catID])){
                    $tmpCats = array();
                    $tmpCats['catid'] = $catID;
                    $tmpCats['catname'] = $catName;
                    $data['specs'][$catID] = $tmpCats;
                }

                $data['specs'][$catID]['specs'][] = $tmp;


            }//print_r($data);exit;
            $data['specs'] = array_values($data['specs']);

            
            foreach($product->getFeature()->toArray() as $row){
                 $tmp = array();
                 $tmp['id'] = $row->getFeature()->getId();
                 $tmp['active'] = $row->getFeature()->getActive();
                 $tmp['name'] = $row->getFeature()->getName();
                
                 $tmp['long'] = $row->getFeature()->getText($lang)->toArray()[0]->getShortDescription();
                 $tmp['long'] = $row->getFeature()->getText($lang)->toArray()[0]->getLongDescription(); 
                $tmp['img'] = $row->getFeature()->getV1();
                $data['features'][] = $tmp;


            }


            // $data['highligted'] = array();
            // foreach($product->getHighlightedText()->toArray() as $row){
            //     if ($row->getTranslate($country,$lang)->toArray()) {
            //         $data['highligted'][] = $row->getTranslate($country,$lang)->toArray()[0]->getText();
            //     }
            // }

            // /*
            // *   div highligeteds to 2 part
            // */
            // $dividedHL = array();
            // $divHLcols = intval(count($data['highligted'])/2);
            // $divHLnonEq = $divHLcols*2 != count($data['highligted']);
            // for($x=0;$x<2;$x++){
            //     if($divHLnonEq && $x==0){
            //         for($y=0;$y<=$divHLcols;$y++){
            //             $dividedHL[$x][] = $data['highligted'][$y];
            //         }
            //     }else if($divHLnonEq && $x==1){
            //         for($y=1;$y<=$divHLcols;$y++){
            //             $dividedHL[$x][] = $data['highligted'][$x*$divHLcols+$y];
            //         }
            //     }else{
            //         for($y=0;$y<$divHLcols;$y++){
            //             $dividedHL[$x][] = $data['highligted'][$x*$divHLcols+$y];
            //         }
            //     }

            // }
            
            // $data['dividedHL'] = $dividedHL;
            $data['pp'] = $data["images"][0]["path"];


            //echo $

            if(count($data['features'])>=6){
                for($x=0;$x<6;$x++){
                    $data['topfeatures'][] = $data['features'][$x];
                }
                for($x=6;$x<count($data['features']);$x++){
                    $data['othfeatures'][] = $data['features'][$x]['name'];
                }

            }

            //print_r($data['topfeatures']);
        //    print_r($data['othfeatures']);




            /*echo $header = $this->render('pdfGen.html.twig', array(
                "product"=>$data,
                "rootDir"=>"",
            ));*/

/*
            $snappyPdf = $this->get("knp_snappy.pdf");
            $fileName =  json_decode($json)[0]->name;
            $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

            $html =  $this->render("jvcPdfGenerator.html.twig", array(
                "product" => json_decode($json)[0],
                'rootDir' => $this->get('kernel')->getRootDir().'/..',
                'baseUrl' => $baseUrl
            ));

            return $html;*/

            //include __DIR__.'/../../vendor/tcpdf/tcpdf_include.php';
             require_once (__DIR__.'/../../vendor/TCPDF/tcpdf.php');


            // create new PDF document
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);




        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Toshiba');
        $pdf->SetTitle($url.'');
        $pdf->SetSubject($url.'');
        $pdf->SetKeywords($url.'');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(0);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

            // ---------------------------------------------------------

            // set default font subsetting mode
            $pdf->setFontSubsetting(true);

            // Set font
            // dejavusans is a UTF-8 Unicode font, if you only need to
            // print standard ASCII chars, you can use core fonts like
            // helvetica or times to reduce file size.
            $pdf->SetFont('roboto', '', 14, '', true);

            // Add a page
            // This method has several options, check the source code documentation for more information.

            $pdf->SetMargins(0, 0, 0, 0);

            $pdf->setImageScale(1);


            //$pdf->Image('http://34.244.173.118:6858/assets/img/demoFrontbg.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);




                        $pdf->setListIndentWidth(8);
                        $pdf->setListIndentWidth(8);




                        $pdf->AddPage();
                        $pdf->Image('http://63.35.162.212/assets/img/pdf-img/pdfbgfront-toshiba.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
                        // $pdf->Image('/../../public/assets/img/pdfbgfront.jpg', 0, 0, 210, 297, 'JPG', '', '', true, 200, '', false, false, 0, false, false, true);
                        // /../../vendor/TCPDF/tcpdf.php
                        
                        $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n'
                        => 0)));
                        $pdf->setHtmlVSpace($tagvs);
                        $pdf->SetFont('Roboto', '', 14, '', true);
                        $html = '<p><font color="white" size="9">'.$data['code'].'</font></p>';
                        $pdf->writeHTMLCell(0, 0, 19, 19, $html, 0, 1, 0, false, '', true);


                        $pdf->SetFont('Roboto', '', 14, '', true);
                        $html = '<p><font color="white" size="27">'.$data['name'].'</font></p>';
                        $pdf->writeHTMLCell(0, 0, 19, 24, $html, 0, 1, 0, false, '', true);
                        $pdf->SetFont('Roboto', '', 14, '', true);

                        $html = '<p><font color="white" size="17">'.$data['keyw'].'</font></p>';
                        $pdf->writeHTMLCell(0, 0, 19, 38, $html, 0, 1, 0, false, '', true);


                        $html = '

                        <br>
                        <br>

                        <div align="center">
                        <center><img align="center" height="400" src="'.$data["pp"].'"></center></div>
                        <br>';
                        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                                    $pdf->SetFont('Roboto', '', 20, '', true);
                        $html ='<font size="18" color="#e6000d"></font><br>';

                        $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);


                        $pdf->SetFont('Roboto', '', 14, '', true);
                        //print_r($data);exit;
                        // if(count($data["dividedHL"]) > 0) {
                        //     $html = '
                        //     <font size="10" family="courier">
                        //     <table width="450">
                        //         <tr>
                        //             <td><ul>';
                        //             foreach($data['dividedHL'][0] as $row){
                        //                 $html .= '<li>'.$row.'</li>';
                        //             }
    
                        //             $html .= '</ul></td>
                        //             <td><ul>';
                        //             foreach($data['dividedHL'][1] as $row){
                        //                 $html .= '<li>'.$row.'</li>';
                        //             }
    
                        //             $html .= '</ul></td>
                        //         </tr>
                        //     </table>
                        //     </font>';
                        // }
                        

                        $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);












$features=array();

//$data["features"] = array_merge($data["features"],$data["features"]);
//$data["features"] = array_merge($data["features"],$data["features"]);



if(count($data["features"])<=8){
    $featuresRaw = array_chunk($data["features"], ceil(count($data["features"])/2));

    $features = '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<table width="530">';
    for($x=0;$x<count($featuresRaw[0]);$x++){

        $features .= '<tr><td height="30" colspan="2"><b><font face="Roboto" align="left" color="#e20e16" size="18">'.$featuresRaw[0][$x]["name"].'</font></b></td><td width="10"></td><td colspan="2"><b><font size="20" face="Roboto" color="#e20e16">'.(isset($featuresRaw[1][$x]["name"]) ? $featuresRaw[1][$x]["name"]:'').'</font></b></td></tr>';
        $features .= '<tr><td width="110" height="100">';
        $features .= '<img src="'.$featuresRaw[0][$x]["img"].'" width="100">';

        $features .= '</td><td>';
        $features .= '<p><font face="Roboto">'.$featuresRaw[0][$x]["long"].'</font></p>';
        $features .= '</td><td width="10">';
        $features .= '</td><td width="110">';
        if(isset($featuresRaw[1][$x])){
            $features .= '<img src="'.$featuresRaw[1][$x]["img"].'" width="100">';
        }
        $features .= '</td><td>';
        if(isset($featuresRaw[1][$x])){
            $features .= '<p><font face="Roboto">'.$featuresRaw[1][$x]["long"].'</font></p>';
        }
        $features .= '</td></tr>';




    }
    $features .= '</table>';
    //    print_r($features);
}else{

    $featuresRaw = array();
    for($x=0;$x<6;$x++){
        $featuresRaw[] =$data["features"][$x];
    }

    $featuresText = array();
    for($x=6;$x<count($data["features"]);$x++){
        $featuresText[] =$data["features"][$x];
    }


    $featuresRaw = array_chunk($featuresRaw, ceil(count($featuresRaw)/2));

    $features = '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<table width="530">';
    for($x=0;$x<count($featuresRaw[0]);$x++){

        $features .= '<tr><td height="30" colspan="2"><b><font face="Roboto" align="left" color="#e20e16" size="18">'.$featuresRaw[0][$x]["name"].'</font>';
        $features .= '</b></td><td width="30"></td><td colspan="2"><b><font size="20" face="Roboto" color="#e20e16">'.(isset($featuresRaw[1][$x]["name"]) ? $featuresRaw[1][$x]["name"]:'').'</font></b></td></tr>';
        $features .= '<tr><td width="110" height="100">';
        $features .= '<img src="'.$featuresRaw[0][$x]["img"].'" width="100">';

        $features .= '</td><td>';
        $features .= '<p><font face="Roboto">'.$featuresRaw[0][$x]["long"].'</font></p>';
        $features .= '</td><td width="30">';
        $features .= '</td><td width="110">';
        if(isset($featuresRaw[1][$x])){
            $features .= '<img src="'.$featuresRaw[1][$x]["img"].'" width="100">';
        }
        $features .= '</td><td>';
        if(isset($featuresRaw[1][$x])){
            $features .= '<p><font face="Roboto">'.$featuresRaw[1][$x]["long"].'</font></p>';
        }
        $features .= '</td></tr>';




    }
    $features .= '</table>';



    $featuresText = array_chunk($featuresText, ceil(count($featuresText)/3));

    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<br>';
    $features .= '<table width="500">';
    $pdf->SetFont('Roboto', '', '', 'li', true);

    $features .= '<tr>';
    $features .= '<td>'.'<font face="Roboto" align="left" color="#e20e16" size="18">More Features</font>'.'</td>';
    $features .= '<td><ul>';
    foreach($featuresText[0] as $row){
        $features .= '<li><font face="Roboto" size="9">'.$row['name'].'</font><br></li>';
    }
    $features .= '</ul></td>';


    $features .= '<td><ul>';
    foreach($featuresText[1] as $row){
        $features .= '<li><font face="Roboto" size="9">'.$row['name'].'</font><br></li>';
    }
    $features .= '</ul></td>';


    $features .= '<td><ul>';
    foreach($featuresText[2] as $row){
        $features .= '<li><font face="Roboto" size="9">'.$row['name'].'</font><br></li>';
    }
    $features .= '</ul></td>';



    $features .= '</tr>';



    /*
    for($x=0;$x<count($featuresText[0]);$x++){
        if($x==0){
            $features .= '<tr>';
            $features .= '<td colspan="4">'.'<font face="courier" align="left" color="#e20e16" size="18">More Features</font>'.'</td>';
            $features .= '</tr>';
        }else{
            $features .= '<tr>';
            $features .= '<td> </td>';
            $features .= '<td>'.(isset($featuresText[0][$x]) ? $featuresText[0][$x]['name']:'').'</td>';
            $features .= '<td>'.(isset($featuresText[1][$x]) ? $featuresText[1][$x]['name']:'').'</td>';
            $features .= '<td>'.(isset($featuresText[2][$x]) ? $featuresText[2][$x]['name']:'').'</td>';
            $features .= '</tr>';
        }



    }*/

    $features .= '</table>';

    //    print_r($features);

}




    $pdf->AddPage();
    $pdf->Image('http://63.35.162.212/assets/img/pdf-img/pdfbgsec-toshiba.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
    $pdf->SetFont('Roboto', '', 9, 'td', true);
    $pdf->SetFont('Roboto', '', 9, 'b', true);
    //$pdf->SetFont('courier', '', 10, 'td', true);

    $html = $features;

    $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);











$tmp = array();
foreach($data["specs"] as $row){
    $tmp[] = '<tr><td colspan="2" height="18"><font size="9" color="#f00" face="Roboto">'.$row['catname'].'</font></td></tr>';
    $x=0;
    foreach($row["specs"] as $inRow){
        $x++;
        $tmp[] = '<tr><td height="18"><font size="9" face="Roboto">'.$inRow['spec'].'</font></td><td><font size="9" face="Roboto">'.$inRow["val"].'</font></td></tr>';
        if($x == count($row["specs"])){
            $tmp[] = '<tr><td height="18" colspan="2"><br></td></tr>';
            $tmp[] = '<tr><td height="18" colspan="2"><br></td></tr>';
        }
    }
}
/*
$tmp = array();
foreach($data["specs"] as $row){
    $tmp[] = $this->specHead($row);
    foreach($row["specs"] as $inRow){
        $tmp[] = $this->specRow($inRow);
    }
}
*/




$specParts = array_chunk($tmp, 32);






    $pdf->AddPage();
    $pdf->Image('http://63.35.162.212/assets/img/pdf-img/pdfbgsec-toshiba.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
    $pdf->SetFont('Roboto', '', 9, 'td', true);
    $pdf->SetFont('Roboto', '', 9, 'b', true);
    //$pdf->SetFont('courier', '', 10, 'td', true);

    $html = '
    <br><br><br><br><br><br>
    <table width="500">
        <tr>
            <td>
            <table width="100%">
            ';
            if(isset($specParts[0])){
                foreach($specParts[0] as $row){
                    $html .= $row;
                }
            }

            $html .= '</table></td><td><table width="100%">';
            if(isset($specParts[1])){
                foreach($specParts[1] as $row){
                    $html .= $row;
                }
            }

            $html .= '
            </table>
            </td></tr>
    </font>';

    $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);





    $pdf->AddPage();
    $pdf->Image('http://63.35.162.212/assets/img/pdf-img/pdfbgsec-toshiba.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);
                $pdf->SetFont('Roboto', '', 14, 'B', true);
                $pdf->SetFont('Roboto', '', 10, 'td', true);

                $html = '
                <br><br><br><br><br><br>
                <table width="500">
                    <tr>
                        <td>
                        <table width="100%">
                        ';
                        if(isset($specParts[2])){
                            foreach($specParts[2] as $row){
                                $html .= $row;
                            }
                        }


                        $html .= '</table></td><td><table width="100%">';
                        if(isset($specParts[3])){
                            foreach($specParts[3] as $row){
                                $html .= $row;
                            }
                        }

                        $html .= '
                        </table>
                        </td></tr>
                </font>';

                $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);





            // ---------------------------------------------------------

            // Close and output PDF document
            // This method has several options, check the source code documentation for more information.
            $pdf->Output($url.'.pdf', 'I');







            //'path' => $path

            die();






        $json = json_encode($data);
        return new Response($json);
    }

    function specHead($row){
        return '<tr><td colspan="2"><br><br><font size="14" color="#f00" family="Roboto">'.$row['catname'].'</font></td></tr>';
    }

    function specRow($inRow){
        return '<tr><td><font size="9" family="Roboto">'.$inRow['spec'].'</font></td><td><font size="9" family="Roboto">'.$inRow["val"].'</font></td></tr>';
    }



             function split2($tmp){
                 $totalSpecCount = count($tmp);
                 $halfSpecCount = intval($totalSpecCount/2);
                 $hasDiffSpecCount = $totalSpecCount != $halfSpecCount*2;
                 $specParts = array();

                 if($hasDiffSpecCount){
                     for($x=0;$x<=$halfSpecCount;$x++){
                         $specParts[0][] = $tmp[$x];
                     }
                     for($x=$halfSpecCount+1;$x<$totalSpecCount;$x++){
                         $specParts[1][] = $tmp[$x];
                     }

                 }
                 return $specParts;
             }



}
/*
/*
            $pdf->AddPage();
            $pdf->Image('http://34.244.173.118:6858/assets/img/pdfbgfront.jpg', 0, 0, 210, 297, 'JPG', '', '', false, 200, '', false, false, 0, false, false, true);


            $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n'
            => 0)));
            $pdf->setHtmlVSpace($tagvs);
            $pdf->SetFont('courier', '', 14, '', true);
            $html = '<p><font color="white" size="9">'.$data['code'].'</font></p>';
            $pdf->writeHTMLCell(0, 0, 19, 19, $html, 0, 1, 0, false, '', true);


            $pdf->SetFont('courier', '', 14, '', true);
            $html = '<p><font color="white" size="27">'.$data['name'].'</font></p>';
            $pdf->writeHTMLCell(0, 0, 19, 24, $html, 0, 1, 0, false, '', true);
            $pdf->SetFont('courier', '', 14, '', true);

            $html = '<p><font color="white" size="17">'.$data['keyw'].'</font></p>';
            $pdf->writeHTMLCell(0, 0, 19, 38, $html, 0, 1, 0, false, '', true);


            $html = '

            <br>
            <br>

            <div align="center">
            <center><img align="center" height="400" src="'.$data["pp"].'"></center></div>
            <br>';
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                        $pdf->SetFont('courier', '', 20, '', true);
            $html ='<font size="18" color="#e6000d">Highligths</font><br>';

            $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);


            $pdf->SetFont('courier', '', 14, '', true);

            $html = '
            <font size="10" family="courier">
            <table width="450">
                <tr>
                    <td><ul>';
                    foreach($data['dividedHL'][0] as $row){
                        $html .= '<li>'.$row.'</li>';
                    }

                    $html .= '</ul></td>
                    <td><ul>';
                    foreach($data['dividedHL'][1] as $row){
                        $html .= '<li>'.$row.'</li>';
                    }

                    $html .= '</ul></td>
                </tr>
            </table>
            </font>';

            $pdf->writeHTMLCell(0, 0, 19, '', $html, 0, 1, 0, true, '', true);
*/






//$specParts = $this->split2($tmp);



/*

//print_r($data['specs']);
$tmp = array();
foreach($data["specs"] as $row){
    $tmp[] = $this->specHead($row);
    foreach($row["specs"] as $inRow){
        $tmp[] = $this->specRow($inRow);
    }
}*/




/*
//try fit1
$tmp = array();

//print first
$tmp[] = $this->specHead($data["specs"][0]);
foreach($data["specs"][0]["specs"] as $inRow){
    $tmp[] = $this->specRow($inRow);
}

//print second
$tmp[] = $this->specHead($data["specs"][1]);
foreach($data["specs"][1]["specs"] as $inRow){
    $tmp[] = $this->specRow($inRow);
}


$specParts[0] = $tmp;
// end first block


### sec block
//print first
$tmp = array();
$tmp[] = $this->specHead($data["specs"][2]);
foreach($data["specs"][2]["specs"] as $inRow){
    $tmp[] = $this->specRow($inRow);
}

//print second
$tmp[] = $this->specHead($data["specs"][3]);
foreach($data["specs"][3]["specs"] as $inRow){
    $tmp[] = $this->specRow($inRow);
}


//print second
$tmp[] = $this->specHead($data["specs"][4]);
foreach($data["specs"][4]["specs"] as $inRow){
    $tmp[] = $this->specRow($inRow);
}

//print second
$tmp[] = $this->specHead($data["specs"][5]);
//print_r($data["specs"][5]["specs"]);

$limit = count($specParts[0])-3-count($tmp);
if($limit > count($data["specs"][5]["specs"])){
    $limit = $data["specs"][5]["specs"];
}
for($x=0;$x<$limit;$x++){
    $tmp[] = $this->specRow($data["specs"][5]["specs"][$x]);
}
//$data["specs"][4]["specs"]

// end first block

//$limit


$specParts[1] = $tmp;
$specParts[2] = $tmp;

if($limit < count($data["specs"][5]["specs"])){


$tmp = array();
//print second
$tmp[] = $this->specHead($data["specs"][5]);
for($x=$limit;$x<count($specParts[0]);$x++){
    $tmp[] = $this->specRow($data["specs"][5]["specs"][$x]);
}


$specParts[2] = $tmp;


}
*/
