<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 23.11.2018
 * Time: 11:09
 */

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;

class SendMailController 
{
    /**
     * @var SendMailController
     */
    private static $instance;

    public function __construct()
    {

    }

    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function sendMail($to, $hash = null, $name = null) {

        $transport = (new \Swift_SmtpTransport("smtp.office365.com", "587", "SSL"))
                        ->setUsername("no-reply@toshiba-tv.com")
                        ->setPassword("T05h1Ba.!");

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message("Toshiba"))
            ->setFrom("no-reply@toshiba-tv.com")
            ->setTo($to)
            ->setBody(
                $this->render(
                    "emailActivation.html.twig",
                    array("hash" => $hash, "name" => $name, "email" => $to)
                ),
                "text/html"
            );  

        return $mailer->send($message);
        
    }
}