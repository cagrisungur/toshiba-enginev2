<?php

namespace App\Controller;

use App\Entity\FeaturedSpec;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Swagger\Annotations as SWG;

class FeaturedSpecController extends CustomController
{
    /**
     * @SWG\Get(
     *     path="/{country}/{language}/featuredSpec",
     *     description="Get All Featured Specs",
     *     @SWG\Parameter(
     *          name="country",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      ),
     *     @SWG\Parameter(
     *          name="language",
     *          in="path",
     *          required=true,
     *          type="string",
     *          default="tr"
     *      )
     * )
     *
     * @Route("/featuredSpec", name="featuredSpec", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getFeaturedSpec(SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();


        $newFeaturedSpec = $em->getRepository(FeaturedSpec::class)->findBy(array(
            "deletedAt" => null,
            "country" => $this->country,
            "lang" => $this->lang
        ));
        $newDataArray = array();
        foreach ($newFeaturedSpec as $fSpec) {
            if(isset($fSpec->getSpec()->getText($this->country, $this->lang)[0])) {
                $specTextName = $fSpec->getSpec()->getText($this->country, $this->lang)[0]->getName();
            } else {
                $specTextName = $fSpec->getSpec()->getName();
            }
            $tmp = array(
                "id" => $fSpec->getId(),
                "description" => $fSpec->getDescription(),
                "title" => $fSpec->getTitle(),
                "subTitle" => $fSpec->getSubTitle(),
                "priority" => $fSpec->getPriority(),
                "name" => $specTextName
            );
            $tmp["image"] = array();
            if ($fSpec->getImage()[0]) {
                $tmp["image"][] = array(
                    "id" => $fSpec->getImage()[0]->getId(),
                    "featured_spec_id" => $fSpec->getId(),
                    "name" => $fSpec->getImage()[0]->getName(),
                    "path" => $fSpec->getImage()[0]->getPath(),
                    "type_id" => $fSpec->getImage()[0]->getType()->getId(),
                    "active" => $fSpec->getImage()[0]->getPriority(),
                    "priority" => $fSpec->getImage()[0]->getPriority()
                );
            }

            $newDataArray[] = $tmp;
        }
        return new JsonResponse($newDataArray);
    }

}