<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCountry;
use App\Entity\ProductStatus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Library\CustomController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;


class NewSearchController extends CustomController
{
    /**
     * @Route("/search", name="getSearchProduct")
     */
    public function getSearchProduct() {
        if(isset($_POST["globalSearchInput"])) {
            $searchTerm = $_POST["globalSearchInput"];
        } 
        
        if(empty($_POST["globalSearchInput"])) {
            return $this->redirectToRoute("index", array(
                "country" => $this->country->getCode(),
                "language" => $this->lang->getCode(),
            ));
        } else {
            $em = $this->getDoctrine()->getManager();

            $dataArray = array();
            $productArray = array();
           
            
            $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
                "code" => "publish"
            ));
            if(!empty($searchTerm)) {
                $productCountry = $em->getRepository(ProductCountry::class)->globalSearch($this->country, $searchTerm, $this->lang->getId());
    
                foreach($productCountry as $countryProduct) {
                    $product = $countryProduct->getProduct();
                    if($product) {
                        $series = $product->getSeries();
                 
                    
                        if($series) {
                            $findSerieProducts = $em->getRepository(Product::class)->seriesGroupProduct($product->getId(), $this->lang, $productStatus);
                            //print_r($findSerieProducts);exit;    
                            foreach($findSerieProducts as $serieProduct) {
                                
                                $productInc = str_split($serieProduct->getName(), 2);
                                $productInc = $productInc[0];
                                if($serieProduct->getSeries() != null) {
                                    $checkSerieProdCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                                        "country" => $this->country,
                                        "product" => $product->getId()
                                    ));
                                    if($serieProduct->getText()[0] == "") {
                                        $textName = $product->getLangText($this->globalLang)[0]->getName();    
                                    } else {
                                        $textName = $serieProduct->getText()[0]->getName();
                                    }
                                    
                                    if($checkSerieProdCountry) {
                                        if($serieProduct->getText() != null) {
                                        $productArray[$serieProduct->getSeries()->getCode()][] = array(
                                            "productId" => $serieProduct->getId(),
                                            "name" => $serieProduct->getName(),
                                            "textName" => $textName,
                                            "link" => $serieProduct->getLink(),
                                            "serie" => $serieProduct->getSeries()->getName(),
                                            "image" => $serieProduct->getPp(),
                                            "inc" => $productInc,
                                            "categoryLink" => $product->getCategory()->getLink()
                                        );
                                        }
                                        $this->arraySortByColumn($productArray[$serieProduct->getSeries()->getCode()], 'inc');
                                    }
                                }
                                
                            }
                        }
                    } else {
                        $productArray = array();
                        return $this->render("searchResult.html.twig", array(
                            "staticText" =>  $this->staticText,
                            "categories" => $this->category,
                            "country" => $this->country,
                            "lang" => $this->lang,
                            "productCat" => $productArray
                        ));
                    }
                    
                }
            } else {
                $productArray = array();
                return $this->render("searchResult.html.twig", array(
                    "staticText" =>  $this->staticText,
                    "categories" => $this->category,
                    "country" => $this->country,
                    "lang" => $this->lang,
                    "productCat" => $productArray
                ));
            }
        }
        
        
        //print_r($productArray);exit;
        return $this->render("searchResult.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang,
            "productCat" => $productArray,
            "searchTerm" => $searchTerm
        ));
    }


    function arraySortByColumn(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }
    
        return array_multisort($sort_col, $dir, $arr);
    }
}