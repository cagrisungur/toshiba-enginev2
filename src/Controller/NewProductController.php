<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Entity\Product;
use App\Entity\ProductText;
use App\Entity\ProductCountry;
use App\Entity\ProductFile;
use App\Library\CustomController;
use App\Library\ProductSerializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\FiltersCategory;
use App\Entity\FiltersValue;
use App\Entity\FiltersValueProduct;
use App\Entity\CategoryFeature;
use App\Entity\FeatureType;
use App\Entity\HighlightedText;
use App\Entity\HighlightedTextTranslate;
use App\Entity\ProductFeature;
use App\Entity\ProductSpec;
use App\Entity\ProductImage;
use App\Entity\ProductStatus;
// use App\Entity\FiltersValueProduct;
use App\Entity\Series;
use App\Entity\SeriesText;
use App\Entity\Specification;
use App\Entity\NewProductFeature;
use App\Entity\NewProductFeatureTranslate;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Feature;
use App\Entity\FeatureText;
use App\Entity\SpecificationCategory;
use App\Entity\SpecificationCategoryText;
use App\Entity\SpecificationText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

use Symfony\Component\HttpFoundation\Session\Session;

class NewProductController extends CustomController
{



  function getProductBySefAsArray($productLink) :array{
    $dataArray = [];
    $em = $this->getDoctrine()->getManager();

    $product = $em->getRepository(Product::class)->findOneBy(array(
        "link" => $productLink,
        "deletedAt" => null
    ));

    $productArray = array();


    /**
     * @var ProductCountry[] $product
     */
    $products = $em->getRepository(ProductCountry::class)->findBy(array(
        "country" => $this->country,
        "product" => $product,
        "active"=> 1,
        "deletedAt"=>null
        )
    );




            if(isset($products[0])) {
                $product = $products[0]->getProduct();

                // print_r(strval($product->getCategory()->getLink()));exit;
                if($product->getStatus()->getCode() == "publish") {
                $productArray["text"] = array();
                $textList = $product->getLangText($this->lang);

                $productArray["categoryLink"] = $product->getCategory()->getLink();
                //print_r($productArray["categoryLink"]);exit;
                if(!isset($textList[0])) {
                    $textList = $product->getLangText($this->globalLang);
                }
                $productArray["text"][] = array(
                    "title" => $textList[0]->getSubtitle(),
                    "name" => $textList[0]->getName(),
                    "shortDescription" => $textList[0]->getShortDescription(),
                    "longDescription" => $textList[0]->getLongDescription(),
                    "link" => $textList[0]->getLink(),
                    "keywords" => $textList[0]->getKeywords(),
                    "video" => $textList[0]->getYtVideo(),
                    "videoThumbnail" => $textList[0]->getVideoThumbnail()
                );

                $productArray["images"] = array();
                $imageList = $product->getImages();

                $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                //$newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());

                foreach($newImage as $image) {
                    $productArray["images"][] = array(
                        "id" => $image->getId(),
                        "name" => $image->getFilename(),
                        "path" => $image->getPath(),
                        "url" => "http://63.35.162.212/uploads/files/files/".$image->getPath()
                    );
                }




                $productArray["series"] = array();
                $series = $product->getSeries();
                $currentProductLink = str_split($product->getLink(), 2);
                $currentProductLink = $currentProductLink[0];

                if($series != null) {
                    $seriesDesc = $em->getRepository(SeriesText::class)->findOneBy(array(
                        "series" => $series,
                        "type" => 1,
                        "lang" => $this->lang
                    ));

                    if($seriesDesc) {
                        $desc = $seriesDesc->getDescription();
                    } else {
                        $desc = "";
                    }
                    $array = array(
                        "name" => $series->getName(),
                        "code" => $series->getCode(),
                        "mainSize" => $currentProductLink,
                        "mainLink" => $product->getLink(),
                        "desc" => $desc
                    );
                    $array["products"] = array();
                    if($series->getProduct($product->getId()) != null) {
                        $productList = $series->getProduct($product->getId());
                        foreach ($productList as $pro) {
                            $findSeriesCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                                "product" => $pro->getId(),
                                "country" => $this->country
                            ));

                            if($findSeriesCountry) {
                                if($pro->getStatus()->getCode() == "publish") {
                                    $sizeSplit = str_split($pro->getLink(), 2);
                                    $array["products"][] = array(
                                        "id" => $pro->getId(),
                                        "size" => $sizeSplit[0],
                                        "link" => $pro->getLink(),
                                        "name" => $pro->getName(),
                                        "code" => $pro->getCode()
                                    );
                                }
                            }
                        }
                    }

                    $productArray["series"] = $array;
                }
              }
            }

          return $productArray;
  }

    /**
     * @Route("product-detail/{productLink}", name="getProductBySef")
     */
    public function getProductBySef($productLink, $internal=false)
    {


          $session = new Session();
          $session->start();

          if(
            $session->get('last3')!=$productLink and
            $session->get('last2')!=$productLink and
            $session->get('last1')!=$productLink
          ){
            $session->set('last3', $session->get('last2'));
            $session->set('last2', $session->get('last1'));
            $session->set('last1', $productLink);
          }




        $dataArray = [];
        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->findOneBy(array(
            "link" => $productLink,
            "isOld" => 0,
            "deletedAt" => null
        ));
        //print_r($product->getName());exit;
        $productArray = array();


        /**
         * @var ProductCountry[] $product
         */
        $products = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "product" => $product,
            "active"=> 1,
            "deletedAt"=>null
            )
        );


        foreach($products as $product) {
                $product = $product->getProduct();
                if($product->getStatus()->getCode() == "publish") {

                $productArray["text"] = array();
                $lang = $em->getRepository(Language::class)->find(35);
                $textList = $product->getLangText($this->lang);
                $productArray["categoryLink"] = $product->getCategory()->getLink();
                if(!$textList[0]) {
                    $textList = $product->getLangText($lang);
                    if(!$textList[0]) {
                        $lang = $em->getRepository(Language::class)->find(37);
                        $textList = $product->getLangText($lang);
                    }
                } 
                // if(!isset($textList[0])) {
                //     $textList = $product->getLangText($this->globalLang);
                //     if(isset($textList[0])) {
                        
                //         $textList = $product->getLangText($lang);
                //     }
                // } else {
                //     $textList = $product->getLangText($this->lang);
                // }
               //print_r($this->lang->getId());exit;
                $productArray["ean"] = $product->getEan();
                $productArray["text"][] = array(
                    "title" => $textList[0]->getSubtitle(),
                    "name" => $textList[0]->getName(),
                    "shortDescription" => $textList[0]->getShortDescription(),
                    "longDescription" => $textList[0]->getLongDescription(),
                    "link" => $textList[0]->getLink(),
                    "keywords" => $textList[0]->getKeywords(),
                    "video" => $textList[0]->getYtVideo(),
                    "videoThumbnail" => $textList[0]->getVideoThumbnail()
                );
                $productArray["images"] = array();
                $imageList = $product->getImages();

                $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                //$newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());

                foreach($newImage as $image) {
                    $productArray["images"][] = array(
                        "id" => $image->getId(),
                        "name" => $image->getFilename(),
                        "path" => $image->getPath(),
                        "url" => "http://63.35.162.212/uploads/files/files/"
                    );
                }
                foreach ($imageList as $image) {
                    if($image->getType() != null) {
                        if($image->getType()->getCode() == "video") {
                            $productArray["video"][$image->getType()->getCode()][] = array(
                                "id" => $image->getId(),
                                "name" => $image->getName(),
                                "path" => $image->getPath(),
                            );
                        }
                    }



                    //  print_r($usort);
                }
                //print_r($productArray["video"]);exit;
                //print_r($productArray["images"]);exit;
                // if(array_search("Perspective 2", $productArray["images"])) {
                //     echo 'gg';exit;
                // },


                $productArray["series"] = array();
                $series = $product->getSeries();
                $currentProductLink = str_split($product->getLink(), 2);
                $currentProductLink = $currentProductLink[0];


                if($series != null) {
                    $seriesDesc = $em->getRepository(SeriesText::class)->findOneBy(array(
                        "series" => $series,
                        "type" => 1,
                        "lang" => $this->lang
                    ));

                    if($seriesDesc) {
                        $desc = $seriesDesc->getDescription();
                    } else {
                        $desc = "";
                    }

                    $array = array(
                        "name" => $series->getName(),
                        "code" => $series->getCode(),
                        "mainSize" => $currentProductLink,
                        "desc" => $desc
                    );
                    $array["products"] = array();
                    if($series->getProduct($product->getId()) != null) {
                        $productList = $series->getProduct($product->getId());
                        foreach ($productList as $pro) {
                            $findSeriesCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                                "product" => $pro->getId(),
                                "country" => $this->country
                            ));

                            if($findSeriesCountry) {
                                if($pro->getStatus()->getCode() == "publish") {
                                    if($product->getCategory()->getCode() == $pro->getCategory()->getCode()) {
                                        $seriesDesc = $em->getRepository(SeriesText::class)->findOneBy(array(
                                            "series" => $series,
                                            "type" => 1,
                                            "lang" => $this->lang
                                        ));

                                        if($seriesDesc) {
                                            $desc = $seriesDesc->getDescription();
                                        } else {
                                            $desc = "";
                                        }
                                        $sizeSplit = str_split($pro->getLink(), 2);
                                        $array["products"][] = array(
                                            "id" => $pro->getId(),
                                            "size" => $sizeSplit[0],
                                            "link" => $pro->getLink(),
                                            "name" => $pro->getName(),
                                            "code" => $pro->getCode(),
                                            "desc" => $desc
                                        );
                                    }

                                }
                            }
                        }
                    }

                    $productArray["series"] = $array;
                }

                $productArray["feature"] = array();
                $featureList = $product->getFeature();

                foreach ($featureList as $feature) {
                    if($feature->getFeature() != null && $feature->getFeature()->getText($this->lang)[0] != null){
                        $featureTmp = array(
                            "name" => $feature->getFeature()->getText($this->lang)[0]->getName(),
                            "shortDescription" => $feature->getFeature()->getText($this->lang)[0]->getShortDescription(),
                            "longDescription" => $feature->getFeature()->getText($this->lang)[0]->getLongDescription(),
                            "typeName" => $feature->getType()->getName(),
                            "v1" => $feature->getFeature()->getV1(),
                            "v2" => $feature->getFeature()->getV2(),
                            "v3" => $feature->getFeature()->getV3(),
                            "icon" => $feature->getFeature()->getIcon()
                        );

                        $featureArray[$feature->getType()->getCode()][] = $featureTmp;
                        $productArray["feature"] = $featureArray;
                    }
                }

                $productArray["spec"] = array();
                $specList = $product->getSpec($this->lang);
                if(!$specList[0]) {
                    $specList = $product->getSpec($this->globalLang);

                }
                $specArray = array();
                foreach ($specList as $spec) {

                    $category = $spec->getSpec()->getCategory();
                    if( $category->getText($this->lang)[0] == null) {
                        $categoryName = $category->getName();
                    }else {
                        $categoryName = $category->getText($this->lang)[0]->getText();
                    }

                    if(empty($specArray[$category->getId()])) {
                        $specArray[$category->getId()] = array(
                            "id" => $category->getId(),
                            "name" => $categoryName
                        );
                    }
                    // $specText = $spec->getSpec()->getName();

                    if( $spec->getSpec()->getText($this->lang)[0] == null) {
                        $specText = $spec->getSpec()->getName();
                    }else {
                        $specText = $spec->getSpec()->getText($this->lang)[0]->getName();
                    }

                    $specArray[$category->getId()]["children"][] = array(
                        "name" => $specText,
                        "value" => $spec->getValue()
                    );
                }

                foreach ($specArray as $spec) {
                    $productArray["spec"][] = $spec;
                }
            }

        }
        $lastVisited = array();
        //burayı ayarla

        // if(!$internal) {
        //     $lastVisitedAddresses = array($session->get('last1'),$session->get('last2'),$session->get('last3'));

        //     foreach($lastVisitedAddresses as $row){
        //         if(strlen($row)>0){
        //         $lastVisited[] = $this->getProductBySefAsArray($row);
        //         }
        //     }
        // }

       //print_r($lastVisited);die();
            //print_r($this->lang->getId());exit;
       //print_r($productArray);exit;
            //print_r($productArray);exit;



        // $tmp="";

        // $refData = $em->getRepository(pfeature::class)->get();
        //    // exit;
        // $translatedData = $em->getRepository(pfeaturetranslate::class)->getFeatureByLangNew($this->lang);
        // foreach($refData as $k=>$row) {


        //     if(!isset($translatedData[$k])){

        //         $translatedData[$k]["name"] = $refData[$k]["name"];
        //         $translatedData[$k]["des"] = $refData[$k]["des"];
        //         $translatedData[$k]["path"] = $refData[$k]["path"];
        //         continue;
        //     }
        //     if($translatedData[$k]["name"] == ""){
        //     $translatedData[$k]["name"] = $refData[$k]["name"];
        //     }
        //     if($translatedData[$k]["des"] == ""){
        //     $translatedData[$k]["des"] = $refData[$k]["des"];
        //     }
        //     if($translatedData[$k]["path"] == ""){
        //     $translatedData[$k]["path"] = $refData[$k]["path"];
        //     }

        // }
        $newFeatureArray = array();
        $findProductFeatures = $em->getRepository(NewProductFeature::class)->findAll();

        foreach($findProductFeatures as $newFeatures) {
            //print_r($this->lang->getId());exit;
            if(!$newFeatures->getProductFeatureTranslate($this->lang)[0]) {
                $des = $newFeatures->getDes();
            } else {
                $des = $newFeatures->getProductFeatureTranslate($this->lang)[0]->getDes();
            }

            if(!$newFeatures->getProductFeatureTranslate($this->lang)[0]) {
                $name = $newFeatures->getName();

            } else {
                $name = $newFeatures->getProductFeatureTranslate($this->lang)[0]->getName();
            }


            $newFeatureArray[] = array(
                "des" => $des,
                "name" => $name,
                "path" => $newFeatures->getPath()
            );
        }
       //print_r($productArray);exit;
        return $this->render("productInfo.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "product" => $productArray,
            "features" => $newFeatureArray,
            // "lastVisited" => array_filter($lastVisited),
            "country" => $this->country,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/allProducts", name="allProduct")
     */
    public function newAllProduct() {
        $em = $this->getDoctrine()->getManager();
        $dataArray = array();
        $productArray = array();
        $catProduct = array();

        /**
         * @var ProductCountry[] $product
         */
        $productCountries = $em->getRepository(ProductCountry::class)->findBy(array(
            "country" => $this->country,
            "active"=> 1,
            "deletedAt"=> null)
        );
        $serieArray = array();

        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));

        $filterArray = array();

        $filterMainCat = $em->getRepository(Category::class)->find(17);

        $findFilters = $em->getRepository(FiltersCategory::class)->findBy(array(
            "category" => $filterMainCat
        ));

        foreach($findFilters as $filters) {
            $filterCategory = $filters->getFilters();
            if($filters->getActive() == 1) {
                // print_r($filterCategory->getId());
                $filterValues = $em->getRepository(FiltersValue::class)->findBy(array(
                    "filtersCategory" => $filters,
                    "category" => $filterMainCat
                ));

                $tmp = array();
                foreach($filterValues as $value) {
                    //print_r($value->getName());
                    $tmp[] = array(
                        "id" => $value->getId(),
                        "name" => $value->getName()
                    );
                }

                $filterArray[] = array(
                    "filterCategoryId" =>  $filterCategory->getId(),
                    "filterCategoryName" =>  $filterCategory->getName(),
                    "values" => $tmp
                );
            }
        }

        foreach($productCountries as $pCountry) {
            $product = $pCountry->getProduct();
            $productCategory = $product->getCategory();
            $series = $product->getSeries();
            $tmpArray = array();

            if($series != null) {
                $findSerieProducts = $em->getRepository(Product::class)->seriesGroupProduct($product->getId(), $this->lang, $productStatus);

                foreach($findSerieProducts as $serieProduct) {
                    $productInc = str_split($serieProduct->getName(), 2);
                    $productInc = $productInc[0];
                    if($serieProduct->getSeries() != null) {
                        $checkSerieProdCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                            "country" => $this->country,
                            "product" => $product->getId()
                        ));
                       if($checkSerieProdCountry) {
                        if($serieProduct->getSeries()->getLangText($this->lang)[0] == null) {
                            $serieDesc = "";
                        } else {
                            $serieDesc = $serieProduct->getSeries()->getLangText($this->lang)[0]->getDescription();
                        }

                            $img = $product->getPp()."pp";

                        // print_r($img);exit;
                        $productArray[$serieProduct->getCategory()->getName()][$serieProduct->getSeries()->getCode()]["productName"] = $serieProduct->getName();
                        $productArray[$serieProduct->getCategory()->getName()][$serieProduct->getSeries()->getCode()]["productText"] = $serieProduct->getText()[0]->getName();
                        $productArray[$serieProduct->getCategory()->getName()][$serieProduct->getSeries()->getCode()]["serie"] = $serieProduct->getSeries()->getPriority();
                           if($serieProduct->getText() != null) {
                            $tmpArray[] = array(
                                "productId" => $serieProduct->getId(),
                                "name" => $serieProduct->getName(),
                                "textName" => $serieProduct->getText()[0]->getName(),
                                "link" => $serieProduct->getLink(),
                                "serie" => $serieProduct->getSeries()->getName(),
                                "serieDesc" => $serieDesc,
                                "seriePrio" => $serieProduct->getSeries()->getPriority(),
                                "image" => $img,
                                "inc" => intval($productInc)
                            );

                           }
                       }
                    }
                    $productArray[$serieProduct->getCategory()->getName()][$serieProduct->getSeries()->getCode()][] = $tmpArray;
                    $this->arraySortByColumn($productArray[$serieProduct->getCategory()->getName()], 'serie');

                }

            }
        }
        return $this->render("allProduct.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "allProducts" => $productArray,
            "country" => $this->country,
            "filters" => $filterArray,
            "lang" => $this->lang
        ));
    }

    /**
     * @Route("/all-tv-filter", name="allTvsFilter")
     */
    public function allTvsFilter() {

      //  $_POST['products'] = '[null,["56"],null,null,null,null]';
        $data = json_decode(stripslashes($_POST['products']));
        $em = $this->getDoctrine()->getManager();


        $productStatus = $em->getRepository(ProductStatus::class)->findOneBy(array(
            "code" => "publish"
        ));

        $productArray = array();
        $data = array_filter($data);

        $newFilterArray = array();

        foreach ($data as $d) {
            foreach($d as $filterValue) {
                $newFilterArray[] = $filterValue;
            }
        }
        $counted = count($newFilterArray);
        $filterValueIdString = '('.implode(",",$newFilterArray).')';
        //print_r($filterValueIdString);exit;
        $conn = $em->getConnection();
        $sql = 'SELECT filters_value_product.product_id FROM filters_value_product
                left join filters_value as fval on fval.id = filters_value_product.filter_value_id
                left join product on product.id = filters_value_product.product_id ';
                if($counted>0){$sql .= "where fval.id IN ".$filterValueIdString;}

            $stmt = $conn->prepare($sql);

            $stmt->execute();
            $arr = $stmt->fetchAll();
            $filtered = array();
            $newArray = array();

            foreach($arr as $row) {
                $filtered[] = $row['product_id'];
            }

            if($counted > 1) {
                $countVal = array_count_values($filtered);
                foreach($filtered as $key => $value) {
                    //echo '<pre>';
                    //print_r($counted."--".$countVal[$value]."-->".$value);
                    if($countVal[$value] == $counted) {
                        //echo '^gg';
                        // echo '<pre>';
                        // print_r($countVal[$value]);
                        // echo '<pre>';
                        // print_r($value);
                        $newArray[] = $value;

                    }

                }

                $filtered = $newArray;
                $filtered = array_unique($filtered);
            }else {
                $filtered = array_unique($filtered);
            }

        $productArray = array();
        //print_r($filtered);exit;
        foreach($filtered as $productId) {

            $product = $em->getRepository(Product::class)->find($productId);
            if($product->getCategory() != null) {
                if($product->getCategory()->getId() != 18) {
                    $series = $product->getSeries();

                    $productInc = str_split($product->getName(), 2);
                    $productInc = $productInc[0];

                    // $productArray[$serieProduct->getSeries()->getCode()][] = array(
                    //     "productId" => $serieProduct->getId(),
                    //     "name" => $serieProduct->getName(),
                    //     "textName" => $textName,
                    //     "link" => $serieProduct->getLink(),
                    //     "serie" => $serieProduct->getSeries()->getName(),
                    //     "image" => $serieProduct->getImages()[0]->getPath(),
                    //     "inc" => $productInc
                    // );

                    if($product->getText()[0] == "") {
                        $textName = $product->getLangText($this->globalLang)[0]->getName();
                    } else {
                        $textName = $product->getText()[0]->getName();
                    }

                    $serieName = "";
                    $serie = $product->getSeries();
                    if($serie) {
                        $serieName = $serie->getName();
                    }

                    $newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                        //$newImage = $em->getRepository(ProductFile::class)->getProductImage($product->getId());
                    $fullPath = "";

                    foreach($newImage as $image) {
                        if(strpos($image->getFileName(), "front") !== false) {
                            $fullPath = "http://63.35.162.212/uploads/files/files/".$image->getPath();
                        }

                    }
                    $serieDesc = "";
                    if($product->getSeries()->getLangText($this->lang)[0] == null) {
                        $serieDesc = "";
                    } else {
                        $serieDesc = $product->getSeries()->getLangText($this->lang)[0]->getDescription();
                    }

                    $productArray[] = array(
                        "productId" => $product->getId(),
                        "category" => $product->getCategory()->getName(),
                        "categoryID" => $product->getCategory()->getId(),
                        "name" => $product->getName(),
                        "textName" => $textName,
                        "link" => $product->getLink(),
                        "serie" => $serie,
                        "serieDesc" => $serieDesc,
                        "inc" => $productInc,
                        "image" => $fullPath

                    );
                }

            }

        }
        //print_r($productArray);exit;
        $groupedCategoryArray = array();

        $series = array();
        foreach($productArray as $arr) {
            if(!isset($series[$arr["category"]])){
                $series[$arr["category"]] = array();
                $series[$arr["category"]]["ID"] = $arr["categoryID"];
                $series[$arr["category"]]["name"] = $arr["category"];
                $series[$arr["category"]]["products"] = array();
            }



            $groupedCategoryArray[$arr["category"]][] = array(
                "productId" => $arr["productId"],
                "name" => $arr["name"],
                "textName" => $arr["textName"],
                "link" => $arr["link"],
                "inc" => $arr["inc"],
                "serieName" => $arr["serie"]->getName(),
                "serieDesc" => $arr["serieDesc"],
                "serieID" => $arr["serie"]->getId(),
                "image" => $arr["image"]
            );


            if(!isset($series[$arr["category"]]["products"][$arr["serie"]->getID()])){
                $series[$arr["category"]]["products"][$arr["serie"]->getID()] = array();
                $series[$arr["category"]]["products"][$arr["serie"]->getID()]["name"] = $arr["serie"]->getName();
                $series[$arr["category"]]["products"][$arr["serie"]->getID()]["serieDesc"] = $arr["serieDesc"];
                $series[$arr["category"]]["products"][$arr["serie"]->getID()]["img"] = $arr["image"];
                $series[$arr["category"]]["products"][$arr["serie"]->getID()]["sizes"] = array();

            }
            $series[$arr["category"]]["products"][$arr["serie"]->getID()]["sizes"][] = array(
                "productId" => $arr["productId"],
                "name" => $arr["name"],
                "textName" => $arr["textName"],
                "link" => $arr["link"],
                "inc" => $arr["inc"],
                "serieName" => $arr["serie"]->getName(),
                "serieDesc" => $arr["serieDesc"],
                "serieID" => $arr["serie"]->getId(),
                "image" => $arr["image"],
            );

        }

        //print_r($series);die();
        /*print_r($groupedCategoryArray["LED TV"][0]["serie"]->getId());
        print_r($groupedCategoryArray["LED TV"][0]["serie"]->getName());
        print_r($groupedCategoryArray["LED TV"][0]["serie"]->getCode());
        print_r(get_class_methods ($groupedCategoryArray["LED TV"][0]["serie"]));exit;*/
        //print_r( ($series));exit;
        if(count($productArray) > 0) {
            return new JsonResponse($series);
            return new JsonResponse($groupedCategoryArray);
        }  else {
            return new JsonResponse(array("error" => "NOMATCH"));
        }
    }

    function arraySortByColumn(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        //print_r($arr);exit;
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        return array_multisort($sort_col, $dir, $arr);
    }

    public function importLink(){

        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        for ($row = 2; $row <= $highestRow; $row++) {
            $prodVal = $sheet2->getCell('A' . $row)->getValue();
            $findProduct = $em->getRepository(Product::class)->find($prodVal);
            $link = $sheet2->getCell('B' . $row)->getValue();
            if($link == "") {
                $link = "no-link-found";
            }
            if($findProduct) {
                $findProduct->setLink($link);
                $em->flush();
            }
         }exit;
    }


    public function importProdName(){
        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->getActiveSheet();
        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;
        for ($row = 10; $row <= $highestRow; $row++) {
            $prodId = $sheet2->getCell('A' . $row)->getValue();

            $findProduct = $em->getRepository(Product::class)->find($prodId);
            if(!$findProduct) {
                continue;
            }
            $name = $sheet2->getCell('B' . $row)->getValue();
            $findProduct->setName($name);
            $em->flush();
         }

         exit;
    }


    public function importCategoryExcel() {
        set_time_limit(50000);
        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->setActiveSheetIndex(1);

        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;
        $categoryName = $sheet2->getCell('G1')->getValue();
        $linked = substr($categoryName, 0, 3);
        for ($row = 2; $row <= $highestRow; $row++) {
            $prodCode = $sheet2->getCell('C' . $row)->getValue();
            //print_r($prodCode);exit;
            $categoryExist = $sheet2->getCell('G' . $row)->getValue();

            $p = $em->getRepository(Product::class)->findOneBy(array(
                "code" => $prodCode,
                "isOld" => 1
            ));

            $checkCategory = $em->getRepository(Category::class)->findOneBy(array(
                "name" => $categoryName
            ));
            //print_r($checkCategory->getName());exit;

            if(!$p) {
                echo $prodCode;
                continue;
            }

            if($categoryExist == 1) {
                $c = new Product();
                $c->setName($p->getName());
                $c->setPp($p->getPp());
                $c->setEan($p->getEan());
                $c->setCode($p->getCode());
                $c->setLink($p->getLink()."-".$linked);
                $c->setKeywords($p->getKeywords());
                $c->setSize($p->getSize());
                $c->setCategory($checkCategory);
                $c->setSeries($p->getSeries());
                $c->setActive($p->getActive());
                $c->setIsPublish(1);
                $c->setStatus($p->getStatus());
                $c->setPriority($p->getPriority());
                $c->setDeletedAt($p->getDeletedAt());
                $c->setIsOld(0);

                $em->persist($c);
                $em->flush();

                echo '<pre>';
                print_r($p->getId()."======>".$c->getId());

                $pFeature = $p->getFeature();

                foreach($pFeature as $f) {
                    $newF = new ProductFeature();
                    $feature = $f->getFeature();

                    $newF->setFeature($feature);
                    $newF->setProduct($c);
                    $newF->setType($f->getType());
                    $newF->setPriority($f->getPriority());
                    $newF->setDeletedAt($f->getDeletedAt());

                    $em->persist($newF);
                    $em->flush();
                }

                $pSpec = $p->getSpec($this->lang);
                if(!$pSpec[0]) {
                    $pSpec = $p->getSpec($this->globalLang);
                }
                foreach($pSpec as $prodSpec) {
                    $spec = $prodSpec->getSpec();

                    $newSpec = new ProductSpec();

                    $newSpec->setSpec($spec);
                    $newSpec->setProduct($c);
                    $newSpec->setLang($prodSpec->getLang());
                    $newSpec->setValue($prodSpec->getValue());
                    $newSpec->setPriority($prodSpec->getPriority());
                    $newSpec->setDeletedAt($prodSpec->getDeletedAt());

                    $em->persist($newSpec);
                    $em->flush();
                }

                $pCountry = $p->getCountry();
                foreach($pCountry as $country) {
                    $pc = new ProductCountry();

                    $pc->setProduct($c);
                    $pc->setCountry($country->getCountry());
                    $pc->setActive($country->getActive());
                    $pc->setDeletedAt($country->getDeletedAt());

                    $em->persist($pc);
                    $em->flush();
                }

                $pImage = $em->getRepository(ProductFile::class)->getProductImage($p->getId());

                // $productArray["images"][] = array(
                //     "id" => $image->getId(),
                //     "name" => $image->getFilename(),
                //     "path" => $image->getPath(),
                //     "url" => "http://63.35.162.212/uploads/files/files/"
                // );

                foreach($pImage as $img) {
                    $type = $img->getType();
                    $country = $img->getCountry();

                    $newI = new ProductFile();

                    $newI->setFileName($img->getFileName());
                    $newI->setProduct($c);
                    $newI->setPath($img->getPath());
                    $newI->setType($type);
                    $newI->setCountry($country);
                    $newI->setPriority($img->getPriority());
                    $newI->setDescription($img->getDescription());
                    $newI->setDeletedAt($img->getDeletedAt());

                    $em->persist($newI);
                    $em->flush();
                }

                $productImg = $p->getImages();

                foreach($productImg as $nProductImg) {
                    $newProductImage = new ProductImage();

                    $newProductImage->setProduct($c);
                    $newProductImage->setType($nProductImg->getType());
                    $newProductImage->setPath($nProductImg->getPath());
                    $newProductImage->setName($nProductImg->getName());
                    $newProductImage->setActive($nProductImg->getActive());
                    $newProductImage->setPriority($nProductImg->getPriority());
                    $newProductImage->setDeletedAt($nProductImg->getDeletedAt());

                    $em->persist($newProductImage);
                    $em->flush();
                }

                $pText = $p->getLangText($this->lang);
                if(!$pText[0]) {
                    $pText = $p->getLangText($this->globalLang);
                }

                foreach($pText as $text) {
                    $nText = new ProductText();

                    $nText->setName($text->getName());
                    $nText->setSubtitle($text->getSubtitle());
                    $nText->setProduct($c);
                    $nText->setLang($text->getLang());
                    $nText->setShortDescription($text->getShortDescription());
                    $nText->setLongDescription($text->getLongDescription());
                    $nText->setLink($p->getLink()."-".$linked);
                    $nText->setKeywords($text->getKeywords());
                    $nText->setDeletedAt($text->getDeletedAt());

                    $em->persist($nText);
                    $em->flush();

                }
            }




            // if($categoryExist == 1) {
            //     $findProduct->setCategory($checkCategory);

            //     $em->flush();
            // }

        }
        exit;
    }


    public function filtreEdit() {
        set_time_limit(50000);
        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->setActiveSheetIndex(3);

        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        $category = $em->getRepository(Category::class)->find(1);

        for ($row = 1; $row <= $highestRow; $row++) {
            $prodCode = $sheet2->getCell('A' . $row)->getValue();
            if($prodCode == "") {
                continue;
            }
            $prodCode = str_replace("======>", ",", $prodCode);
            $prodCode = explode(",", $prodCode);

            $oldProd =  $prodCode[0];
            $newProd = $prodCode[1];


            $oldProduct = $em->getRepository(Product::class)->findOneBy(array(
                "id" => $oldProd,
            ));

            $newProduct = $em->getRepository(Product::class)->findOneBy(array(
                "id" => $newProd,
            ));

            $findFilterProduct = $em->getRepository(FiltersValueProduct::class)->findBy(array(
                "product" => $oldProd
            ));
            //print_r($category->getName());exit;

            if($findFilterProduct) {
                foreach($findFilterProduct as $filterProduct) {
                    $filterProduct->setProduct($newProduct);
                    $em->flush();

                }
            } else {
                echo '<pre>';

                print_r($oldProd."-".$newProd);
            }

        }

        exit;
    }


    function allProdDummy() {
        exit;
        $em = $this->getDoctrine()->getManager();

        $conn = $em->getConnection();

        // $sql = "SELECT product.name,product.id FROM product LEFT JOIN category ON category.id = product.category_id LEFT JOIN product_country ON product_country.product_id = product.id  where product.active = 1 and product.status_id = 1 and product.deleted_at is null and product.category_id = 3 and product_country.country_id = 39";

        $sql = 'SELECT product.id, product_spec.value
        FROM product
        left JOIN product_spec on  product_spec.product_id = product.id
        left JOIN product_country ON product_country.product_id = product.id
        where product.active = 1
        and product.status_id = 1
        and product.deleted_at is null
        and product_spec.spec_id = 3
        and product_country.country_id = 39
        and product_spec.value like "%3840%"
        and product_spec.deleted_at is null
        group by product.id';

        $stmt = $conn->prepare($sql);

        $stmt->execute();
        $arr = $stmt->fetchAll();
        //print_r($arr);exit;
        $category = $em->getRepository(Category::class)->find(17);
        foreach($arr as $row) {
            // $productInc = str_split($row["name"], 2);
            // $productInc = $productInc[0];
            //print_r($productInc);exit;
            //print_r($row["name"]);
            $filterCategory = $em->getRepository(FiltersCategory::class)->find(20);

            $findInc = $em->getRepository(FiltersValue::class)->findOneBy(array(
                "filtersCategory" => $filterCategory,
                "category" => $category,
                "code" => "4KUHD"
            ));

            $findProduct = $em->getRepository(Product::class)->find($row["id"]);

            if($findInc) {
                $newFilterProducts = new FiltersValueProduct;
                $newFilterProducts->setProduct($findProduct);
                $newFilterProducts->setFiltersValue($findInc);

                $em->persist($newFilterProducts);
                $em->flush();

            }
        }
        exit;
    }


    public function serieDescProd() {
        set_time_limit(50000);
        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->setActiveSheetIndex(0);

        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        for ($row = 2; $row <= $highestRow; $row++) {
            $serieName = $sheet2->getCell('B' . $row)->getValue();
            $desc = $sheet2->getCell('C' . $row)->getValue();
            if($serieName == "") {
                continue;
            }

            $findSerie = $em->getRepository(Series::class)->findOneBy(array(
                "name" => $serieName
            ));

            if($findSerie) {
                $newText = new SeriesText();

                $newText->setType(1);
                $newText->setDescription($desc);
                $newText->setSeries($findSerie);
                $newText->setLang($this->lang);

                $em->persist($newText);
                $em->flush();
            }

        }
        exit;
    }


    public function smartTvFilterAdd() {
        set_time_limit(50000);
        $inputFileType = 'Xlsx';
        $tmpFile = $_FILES["uploaded_file"]["tmp_name"];

        $em = $this->getDoctrine()->getManager();

        $reader = IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($tmpFile);

        $sheet2 = $spreadsheet->setActiveSheetIndex(0);

        $highestRow = $sheet2->getHighestRow();
        $highestColumn = $sheet2->getHighestColumn();
        $highestColumn++;

        $category = $em->getRepository(Category::class)->find(17);
        $filterCategory = $em->getRepository(FiltersCategory::class)->find(21);

        for ($row = 3; $row <= $highestRow; $row++) {
            $prodCode = $sheet2->getCell('A' . $row)->getValue();

            if($prodCode == "") {
                continue;
            }

            $link = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $prodCode));
            //print_r($link);exit;
            $product = $em->getRepository(Product::class)->findOneBy(array(
                "name" => $link,
            ));


            if($product) {
                $priority = 10;
                $specCategoryArray = $sheet2->rangeToArray('O1:U1')[0];
                $specArray = $sheet2->rangeToArray('O2:U2')[0];
                $specTextValue = $sheet2->rangeToArray('O' . $row . ':' . 'U' . $row);
                $this->extendArray($specCategoryArray);
                //print_r($specTextValue[0]);exit;
                foreach ($specTextValue[0] as $key => $filterTextValue) {
                    if(strtolower($filterTextValue) == "x") {
                        //print_r();
                        $filterValue = $em->getRepository(FiltersValue::class)->findOneBy(array(
                            "code" => $specArray[$key],
                            "category" => $category,
                            "filtersCategory" => $filterCategory
                        ));
                        // if(!$filterValue) {
                        //     print_r($specArray[$key]);
                        // }

                        $filtersProd = new FiltersValueProduct();

                        $filtersProd->setFiltersValue($filterValue);
                        $filtersProd->setProduct($product);

                        $em->persist($filtersProd);
                        $em->flush();
                    }

                }
            }

        }     exit;
    }

    private function extendArray(&$array) {
        $lastItem = "";

        foreach ($array as &$item) {
            if($item != "") {
                $lastItem = $item;
            }else {
                $item = $lastItem;
            }
        }

        return $array;
    }
}
