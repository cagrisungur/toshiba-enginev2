<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Country;
use App\Entity\WarrantyUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

class NewSupportLoginController extends CustomController 
{
    /**
     * @Route("support-login", name="loginSupport")
     */
    public function loginSupport(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        $em = $this->getDoctrine()->getManager();
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));


        if(isset($_POST["login"])) {
            if(isset($_POST["email"]) && isset($_POST["pwd"])) {
                $email = $_POST["email"];
                $pwd = $_POST["pwd"];
                $message = "";
                $checkUser = $em->getRepository(WarrantyUser::class)->findOneBy(array(
                    "email" => $email,
                    "password" => md5($pwd)
                ));

                if($checkUser) {
                    $userArr = array(
                        "id" => $checkUser->getId()
                    );
                    $session->set("userInfo", $userArr);
                    return $this->redirectToRoute("dashboard", array(
                        "country" => $this->country->getCode(),
                        "language" => $this->lang->getCode(),
                    ));
                } else {
                    return $this->redirectToRoute("register", array(
                        "country" => $this->country->getCode(),
                        "language" => $this->lang->getCode(),
                    ));
                }
            }
        }
    }

     /**
     * @Route("register-home", name="registerHome")
     */
    public function registerHomeLogin(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        //print_r($session->get('userInfo'));exit;
        if($session->get('userInfo') != "") {
            return $this->redirectToRoute("dashboard", array(
                "country" => $this->country->getCode(),
                "language" => $this->lang->getCode(),
            ));
        }
        $em = $this->getDoctrine()->getManager();
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));


        if(isset($_POST["login"])) {
            if(isset($_POST["email"]) && isset($_POST["pwd"])) {
                $email = $_POST["email"];
                $pwd = $_POST["pwd"];
                $message = "";
                $checkUser = $em->getRepository(WarrantyUser::class)->findOneBy(array(
                    "email" => $email,
                    "password" => md5($pwd)
                ));
                // print_r(md5($pwd));exit;
                if($checkUser) {
                    
                    $userArr = array(
                        "id" => $checkUser->getId()
                    );
                    $session->set("userInfo", $userArr);
                    return $this->redirectToRoute("dashboard", array(
                        "country" => $this->country->getCode(),
                        "language" => $this->lang->getCode(),
                    ));
                } else {
                    
                    return $this->redirectToRoute("register", array(
                        "country" => $this->country->getCode(),
                        "language" => $this->lang->getCode(),
                    ));
                }
            }
        }
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));

       return $this->render("registerHome.html.twig", array(
           "staticText" =>  $this->staticText,
           "categories" => $this->category,
           "country" => $this->country,
           "lang" => $this->lang ,
           "countries" => $countries
       ));
    }
}