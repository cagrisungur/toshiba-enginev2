<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\OauthClientEndpoints;
use App\Entity\OauthClients;
use App\Entity\OauthSessionAccessTokens;
use App\Entity\OauthSessionAuthcodes;
use App\Entity\OauthSessionRefreshTokens;
use App\Entity\OauthSessions;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserModule;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;

class OauthController extends Controller
{
    public function generateUUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function generateJWT($id, $name, $moduleArray = array())
    {
        $privateKey = file_get_contents("private.pem", true);
        $payload = [
            "user_id" => $id,
            "user_name" => $name,
            "module" => $moduleArray,
            "iat" => time(),
            "exp" => time() + (50000 * 50000)
        ];
        $token = JWT::encode($payload, $privateKey, 'RS256');
        return $token;
    }

    public function authorize(Request $request, SerializerInterface $serializer)
    {
        $username = $request->request->get("username");
        $password = $request->request->get("password");
        $clientId = $request->request->get("clientId");
        $redirectUri = $request->request->get("redirectUri");

        $auth_code = [];
        $message = "";

        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(OauthClients::class)->find($clientId);
        if($client) {
            $user = $em->getRepository(User::class)->findOneBy(array(
                "username" => $username,
                "password" => $password
            ));

            if($user) {
                $endpoint = $em->getRepository(OauthClientEndpoints::class)->findOneBy(array("clientId" => $clientId, "redirectUri" => $redirectUri));
                if($endpoint) {
                    $session = new OauthSessions();
                    $session->setClientId($clientId);
                    $session->setOwnerId($user->getId());
                    $session->setOwnerType("user");
                    $em->persist($session);
                    $em->flush();

                    $authCode = new OauthSessionAuthcodes();
                    $uuid = $this->generateUUID();
                    $authCode->setAuthCode($uuid);
                    $authCode->setSessionId($session->getId());
                    $authCode->setAuthCodeExpires(600);
                    $em->persist($authCode);
                    $em->flush();

                    $auth_code['authCode'] = $uuid;
                    //$message = "Successful Authorize. Auth Code is " . $uuid;
                } else {
                    $message = "Endpoint is wrong so unsuccessful authorize";
                }
            } else {
                $message = "User information is wrong so unsuccessful authorize";
            }
        } else {
            $message = "Client id is wrong so unsuccessful authorize";
        }
        if($message == "") {
            return new JsonResponse($auth_code);
        }
        return new JsonResponse(array("message" => $message));
    }

    public function token(Request $request, SerializerInterface $serializer)
    {
        date_default_timezone_set("Europe/Istanbul");
        $code = $request->request->get("code");
        $clientId = $request->request->get("clientId");
        $clientSecret = $request->request->get("clientSecret");
        $redirectUri = $request->request->get("redirectUri");

        $em = $this->getDoctrine()->getManager();
        $token = [];
        $message ="";

        $authCode = $em->getRepository(OauthSessionAuthcodes::class)->findOneBy(array("authCode" => $code));
        if($authCode) {
            $createdAt = $authCode->getCreatedAt()->format('Y-m-d H:i:s');
            $diff =  strtotime(date("Y-m-d H:i:s")) - strtotime($createdAt);
            if($diff <= $authCode->getAuthCodeExpires()) {
                $client = $em->getRepository(OauthClients::class)->findOneBy(array("id" => $clientId, "secret" => $clientSecret));
                if($client) {
                    $endpoint = $em->getRepository(OauthClientEndpoints::class)->findOneBy(array("clientId" => $clientId, "redirectUri" => $redirectUri));
                    if($endpoint) {
                        $session = $em->getRepository(OauthSessions::class)->find($authCode->getSessionId());
                        if($session) {
                            $user = $em->getRepository(User::class)->find($session->getOwnerId());
                            if($user) {
                                $findUserModules = $em->getRepository(UserModule::class)->findBy(array(
                                    "user" => $user
                                ));
                                $moduleArray = array();

                                foreach ($findUserModules as $modules) {
                                    $moduleArray[] = array(
                                        "moduleName" => $modules->getModule()->getName(),
                                        "code" => $modules->getModule()->getCode()
                                    );
                                }
                                $accessToken = $this->generateJWT($user->getId(), $user->getName(), $moduleArray);
                                $isSessionAccessToken = $em->getRepository(OauthSessionAccessTokens::class)->findOneBy(array("accessToken" => $accessToken));
                                if($isSessionAccessToken) {
                                    $isSessionRefreshToken = $em->getRepository(OauthSessionRefreshTokens::class)->findOneBy(
                                        array("sessionAccessTokenId" => $isSessionAccessToken->getId()));
                                    $refreshToken = $isSessionRefreshToken->getRefreshToken();

                                    if($isSessionAccessToken->getUpdatedAt() == null) {
                                        $dateAccessToken = $isSessionAccessToken->getCreatedAt()->format('Y-m-d H:i:s');
                                        $dateRefreshToken = $isSessionRefreshToken->getCreatedAt()->format('Y-m-d H:i:s');
                                    } else {
                                        $dateAccessToken = $isSessionAccessToken->getUpdatedAt()->format('Y-m-d H:i:s');
                                        $dateRefreshToken = $isSessionRefreshToken->getUpdatedAt()->format('Y-m-d H:i:s');
                                    }
                                    $diffAccessToken =  strtotime(date("Y-m-d H:i:s")) - strtotime($dateAccessToken);
                                    if($diffAccessToken > $isSessionAccessToken->getAccessTokenExpires()) {
                                        $diffRefreshToken = strtotime(date("Y-m-d H:i:s")) - strtotime($dateRefreshToken);
                                        if($diffRefreshToken <= $isSessionRefreshToken->getRefreshTokenExpires()) {
                                            $isSessionAccessToken->setSessionId($session->getId());
                                            $isSessionAccessToken->setUpdatedAt(new \DateTime());
                                            $isSessionRefreshToken->setUpdatedAt(new \DateTime());
                                            $em->persist($isSessionAccessToken);
                                            $em->persist($isSessionRefreshToken);
                                            $em->flush();

                                            $token['accessToken'] = $accessToken;
                                            $token['refreshToken'] = $refreshToken;

                                            //$message = "Update Access and Refresh Token is " . $accessToken;
                                        } else {
                                            $message = "Access Token is expired error.";
                                        }
                                    } else {
                                        $token['accessToken'] = $accessToken;
                                        $token['refreshToken'] = $refreshToken;
                                        //$message = "Token is already use " . $accessToken;
                                    }
                                } else {
                                    $sessionAccessToken = new OauthSessionAccessTokens();
                                    $sessionAccessToken->setSessionId($session->getId());
                                    $sessionAccessToken->setAccessTokenExpires(86400);
                                    $findUserModules = $em->getRepository(UserModule::class)->findBy(array(
                                        "user" => $user
                                    ));
                                    $moduleArray = array();

                                    foreach ($findUserModules as $modules) {
                                        $moduleArray[] = array(
                                            "moduleName" => $modules->getModule()->getName(),
                                            "code" => $modules->getModule()->getCode()
                                        );
                                    }
                                    $accessToken = $this->generateJWT($user->getId(), $user->getName(), $moduleArray);
                                    $sessionAccessToken->setAccessToken($accessToken);
                                    $em->persist($sessionAccessToken);
                                    $em->flush();

                                    $sessionRefreshToken = new OauthSessionRefreshTokens();
                                    $sessionRefreshToken->setSessionAccessTokenId($sessionAccessToken->getId());
                                    $refreshToken = $this->generateUUID();
                                    $sessionRefreshToken->setRefreshToken($refreshToken);
                                    $sessionRefreshToken->setRefreshTokenExpires(63113851);
                                    $sessionRefreshToken->setClientId($clientId);
                                    $em->persist($sessionRefreshToken);
                                    $em->flush();
                                    $token['accessToken'] = $accessToken;
                                    $token['refreshToken'] = $refreshToken;
                                    //$message = "Generate Token is successful. Token is ". $accessToken . "\n" . "Generate Refresh Token is successful. Refresh Token is ". $refreshToken;
                                }
                            } else {
                                $message = "User error";
                            }
                        } else {
                            $message = "Session error";
                        }
                    } else {
                        $message = "RedirectUri error.";
                    }
                } else {
                    $message = "Client Id or Secret error.";
                }
            } else {
                $message = "Auth Code Expires error";
            }
        } else {
            $message = "Auth Code Error";
        }

        if($message == "") {
            return new JsonResponse($token);

        }
        return new JsonResponse(array("message" => $message));
    }
}