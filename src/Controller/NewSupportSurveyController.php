<?php

namespace App\Controller;


use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Product;
use App\Entity\WarrantyUser;
use App\Entity\WarrantyProduct;
use App\Entity\FeaturedCategory;
use App\Entity\CustomPage;
use App\Entity\StaticText;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NewSupportSurveyController extends CustomController
{

    public $redirectDashboard;

    /**
     * @Route("/register-next", name="registerNext")
     */
    public function registerNext(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();
        if(!$session) {
            session_start();
        }
        //print_r($session->get("userInfo"));exit;
        if(!$session->get("userInfo")) {
            echo "gg";
            exit;

        }
        //print_r($session->get("userInfo"));exit;
        $this->redirectDashboard = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()."/".$this->country->getCode(). "/".$this->lang->getCode()."/dashboard";

        //$session->remove('surveyStatus');exit;
        $langId = $this->lang->getId();
        //sonra code a cevir
        $product = $em->getRepository(Product::class)->findOneBy(array(
            "code" => $session->get("userInfo")["product"],
            "deletedAt" => null
        ));
        // print_r($product->getName());exit;
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT if(b.answer is not null OR b.answer != '', b.answer, a.answer) as answer, a.question_id, a.id as answer_id from warranty_answers a left join warranty_answer_lang b on b.answer_id = a.id WHERE (b.lang_id='$langId' or b.lang_id is null) and a.active = 1";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $answersTmp = $stmt->fetchAll();
        $error = "";
        $answers = array();

        foreach($answersTmp as $answer) {
            if(!isset($answers[$answer["question_id"]])) {
                $answers[$answer["question_id"]] = array();
            }
            $answers[$answer["question_id"]][] = $answer;
        }
        $message = "";

        $newSql = "select if(b.question != '' OR b.question is not null, b.question, a.question) as question, a.question_type, a.id as question_id from warranty_questions a left join warranty_question_lang b on b.question_id = a.question where (b.lang_id = '$langId' or b.lang_id is null) and a.active = 1 order by a.priority asc";
        $query = $conn->prepare($newSql);
        $query->execute();
        $questions = $query->fetchAll();

        $user = $em->getRepository(WarrantyUser::class)->find($session->get("userInfo")["id"]);
        $userId = $user->getId();

        // $userProduct = $em->getRepository(Product::class)->findOneBy(array(
        //     "code" => $product
        // ));

        $findSerialNumber = $em->getRepository(WarrantyProduct::class)->findOneBy(array(
            "user" =>  $user,
            "modelNo" => $session->get("userInfo")["product"]
        ));

        if(isset($_POST['submit'])) {
            if(isset($_POST['checkbox'])) {
                foreach ($_POST['checkbox'] as $key=>$checkbox) {
                    foreach ($checkbox as $ch) {
                        $sql = "INSERT INTO warranty_survey SET
                        user_id = '$userId', question_id = '$key', answer_id = '$ch'";

                        $newQ = $conn->prepare($sql);
                        $insert = $newQ->execute();

                        if(!$insert) {
                            $error = "there is an error, try again";
                        }
                    }
                }
            }
        
            if(isset($_POST['optradio'])) {
                foreach ($_POST['optradio'] as $radio) {
                    $radio = explode("-", $radio);
                    $sql1 = "INSERT INTO warranty_survey SET
                    user_id = '$userId', question_id = '$radio[0]', answer_id = '$radio[1]'";

                    $newQ1 = $conn->prepare($sql1);
                    $insert = $newQ1->execute();

                    if(!$insert) {
                        $error = "there is an error, try again";
                    }
                }
            }
        
            if(isset($_POST['text'])) {
                foreach ($_POST['text'] as $key=>$text) {
                    if(isset($_POST["optradio"][$key])) {
                        $sql2 = "UPDATE warranty_survey SET text_answer = '$text' 
                        WHERE question_id = '$key' and user_id = '$userId'";

                        $newQ2 = $conn->prepare($sql2);
                        $insert = $newQ2->execute();

                        if(!$insert) {
                            $error = "there is an error, try again";
                        }

                    }else {
                        $sql3 = "INSERT INTO warranty_survey SET
                        user_id = '$userId', question_id = '$key', text_answer = '$text'";

                        $newQ3 = $conn->prepare($sql3);
                        $insert = $newQ3->execute();

                        if(!$insert) {
                            $error = "there is an error, try again";
                        }
                    }
        
                }
            }
            

            
            //$message = $message["SUPPORTSURVEYSUCCESS"];
            $countries = $em->getRepository(Country::class)->findBy(array(
                "deletedAt" => null,
                "active" => 7
            ));
            if($error !== "") {
                return $this->render("registerNext.html.twig", array(
                    "staticText" =>  $this->staticText,
                    "categories" => $this->category,
                    "country" => $this->country,
                    "questions" => $questions,
                    "answers" => $answers,
                    "product" => $product,
                    "warrantyProduct" => $findSerialNumber,
                    "serialNo" => $findSerialNumber->getModelNo(),
                    "lang" => $this->lang,
                    "countries" => $countries,
                    "error" => $error
                ));
            } else {
                $session->set("surveyStatus", true);
                $user->setWarrantyStatus(1);
                // if(!isset($_POST["birthdate"])) {
                //     $user->setBirthDate($_POST["birthdate"]);
                // }

                // if(isset($_POST["gender"])) {
                //     $user->setGender($_POST["gender"]);
                // }

                // if(isset($_POST["education"])) {
                //     $user->setEducation($_POST["education"]);
                // }

                // if(isset($_POST["place"])) {
                //     $user->setPlace($_POST["place"]);
                // }

                $em->flush();

                return $this->render("registerNext.html.twig", array(
                    "staticText" =>  $this->staticText,
                    "categories" => $this->category,
                    "country" => $this->country,
                    "lang" => $this->lang ,
                    "countries" => $countries,
                    "warrantyProduct" => $findSerialNumber,
                    "questions" => $questions,
                    "product" => $product,
                    "serialNo" => $findSerialNumber->getModelNo(),
                    "answers" => $answers,
                    "surveyStatus" => $user->getWarrantyStatus(),
                    "redirectUrl" => $this->redirectDashboard
                ));
            }


            //sleep(5);
            //header("Location:support-registered-products");
        }
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
            "active" => 7
        ));

        
       
        return $this->render("registerNext.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "country" => $this->country,
            "lang" => $this->lang ,
            "countries" => $countries,
            "questions" => $questions,
            "product" => $product,
            "warrantyProduct" => $findSerialNumber,
            // "serialNo" => $findSerialNumber->getModelNo(),
            "answers" => $answers,
            "surveyStatus" => $user->getWarrantyStatus(),
            "redirectUrl" => $this->redirectDashboard
        ));

   }
}
