<?php
namespace App\Controller;

use App\Entity\Category;
use App\Entity\ProductCountry;
use App\Entity\FilterCategory;
use App\Entity\Filter;
use App\Library\CustomController;
use App\Library\FilterSerializer;
use App\Library\ProductSerializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

class FilterController extends CustomController {



        function isExistChild($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
        }



        function getChilds($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT id FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            $all = array();
            foreach($arr as $row){
                $all[] = $row['id'];
                if($this->isExistChild($row['id'])){
                    $res = $this->getChilds($row['id']);
                    foreach( $res as $inRow){
                        $all[] = $inRow;
                    }

                }

            }
            return $all;//
        }








    /**
     * @SWG\Get(
     *   path="/{country}/{language}/filter",
     *   description="Filter",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="eu"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="en"
     *     )
     * )
     * @Route("/filter/{categoryId}", name="getFilter", methods={"GET"})
     */
    public function getFilter($categoryId, FilterSerializer $serializer) {
        $em = $this->getDoctrine()->getManager();

        $productCategory = $em->getRepository(Category::class)->findOneBy(array(
            "link" => $categoryId
        ));





                $subCats = $this->getChilds($productCategory->getId());
                $subCats[] = $productCategory->getId();


                $filterExp = '(0)';
                if(count($subCats) > 0){
                    $filterExp = '('.implode(",",$subCats).')';
                }

                //echo $filterExp;


                                  $conn = $this->getDoctrine()->getConnection();
                                /*  $sql = '



                                               SELECT

                                 fi.id,
                                 fi.name,
                                 fc.name as fname,
                                 fc.id as fcatid,
                                count(ps.product_id) as matched

                                 FROM filters_cats fc

                                 left join filters fi on fi.filtercat = fc.id
                                 left join filters_specids sids on sids.filter = fi.id
                                 left join filters_specvalues svals on svals.parent = fi.id
                                 left join specification s on s.id = sids.spec

                                 left join product_spec ps on
                                              ps.spec_id = sids.spec and
                                              svals.val = ps.refval and
                                              ps.product_id IN
                                      (select p.id from product p where p.category_id IN '.$filterExp.')


                                 where
                                 ps.lang_id = 1 and
                                  svals.val = ps.refval and
                                 fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN '.$filterExp.' group by pcr.filtercat)

                                 group by fi.id

                                                                  order by fc.prio desc

                                ';
*/


                                $sql = '(       SELECT

                                 fi.id,
                                 fi.name,
                                 fc.name as fname,
                                 fc.id as fcatid,
                                 fc.prio

                                 FROM filters_cats fc

                                 left join filters fi on fi.filtercat = fc.id
                                 left join filters_specids sids on sids.filter = fi.id
                                 left join filters_specvalues svals on svals.parent = fi.id
                                 left join specification s on s.id = sids.spec

                                 left join product_spec ps on
                                              ps.spec_id = sids.spec and
                                              svals.val = ps.refval and
                                              ps.product_id IN
                                      (select p.id from product p where p.category_id IN ('.$filterExp.'))


                                 where
                                 ps.lang_id = 1 and
                                  svals.val = ps.refval and
                                 fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN ('.$filterExp.') group by pcr.filtercat)

                                 group by fi.id

                                                                  order by fc.prio desc)

                                                                  union all (

SELECT


                                 fi.id,
                                 fi.name,
                                 fc.name as fname,
                                 fc.id as fcatid,
                                 fc.prio



                                 from filters_featureids fids





                                 left join product_feature pf on pf.feature_id = fids.feature



                                 left join filters fi on fi.id = fids.filter
                                 left join filters_cats fc on fc.id=fi.filtercat
                                 left join feature f on f.id = fids.feature

                                 where
                                      fc.id IN (SELECT pcr.filtercat FROM filters_productcatrel pcr WHERE pcr.cats IN ('.$filterExp.') group by pcr.filtercat)

								group by f.id)

                                 order by prio desc
                                 ';


//SELECT * FROM `product_feature` WHERE `feature_id` = 120

                                  $stmt = $conn->prepare($sql);
                                  $stmt->execute();
                                  $arr = $stmt->fetchAll();


                                  $fcatids = array();
                                  foreach($arr as $row){
                                      $fcatids[$row['fcatid']] = $row['fname'];
                                  }


                                  $filterPool = array();
                                  foreach($arr as $row){
                                      $filterPool[$row['fcatid']]['filters'][] = array('name'=>$row['name'],'matched'=>0,'id'=>$row["id"]);
                                  }

                                  foreach($fcatids as $key=>$val){
                                      $filterPool[$key]['name'] = $val;
                                      //$filterPool[$key]['id'] = $key;
                                  }
                                  $filterPool = array_values($filterPool);

return new JsonResponse($filterPool);


die();




        $subCats = $this->getChilds($productCategory->getId());
        $subCats[] = $productCategory->getId();






        $mergedJson = array();
             foreach($subCats as $row){
                 $productCategory = $em->getRepository(Category::class)->findOneBy(array(
                     "id" => $row
                 ));



                 $filterList = $em->getRepository(FilterCategory::class)->findBy(array(
                     "deletedAt" => null,
                     "category" => $productCategory
                 ), array(
                     "priority" => "ASC"
                 ));
                 $serializer->setLocale($this->country, $this->lang);
                 $json = $serializer->serialize(
                     $filterList,
                     "json"
                 );

                 foreach($json as $inRow){
                     $mergedJson[] = $inRow;
                 }


             }




        /*      $productCategory = $em->getRepository(Category::class)->findOneBy(array(
                "id" => $subCats
            ));


            $filterList = $em->getRepository(FilterCategory::class)->findBy(array(
                "deletedAt" => null,
                "category" => $productCategory
            ), array(
                "priority" => "ASC"
            ));


            $serializer->setLocale($this->country, $this->lang);
            $json = $serializer->serialize(
                $filterList,
                "json"
            );   */

        return new JsonResponse($json);
    }

    /**
     * @Route("/filter/product/{categoryLink}", name="getProductsByFilter", methods={"GET"})
     */
    public function getProductsByFilter($categoryLink, Request $request,
                                        ProductSerializer $serializer) {
        $em = $this->getDoctrine()->getManager();
        $filterList = $request->query->get("filter");

        $featureID = $request->query->get("feature");


        if(strlen($featureID) < 1){
            $featureID = null;
        }




        /*$filters = $em->getRepository(Filter::class)->findBy(array(
            "id" => $filterList
        ));

        $filterArray = array();

        foreach($filters as $filter) {
            if(!array_key_exists($filter->getCategory()->getId(), $filterArray)) {
                $filterArray[$filter->getCategory()->getId()] = array();
            }

            $filterArray[$filter->getCategory()->getId()][] = $filter;
        }

        print_r(get_class_methods($filterArray[1]));*/




        $productList = $em->getRepository(ProductCountry::class)->getProductByFilter(
            $this->country, $this->lang, $categoryLink, $filterList,$featureID);

        $serializer->setLocale($this->country, $this->lang,
            $this->globalCountry, $this->globalLang);

        $json = $serializer->serialize(
            $productList,
            "json"
        );

        return new Response($json);
    }
}
