<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Entity\Migration;
use App\Library\CustomController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends CustomController
{
    /**
     * @Route("/migration", name="migration")
     */
    public function migration()
    {
        $sql = "";
        $globbed = glob(__DIR__."/../../migration/*.sql");
        $em = $this->getDoctrine()->getManager();
        $pdo = $em->getConnection()->getWrappedConnection();
        $pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);

        foreach ($globbed as $file) {
            $explodeArr = explode("/", $file);
            $fileName = $explodeArr[count($explodeArr) - 1];

            $checkFile = $em->getRepository(Migration::class)->findBy(array(
                "fileName" => $fileName
            ));

            if (count($checkFile) == 0) {

                $path = __DIR__."/../../migration/" . $fileName;
                $file = fopen($path, "r");
                $sql = fread($file, filesize($path));
                $preparePdo = $pdo->prepare($sql);
                $preparePdo->execute();
                $preparePdo->closeCursor();
                fclose($file);

                $migration = new Migration();

                $migration->setFileName($fileName);
                $em->persist($migration);
                $em->flush();
            }
        }

        return new Response();
    }

    /**
     * @Route("/deneme", name="deneme")
     * @return Response
     */
    public function deneme()
    {
        return $this->render("base.html.twig");
    }
}