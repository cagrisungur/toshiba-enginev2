<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 2.10.2018
 * Time: 12:03
 */

namespace App\Controller;


use App\Entity\Subscribe;
use App\Library\CustomController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

class SubscribeController extends CustomController
{

    /**
     * @SWG\Post(
     *   path="/{country}/{language}/subscribe",
     *   description="Import to Product Specs Related Excel",
     *   @SWG\Parameter(
     *         name="country",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="eu"
     *     ),
     *   @SWG\Parameter(
     *         name="language",
     *         in="path",
     *         required=true,
     *         type="string",
     *          default="en"
     *     ),
     *   @SWG\Parameter(
     *      name="mail",
     *      in="formData",
     *      required="true",
     *      type="string"
     *       )
     * )
     *
     * @Route("subscribe", name="subscribedMail", methods={"POST"})
     */
    public function subscribedMail(Request $request) {

        $postedMail = $request->request->get("mail");

        $em = $this->getDoctrine()->getManager();
        if (isset($postedMail) && $_POST["mail"] != null) {
            $subscribe = new Subscribe();

            $subscribe->setMail($postedMail);
            $em->persist($subscribe);
            $em->flush();
        }

        return new \Symfony\Component\HttpFoundation\Response();
    }
}