<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 6.07.2018
 * Time: 09:51
 */

namespace App\Controller;


use App\Entity\Product;
use App\Entity\ProductText;
use App\Entity\ProductFile;
use App\Entity\ProductCountry;
use App\Entity\Faq;
use App\Entity\FaqCategory;
use App\Entity\SupportVideos;
use App\Entity\SupportVideoContents;
use App\Library\CustomController;
use App\Library\ProductSerializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Language;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

class NewSupportFilterController extends CustomController {

    /**
     * @Route("support-filter-product", name="allProducts", methods={"GET"})
     */
    public function allProducts(Request $request) {

        if ($request->isXMLHttpRequest() or true) {

        $em = $this->getDoctrine()->getManager();
        $dataArray = array();
        $productArray = array();
        $categoryArray = array();
        $tmpArray  = array();
        $categories = $em->getRepository(Category::class)->findBy(array(
            "active" => 1,
            "deletedAt" => null
        ));
        $newLang = $em->getRepository(Language::class)->find(39);

        foreach($categories as $category) {
            $products = $category->getProduct();



            foreach($products as $product) {
                $checkCountry = $em->getRepository(ProductCountry::class)->findOneBy(array(
                    "product" => $product,
                    "country" => $this->country
                ));
                if($checkCountry) {

                    if($product->getLangText($this->lang)[0]) {
                        $text = $product->getLangText($this->lang)[0]->getName();
                    } else if($product->getLangText($newLang)[0]) {
                        $text = $product->getLangText($newLang)[0]->getName();
                    } else {
                        $text = $product->getName();
                    }
                    $imageList = $em->getRepository(ProductFile::class)->getProductImage($product->getId());

                    $fullPath = "";
                    foreach ($imageList as $image) {
                        if(strpos($image->getFileName(), "front") !== false) {
                            $fullPath = "http://63.35.162.212/uploads/files/files/".$image->getPath();
                        }
                        if($image->getType() != null) {
                            $productArray["images"] = $fullPath;
                        }
                    }

                    $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() ."/". $this->country->getCode() ."/". $this->lang->getCode() ."/product/support/" . $product->getLink();

                    $tmpArray[$category->getCode()][] = array(
                            "id" => $product->getId(),
                            "code" => $product->getName(),
                            "productText" => $text,
                            "link" => $product->getLink(),
                            "frontImage" => $fullPath,
                            "baseUrl" => $baseurl
                    );

                }
            }
        }

        $reqCat = $_GET["cat"];
        if(strlen($reqCat) != 0){

          if(isset($_GET["size"])){
              $reqSize = trim($_GET["size"]);

          if(strlen($reqSize) != 0){
              $tmpArray= $tmpArray[$reqCat];
              $relatedProducts = array();
              foreach($tmpArray as $row){
                if(substr($row["code"],0,2) == $reqSize){
                  $relatedProducts[] = $row;
                }
              }
              return new JsonResponse($relatedProducts);
          }
        }



          $tmpArray= $tmpArray[$reqCat];
          $sizes = array();
          foreach($tmpArray as $row){
            $sizes[] = substr($row["code"],0,2);
          }
          $sizes = array_unique($sizes);
          $sizes = array_values($sizes);
          sort($sizes);

          return new JsonResponse($sizes);
          return die();
        }


        return new JsonResponse($tmpArray);
      } else {
          return new JsonResponse("Not Allowed");
      }
    }

    /**
     * @Route("/support-homev2", name="supportHomev2", methods={"GET", "POST"})
     */
    public function supportHomev2(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $isSearched = 0;
        $searchArray = array();
        $faqArray = array();
        $searchedFaq = array();

        $faqList = $em->getRepository(Faq::class)->findBy(array(
            "deletedAt" => null
        ));

        $highlightedString = "";

        $videoList = $em->getRepository(SupportVideos::class)->findBy(array(
            "deletedAt" => null
        ));

        $videoArray = array();

        foreach($videoList as $video) {
            $videoText = $video->getVideoText($this->lang);

            foreach($videoText as $text) {
                $videoArray[$video->getVideoFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                    "title" => $text->getTitle(),
                    "video" => $text->getVideo(),
                    "id" => $video->getId()
                );
            }
        }

        foreach($faqList as $faq) {
            $faqText = $faq->getText($this->lang);
            foreach($faqText as $text) {
                if($text->getQuestion() == "") {
                    $question = $faq->getName();
                } else {
                    $question = $text->getQuestion();
                }

                $faqArray[$faq->getFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                    "id" => $faq->getId(),
                    "question" => $text->getQuestion(),
                    "answer" => $text->getAnswer(),
                );
            }
        }


        if(isset($_POST["submit"])) {
            $searchFaq = $em->getRepository(Faq::class)->faqSearch($this->lang->getId(),  $_POST["search-input"]);
            $highlightedString = $_POST["search-input"];

            foreach($searchFaq as $faq) {
                $faq = $faq[0];
                $faqText = $faq->getText($this->lang);
                foreach($faqText as $text) {
                    if($text->getQuestion() == "") {
                        $question = $faq->getName();
                    } else {
                        $question = $text->getQuestion();
                    }

                    $answer = str_ireplace($highlightedString, '<span class="color-red-500 text-m-bold">'.$highlightedString.'</span>', $text->getAnswer());
                    $question = str_ireplace($highlightedString, '<span class="color-red-500 text-m-bold">'.$highlightedString.'</span>', $text->getQuestion());

                    $searchedFaq[$faq->getFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                        "id" => $faq->getId(),
                        "question" => $question,
                        "answer" => $answer
                    );
                }
            }

            $isSearched = 1;
            $searchProduct =  $em->getRepository(ProductCountry::class)->supportSearch($this->country->getId(), $_POST["search-input"], $this->lang->getId());

            foreach ($searchProduct as $value) {
                $findSerieProduct = $em->getRepository(Product::class)->find($value["product"]["id"]);
                if($findSerieProduct->getSeries() == null) {
                    $serie =  "Serie doesn't exist-".$value["product"]["name"];
                } else{
                    $serie = $findSerieProduct->getSeries()->getName();
                }
                $array  = array(
                    "id" => $value["product"]["id"],
                    "name" => $value["product"]["name"],
                    "link" => $value["product"]["link"],
                    "serieName" => $serie

                );
                $tmpImg = array();
                foreach ($value["product"]["images"] as $image) {
                    $tmpImg[$image["type"]["code"]][] = array(
                      "name" => $image["name"],
                      "path" => $image["path"]
                    );
                    $array["images"] = $tmpImg;
                }
                $tmpText = array();
                foreach ($value["product"]["text"] as $text) {
                    $tmpText[] = array(
                        "subtitle" => $text["subtitle"],
                        "shortDescription" => $text["shortDescription"],
                        "longDescription" => $text["longDescription"]
                    );
                    $array["text"] = $tmpText;
                }

                $catTmpText = array();
                foreach($value["product"]["category"]["text"] as $catText) {
                    $catTmpText[] = array(
                        "name" => $catText["name"]
                    );
                    $array["categoryText"] = $catTmpText;
                }


            $searchArray[] = $array;
            }
        }

        return $this->render("supportHomeOLD.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "searchedArray" => $searchArray,
            "country" => $this->country,
            "lang" => $this->lang,
            "faqs" => $faqArray,
            "searchedFaq" => $searchedFaq,
            "isSearch" => $isSearched,
            "highString" => $highlightedString,
            "videos" => $videoArray
        ));
    }

    /**
     * @Route("/support-home", name="supportHome", methods={"GET", "POST"})
     */
    public function supportHome(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $isSearched = 0;
        $searchArray = array();
        $faqArray = array();
        $searchedFaq = array();

        $faqList = $em->getRepository(Faq::class)->findBy(array(
            "deletedAt" => null
        ));

        $highlightedString = "";

        $videoList = $em->getRepository(SupportVideos::class)->findBy(array(
            "deletedAt" => null
        ));

        $videoArray = array();

        foreach($videoList as $video) {
            $videoText = $video->getVideoText($this->lang);

            foreach($videoText as $text) {
                $videoArray[$video->getVideoFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                    "title" => $text->getTitle(),
                    "video" => $text->getVideo(),
                    "id" => $video->getId()
                );
            }
        }

        foreach($faqList as $faq) {
            $faqText = $faq->getText($this->lang);
            foreach($faqText as $text) {
                if($text->getQuestion() == "") {
                    $question = $faq->getName();
                } else {
                    $question = $text->getQuestion();
                }

                $faqArray[$faq->getFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                    "id" => $faq->getId(),
                    "question" => $text->getQuestion(),
                    "answer" => $text->getAnswer(),
                );
            }
        }


        if(isset($_POST["submit"])) {
            $searchFaq = $em->getRepository(Faq::class)->faqSearch($this->lang->getId(),  $_POST["search-input"]);
            $highlightedString = $_POST["search-input"];

            foreach($searchFaq as $faq) {
                $faq = $faq[0];
                $faqText = $faq->getText($this->lang);
                foreach($faqText as $text) {
                    if($text->getQuestion() == "") {
                        $question = $faq->getName();
                    } else {
                        $question = $text->getQuestion();
                    }

                    $answer = str_ireplace($highlightedString, '<span class="color-red-500 text-m-bold">'.$highlightedString.'</span>', $text->getAnswer());
                    $question = str_ireplace($highlightedString, '<span class="color-red-500 text-m-bold">'.$highlightedString.'</span>', $text->getQuestion());

                    $searchedFaq[$faq->getFaqCategory()->getFaqCategoryText($this->lang)[0]->getName()][] = array(
                        "id" => $faq->getId(),
                        "question" => $question,
                        "answer" => $answer
                    );
                }
            }

            $isSearched = 1;
            $searchProduct =  $em->getRepository(ProductCountry::class)->supportSearch($this->country->getId(), $_POST["search-input"], $this->lang->getId());

            foreach ($searchProduct as $value) {
                $findSerieProduct = $em->getRepository(Product::class)->find($value["product"]["id"]);
                if($findSerieProduct->getSeries() == null) {
                    $serie =  "Serie doesn't exist-".$value["product"]["name"];
                } else{
                    $serie = $findSerieProduct->getSeries()->getName();
                }
                $array  = array(
                    "id" => $value["product"]["id"],
                    "name" => $value["product"]["name"],
                    "link" => $value["product"]["link"],
                    "serieName" => $serie

                );
                $tmpImg = array();
                foreach ($value["product"]["images"] as $image) {
                    $tmpImg[$image["type"]["code"]][] = array(
                      "name" => $image["name"],
                      "path" => $image["path"]
                    );
                    $array["images"] = $tmpImg;
                }
                $tmpText = array();
                foreach ($value["product"]["text"] as $text) {
                    $tmpText[] = array(
                        "subtitle" => $text["subtitle"],
                        "shortDescription" => $text["shortDescription"],
                        "longDescription" => $text["longDescription"]
                    );
                    $array["text"] = $tmpText;
                }

                $catTmpText = array();
                foreach($value["product"]["category"]["text"] as $catText) {
                    $catTmpText[] = array(
                        "name" => $catText["name"]
                    );
                    $array["categoryText"] = $catTmpText;
                }


            $searchArray[] = $array;
            }
        }
        //var_dump($this->category);exit;
        //print_r($searchArray);exit;
        return $this->render("supportHome.html.twig", array(
            "staticText" =>  $this->staticText,
            "categories" => $this->category,
            "searchedArray" => $searchArray,
            "country" => $this->country,
            "lang" => $this->lang,
            "faqs" => $faqArray,
            "searchedFaq" => $searchedFaq,
            "isSearch" => $isSearched,
            "highString" => $highlightedString,
            "videos" => $videoArray
        ));
    }

}
