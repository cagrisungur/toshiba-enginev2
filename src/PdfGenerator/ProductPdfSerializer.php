<?php

namespace App\PdfGenerator;

use App\Entity\ProductCountry;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\Serializer\SerializerInterface;

class ProductPdfSerializer implements SerializerInterface
{
    private $country;
    private $lang;

    public function serialize($data, $format, array $context = array())
    {
        $dataArray = [];

        /**
         * @var ProductCountry $d
         */
        foreach ($data as $d)
        {
            $product = $d->getProduct();
            $productArray = $this->serializeProperty($product);
            $productArray["spec"] = array();
            $specList = $product->getSpec($this->lang);
            $productArray["sizeImage"] = array();

            $pSeries = $product->getProductSize();
            foreach ($pSeries as $series) {
                $seriesImage = $series->getProductSizeImage();
                foreach ($seriesImage as $img) {
                    $productArray["sizeImage"][$img->getType()->getCode()] = $img->getPath();
                }
            }

            $productArray["controller"] = array();
            $productController = $product->getProductController();
            foreach ($productController as $controller) {
                $controllerImage = $controller->getController()->getImage();
                foreach ($controllerImage as $img) {
                    $productArray["controller"][$img->getType()->getCode()] = $img->getPath();
                }
            }

            $productArray["connection"] = array();
            $productController = $product->getProductConnection();
            foreach ($productController as $controller) {
                $controllerImage = $controller->getConnection()->getImage();
                foreach ($controllerImage as $img) {
                    $productArray["connection"][$img->getType()->getCode()] = $img->getPath();
                }
            }

            $specArray = array();
            foreach ($specList as $spec) {
                $category = $spec->getSpec()->getCategory();

                if( $category->getText($this->country,$this->lang)[0] == null) {
                    $categoryName = null;
                }else {
                    $categoryName = $category->getText($this->country,$this->lang)[0]->getText();
                }

                if(empty($specArray[$category->getId()])) {
                    $specArray[$category->getId()] = array(
                        "id" => $category->getId(),
                        "name" => $categoryName
                    );
                }
                // $specText = $spec->getSpec()->getName();

                if( $spec->getSpec()->getText($this->country, $this->lang)[0] == null) {
                    $specText = $spec->getSpec()->getName();
                }else {
                    $specText = $spec->getSpec()->getText($this->country, $this->lang)[0]->getName();
                }

                $specArray[$category->getId()]["children"][] = array(
                    "name" => $specText,
                    "value" => $spec->getValue()
                );
            }

            foreach ($specArray as $spec) {
                $productArray["spec"][] = $spec;
            }

            $productArray ["images"] = array();
            $imageList = $product->getImages();

            foreach ($imageList as $image) {
                if($image->getType() != null) {
                    $productArray["images"][$image->getType()->getCode()][] = array(
                        "name" => $image->getName(),
                        "path" => $image->getPath()
                    );
                }
            }
            $productArray["feature"] = array();
            $featureList = $product->getFeature();
            foreach ($featureList as $feature) {
                if($feature->getFeature() != null && $feature->getFeature()->getText($this->country, $this->lang)[0] != null){
                    $tmp = array(
                        "name" => $feature->getFeature()->getText($this->country, $this->lang)[0]->getName(),
                        "shortDescription" => $feature->getFeature()->getText($this->country, $this->lang)[0]->getShortDescription(),
                        "longDescription" => $feature->getFeature()->getText($this->country, $this->lang)[0]->getLongDescription(),
                        "typeName" => $feature->getType()->getName(),
                    );
                    $imageList = $feature->getFeature()->getImage();
                    $tmp["images"] = array();
                    foreach ($imageList as $image) {
                        $tmp["images"][$image->getType()->getCode()] = $image->getPath();

                    }
                    $array[$feature->getType()->getCode()][] = $tmp;
                    $productArray["feature"] = $array;
                }
            }

            $productArray["file"] = array();
            $fileList = $product->getFile();

            foreach ($fileList as $file) {
                if($file->getFile() != null && $file->getFile()->getType() != null) {
                    $productArray["file"][] = array(
                        "name" => $file->getFile()->getName(),
                        "path" => $file->getFile()->getPath(),
                        "typeCode" => $file->getFile()->getType()->getCode(),
                        "typeName" => $file->getFile()->getType()->getName()
                    );
                }
            }

            $fileList = $product->getCategory()->getFile();

            foreach ($fileList as $file) {
                if($file->getFile() != null && $file->getFile()->getType() != null) {

                    $productArray["file"][] = array(
                        "name" => $file->getFile()->getName(),
                        "path" => $file->getFile()->getPath(),
                        "typeCode" => $file->getFile()->getType()->getCode(),
                        "typeName" => $file->getFile()->getType()->getName()
                    );
                }
            }

            if($product->getProductBuyCountry($this->country)) {
                $productArray["buyNowLink"] = $product->getProductBuyCountry($this->country)->getLink();
            }else {
                $productArray["buyNowLink"] = null;
            }


            $productArray["related"] = array();
            $relatedList = $product->getRelated();
            foreach ($relatedList as $related) {

                if($related->getRelated() != null) {
                    if ($related->getRelated()->getFile()[0] == null) {
                        $relatedFile = "";
                    } else {
                        $relatedFile = $related->getRelated()->getFile()[0]->getFile()->getPath();
                    }
                    $productArray["related"][] = array(
                        "name" => $related->getRelated()->getName(),
                        "code" => $related->getRelated()->getCode(),
                        "link" => $related->getRelated()->getLink(),
                        "file" => $relatedFile,
                        "keywords" => $related->getRelated()->getKeywords()
                    );
                }
            }

            $productArray["text"] = array();
            $textList = $product->getText();

            foreach ($textList as $text) {
                $productArray["text"][] = array(
                    "title" => $text->getSubtitle(),
                    "shortDescription" => $text->getShortDescription(),
                    "longDescription" => $text->getLongDescription(),
                    "link" => $text->getLink(),
                    "keywords" => $text->getKeywords()
                );
            }

            $productArray["highlightedText"] = array();
            $highlightedTextList = $product->getHighlightedText();

            foreach ($highlightedTextList as $highlightedText) {
                if($highlightedText->getTranslate($this->country, $this->lang)[0] != null) {
                    foreach ($highlightedText->getTranslate($this->country, $this->lang) as $trams) {
                        $productArray["highlightedText"][] = array(
                            "text" => $trams->getText()
                        );
                    }
                }
            }

            $productArray["category"] = array();
            $category = $product->getCategory();

            $productArray["category"][] = array(
                "id" => $category->getId(),
                "name" => $category->getName(),
                "link" => $category->getLink(),
                "keywords" => $category->getKeywords(),
                "active" => $category->getActive(),
                "parentId" => $category->getParentId()
            );

            $productArray["series"] = array();
            $series = $product->getSeries();

            if($series != null) {
                $array = array(
                    "name" => $series->getName(),
                    "code" => $series->getCode()
                );
                $array["products"] = array();
                if($series->getProduct($product->getId()) != null) {
                    $productList = $series->getProduct($product->getId());
                    foreach ($productList as $pro) {
                        $array["products"][] = array(
                            "id" => $pro->getId(),
                            "size" => $pro->getSize(),
                            "link" => $pro->getLink()
                        );
                    }
                }

                $productArray["series"] = $array;
            }

            $dataArray[] = $productArray;
        }
        return json_encode($dataArray);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionClass(ClassUtils::getClass($data));
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if($functionName != "getSpec" && gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }
        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}