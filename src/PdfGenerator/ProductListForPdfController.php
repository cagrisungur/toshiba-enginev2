<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 24.01.2019
 * Time: 09:57
 */

namespace App\PdfGenerator;


use App\Entity\Product;
use App\Entity\ProductCountry;
use App\Library\CustomController;
use App\PdfGenerator\ProductPdfSerializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductListForPdfController extends CustomController
{
    /**
     * @Route("/pdf/product/{id}", name="getProductPdf", methods={"GET"})
     * @param \App\PdfGenerator\ProductPdfSerializer $serializer
     * @param $id
     * @return Response
     */
    public function getProductPdf($id, \App\PdfGenerator\ProductPdfSerializer $serializer)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var ProductCountry[] $product
         */
        $product = $em->getRepository(ProductCountry::class)->findBy(array(
                "country" => $this->country,
                "product" => $id,
                "active"=>1,
                "deletedAt"=>null
            )
        );



        $serializer->setLocale($this->country, $this->lang);
        $json = $serializer->serialize(
            $product,
            "json"
        );

        $snappyPdf = $this->get("knp_snappy.pdf");
        $fileName =  json_decode($json)[0]->name;


        $html =  $this->renderView("pdfGenerator.html.twig", array(
            "product" => json_decode($json)[0],
            'rootDir' => $this->get('kernel')->getRootDir().'/..',
        ));

        //  return $html;

        return new Response($snappyPdf->getOutputFromHtml($html, array(
                'orientation'=>'Landscape',
                'enable-javascript' => true,
                'margin-top'    => 0,
                'margin-right'  => 0,
                'margin-bottom' => 0,
                'margin-left'   => 0,
            )
        ),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' =>'inline; filename="' .$fileName.'.pdf"'
            ));
    }
}