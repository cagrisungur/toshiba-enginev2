<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FormMessage
 *
 * @ORM\Table(name="form_message")
 * @ORM\Entity
 */
class FormMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=128, nullable=false)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=128, nullable=false)
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=128, nullable=false)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=128, nullable=false)
     */
    private $subject;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="send_date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $sendDate;

    /**
     * @var FormMessageType
     *
     * @ORM\ManyToOne(targetEntity="FormMessageType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="message_type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return \DateTime|null
     */
    public function getSendDate(): ?\DateTime
    {
        return $this->sendDate;
    }

    /**
     * @param \DateTime|null $sendDate
     */
    public function setSendDate(?\DateTime $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    /**
     * @return FormMessageType
     */
    public function getType(): FormMessageType
    {
        return $this->type;
    }

    /**
     * @param FormMessageType $type
     */
    public function setType(FormMessageType $type): void
    {
        $this->type = $type;
    }

}