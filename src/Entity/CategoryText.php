<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryText
 * @ORM\Table(name="category_text", indexes={@ORM\Index(name="category_id", columns={"category_id"}), @ORM\Index(name="country_id", columns={"country_id"}), @ORM\Index(name="lang_id", columns={"lang_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CategoryTextRepository")
 */
class CategoryText
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sub_description", type="text", nullable=true)
     */
    private $subDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="button", type="text", nullable=true)
     */
    private $button;

    /**
     * @var string|null
     *
     * @ORM\Column(name="button_link", type="text", nullable=true)
     */
    private $buttonLink;

    /**
     * @var string|null
     *
     * @ORM\Column(name="short_description", type="text", length=65535, nullable=true)
     */
    private $shortDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="long_description", type="text", length=65535, nullable=true)
     */
    private $longDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=64, nullable=false)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="text", length=65535, nullable=true)
     */
    private $keywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v1", type="text", nullable=true)
     */
    private $v1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v2", type="text", nullable=true)
     */
    private $v2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v3", type="text", nullable=true)
     */
    private $v3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Category
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    ///**
     //* @var Country
    // *
     //* @ORM\Id
     //* @ORM\GeneratedValue(strategy="NONE")
     //* @ORM\OneToOne(targetEntity="Country")
     //* @ORM\JoinColumns({
     //*   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     //* })
     //*/
    //private $country;

    /**
     * @var Language
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getV1(): ?string
    {
        return $this->v1;
    }

    public function setV1(string $v1): self
    {
        $this->v1 = $v1;

        return $this;
    }

    public function getV2(): ?string
    {
        return $this->v2;
    }

    public function setV2(string $v2): self
    {
        $this->v2 = $v2;

        return $this;
    }

    public function getV3(): ?string
    {
        return $this->v3;
    }

    public function setV3(string $v3): self
    {
        $this->v3 = $v3;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getLongDescription(): ?string
    {
        return $this->longDescription;
    }

    public function setLongDescription(?string $longDescription): self
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }


    public function getLang(): Language
    {
        return $this->lang;
    }

    public function setLang(Language $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param null|string $subtitle
     */
    public function setSubtitle(?string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return null|string
     */
    public function getSubDescription(): ?string
    {
        return $this->subDescription;
    }

    /**
     * @param null|string $subDescription
     */
    public function setSubDescription(?string $subDescription): void
    {
        $this->subDescription = $subDescription;
    }

    /**
     * @return null|string
     */
    public function getButton(): ?string
    {
        return $this->button;
    }

    /**
     * @param null|string $button
     */
    public function setButton(?string $button): void
    {
        $this->button = $button;
    }

    /**
     * @return null|string
     */
    public function getButtonLink(): ?string
    {
        return $this->buttonLink;
    }

    /**
     * @param null|string $buttonLink
     */
    public function setButtonLink(?string $buttonLink): void
    {
        $this->buttonLink = $buttonLink;
    }
}
