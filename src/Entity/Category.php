<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;


/**
 * Category
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="is_featured", type="integer", nullable=true)
     */
    private $isFeatured = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=64, nullable=false)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="text", length=65535, nullable=true)
     */
    private $keywords;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '0';

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="id")
     * @ORM\JoinTable(name="category",
     *      joinColumns={@ORM\JoinColumn(name="id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id", referencedColumnName="parent_id")}
     *      )
     */
    private $child;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CategoryFile", mappedBy="category")
     */
    private $file;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CategoryFeature", mappedBy="category")
     */
    private $feature;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CategoryImage", mappedBy="category")
     */
    private $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CategorySpec", mappedBy="category")
     */
    private $spec;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CategoryText", mappedBy="category")
     */
    private $text;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SliderCategory", mappedBy="category")
     */
    private $slider;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->feature = new ArrayCollection();
        $this->file = new ArrayCollection();
        $this->image = new ArrayCollection();
        $this->spec = new ArrayCollection();
        $this->text = new ArrayCollection();
        $this->product = new ArrayCollection();
        $this->child = new ArrayCollection();
    }


    /**
     * @return ArrayCollection
     */
    public function getChild()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt",null))
            ->andWhere(Criteria::expr()->eq("active",1))
            ->orderBy(array("priority" => Criteria::ASC));
        return $this->child->matching($criteria);
    }

    /**
     * @param ArrayCollection $child
     */
    public function setChild($child): void
    {
        $this->child = $child;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsFeatured(): ?int
    {
        return $this->isFeatured;
    }

    public function setIsFeatured(int $isFeatured): self
    {
        $this->isFeatured = $isFeatured;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(?int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|CategoryFile[]
     */
    public function getFile(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));

        return $this->file->matching($criteria);
    }

    public function addFile(CategoryFile $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
        }

        return $this;
    }

    public function removeFile(CategoryFile $file): self
    {
        if ($this->file->contains($file)) {
            $this->file->removeElement($file);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
     public function getProduct(): Collection
     {

         $criteria = Criteria::create()
             ->where(Criteria::expr()->eq('deletedAt', null))
             ->andWhere(Criteria::expr()->eq('active', 1))
             ->orderBy(array("priority" => Criteria::ASC));

         return $this->product->matching($criteria);
     }


     public function getProductAllChilds(): Collection
     {
        //$test = $this->getDoctrine();
         $criteria = Criteria::create()
             ->where(Criteria::expr()->eq('deletedAt', null))
             ->andWhere(Criteria::expr()->eq('active', 1))
             ->orderBy(array("priority" => Criteria::ASC));

         return $this->product->matching($criteria);
     }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }

    /**
     * @return Collection|CategoryFeature[]
     */
    public function getFeature(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));
        return $this->feature->matching($criteria);
    }

    public function addFeature(CategoryFeature $feature): self
    {
        if (!$this->feature->contains($feature)) {
            $this->feature[] = $feature;
        }

        return $this;
    }

    public function removeFeature(CategoryFeature $feature): self
    {
        if ($this->feature->contains($feature)) {
            $this->feature->removeElement($feature);
        }

        return $this;
    }

    /**
     * @return Collection|CategoryImage[]
     */
    public function getImage(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('active', 1))
            ->andWhere(Criteria::expr()->eq('deletedAt', null))
            ->orderBy(array("priority"=>Criteria::ASC));

        return $this->image->matching($criteria);
    }

    public function addImage(CategoryImage $categoryImage)
    {
        if(!$this->image->contains($categoryImage)){
            $this->image[] = $categoryImage;
        }
        return $this;
    }

    public function removeImage(CategoryImage $categoryImage)
    {
        if($this->image->contains($categoryImage)){
            $this->image->removeElement($categoryImage);
        }
        return $this;
    }

    /**
     * @return Collection|CategorySpec[]
     */
    public function getSpec(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));

        return $this->spec->matching($criteria);
    }

    public function addSpec(CategorySpec $categorySpec)
    {
        if(!$this->spec->contains($categorySpec)){
            $this->spec[] = $categorySpec;
        }

        return $this;
    }

    public function removeSpec(CategorySpec $categorySpec)
    {
        if($this->spec->contains($categorySpec)){
            $this->spec->removeElement($categorySpec);
        }

        return $this;
    }


    /**
     * @return Collection|CategoryText[]
     */
    public function getText(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));

        return $this->text->matching($criteria);
    }

    /**
     * @param Language $lang
     * @return Collection|CategoryText[] |null
     */
    public function getCatText($lang): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("lang", $lang))
            ->andWhere(Criteria::expr()->eq("deletedAt",NULL));

        if($criteria == null){
            return null;
        }
        return $this->text->matching($criteria);
    }

    public function addText(CategoryText $categoryText)
    {
        if(!$this->text->contains($categoryText)){
            $this->text[] = $categoryText;
        }

        return $this;
    }

    public function removeText(CategoryText $categoryText)
    {
        if($this->text->contains($categoryText)){
            $this->text->removeElement($categoryText);
        }

        return $this;
    }

    public function setText(ArrayCollection $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSlider(): Collection
    {
        return $this->slider;
    }

    /**
     * @param Collection $slider
     */
    public function setSlider(Collection $slider): void
    {
        $this->slider = $slider;
    }

}
