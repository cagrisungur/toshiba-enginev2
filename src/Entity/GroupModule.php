<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupModule
 * @ORM\Table(name="group_module")
 * @ORM\Entity
 */
class GroupModule
{
    /**
     * @var UserGroup
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="UserGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @var Module
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */

    private $module;

    /**
     * @return UserGroup
     */
    public function getGroup(): UserGroup
    {
        return $this->group;
    }

    /**
     * @param UserGroup $group
     */
    public function setGroup(UserGroup $group): void
    {
        $this->group = $group;
    }

    /**
     * @return Module
     */
    public function getModule(): Module
    {
        return $this->module;
    }

    /**
     * @param Module $module
     */
    public function setModule(Module $module): void
    {
        $this->module = $module;
    }



}