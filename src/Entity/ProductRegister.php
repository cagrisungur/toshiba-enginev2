<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Class ProductRegister
 * @ORM\Table(name="product_register")
 * @ORM\Entity
 */
class ProductRegister {
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

	/**
     * @var Product
     *
     * @ORM\OneToOne(targetEntity="Product", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
	private $product;
	
	/**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", nullable=false)
     */
	private $serialNumber;
	
	/**
     *
     * @ORM\Column(name="date_of_purchase", type="date", nullable=false)
     */
	private $dateOfPurchase;
	
	/**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="retailer_country_id", referencedColumnName="id")
     */
	private $retailerCountry;
	
	/**
     * @var string
     *
     * @ORM\Column(name="retailer_name", type="string", nullable=false)
     */
	private $retailerName;

	/**
     * @var string
     *
     * @ORM\Column(name="retailer_address", type="string", nullable=false)
     */
	private $retailerAddress;

	/**
     * @var string
     *
     * @ORM\Column(name="retailer_address_2", type="string", nullable=false)
     */
	private $retailerAddress2;

	/**
     * @var string
     *
     * @ORM\Column(name="retailer_city", type="string", nullable=false)
     */
	private $retailerCity;

	/**
     * @var string
     *
     * @ORM\Column(name="retailer_postcode", type="string", nullable=false)
     */
	private $retailerPostcode;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="title", type="string", nullable=false)
	 */
	private $title;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="first_name", type="string", nullable=false)
	 */
	private $firstName;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="last_name", type="string", nullable=false)
	 */
	private $lastName;

	/**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
	private $country;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="address", type="string", nullable=false)
	 */
	private $address;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="address_2", type="string", nullable=false)
	 */
	private $address2;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="city", type="string", nullable=false)
	 */
	private $city;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="postcode", type="string", nullable=false)
	 */
	private $postcode;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="phone", type="string", nullable=false)
	 */
	private $phone;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="email", type="string", nullable=false)
	 */
	private $email;

	/**
	 * @var bool
	 * 
	 * @ORM\Column(name="contact", type="boolean", nullable=false)
	 */
	private $contact;

	/**
	 * @var bool
	 * 
	 * @ORM\Column(name="share", type="boolean", nullable=false)
	 */
	private $share;

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

	/**
	 * Get the value of product
	 *
	 * @return  Product
	 */ 
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * Set the value of product
	 *
	 * @param  Product  $product
	 *
	 * @return  self
	 */ 
	public function setProduct(Product $product)
	{
		$this->product = $product;

		return $this;
	}

	/**
	 * Get the value of serialNumber
	 *
	 * @return  string
	 */ 
	public function getSerialNumber()
	{
		return $this->serialNumber;
	}

	/**
	 * Set the value of serialNumber
	 *
	 * @param  string  $serialNumber
	 *
	 * @return  self
	 */ 
	public function setSerialNumber(string $serialNumber)
	{
		$this->serialNumber = $serialNumber;

		return $this;
	}

	/**
	 * Get the value of dateOfPurchase
	 */ 
	public function getDateOfPurchase()
	{
		return $this->dateOfPurchase;
	}

	/**
	 * Set the value of dateOfPurchase
	 *
	 * @return  self
	 */ 
	public function setDateOfPurchase($dateOfPurchase)
	{
		$this->dateOfPurchase = $dateOfPurchase;

		return $this;
	}

	/**
	 * Get the value of retailerCountry
	 */ 
	public function getRetailerCountry()
	{
		return $this->retailerCountry;
	}

	/**
	 * Set the value of retailerCountry
	 *
	 * @return  self
	 */ 
	public function setRetailerCountry($retailerCountry)
	{
		$this->retailerCountry = $retailerCountry;

		return $this;
	}

	/**
	 * Get the value of retailerName
	 *
	 * @return  string
	 */ 
	public function getRetailerName()
	{
		return $this->retailerName;
	}

	/**
	 * Set the value of retailerName
	 *
	 * @param  string  $retailerName
	 *
	 * @return  self
	 */ 
	public function setRetailerName(string $retailerName)
	{
		$this->retailerName = $retailerName;

		return $this;
	}

	/**
	 * Get the value of retailerAddress
	 *
	 * @return  string
	 */ 
	public function getRetailerAddress()
	{
		return $this->retailerAddress;
	}

	/**
	 * Set the value of retailerAddress
	 *
	 * @param  string  $retailerAddress
	 *
	 * @return  self
	 */ 
	public function setRetailerAddress(string $retailerAddress)
	{
		$this->retailerAddress = $retailerAddress;

		return $this;
	}

	/**
	 * Get the value of retailerAddress2
	 *
	 * @return  string
	 */ 
	public function getRetailerAddress2()
	{
		return $this->retailerAddress2;
	}

	/**
	 * Set the value of retailerAddress2
	 *
	 * @param  string  $retailerAddress2
	 *
	 * @return  self
	 */ 
	public function setRetailerAddress2(string $retailerAddress2)
	{
		$this->retailerAddress2 = $retailerAddress2;

		return $this;
	}

	/**
	 * Get the value of retailerCity
	 *
	 * @return  string
	 */ 
	public function getRetailerCity()
	{
		return $this->retailerCity;
	}

	/**
	 * Set the value of retailerCity
	 *
	 * @param  string  $retailerCity
	 *
	 * @return  self
	 */ 
	public function setRetailerCity(string $retailerCity)
	{
		$this->retailerCity = $retailerCity;

		return $this;
	}

	/**
	 * Get the value of retailerPostcode
	 *
	 * @return  string
	 */ 
	public function getRetailerPostcode()
	{
		return $this->retailerPostcode;
	}

	/**
	 * Set the value of retailerPostcode
	 *
	 * @param  string  $retailerPostcode
	 *
	 * @return  self
	 */ 
	public function setRetailerPostcode(string $retailerPostcode)
	{
		$this->retailerPostcode = $retailerPostcode;

		return $this;
	}

	/**
	 * Get the value of title
	 *
	 * @return  string
	 */ 
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set the value of title
	 *
	 * @param  string  $title
	 *
	 * @return  self
	 */ 
	public function setTitle(string $title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get the value of firstName
	 *
	 * @return  string
	 */ 
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set the value of firstName
	 *
	 * @param  string  $firstName
	 *
	 * @return  self
	 */ 
	public function setFirstName(string $firstName)
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * Get the value of lastName
	 *
	 * @return  string
	 */ 
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set the value of lastName
	 *
	 * @param  string  $lastName
	 *
	 * @return  self
	 */ 
	public function setLastName(string $lastName)
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * Get the value of country
	 */ 
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * Set the value of country
	 *
	 * @return  self
	 */ 
	public function setCountry($country)
	{
		$this->country = $country;

		return $this;
	}

	/**
	 * Get the value of address
	 *
	 * @return  string
	 */ 
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Set the value of address
	 *
	 * @param  string  $address
	 *
	 * @return  self
	 */ 
	public function setAddress(string $address)
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * Get the value of address2
	 *
	 * @return  string
	 */ 
	public function getAddress2()
	{
		return $this->address2;
	}

	/**
	 * Set the value of address2
	 *
	 * @param  string  $address2
	 *
	 * @return  self
	 */ 
	public function setAddress2(string $address2)
	{
		$this->address2 = $address2;

		return $this;
	}

	/**
	 * Get the value of city
	 *
	 * @return  string
	 */ 
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set the value of city
	 *
	 * @param  string  $city
	 *
	 * @return  self
	 */ 
	public function setCity(string $city)
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * Get the value of postcode
	 *
	 * @return  string
	 */ 
	public function getPostcode()
	{
		return $this->postcode;
	}

	/**
	 * Set the value of postcode
	 *
	 * @param  string  $postcode
	 *
	 * @return  self
	 */ 
	public function setPostcode(string $postcode)
	{
		$this->postcode = $postcode;

		return $this;
	}

	/**
	 * Get the value of phone
	 *
	 * @return  string
	 */ 
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Set the value of phone
	 *
	 * @param  string  $phone
	 *
	 * @return  self
	 */ 
	public function setPhone(string $phone)
	{
		$this->phone = $phone;

		return $this;
	}

	/**
	 * Get the value of email
	 *
	 * @return  string
	 */ 
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @param  string  $email
	 *
	 * @return  self
	 */ 
	public function setEmail(string $email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get the value of contact
	 *
	 * @return  bool
	 */ 
	public function getContact()
	{
		return $this->contact;
	}

	/**
	 * Set the value of contact
	 *
	 * @param  bool  $contact
	 *
	 * @return  self
	 */ 
	public function setContact(bool $contact)
	{
		$this->contact = $contact;

		return $this;
	}

	/**
	 * Get the value of share
	 *
	 * @return  bool
	 */ 
	public function getShare()
	{
		return $this->share;
	}

	/**
	 * Set the value of share
	 *
	 * @param  bool  $share
	 *
	 * @return  self
	 */ 
	public function setShare(bool $share)
	{
		$this->share = $share;

		return $this;
	}
}

?>