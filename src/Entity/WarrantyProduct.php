<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserLang
 * @ORM\Table(name="warranty_product")
 * @ORM\Entity
 */
class WarrantyProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var WarrantyUser
     *
     * @ORM\ManyToOne(targetEntity="WarrantyUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_no", type="string", length=20, nullable=false)
     */
    private $serialNo;

    /**
     * @var string
     *
     * @ORM\Column(name="model_number", type="string", length=32, nullable=false)
     */
    private $modelNo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sales_date", type="datetime", nullable=false)
     */
    private $salesDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="call_center_date", type="datetime", nullable=true)
     */
    private $callCenterDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dealer", type="string", length=32, nullable=false)
     */
    private $dealer;

    /**
     * @var string
     *
     * @ORM\Column(name="sales_doc", type="string", nullable=true)
     */
    private $salesDoc;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer", length=11, nullable=false)
     */
    private $status = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of user
     *
     * @return  WarrantyUser
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param  WarrantyUser  $user
     *
     * @return  self
     */ 
    public function setUser(WarrantyUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of serialNo
     *
     * @return  string
     */ 
    public function getSerialNo()
    {
        return $this->serialNo;
    }

    /**
     * Set the value of serialNo
     *
     * @param  string  $serialNo
     *
     * @return  self
     */ 
    public function setSerialNo(string $serialNo)
    {
        $this->serialNo = $serialNo;

        return $this;
    }

    /**
     * Get the value of modelNo
     *
     * @return  string
     */ 
    public function getModelNo()
    {
        return $this->modelNo;
    }

    /**
     * Set the value of modelNo
     *
     * @param  string  $modelNo
     *
     * @return  self
     */ 
    public function setModelNo(string $modelNo)
    {
        $this->modelNo = $modelNo;

        return $this;
    }

    /**
     * Get the value of salesDate
     *
     * @return  \DateTime
     */ 
    public function getSalesDate()
    {
        return $this->salesDate;
    }

    /**
     * Set the value of salesDate
     *
     * @param  \DateTime  $salesDate
     *
     * @return  self
     */ 
    public function setSalesDate(\DateTime $salesDate)
    {
        $this->salesDate = $salesDate;

        return $this;
    }

    /**
     * Get the value of status
     *
     * @return  string
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @param  string  $status
     *
     * @return  self
     */ 
    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of createdAt
     *
     * @return  \DateTime
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @param  \DateTime  $createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of deletedAt
     *
     * @return  \DateTime|null
     */ 
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set the value of deletedAt
     *
     * @param  \DateTime|null  $deletedAt
     *
     * @return  self
     */ 
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get the value of dealer
     *
     * @return  string
     */ 
    public function getDealer()
    {
        return $this->dealer;
    }

    /**
     * Set the value of dealer
     *
     * @param  string  $dealer
     *
     * @return  self
     */ 
    public function setDealer(string $dealer)
    {
        $this->dealer = $dealer;

        return $this;
    }

    /**
     * Get the value of salesDoc
     *
     * @return  string
     */ 
    public function getSalesDoc()
    {
        return $this->salesDoc;
    }

    /**
     * Set the value of salesDoc
     *
     * @param  string  $salesDoc
     *
     * @return  self
     */ 
    public function setSalesDoc(string $salesDoc)
    {
        $this->salesDoc = $salesDoc;

        return $this;
    }
}