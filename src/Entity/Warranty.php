<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Warranty
 * @ORM\Table(name="warranty")
 * @ORM\Entity
 */
class Warranty
{
    /**
     * @var Language
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="warranty", type="string", nullable=false)
     */
    private $warranty;
    

    /**
     * Get the value of lang
     *
     * @return  Language
     */ 
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set the value of lang
     *
     * @param  Language  $lang
     *
     * @return  self
     */ 
    public function setLang(Language $lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get the value of warranty
     *
     * @return  string
     */ 
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * Set the value of warranty
     *
     * @param  string  $warranty
     *
     * @return  self
     */ 
    public function setWarranty(string $warranty)
    {
        $this->warranty = $warranty;

        return $this;
    }
}    