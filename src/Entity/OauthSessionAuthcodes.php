<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OauthSessionAuthcodes
 *
 * @ORM\Table(name="oauth_session_authcodes", indexes={@ORM\Index(name="oauth_session_authcodes_session_id_index", columns={"session_id"})})
 * @ORM\Entity
 */
class OauthSessionAuthcodes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="auth_code", type="string", length=40, nullable=false)
     */
    private $authCode;

    /**
     * @var int
     *
     * @ORM\Column(name="auth_code_expires", type="integer", nullable=false)
     */
    private $authCodeExpires;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function setSessionId(int $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getAuthCode(): ?string
    {
        return $this->authCode;
    }

    public function setAuthCode(string $authCode): self
    {
        $this->authCode = $authCode;

        return $this;
    }

    public function getAuthCodeExpires(): ?int
    {
        return $this->authCodeExpires;
    }

    public function setAuthCodeExpires(int $authCodeExpires): self
    {
        $this->authCodeExpires = $authCodeExpires;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


}
