<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * pfeature
 * @ORM\Table(name="pfeature")
 * @ORM\Entity()
 */
class NewProductFeature
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path", type="text", length=65535, nullable=true)
     */
    private $path;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;


    /**
     * @var string|null
     *
     * @ORM\Column(name="des", type="text", length=65535, nullable=true)
     */
    private $des;

    /**
     * @var NewProductFeatureTranslate
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="NewProductFeatureTranslate", mappedBy="pFeature" )
     */
    private $productFeatureTranslate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    public function __construct()
    {
        $this->productFeatureTranslate = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getDes(): ?string
    {
        return $this->des;
    }

    /**
     * @return null|string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getProductFeatureTranslate($lang)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("lang", $lang));
        if($criteria == null){
            return null;
        }
    
        return $this->productFeatureTranslate->matching($criteria);
    }

    /**
     * @param mixed $productFeatureTranslate
     */
    public function setProductFeatureTranslate($productFeatureTranslate): void
    {
        $this->productFeatureTranslate = $productFeatureTranslate;
    }
}
