<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqText
 * @ORM\Table(name="faq_category_content")
 * @ORM\Entity
 */
class FaqCategoryContent
{
    

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var Language
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var Faq
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="FaqCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $faqCategory;


    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

   
    /**
     * @return Language
     */
    public function getLang(): ?Language
    {
        return $this->lang;
    }

    /**
     * @param Language $lang
     */
    public function setLang(?Language $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return FaqCategory
     */
    public function getFaqCategory(): ?FaqCategory
    {
        return $this->faqCategory;
    }

    /**
     * @param FaqCategory $faqCategory
     */
    public function setFaqCategory(?FaqCategory $faqCategory): void
    {
        $this->faqCategory = $faqCategory;
    }

}