<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Feature
 * @ORM\Table(name="feature")
 * @ORM\Entity(repositoryClass="App\Repository\FeatureRepository")
 */
class Feature
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="FeatureText", mappedBy="feature")
     */
    private $text;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="FeatureImage", mappedBy="feature")
     */
    private $image;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v1", type="text", nullable=true)
     */
    private $v1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v2", type="text", nullable=true)
     */
    private $v2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="v3", type="text", nullable=true)
     */
    private $v3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="text", nullable=true)
     */
    private $icon;

    public function __construct()
    {
        $this->text = new ArrayCollection();
        $this->image = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @param Language $lang
     * @return Collection|FeatureText[]
     */
    public function getText($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("lang", $lang))
            ->andWhere(Criteria::expr()->eq("deletedAt",NULL));
        
        if($criteria == null){
            return null;
        }
        return $this->text->matching($criteria);
    }

    /**
     * @return Collection|FeatureText[]
     */
    public function getAdminText() : Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null));

        if($criteria == null){
            return null;
        }
        return $this->text->matching($criteria);
    }

    /**
     * @return Collection|FeatureText[]
     */
    public function getTextWithoutCriteria(): Collection
    {
        return $this->text;
    }

    /**
     * @return Collection|FeatureImage[]
     */
    public function getImage(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", NULL));
        
        if($criteria == null){
            return null;
        }
        return $this->image->matching($criteria);
    }


    public function getV1(): ?string
    {
        return $this->v1;
    }

    public function setV1(?string $v1): self
    {
        $this->v1 = $v1;

        return $this;
    }

    public function getV2(): ?string
    {
        return $this->v2;
    }

    public function setV2(?string $v2): self
    {
        $this->v2 = $v2;

        return $this;
    }

    public function getV3(): ?string
    {
        return $this->v3;
    }

    public function setV3(?string $v3): self
    {
        $this->v3 = $v3;

        return $this;
    }


    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
