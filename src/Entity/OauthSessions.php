<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OauthSessions
 *
 * @ORM\Table(name="oauth_sessions", indexes={@ORM\Index(name="oauth_sessions_client_id_owner_type_owner_id_index", columns={"client_id", "owner_type", "owner_id"}), @ORM\Index(name="owner_id", columns={"owner_id"})})
 * @ORM\Entity
 */
class OauthSessions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=40, nullable=false)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="owner_type", type="string", length=0, nullable=false, options={"default"="user"})
     */
    private $ownerType = 'user';

    /**
     * @var string
     *
     * @ORM\Column(name="owner_id", type="string", length=255, nullable=false)
     */
    private $ownerId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getOwnerType(): ?string
    {
        return $this->ownerType;
    }

    public function setOwnerType(string $ownerType): self
    {
        $this->ownerType = $ownerType;

        return $this;
    }

    public function getOwnerId(): ?string
    {
        return $this->ownerId;
    }

    public function setOwnerId(string $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


}
