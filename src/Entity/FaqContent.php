<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqText
 * @ORM\Table(name="faq_contents")
 * @ORM\Entity
 */
class FaqContent
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="question", type="text", length=65535, nullable=true)
     */
    private $question;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer", type="text", length=65535, nullable=true)
     */
    private $answer;

    /**
     * @var Language
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var Faq
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Faq")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="faq_id", referencedColumnName="id")
     * })
     */
    private $faq;

    /**
     * @return null|string
     */
    public function getQuestion(): ?string
    {
        return $this->question;
    }

    /**
     * @param null|string $question
     */
    public function setQuestion(?string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return null|string
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @param null|string $answer
     */
    public function setAnswer(?string $answer): void
    {
        $this->answer = $answer;
    }

    
    /**
     * @return Language
     */
    public function getLang(): ?Language
    {
        return $this->lang;
    }

    /**
     * @param Language $lang
     */
    public function setLang(?Language $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return Faq
     */
    public function getFaq(): ?Faq
    {
        return $this->faq;
    }

    /**
     * @param Faq $faq
     */
    public function setFaq(?Faq $faq): void
    {
        $this->faq = $faq;
    }

}