<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Campaign
 * @ORM\Table(name="campaign", indexes={@ORM\Index(name="country_id", columns={"country_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", nullable=false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", nullable=false)
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '0';

    /**
     * @Assert\DateTime()
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Language", inversedBy="campaign")
     * @ORM\JoinTable(name="campaign_text",
     *   joinColumns={
     *     @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     *   }
     * )
     */
    private $lang;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @MaxDepth(2)
     * @ORM\OneToMany(targetEntity="CampaignText", mappedBy="campaign")
     */
    private $text;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CampaignAction", mappedBy="campaign", fetch="EAGER")
     */
    private $action;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->text = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTime $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getLang(): Collection
    {
        return $this->lang;
    }

    public function addLang(Language $lang): self
    {
        if (!$this->lang->contains($lang)) {
            $this->lang[] = $lang;
        }

        return $this;
    }

    public function removeLang(Language $lang): self
    {
        if ($this->lang->contains($lang)) {
            $this->lang->removeElement($lang);
        }

        return $this;
    }

    /**
     * @return Collection|CampaignText[]
     */
    public function getText(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));
        return $this->text->matching($criteria);
    }

    public function addText(CampaignText $campaignText): self
    {
        if (!$this->text->contains($campaignText)) {
            $this->text[] = $campaignText;
        }

        return $this;
    }

    public function removeText(CampaignText $campaignText): self
    {
        if ($this->text->contains($campaignText)) {
            $this->text->removeElement($campaignText);
        }

        return $this;
    }

    public function setText(ArrayCollection $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection|CampaignAction[]
     */
    public function getAction(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('deletedAt', null));
        return $this->action->matching($criteria);
    }

    public function addAction(CampaignAction $campaignAction): self
    {
        if (!$this->action->contains($campaignAction)) {
            $this->action[] = $campaignAction;
        }

        return $this;
    }

    public function removeAction(CampaignAction $campaignAction): self
    {
        if ($this->action->contains($campaignAction)) {
            $this->action->removeElement($campaignAction);
        }

        return $this;
    }
}
