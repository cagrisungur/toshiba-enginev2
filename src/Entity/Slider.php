<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Slider
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="App\Repository\SliderRepository")
 */
class Slider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var SliderType
     *
     * @ORM\ManyToOne(targetEntity="SliderType")
     * @ORM\JoinColumn(name="type_id",referencedColumnName="id")
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default":"0000-00-00 00:00:00"})
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SliderCategory", mappedBy="slider")
     */
    private $category;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SliderItem", mappedBy="slider")
     */
    private $item;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Language", inversedBy="slider")
     * @ORM\JoinTable(name="slider_item_text",
     *   joinColumns={
     *     @ORM\JoinColumn(name="slider_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     *   }
     * )
     */
    private $lang;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SliderProduct", mappedBy="slider")
     */
    private $product;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->lang = new ArrayCollection();
        $this->product = new ArrayCollection();
        $this->item = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getType() :SliderType
    {
        if($this->type->getDeletedAt() != null) {
            return null;
        }

        return $this->type;
    }

    public function setType(SliderType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->andWhere(Criteria::expr()->eq("deletedAt", null));

        return $this->category->matching($criteria);
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     * @return Collection|SliderItem[]
     */
    public function getItem(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->andWhere(Criteria::expr()->eq("deletedAt", null));

        return $this->item->matching($criteria);
    }

    public function addItem(SliderItem $item): self
    {
        if (!$this->item->contains($item)) {
            $this->item[] = $item;
        }

        return $this;
    }

    public function removeItem(SliderItem $item): self
    {
        if ($this->item->contains($item)) {
            $this->item->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|Language[]
     */
    public function getLang(): Collection
    {
        return $this->lang;
    }

    public function addLang(Language $lang): self
    {
        if (!$this->lang->contains($lang)) {
            $this->lang[] = $lang;
        }

        return $this;
    }

    public function removeLang(Language $lang): self
    {
        if ($this->lang->contains($lang)) {
            $this->lang->removeElement($lang);
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->andWhere(Criteria::expr()->eq("deletedAt", null));

        return $this->product->matching($criteria);
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }

    public function setLang(ArrayCollection $lang)
    {
        $this->lang = $lang;

        return $this;
    }
}
