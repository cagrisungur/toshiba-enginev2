<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserLang
 * @ORM\Table(name="warranty_user")
 * @ORM\Entity
 */
class WarrantyUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=128, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=256, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=14, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=14, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=1024, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     */
    private $city;
    
    /**
     * @Assert\DateTime()
     * @ORM\Column(name="birth_date", type="datetime", nullable=false)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="integer", length=11, nullable=true)
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="integer", length=11, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=32, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="education", type="integer", length=11, nullable=true)
     */
    private $education;

    /**
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="zipCode", type="string", length=16, nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_mail", type="integer", length=11, nullable=false)
     */
    private $isContactMail = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="contact_phone", type="integer", length=11, nullable=false)
     */
    private $isContactPhone = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="warranty_status", type="integer", nullable=false)
     */
    private $warrantyStatus = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="contact_letter", type="integer", length=11, nullable=false)
     */
    private $isContactLetter = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", length=11, nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="WarrantyProduct", mappedBy="user")
     */
    private $warrantyProduct;


    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of warrantyProduct
     *
     * @return  \Doctrine\Common\Collections\Collection
     */ 
    public function getWarrantyProduct()
    {
        return $this->warrantyProduct;
    }

    /**
     * Set the value of warrantyProduct
     *
     * @param  \Doctrine\Common\Collections\Collection  $warrantyProduct
     *
     * @return  self
     */ 
    public function setWarrantyProduct(\Doctrine\Common\Collections\Collection $warrantyProduct)
    {
        $this->warrantyProduct = $warrantyProduct;

        return $this;
    }

    /**
     * Get the value of deletedAt
     *
     * @return  \DateTime|null
     */ 
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set the value of deletedAt
     *
     * @param  \DateTime|null  $deletedAt
     *
     * @return  self
     */ 
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get the value of createdAt
     *
     * @return  \DateTime
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @param  \DateTime  $createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of active
     *
     * @return  string
     */ 
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of active
     *
     * @param  string  $active
     *
     * @return  self
     */ 
    public function setActive(string $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get the value of isContactLetter
     *
     * @return  string
     */ 
    public function getIsContactLetter()
    {
        return $this->isContactLetter;
    }

    /**
     * Set the value of isContactLetter
     *
     * @param  string  $isContactLetter
     *
     * @return  self
     */ 
    public function setIsContactLetter(string $isContactLetter)
    {
        $this->isContactLetter = $isContactLetter;

        return $this;
    }

    /**
     * Get the value of isContactPhone
     *
     * @return  string
     */ 
    public function getIsContactPhone()
    {
        return $this->isContactPhone;
    }

    /**
     * Set the value of isContactPhone
     *
     * @param  string  $isContactPhone
     *
     * @return  self
     */ 
    public function setIsContactPhone(string $isContactPhone)
    {
        $this->isContactPhone = $isContactPhone;

        return $this;
    }

    /**
     * Get the value of isContactMail
     *
     * @return  string
     */ 
    public function getIsContactMail()
    {
        return $this->isContactMail;
    }

    /**
     * Set the value of isContactMail
     *
     * @param  string  $isContactMail
     *
     * @return  self
     */ 
    public function setIsContactMail(string $isContactMail)
    {
        $this->isContactMail = $isContactMail;

        return $this;
    }

    /**
     * Get the value of zipCode
     *
     * @return  string
     */ 
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set the value of zipCode
     *
     * @param  string  $zipCode
     *
     * @return  self
     */ 
    public function setZipCode(string $zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get the value of education
     *
     * @return  string
     */ 
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set the value of education
     *
     * @param  string  $education
     *
     * @return  self
     */ 
    public function setEducation(string $education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */ 
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Get the value of email
     *
     * @return  string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param  string  $email
     *
     * @return  self
     */ 
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of phone
     *
     * @return  string
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @param  string  $phone
     *
     * @return  self
     */ 
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of mobile
     *
     * @return  string
     */ 
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set the value of mobile
     *
     * @param  string  $mobile
     *
     * @return  self
     */ 
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get the value of address
     *
     * @return  string
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @param  string  $address
     *
     * @return  self
     */ 
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of city
     *
     * @return  string
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @param  string  $city
     *
     * @return  self
     */ 
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of birthDate
     */ 
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set the value of birthDate
     *
     * @return  self
     */ 
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the value of job
     *
     * @return  string
     */ 
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set the value of job
     *
     * @param  string  $job
     *
     * @return  self
     */ 
    public function setJob(string $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of password
     *
     * @return  string
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @param  string  $password
     *
     * @return  self
     */ 
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of surname
     *
     * @return  string
     */ 
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set the value of surname
     *
     * @param  string  $surname
     *
     * @return  self
     */ 
    public function setSurname(string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get the value of warrantyStatus
     *
     * @return  int
     */ 
    public function getWarrantyStatus()
    {
        return $this->warrantyStatus;
    }

    /**
     * Set the value of warrantyStatus
     *
     * @param  int  $warrantyStatus
     *
     * @return  self
     */ 
    public function setWarrantyStatus(int $warrantyStatus)
    {
        $this->warrantyStatus = $warrantyStatus;

        return $this;
    }
}