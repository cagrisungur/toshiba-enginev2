<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductSize
 * @ORM\Table(name="product_size")
 * @ORM\Entity
 */
class ProductSize
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="text", length=128, nullable=false)
     */
    private $name;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @var ProductSizeImage
     * @ORM\OneToMany(targetEntity="ProductSizeImage", mappedBy="productSize")
     */
    private $productSizeImage;

    public function __construct()
    {
        $this->productSizeImage = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return Collection|ProductSizeImage[]
     */
    public function getProductSizeImage(): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->orderBy(array("priority" => Criteria::ASC));

        return $this->productSizeImage->matching($criteria);
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $productSizeImage
     */
    public function setProductSizeImage(Collection $productSizeImage): void
    {
        $this->productSizeImage = $productSizeImage;
    }



}