<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Psr\Log\NullLogger;

/**
 * SpecificationCategory
 * @ORM\Table(name="specification_category")
 * @ORM\Entity(repositoryClass="App\Repository\SpecificationCategoryRepository")
 */
class SpecificationCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority = "0";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SpecificationCategoryText", mappedBy="specCategory")
     */
    private $text;

    public function __construct()
    {
        $this->text = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|SpecificationCategoryText[]
     * @param Language $lang
     */
    public function getText($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("lang",$lang))
            ->andWhere(Criteria::expr()->eq("deletedAt", null));
        
        if($criteria == NULL){
            return $this->text;
        }
        return $this->text->matching($criteria);
    }

    /**
     * @return Collection|SpecificationCategoryText[]
     */
    public function getAdminText(): Collection {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null));

        if($criteria == NULL){
            return $this->text;
        }
        return $this->text->matching($criteria);
    }

    /**
     * @return Collection|SpecificationCategoryText[]
     */
    public function getTextWithoutCriteria(): Collection
    {
        return $this->text;
    }
}
