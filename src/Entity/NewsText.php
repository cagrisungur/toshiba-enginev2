<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsText
 *
 * @ORM\Table(name="news_text")
 * @ORM\Entity(repositoryClass="App\Repository\NewsTextRepository")
 */
class NewsText
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="string", length=128, nullable=true)
     */
    private $subtitle;

    /**
     * @var Language
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var News
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     * })
     */
    private $news;

    /**
     * @var string
     *
     * @ORM\Column(name="sef", type="text", nullable=false)
     */
    private $sef;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="banner_path", type="string", nullable=true)
     */
    private $bannerPath;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @return null|string
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param null|string $subtitle
     */
    public function setSubtitle(?string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return Language
     */
    public function getLang(): Language
    {
        return $this->lang;
    }

    /**
     * @param Language $lang
     */
    public function setLang(Language $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }

    /**
     * @param News $news
     */
    public function setNews(News $news): void
    {
        $this->news = $news;
    }

    /**
     * @return string
     */
    public function getSef(): string
    {
        return $this->sef;
    }

    /**
     * @param string $sef
     */
    public function setSef(string $sef): void
    {
        $this->sef = $sef;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     */
    public function setDeletedAt(?\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * Get the value of bannerPath
     *
     * @return  string
     */ 
    public function getBannerPath()
    {
        return $this->bannerPath;
    }

    /**
     * Set the value of bannerPath
     *
     * @param  string  $bannerPath
     *
     * @return  self
     */ 
    public function setBannerPath(string $bannerPath)
    {
        $this->bannerPath = $bannerPath;

        return $this;
    }
}