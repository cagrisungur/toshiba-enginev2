<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SupportServices
 * @ORM\Table(name="support_services")
 * @ORM\Entity(repositoryClass="App\Repository\SupportServicesRepository")
 */
class SupportServices
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(name="VBELN_VL", type="string", length=10, nullable=false)
     * 
    */
    private $VBELN_VL;
    
    /**
     * @var string
     *
     * @ORM\Column(name="POSNR_VL", type="string", length=6, nullable=false)
    */
    private $POSNR_VL;

    /**
     * @var string
     *
     * @ORM\Column(name="VBELN_VA", type="string", length=10, nullable=false)
    */
    private $VBELN_VA;

    /**
     * @var string
     *
     * @ORM\Column(name="POSNR_VA", type="string", length=6, nullable=false)
    */
    private $POSNR_VA;

    /**
     * @var string
     *
     * @ORM\Column(name="VBELN_VF", type="string", length=10, nullable=false)
    */
    private $VBLEN_VF;

    /**
     * @var string
     *
     * @ORM\Column(name="POSNR_VF", type="string", length=6, nullable=false)
    */
    private $POSNR_VF;

    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(name="MATNR", type="string", length=10, nullable=false)
     * 
    */
    private $MATNR;

    /**
     * @var string
     *
     * @ORM\Column(name="LFIMG", type="integer", length=11, nullable=false)
    */
    private $LFIMG;

    /**
     * @var string
     *
     * @ORM\Column(name="KWMENG", type="integer", length=11, nullable=false)
    */
    private $KWMENG;

    /**
     * @var string
     *
     * @ORM\Column(name="FKIMG", type="integer", length=11, nullable=false)
    */
    private $FKIMG;

    /**
     * @var string
     *
     * @ORM\Column(name="KUNAG", type="string", length=10, nullable=false)
    */
    private $KUNAG;

    /**
     * @var string
     *
     * @ORM\Column(name="NAMAG", type="string", length=30, nullable=false)
    */
    private $NAMAG;

    /**
     * @var string
     *
     * @ORM\Column(name="KUNCN", type="string", length=10, nullable=false)
    */
    private $KUNCN;

    /**
     * @var string
     *
     * @ORM\Column(name="NAMCN", type="string", length=30, nullable=false)
    */
    private $NAMCN;

    /**
     * @Assert\DateTime()
     * @ORM\Column(name="AUDAT", type="datetime", nullable=false)
     */
    private $AUDAT;

    /**
     * @var string
     *
     * @ORM\Column(name="BSTNK", type="string", length=20, nullable=false)
    */
    private $BSTNK;

    /**
     * @Assert\DateTime()
     * @ORM\Column(name="PRDAT", type="datetime", nullable=false)
     */
    private $PRDAT;

    /**
     * @Assert\DateTime()
     * @ORM\Column(name="CNDAT", type="datetime", nullable=false)
     */
    private $CNDAT;

    /**
     * @var string
     *
     * @ORM\Column(name="CHASSIS", type="string", length=30, nullable=false)
    */
    private $CHASSIS;

    /**
     * @var string
     *
     * @ORM\Column(name="PANELS", type="string", length=30, nullable=false)
    */
    private $PANELS;

    /**
     * @var string
     *
     * @ORM\Column(name="PRDCAT", type="string", length=30, nullable=false)
    */
    private $PRDCAT;

    /**
     * @var string
     *
     * @ORM\Column(name="MODELNO", type="string", length=30, nullable=false)
    */
    private $MODELNO;

    /**
     * @var string
     *
     * @ORM\Column(name="RETURN_MESSAGE", type="string", length=255, nullable=false)
    */
    private $RETURN_MESSAGE;


    /**
     * Get the value of VBELN_VL
     *
     * @return  string
     */ 
    public function getVBELN_VL()
    {
        return $this->VBELN_VL;
    }

    /**
     * Set the value of VBELN_VL
     *
     * @param  string  $VBELN_VL
     *
     * @return  self
     */ 
    public function setVBELN_VL(string $VBELN_VL)
    {
        $this->VBELN_VL = $VBELN_VL;

        return $this;
    }

    /**
     * Get the value of MATNR
     *
     * @return  string
     */ 
    public function getMATNR()
    {
        return $this->MATNR;
    }

    /**
     * Set the value of MATNR
     *
     * @param  string  $MATNR
     *
     * @return  self
     */ 
    public function setMATNR(string $MATNR)
    {
        $this->MATNR = $MATNR;

        return $this;
    }
}