<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * WarrantyQuestions
 * @ORM\Table(name="warranty_questions")
 * @ORM\Entity
 */
class WarrantyQuestions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Language
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", nullable=false)
     */
    private $question;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="question_type", type="integer", nullable=false)
     */
    private $questionType;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of lang
     *
     * @return  Language
     */ 
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set the value of lang
     *
     * @param  Language  $lang
     *
     * @return  self
     */ 
    public function setLang(Language $lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get the value of question
     *
     * @return  string
     */ 
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set the value of question
     *
     * @param  string  $question
     *
     * @return  self
     */ 
    public function setQuestion(string $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get the value of active
     *
     * @return  int
     */ 
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of active
     *
     * @param  int  $active
     *
     * @return  self
     */ 
    public function setActive(int $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get the value of questionType
     *
     * @return  int
     */ 
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * Set the value of questionType
     *
     * @param  int  $questionType
     *
     * @return  self
     */ 
    public function setQuestionType(int $questionType)
    {
        $this->questionType = $questionType;

        return $this;
    }

    /**
     * Get the value of priority
     *
     * @return  int
     */ 
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set the value of priority
     *
     * @param  int  $priority
     *
     * @return  self
     */ 
    public function setPriority(int $priority)
    {
        $this->priority = $priority;

        return $this;
    }
}