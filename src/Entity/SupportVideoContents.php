<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="support_video_contents")
 * @ORM\Entity
 */
class SupportVideoContents
{

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="text", nullable=false)
     */
    private $video;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="text", nullable=false)
     */
    private $thumbnail;

    /**
     * @var int
     *
     * @ORM\Column(name="video_view", type="integer", nullable=false)
     */
    private $videoView = '0';

    /**
     * @var Language
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var SupportVideos
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="SupportVideos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     * })
     */
    private $supportVideosText;

    /**
     * Get the value of description
     *
     * @return  string|null
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string|null  $description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    
    /**
     * Get the value of lang
     *
     * @return  Language
     */ 
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set the value of lang
     *
     * @param  Language  $lang
     *
     * @return  self
     */ 
    public function setLang(Language $lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get the value of videoView
     *
     * @return  int
     */ 
    public function getVideoView()
    {
        return $this->videoView;
    }

    /**
     * Set the value of videoView
     *
     * @param  int  $videoView
     *
     * @return  self
     */ 
    public function setVideoView(int $videoView)
    {
        $this->videoView = $videoView;

        return $this;
    }

    /**
     * Get the value of thumbnail
     *
     * @return  string
     */ 
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set the value of thumbnail
     *
     * @param  string  $thumbnail
     *
     * @return  self
     */ 
    public function setThumbnail(string $thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get the value of keywords
     *
     * @return  string|null
     */ 
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the value of keywords
     *
     * @param  string|null  $keywords
     *
     * @return  self
     */ 
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get the value of video
     *
     * @return  string
     */ 
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set the value of video
     *
     * @param  string  $video
     *
     * @return  self
     */ 
    public function setVideo(string $video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get the value of supportVideosText
     *
     * @return  SupportVideos
     */ 
    public function getSupportVideosText()
    {
        return $this->supportVideosText;
    }

    /**
     * Set the value of supportVideosText
     *
     * @param  SupportVideos  $supportVideosText
     *
     * @return  self
     */ 
    public function setSupportVideosText(SupportVideos $supportVideosText)
    {
        $this->supportVideosText = $supportVideosText;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return  string|null
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  string|null  $title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }
}