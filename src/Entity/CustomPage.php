<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * CustomPage
 * @ORM\Table(name="custom_page", indexes={@ORM\Index(name="type_id", columns={"type_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CustomPageRepository")
 */
class CustomPage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var PageType
     *
     * @ORM\ManyToOne(targetEntity="PageType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var CustomPageText
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="CustomPageText", mappedBy="page" )
     */
    private $pageText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path", type="text", length=65535, nullable=true)
     */
    private $path;

    /**
     * @var string|null
     *
     * @ORM\Column(name="thumbnail_path", type="text", length=65535, nullable=true)
     */
    private $thumbNailPath;

    public function __construct()
    {
        $this->pageText = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getType(): ?PageType
    {
        return $this->type;
    }

    public function setType(?PageType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPageText()
    {
        return $this->pageText;
    }

    /**
     * @return Collection|CustomPageText[]
     * @param $country
     * @param $lang
     */
    public function getNewPageText($country,$lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('country', $country))
            ->andWhere(Criteria::expr()->eq('lang', $lang));
        return $this->pageText->matching($criteria);
    }

    /**
     * @param mixed $pageText
     */
    public function setPageText($pageText): void
    {
        $this->pageText = $pageText;
    }

    /**
     * @return null|string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param null|string $path
     */
    public function setPath(?string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return null|string
     */
    public function getThumbNailPath(): ?string
    {
        return $this->thumbNailPath;
    }

    /**
     * @param null|string $thumbNailPath
     */
    public function setThumbNailPath(?string $thumbNailPath): void
    {
        $this->thumbNailPath = $thumbNailPath;
    }

}
