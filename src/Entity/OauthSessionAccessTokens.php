<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OauthSessionAccessTokens
 *
 * @ORM\Table(name="oauth_session_access_tokens", uniqueConstraints={@ORM\UniqueConstraint(name="access_token_session_id", columns={"access_token", "session_id"})}, indexes={@ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class OauthSessionAccessTokens
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="text", length=65535, nullable=false)
     */
    private $accessToken;

    /**
     * @var int
     *
     * @ORM\Column(name="access_token_expires", type="integer", nullable=false)
     */
    private $accessTokenExpires;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    public function setSessionId(int $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getAccessTokenExpires(): ?int
    {
        return $this->accessTokenExpires;
    }

    public function setAccessTokenExpires(int $accessTokenExpires): self
    {
        $this->accessTokenExpires = $accessTokenExpires;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


}
