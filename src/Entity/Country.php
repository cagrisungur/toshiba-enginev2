<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="App\Repository\CountryLangRepository")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="orig_name", type="string", length=255, nullable=false)
     */
    private $origName;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3, nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="maintance", type="integer", nullable=false)
     */
    private $maintance = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="register", type="integer", nullable=false)
     */
    private $register = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Continent
     *
     * @ORM\ManyToOne(targetEntity="Continent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="continent_id", referencedColumnName="id")
     * })
     */
    private $continent;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CountryLang", mappedBy="country")
     */
    private $countryLang;

    public function __construct()
    {
        $this->countryLang = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrigName(): ?string
    {
        return $this->origName;
    }

    public function setOrigName(string $origName): self
    {
        $this->origName = $origName;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getMaintance(): ?int
    {
        return $this->maintance;
    }

    public function setMaintance(int $maintance): self
    {
        $this->maintance = $maintance;

        return $this;
    }

    public function getRegister(): ?int
    {
        return $this->register;
    }

    public function setRegister(int $register): self
    {
        $this->register = $register;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|CountryLang[]
     */
    public function getCountryLang(): Collection
    {
        return $this->countryLang;
    }

    public function addCountryLang(CountryLang $countryLang): self
    {
        if (!$this->countryLang->contains($countryLang)) {
            $this->countryLang[] = $countryLang;
        }

        return $this;
    }

    public function removeCountryLang(CountryLang $countryLang): self
    {
        if ($this->countryLang->contains($countryLang)) {
            $this->countryLang->removeElement($countryLang);
        }

        return $this;
    }

    public function setCountryLang(CountryLang $countryLang)
    {
        $this->countryLang[] = $countryLang;
        return $this;
    }

    /**
     * @return Continent
     */
    public function getContinent(): Continent
    {
        return $this->continent;
    }

    /**
     * @param Continent $continent
     */
    public function setContinent(Continent $continent): void
    {
        $this->continent = $continent;
    }

}
