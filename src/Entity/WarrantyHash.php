<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserLang
 * @ORM\Table(name="warranty_hash")
 * @ORM\Entity
 */
class WarrantyHash
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var WarrantyUser
     *
     * @ORM\ManyToOne(targetEntity="WarrantyUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $warrantyUser;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     */
    private $hash;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=11, nullable=false)
     */
    private $status = '0';

     /**
     * @var int
     *
     * @ORM\Column(name="is_forget_password", type="integer", length=11, nullable=false)
     */
    private $isForgetPassword = '0';

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of hash
     *
     * @return  string
     */ 
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set the value of hash
     *
     * @param  string  $hash
     *
     * @return  self
     */ 
    public function setHash(string $hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get the value of status
     *
     * @return  string
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @param  string  $status
     *
     * @return  self
     */ 
    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }


    /**
     * Get the value of warrantyUser
     *
     * @return  WarrantyUser
     */ 
    public function getWarrantyUser()
    {
        return $this->warrantyUser;
    }

    /**
     * Set the value of warrantyUser
     *
     * @param  WarrantyUser  $warrantyUser
     *
     * @return  self
     */ 
    public function setWarrantyUser(WarrantyUser $warrantyUser)
    {
        $this->warrantyUser = $warrantyUser;

        return $this;
    }

    /**
     * Get the value of isForgetPassword
     *
     * @return  int
     */ 
    public function getIsForgetPassword()
    {
        return $this->isForgetPassword;
    }

    /**
     * Set the value of isForgetPassword
     *
     * @param  int  $isForgetPassword
     *
     * @return  self
     */ 
    public function setIsForgetPassword(int $isForgetPassword)
    {
        $this->isForgetPassword = $isForgetPassword;

        return $this;
    }
}