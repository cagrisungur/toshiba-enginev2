<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FiltersValueProduct
 * @ORM\Table(name="filters_value_product")
 * @ORM\Entity
 */
class FiltersValueProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Product
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var FiltersValue
     * @ORM\OneToOne(targetEntity="FiltersValue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="filter_value_id", referencedColumnName="id")
     * })
     */
    private $filtersValue;

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of product
     *
     * @return  Product
     */ 
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set the value of product
     *
     * @param  Product  $product
     *
     * @return  self
     */ 
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get })
     *
     * @return  FiltersValue
     */ 
    public function getFiltersValue()
    {
        return $this->filtersValue;
    }

    /**
     * Set })
     *
     * @param  FiltersValue  $filtersValue  })
     *
     * @return  self
     */ 
    public function setFiltersValue(FiltersValue $filtersValue)
    {
        $this->filtersValue = $filtersValue;

        return $this;
    }
}    