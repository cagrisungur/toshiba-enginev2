<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * SupportServices
 * @ORM\Table(name="support_videos")
 * @ORM\Entity()
 */
class SupportVideos
{
/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="video", type="string", nullable=true)
     */
    private $video;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="string", nullable=true)
     */
    private $keywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="epidemic", type="integer", nullable=false)
     */
    private $epidemic = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="view", type="integer", nullable=false)
     */
    private $view;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var FaqCategory
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="FaqCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $videoFaqCategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SupportVideoContents", mappedBy="supportVideosText")
     */
    private $videoText;

    public function __construct()
    {
        $this->videoText = new ArrayCollection();
    }

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of videoFaqCategory
     *
     * @return  FaqCategory
     */ 
    public function getVideoFaqCategory()
    {
        return $this->videoFaqCategory;
    }

    /**
     * Set the value of videoFaqCategory
     *
     * @param  FaqCategory  $videoFaqCategory
     *
     * @return  self
     */ 
    public function setVideoFaqCategory(FaqCategory $videoFaqCategory)
    {
        $this->videoFaqCategory = $videoFaqCategory;

        return $this;
    }

    /**
     * Get the value of deletedAt
     *
     * @return  \DateTime|null
     */ 
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set the value of deletedAt
     *
     * @param  \DateTime|null  $deletedAt
     *
     * @return  self
     */ 
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get the value of view
     *
     * @return  int
     */ 
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set the value of view
     *
     * @param  int  $view
     *
     * @return  self
     */ 
    public function setView(int $view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get the value of epidemic
     *
     * @return  int
     */ 
    public function getEpidemic()
    {
        return $this->epidemic;
    }

    /**
     * Set the value of epidemic
     *
     * @param  int  $epidemic
     *
     * @return  self
     */ 
    public function setEpidemic(int $epidemic)
    {
        $this->epidemic = $epidemic;

        return $this;
    }

    /**
     * Get the value of description
     *
     * @return  string|null
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param  string|null  $description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of keywords
     *
     * @return  string|null
     */ 
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set the value of keywords
     *
     * @param  string|null  $keywords
     *
     * @return  self
     */ 
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get the value of video
     *
     * @return  string|null
     */ 
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set the value of video
     *
     * @param  string|null  $video
     *
     * @return  self
     */ 
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  string  $title
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param Language $lang
     * @return Collection|SupportVideContents[]
     */
    public function getVideoText($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("lang", $lang));

        if($criteria == null){
            return null;
        }
        return $this->videoText->matching($criteria);
    }
}