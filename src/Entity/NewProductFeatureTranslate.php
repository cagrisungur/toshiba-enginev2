<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * pfeaturetranslate
 * @ORM\Table(name="pfeaturetranslate")
 * @ORM\Entity()
 */
class NewProductFeatureTranslate
{
    
    
    /**
     * @var Language
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lang_id", referencedColumnName="id")
     * })
     */
    private $lang;

    /**
     * @var NewProductFeature
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="NewProductFeature")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pFeature_id", referencedColumnName="id")
     * })
     */
    private $pFeature;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path", type="text", length=65535, nullable=true)
     */
    private $path;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="des", type="text", length=65535, nullable=true)
     */
    private $des;


    public function __construct()
    {
        $this->text = new ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getDes(): ?string
    {
        return $this->des;
    }

    /**
     * @return null|string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }


    /**
     * Get the value of lang
     *
     * @return  Language
     */ 
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set the value of lang
     *
     * @param  Language  $lang
     *
     * @return  self
     */ 
    public function setLang(Language $lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get the value of pFeature
     *
     * @return  NewProductFeature
     */ 
    public function getPFeature()
    {
        return $this->pFeature;
    }

    /**
     * Set the value of pFeature
     *
     * @param  NewProductFeature  $pFeature
     *
     * @return  self
     */ 
    public function setPFeature(NewProductFeature $pFeature)
    {
        $this->pFeature = $pFeature;

        return $this;
    }
}
