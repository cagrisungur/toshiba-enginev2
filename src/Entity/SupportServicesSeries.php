<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SupportServicesSeries
 * @ORM\Table(name="support_services_series")
 * @ORM\Entity(repositoryClass="App\Repository\SupportServicesSeriesRepository")
 */
class SupportServicesSeries
{
    /**
     * @var String
     * @ORM\Id()
     * @ORM\Column(name="SERINO", type="string", length=30, nullable=false)
     * 
     * 
    */
    private $SERINO;

    /**
     * @var string
     *
     * @ORM\Column(name="VBELN_VL", type="string", length=6, nullable=false)
    */
    private $VBELN_VL;

    
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(name="MATNR", type="string", length=6, nullable=false)
    */
    private $MATNR;

    /**
     * Get the value of MATNR
     *
     * @return  string
     */ 
    public function getMATNR()
    {
        return $this->MATNR;
    }

    /**
     * Set the value of MATNR
     *
     * @param  string  $MATNR
     *
     * @return  self
     */ 
    public function setMATNR(string $MATNR)
    {
        $this->MATNR = $MATNR;

        return $this;
    }

    /**
     * Get the value of VBELN_VL
     *
     * @return  string
     */ 
    public function getVBELN_VL()
    {
        return $this->VBELN_VL;
    }

    /**
     * Set the value of VBELN_VL
     *
     * @param  string  $VBELN_VL
     *
     * @return  self
     */ 
    public function setVBELN_VL(string $VBELN_VL)
    {
        $this->VBELN_VL = $VBELN_VL;

        return $this;
    }

    /**
     * Get the value of SERINO
     *
     * @return  String
     */ 
    public function getSERINO()
    {
        return $this->SERINO;
    }

    /**
     * Set the value of SERINO
     *
     * @param  String  $SERINO
     *
     * @return  self
     */ 
    public function setSERINO(String $SERINO)
    {
        $this->SERINO = $SERINO;

        return $this;
    }
}