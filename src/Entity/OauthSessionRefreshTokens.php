<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OauthSessionRefreshTokens
 *
 * @ORM\Table(name="oauth_session_refresh_tokens", indexes={@ORM\Index(name="oauth_session_refresh_tokens_client_id_index", columns={"client_id"}), @ORM\Index(name="refresh_token", columns={"refresh_token"})})
 * @ORM\Entity
 */
class OauthSessionRefreshTokens
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="session_access_token_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $sessionAccessTokenId;

    /**
     * @var string
     *
     * @ORM\Column(name="refresh_token", type="string", length=40, nullable=false)
     */
    private $refreshToken;

    /**
     * @var int
     *
     * @ORM\Column(name="refresh_token_expires", type="integer", nullable=false)
     */
    private $refreshTokenExpires;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=40, nullable=false)
     */
    private $clientId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getSessionAccessTokenId(): ?int
    {
        return $this->sessionAccessTokenId;
    }

    public function setSessionAccessTokenId(int $sessionAccessTokenId)
    {
        $this->sessionAccessTokenId = $sessionAccessTokenId;
        return $this;
    }
    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getRefreshTokenExpires(): ?int
    {
        return $this->refreshTokenExpires;
    }

    public function setRefreshTokenExpires(int $refreshTokenExpires): self
    {
        $this->refreshTokenExpires = $refreshTokenExpires;

        return $this;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


}
