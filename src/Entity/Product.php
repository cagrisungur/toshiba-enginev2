<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="category_id", columns={"category_id","name","link"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="size", type="text", nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=128, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="pp", type="string", length=128, nullable=false)
     */
    private $pp;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=64, nullable=false)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="text", length=65535, nullable=true)
     */
    private $keywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="priority", type="text", length=65535, nullable=true)
     */
    private $priority;


    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = "0";

    /**
     * @var int
     *
     * @ORM\Column(name="is_publish", type="integer", nullable=false)
     */
    private $isPublish;

    /**
     * @var int
     *
     * @ORM\Column(name="is_landing_page", type="integer", nullable=false)
     */
    private $isLandingPage = "0";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var Series
     *
     * @ORM\ManyToOne(targetEntity="Series", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="series_id", referencedColumnName="id")
     * })
     */
    private $series;

    /**
     * @var ProductStatus
     *
     * @ORM\ManyToOne(targetEntity="ProductStatus", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductSize", mappedBy="product")
     */
    private $productSize;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductController", mappedBy="product")
     */
    private $productController;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductBuy", mappedBy="product")
     */
    private $productBuy;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductConnection", mappedBy="product")
     */
    private $productConnection;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductCountry", mappedBy="product")
     */
    private $country;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ProductFile", mappedBy="product")
     */
    private $file;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="RelatedProduct", mappedBy="host")
     */
    private $related;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="SliderProduct", mappedBy="product")
     */
    private $slider;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductText", mappedBy="product")
     */
    private $text;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductFeature", mappedBy="product")
     */
    private $feature;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductSpec", mappedBy="product")
     */
    private $spec;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="HighlightedText", mappedBy="product")
     */
    private $highlightedText;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product")
     */
    private $images;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_best", type="integer", nullable=true)
     */
    private $isBest = "0";

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_show_tru", type="integer", nullable=true)
     */
    private $isShowTru = "0";

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_offer", type="integer", nullable=true)
     */
    private $isOffer = "0";

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_new", type="integer", nullable=true)
     */
    private $isNew = "0";

    /**
     * @var integer|null
     *
     * @ORM\Column(name="smart", type="integer", nullable=true)
     */
    private $smart = "0";

    /**
     * @var integer|null
     *
     * @ORM\Column(name="is_old", type="integer", nullable=true)
     */
    private $isOld = "0";

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductConnection", mappedBy="product")
     */
    private $connection;

    /**
     * @var int|null
     *
     * @ORM\Column(name="min_ft", type="integer", nullable=true)
     */
    private $minFt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="max_ft", type="integer", nullable=true)
     */
    private $maxFt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ProductController", mappedBy="product")
     */
    private $controller;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ean", type="string", nullable=true)
     */
    private $ean;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->series = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->country = new ArrayCollection();
        $this->file = new ArrayCollection();
        $this->related = new ArrayCollection();
        $this->slider = new ArrayCollection();
        $this->text = new ArrayCollection();
        $this->feature = new ArrayCollection();
        $this->spec = new ArrayCollection();
        $this->highlightedText = new ArrayCollection();
        $this->productController = new ArrayCollection();
        $this->productSize = new ArrayCollection();
        $this->productConnection = new ArrayCollection();
        $this->productBuy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getPriority(): ?string
    {
        return $this->priority;
    }

    /**
     * @param null|string $priority
     */
    public function setPriority(?string $priority): void
    {
        $this->priority = $priority;
    }


    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        // if($this->category->getActive()==0 || $this->category->getDeletedAt() != null){
        //     return null;
        // }

        return $this->category;
    }


    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Series|null
     */
    public function getSeries(): ?Series
    {
        return $this->series;
    }

    /**
     * @return ProductStatus|null
     */
    public function getStatus(): ?ProductStatus
    {
        return $this->status;
    }

    /**
     * @return Series|null
     */
    public function getAdminSeries(): ?Series
    {
        return $this->series;

    }


    public function setSeries(Series $series): self
    {
        $this->series = $series;
        return $this;

    }

    public function setStatus(?ProductStatus $status): self
    {
        $this->status = $status;
        return $this;

    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImages(): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->andWhere(Criteria::expr()->eq("deletedAt", null))
            ->orderBy(array("priority"=>Criteria::ASC));

        return $this->images->matching($criteria);
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getFrontImages(): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("active",1))
            ->andWhere(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->contains("name", "front"))
            ->orderBy(array("priority"=>Criteria::ASC));

        return $this->images->matching($criteria);
    }

    /**
     * @param Collection $images
     */
    public function setImages(Collection $images): void
    {
        $this->images = $images;
    }

    /**
     * @return Collection|ProductCountry[]|null
     */
    public function getSelectedCountry($country): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("country", $country))
            ->andWhere(Criteria::expr()->eq('active', 1));

        if($criteria == null){
            return null;
        }

        return $this->country->matching($criteria);
    }

    /**
     * @return Collection|ProductCountry[]|null
     */
    public function getCountry(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq('active', 1));

        if($criteria == null){
            return null;
        }

        return $this->country->matching($criteria);
    }

     /**
     * @return Collection|ProductCountry[]|null
     */
    public function getIfCountryExist($country): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq('country', $country))
            ->andWhere(Criteria::expr()->eq('active', 1));

        if($criteria == null){
            return null;
        }

        return $this->country->matching($criteria);
    }

    /**
     * @return Collection|ProductCountry[]
     * @param $country
     */
    public function getCountryProduct($country): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("country", $country));
        if($criteria == null)
            return null;

        return $this->country->matching($criteria);
    }

    /**
     * @return Collection|ProductCountry[]
     */
    public function getAdminCountryProduct(): ?Collection
    {
        return $this->country;
    }

    public function addCountry(ProductCountry $country): self
    {
        if (!$this->country->contains($country)) {
            $this->country[] = $country;
        }

        return $this;
    }

    /**
     * @param Collection $country
     */
    public function setCountry(Collection $country): void
    {
        $this->country = $country;
    }

    public function removeLang(ProductCountry $country): self
    {
        if ($this->country->contains($country)) {
            $this->country->removeElement($country);
        }

        return $this;
    }

    /**
     * @return Collection|ProductFile[]|null
     */
    public function getFile($country): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("country", $country))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null){
            return null;
        }

        return $this->file->matching($criteria);
    }


    public function addFile(ProductFile $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
        }

        return $this;
    }

    public function removeFile(ProductFile $file): self
    {
        if ($this->file->contains($file)) {
            $this->file->removeElement($file);
        }

        return $this;
    }

    /**
     * @return Collection|ProductSpec[]|null
     */
    public function getSpec($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("lang", $lang))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null){
            return null;
        }

        return $this->spec->matching($criteria);
    }
    /**
     * @return Collection|ProductSpec[]
     */
    public function getAdminSpec() {
        return $this->spec;
    }

    public function addSpec(ProductSpec $spec): self
    {
        if (!$this->spec->contains($spec)) {
            $this->spec[] = $spec;
        }

        return $this;
    }

    public function removeSpec(ProductSpec $spec): self
    {
        if ($this->spec->contains($spec)) {
            $this->spec->removeElement($spec);
        }

        return $this;
    }

    /**
     * @return Collection|RelatedProduct[]|null
     */
    public function getRelated(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null){
            return null;
        }

        return $this->related->matching($criteria);
    }

    /**
     * @return Collection|SliderProduct[]|null
     */
    public function getSlider(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("active",1))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null){
            return null;
        }

        return $this->slider->matching($criteria);
    }

    public function addSlider(SliderProduct $slider): self
    {
        if (!$this->slider->contains($slider)) {
            $this->slider[] = $slider;
        }

        return $this;
    }

    public function removeSlider(SliderProduct $slider): self
    {
        if ($this->slider->contains($slider)) {
            $this->slider->removeElement($slider);
        }

        return $this;
    }

    /**
     * @return Collection|ProductText[]|null
     */
    public function getLangText($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq('lang',$lang));
        if($criteria == null){
            return null;
        }

        return $this->text->matching($criteria);
    }

    /**
     * @return Collection|ProductText[]|null
     */
    public function getText(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null));
        if($criteria == null){
            return null;
        }

        return $this->text->matching($criteria);
    }

    public function addText(ProductText $productText): self
    {
        if (!$this->text->contains($productText)) {
            $this->text[] = $productText;
        }

        return $this;
    }

    public function removeText(ProductText $productText): self
    {
        if ($this->text->contains($productText)) {
            $this->text->removeElement($productText);
        }

        return $this;
    }

    public function setText(ArrayCollection $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection|ProductFeature[]|null
     */
    public function getFeature(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null){
            return null;
        }

        return $this->feature->matching($criteria);
    }

    public function addFeature(ProductFeature $slider): self
    {
        if (!$this->feature->contains($slider)) {
            $this->feature[] = $slider;
        }

        return $this;
    }

    public function removeFeature(ProductFeature $slider): self
    {
        if ($this->feature->contains($slider)) {
            $this->feature->removeElement($slider);
        }

        return $this;
    }

    /**
     * @return Collection |HighlightedText[] |null
     */
    public function getHighlightedText(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria ==null) {
            return null;
        }
        return $this->highlightedText->matching($criteria);
    }

    /**
     * @param HighlightedText $highlightedText
     */
    public function setHighlightedText(HighlightedText $highlightedText): void
    {
        $this->highlightedText = $highlightedText;
    }

    /**
     * @return null|string
     */
    public function getSize(): ?string
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     */
    public function setSize(?string $size): void
    {
        $this->size = $size;
    }


    /**
     * @return Collection
     */
    public function getProductSize(): Collection
    {
        return $this->productSize;
    }

    /**
     * @param Collection $productSize
     */
    public function setProductSize(Collection $productSize): void
    {
        $this->productSize = $productSize;
    }

    /**
     * @return Collection
     */
    public function getProductController(): Collection
    {
        return $this->productController;
    }

    /**
     * @param Collection $productController
     */
    public function setProductController(Collection $productController): void
    {
        $this->productController = $productController;
    }

    /**
     * @return Collection
     */
    public function getProductConnection(): Collection
    {
        return $this->productConnection;
    }

    /**
     * @param Collection $productConnection
     */
    public function setProductConnection(Collection $productConnection): void
    {
        $this->productConnection = $productConnection;
    }

    /**
     * @return int
     */
    public function getisBest(): int
    {
        return $this->isBest;
    }

    /**
     * @param int $isBest
     */
    public function setIsBest(int $isBest): void
    {
        $this->isBest = $isBest;
    }

    /**
     * @return int
     */
    public function getisOffer(): int
    {
        return $this->isOffer;
    }

    /**
     * @param int $isOffer
     */
    public function setIsOffer(int $isOffer): void
    {
        $this->isOffer = $isOffer;
    }

    /**
     * @return int
     */
    public function getisNew(): int
    {
        return $this->isNew;
    }

    /**
     * @param int $isNew
     */
    public function setIsNew(int $isNew): void
    {
        $this->isNew = $isNew;
    }

    /**
     * @return int
     */
    public function getSmart(): int
    {
        return $this->smart;
    }

    /**
     * @param int $smart
     */
    public function setSmart(int $smart): void
    {
        $this->smart = $smart;
    }

    /**
     * @return Collection
     */
    public function getConnection(): Collection
    {
        return $this->connection;
    }

    /**
     * @param Collection $connection
     */
    public function setConnection(Collection $connection): void
    {
        $this->connection = $connection;
    }

    /**
     * @return Collection
     */
    public function getController(): Collection
    {
        return $this->controller;
    }

    /**
     * @param Collection $controller
     */
    public function setController(Collection $controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @return int|null
     */
    public function getMinFt(): ?int
    {
        return $this->minFt;
    }

    /**
     * @param int|null $minFt
     */
    public function setMinFt(?int $minFt): void
    {
        $this->minFt = $minFt;
    }

    /**
     * @return int|null
     */
    public function getMaxFt(): ?int
    {
        return $this->maxFt;
    }

    /**
     * @param int|null $maxFt
     */
    public function setMaxFt(?int $maxFt): void
    {
        $this->maxFt = $maxFt;
    }

    public function getProductBuy()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null));
        if($criteria == null)
            return null;

        return $this->productBuy->matching($criteria)[0];
    }

    public function getProductBuyCountry($country)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("country", $country))
            ->andWhere(Criteria::expr()->eq("active", 1));
        if($criteria == null)
            return null;

        return $this->productBuy->matching($criteria)[0];
    }

    /**
     * @param Collection $productBuy
     */
    public function setProductBuy(Collection $productBuy): void
    {
        $this->productBuy = $productBuy;
    }

    /**
     * Get the value of isShowTru
     *
     * @return  int|null
     */ 
    public function getIsShowTru()
    {
        return $this->isShowTru;
    }

    /**
     * Set the value of isShowTru
     *
     * @param  int|null  $isShowTru
     *
     * @return  self
     */ 
    public function setIsShowTru($isShowTru)
    {
        $this->isShowTru = $isShowTru;

        return $this;
    }

    /**
     * Get the value of isOld
     *
     * @return  int|null
     */ 
    public function getIsOld()
    {
        return $this->isOld;
    }

    /**
     * Set the value of isOld
     *
     * @param  int|null  $isOld
     *
     * @return  self
     */ 
    public function setIsOld($isOld)
    {
        $this->isOld = $isOld;

        return $this;
    }

    /**
     * Get the value of ean
     *
     * @return  string|null
     */ 
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Set the value of ean
     *
     * @param  string|null  $ean
     *
     * @return  self
     */ 
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get the value of isLandingPage
     *
     * @return  int
     */ 
    public function getIsLandingPage()
    {
        return $this->isLandingPage;
    }

    /**
     * Set the value of isLandingPage
     *
     * @param  int  $isLandingPage
     *
     * @return  self
     */ 
    public function setIsLandingPage(int $isLandingPage)
    {
        $this->isLandingPage = $isLandingPage;

        return $this;
    }

    /**
     * Get the value of pp
     *
     * @return  string
     */ 
    public function getPp()
    {
        return $this->pp;
    }

    /**
     * Set the value of pp
     *
     * @param  string  $pp
     *
     * @return  self
     */ 
    public function setPp(string $pp)
    {
        $this->pp = $pp;

        return $this;
    }

    /**
     * Get the value of isPublish
     *
     * @return  int
     */ 
    public function getIsPublish()
    {
        return $this->isPublish;
    }

    /**
     * Set the value of isPublish
     *
     * @param  int  $isPublish
     *
     * @return  self
     */ 
    public function setIsPublish(int $isPublish)
    {
        $this->isPublish = $isPublish;

        return $this;
    }
}
