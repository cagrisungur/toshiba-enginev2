<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * File
 * @ORM\Table(name="series")
 * @ORM\Entity(repositoryClass="App\Repository\SeriesRepository")
 */
class Series
{
    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var int 
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority = "10";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="series" ,cascade={"persist", "remove"})
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="SeriesText", mappedBy="series")
     */
    private $text;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->text = new ArrayCollection();
    }

    /**
     * @return Collection|Product
     * @param $currentProductId
     */
    public function getProductAll($currentProductId): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("active", 1))
            ->andWhere(Criteria::expr()->neq("id", $currentProductId))
            ->andWhere(Criteria::expr()->groupBy("product"))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null ){
            return null;
        }

        return $this->product->matching($criteria);
    }

    /**
     * @return Collection|Product
     * @param $currentProductId
     */
    public function getProduct($currentProductId): ?Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("deletedAt", null))
            ->andWhere(Criteria::expr()->eq("active", 1))
            ->andWhere(Criteria::expr()->neq("id", $currentProductId))
            ->orderBy(array("priority"=>Criteria::ASC));

        if($criteria == null ){
            return null;
        }

        return $this->product->matching($criteria);
    }

    /**
     * @param Collection $product
     */
    public function setProduct(Collection $product): void
    {
        $this->product = $product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function getSeriesProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active): void
    {
        $this->active = $active;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|SeriesText[]|null
     */
    public function getLangText($lang): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('lang',$lang));
        if($criteria == null){
            return null;
        }

        return $this->text->matching($criteria);
    }


    /**
     * Get the value of priority
     *
     * @return  int
     */ 
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set the value of priority
     *
     * @param  int  $priority
     *
     * @return  self
     */ 
    public function setPriority(int $priority)
    {
        $this->priority = $priority;

        return $this;
    }
}
