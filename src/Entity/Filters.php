<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Filters
 * @ORM\Table(name="filters")
 * @ORM\Entity
 */
class Filters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
	 * @ORM\Column(name="priority", type="integer")
	 */
    private $priority;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
	 * Get the value of priority
	 */ 
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * Set the value of priority
	 *
	 * @return  self
	 */ 
	public function setPriority($priority)
	{
		$this->priority = $priority;

		return $this;
	}

    /**
     * Get the value of active
     *
     * @return  int
     */ 
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of active
     *
     * @param  int  $active
     *
     * @return  self
     */ 
    public function setActive(int $active)
    {
        $this->active = $active;

        return $this;
    }
}    