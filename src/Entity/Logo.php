<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 * 
 * @ORM\Table(name="logo")
 * @ORM\Entity(repositoryClass="App\Repository\LogoRepository")
 */
class Logo implements \JsonSerializable {
	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;

	/**
	 * @ORM\Column(name="link", type="string")
	 */
	private $link;

	/**
	 * @ORM\Column(name="name", type="string")
	 */
	private $name;

    /**
     * @ORM\Column(name="size", type="float")
     */
    private $size;

	/**
	 * @ORM\Column(name="thumbnail", type="string")
	 */
	private $thumbnail;

	/**
	 * @ORM\Column(name="priority", type="integer")
	 */
	private $priority;

	/**
	 * @ORM\Column(name="active", type="boolean")
	 */
	private $active;
	
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt ;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
	private $deletedAt;
	
	/**
	 * Get the value of id
	 *
	 * @return  int
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @param  int  $id
	 *
	 * @return  self
	 */ 
	public function setId(int $id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of link
	 */ 
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * Set the value of link
	 *
	 * @return  self
	 */ 
	public function setLink($link)
	{
		$this->link = $link;

		return $this;
	}

	/**
	 * Get the value of name
	 */ 
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set the value of name
	 *
	 * @return  self
	 */ 
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }


	/**
	 * Get the value of priority
	 */ 
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * Set the value of priority
	 *
	 * @return  self
	 */ 
	public function setPriority($priority)
	{
		$this->priority = $priority;

		return $this;
	}

	/**
	 * Get the value of active
	 */ 
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * Set the value of active
	 *
	 * @return  self
	 */ 
	public function setActive($active)
	{
		$this->active = $active;

		return $this;
	}

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     *
     * @return  \DateTime|null
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @param  \DateTime|null  $updatedAt
     *
     * @return  self
     */ 
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of deletedAt
     *
     * @return  \DateTime|null
     */ 
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set the value of deletedAt
     *
     * @param  \DateTime|null  $deletedAt
     *
     * @return  self
     */ 
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

	/**
	 * Get the value of thumbnail
	 */ 
	public function getThumbnail()
	{
		return $this->thumbnail;
	}

	/**
	 * Set the value of thumbnail
	 *
	 * @return  self
	 */ 
	public function setThumbnail($thumbnail)
	{
		$this->thumbnail = $thumbnail;

		return $this;
	}

	public function jsonSerialize() {
        return array(
            "id" => $this->id,
            "name" => $this->name,
            "thumbnail" => $this->thumbnail,
            "link" => $this->link,
            "size" => $this->size
        );
    }
}

?>