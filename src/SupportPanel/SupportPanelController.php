<?php


namespace App\SupportPanel;

use App\Entity\SupportUsers;
use App\Entity\FaqCategory;
use App\Entity\FaqCategoryContent;
use App\Entity\Language;
use App\Entity\Country;
use App\Entity\Faq;
use App\Entity\FaqContent;
use App\Entity\WarrantyProduct;
use App\Entity\SupportVideos;
use App\Entity\SupportVideoContents;
use App\Entity\WarrantyUser;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use \PDO; 

class SupportPanelController extends Controller
{

    public function supportPanelLogin(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $message = "";
        if(isset($_POST["support-login"])) {
            if(isset($_POST["username"]) && isset($_POST["password"])) {
                $username = $_POST["username"];
                $password = $_POST["password"];

                $userCheck = $em->getRepository(SupportUsers::class)->findOneBy(array(
                    "username" => $username,
                    "password" => md5($password),
                    "deletedAt" => null
                ));

                if($userCheck) {
                    //redirect
                    $session = new Session();
                    $session->start();

                    $session->set('support-user', $userCheck);

                    if($session->get('support-user')) {
                        return $this->redirectToRoute("support-panel-index");
                    }
                } else {
                    $message = "Password or username is wrong. Please Try Again";
                }
            }
        }
        
        return $this->render("support-panel/login.html.twig", array(
            "message" => $message
        ));
    }

    public function supportLogout(Request $request) {
        $session = $request->getSession();

        $session->clear();

        return $this->redirectToRoute("support-panel-login");
    }

    public function supportPanel(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        return $this->render("support-panel/dashboard.html.twig", array(
            "message" => "test"
        ));
    }

    public function getSupportFaqCategories(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        $em = $this->getDoctrine()->getManager();

        $faqCategories = $em->getRepository(FaqCategory::class)->findBy(array(
            "deletedAt" => null
        ));

        return $this->render("support-panel/faqCategoriesList.html.twig", array(
            "faqCategories" => $faqCategories
        ));
    }

   
    public function editFaqCats(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        if($_GET["id"] == "") {
            return $this->redirectToRoute("support-panel-faq-categories-list");
        } else {
            $em = $this->getDoctrine()->getManager();
            $faqCategory = $em->getRepository(FaqCategory::class)->find($_GET["id"]);

            $faqContent = $em->getRepository(FaqCategoryContent::class)->findBy(array(
                "faqCategory" => $faqCategory
            ));

        }

        $langs = $em->getRepository(Language::class)->findBy(array(
            "dummy" => 0,
            "deletedAt" => null
        ));

        $faqCategoryArray = array();
        foreach ($faqContent as $content) {
            $faqCategoryArray[$content->getLang()->getName()] = array(
                "id" => $content->getFaqCategory()->getId(),
                "lang_id" => $content->getLang()->getId(),
                "name" => $content->getName()
            );
        }

        if(isset($_POST["faq-submit"])) {
            if($_POST["translate"]) {
                $translates = $_POST["translate"];
                foreach($translates as $key => $value) {
                    $lang = $em->getRepository(Language::class)->find($key);

                    $editFaqCat = $em->getRepository(FaqCategoryContent::class)->findOneBy(array(
                        "faqCategory" => $faqCategory,
                        "lang" => $lang
                    ));

                    if($editFaqCat) {
                        $editFaqCat->setName($value);

                        $em->flush();
                    } else {
                        $editFaqCat = new FaqCategoryContent();

                        $editFaqCat->setFaqCategory($faqCategory);
                        $editFaqCat->setLang($lang);
                        $editFaqCat->setName($value);

                        $em->persist($editFaqCat);
                        $em->flush();
                    }

                }
                return $this->redirectToRoute("support-panel-faq-categories-list");

            }
        }

        return $this->render("support-panel/editFaqCategories.html.twig", array(
            "faqCategories" => $faqCategoryArray,
            "faqContent" => $faqContent,
            "langs" => $langs,
            "faqCategory" => $faqCategory
        ));
    }

    public function faqList(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository(Faq::class)->findBy(array(
            "deletedAt" => null 
        ));

        return $this->render("support-panel/faqList.html.twig", array(
            "faqs" => $faqs
        ));
    }

    public function editSupportFaqs(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        if($_GET["id"] == "") {
            return $this->redirectToRoute("support-faq-list");
        } else {
            $em = $this->getDoctrine()->getManager();
            $faq = $em->getRepository(Faq::class)->find($_GET["id"]);

            $faqContents = $em->getRepository(FaqContent::class)->findBy(array(
                "faq" => $faq
            ));

            $faqContentArray = array();

            foreach ($faqContents as $content) {
                $faqContentArray[$content->getLang()->getName()] = array(
                    "id" => $content->getFaq()->getId(),
                    "question" => $content->getQuestion(),
                    "answer" => $content->getAnswer()
                );
            }

        }
        $langs = $em->getRepository(Language::class)->findBy(array(
            "dummy" => 0,
            "deletedAt" => null
        ));

        //print_r($langArray);exit;
        if(isset($_POST["faq-submit"])) {
            if(isset($_POST["translate"])) {
                $translates  = $_POST["translate"];
                // print_r($translates);exit;

                foreach($translates as $key => $value) {
                    $lang = $em->getRepository(Language::class)->find($key);

                    $editFaq = $em->getRepository(FaqContent::class)->findOneBy(array(
                        "faq" => $faq,
                        "lang" => $lang
                    ));

                    if($editFaq) {
                        $editFaq->setQuestion($value["question"]);
                        $editFaq->setAnswer($value["answer"]);

                        $em->flush();
                    } else {
                        $editFaq = new FaqContent();

                        $editFaq->setFaq($faq);
                        $editFaq->setLang($lang);
                        $editFaq->setQuestion($value["question"]);
                        $editFaq->setAnswer($value["answer"]);

                        $em->persist($editFaq);
                        $em->flush();
                    }
                }

                return $this->redirectToRoute("support-faq-list");
            }
        }

        return $this->render("support-panel/editFaqs.html.twig", array(
            "faq" => $faq,
            "faqContents" => $faqContentArray,
            "langs" => $langs
        ));
    }

    public function confirmProductRegistration(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        $em = $this->getDoctrine()->getManager();

        $warrantyProducts = $em->getRepository(WarrantyProduct::class)->findBy(array(
            "status" => 0,
            "deletedAt" => null
        ));

        return $this->render("support-panel/confirmProductRegister.html.twig", array(
            "products" => $warrantyProducts
        ));
    }

    public function approveProductRegistration(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        if($_GET["id"] == "") {
            return $this->redirectToRoute("support-confirm-registration");
        } else {
            $em = $this->getDoctrine()->getManager();
            $warrantyProduct = $em->getRepository(WarrantyProduct::class)->find($_GET["id"]);

            if($warrantyProduct) {
                $warrantyProduct->setStatus(1);

                $em->flush();
            }
        }

        return $this->redirectToRoute("support-confirm-registration");
    }

    public function approvedProductList(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        $em = $this->getDoctrine()->getManager();

        $warrantyProducts = $em->getRepository(WarrantyProduct::class)->findBy(array(
            "status" => 1,
            "deletedAt" => null
        ));

        return $this->render("support-panel/approvedProductList.html.twig", array(
            "products" => $warrantyProducts
        ));
          
    }


    public function editSupportProduct(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        if($_GET["id"] == "") {
            return $this->redirectToRoute("approved-products");
        } else {
            $em = $this->getDoctrine()->getManager();
            $warrantyProduct = $em->getRepository(WarrantyProduct::class)->find($_GET["id"]);

            if($warrantyProduct) {
                $warrantyProduct->setStatus(1);

                $em->flush();
            }
        }

        if(isset($_POST["product-submit"])) {
            $warrantyUser = $em->getRepository(WarrantyUser::class)->find($warrantyProduct->getUser()->getId());
            $warrantyUser->setName($_POST["username"]);
            $time = strtotime($_POST["datetime"]);
            $myDateTime = \DateTime::createFromFormat('d-m-Y', $_POST["datetime"]);
            
            
            $warrantyUser->setBirthDate($myDateTime);
            $warrantyUser->setPhone($_POST["phone"]);
            $warrantyUser->setEmail($_POST["email"]);
            $warrantyUser->setAddress($_POST["address"]);
            $warrantyUser->setZipCode($_POST["zipcode"]);

            $country = $em->getRepository(Country::class)->find($_POST["country"]);

            $warrantyUser->setCountry($country);
            
            
            $em->persist($warrantyUser);
            $em->flush();

        }
        $countries = $em->getRepository(Country::class)->findBy(array(
            "deletedAt" => null,
        ));

        return $this->render("support-panel/editRegisteredProduct.html.twig", array(
            "product" => $warrantyProduct,
            "countries" => $countries
        ));
    }

    public function surveyList(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        $countries = $this->query("SELECT id,name FROM support_countries WHERE deleted= 0 ORDER BY name ASC");

        $professions = $this->query("SELECT id,name FROM support_profession");
        $educations = $this->query("SELECT id,name FROM support_education");

        if(isset($_POST['selectData'])) {
            $where = "";
            $country = $_POST['countryData'];
        
            // if($auth["group_id"] > 2 && isset($auth["country_id"]) && ($auth["country_id"] != 0 || $auth["country_id"] != "") ) {
            //     $country = $auth["country_id"];
            // }
        
            $education = $_POST['educationData'];
            $profession = $_POST['professionData'];
            if($country != ""){
                $where .= " AND (d.country_id =".$country." OR d.country_id is null) ";
            }
            if($education != ""){
                $where .= " AND (d.education =".$education." OR d.education is null) ";
            }
            if($profession != ""){
                $where .= " AND (d.job =".$profession." OR d.job is null) ";
            }
            $surveySql = "SELECT a.*,
            ( SELECT COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id".$where." ) as sum,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 1".$where." ) as male,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 1".$where." ) as male_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 0".$where." ) as female,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 0".$where." ) as female_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 2".$where." ) as disclosed,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 2".$where." ) as disclosed_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$where." ) as under24,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$where." ) as under24_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$where." ) as under44,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$where." ) as under44_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$where." ) as above45,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$where." ) as above45_count
            FROM ( SELECT a.question, a.question_type, b.answer, a.id as question_id, b.id as answer_id, COUNT(d.id) as cnt
            FROM warranty_questions a LEFT JOIN warranty_answers b ON b.question_id = a.id LEFT JOIN warranty_survey c ON c.question_id = a.id AND c.answer_id = b.id LEFT JOIN warranty_user d ON d.id = c.user_id WHERE 1=1".$where." group by a.id, b.id ) a WHERE a.question_type != 2";
            
            $surveySql2 = "SELECT a.*,
            ( SELECT COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id".$where." ) as sum,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 1".$where." ) as male,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 1".$where." ) as male_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 0".$where." ) as female,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 0".$where." ) as female_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 2".$where." ) as disclosed,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 2".$where." ) as disclosed_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$where." ) as under24,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$where." ) as under24_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$where." ) as under44,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$where." ) as under44_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$where." ) as above45,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$where." ) as above45_count
            FROM ( SELECT a.question, a.question_type, b.answer, a.id as question_id, b.id as answer_id, COUNT(d.id) as cnt
            FROM warranty_questions a LEFT JOIN warranty_answers b ON b.question_id = a.id LEFT JOIN warranty_survey c ON c.question_id = a.id LEFT JOIN warranty_user d ON d.id = c.user_id WHERE 1=1".$where." group by a.id, b.id ) a WHERE a.question_type = 2";
          
            $union = $surveySql . " UNION ALL " . $surveySql2;
            $survey = $this->query($union);
            $output = array();
            
            foreach($survey as $item) {
                if(!isset($output[$item['question_id']])) {
                    $output[$item['question_id']] = array();
                }
                $output[$item['question_id']][] = $item;        
            }
            
            echo json_encode($output);
            exit;
        }else if(isset($_POST['question'])) {
            $countrySql = "";
        
            // if($auth["group_id"] > 2 && isset($auth["country_id"]) && ($auth["country_id"] != 0 || $auth["country_id"] != "") ) {
            //     $countrySql = " AND (d.country = ".$auth["country_id"]." OR d.country is null )";
            // }
        
            $surveySql = "SELECT a.*,
            ( SELECT COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id".$countrySql." ) as sum,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 1".$countrySql." ) as male,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 1".$countrySql." ) as male_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 0".$countrySql." ) as female,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 0".$countrySql." ) as female_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 2".$countrySql." ) as disclosed,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and d.gender = 2".$countrySql." ) as disclosed_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45_count
            FROM ( SELECT a.question, a.question_type, c.text_answer as answer, a.id as question_id, c.id as answer_id, COUNT(d.id) as cnt
            FROM warranty_questions a LEFT JOIN warranty_survey c ON c.question_id = a.id LEFT JOIN warranty_user d ON d.id = c.user_id WHERE (c.answer_id is null or (c.answer_id is not null and c.text_answer is not null))".$countrySql." group by a.id, c.id ) a";
        
            $survey = $this->query($surveySql);
            
            $output = array();
            foreach($survey as $item) {
                if(!isset($output[$item['question_id']])) {
                    $output[$item['question_id']] = array();
                }
                $output[$item['question_id']][] = $item;        
            }
            
        
            echo json_encode($output[$_POST['question']]);
            exit;
        } else {
            $countrySql = "";
        
            // if($auth["group_id"] > 2 && isset($auth["country_id"]) && ($auth["country_id"] != 0 || $auth["country_id"] != "") ) {
            //     $countrySql = " AND (d.country = ".$auth["country_id"]." OR d.country is null )";
            // }
        
            $surveySql = "SELECT a.*,
            ( SELECT COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id".$countrySql." ) as sum,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 1".$countrySql." ) as male,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 1".$countrySql." ) as male_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 0".$countrySql." ) as female,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 0".$countrySql." ) as female_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 2".$countrySql." ) as disclosed,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and d.gender = 2".$countrySql." ) as disclosed_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and c.answer_id = a.answer_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45_count
            FROM ( SELECT a.question, a.question_type, b.answer, a.id as question_id, b.id as answer_id, COUNT(d.id) as cnt
            FROM warranty_questions a LEFT JOIN warranty_answers b ON b.question_id = a.id LEFT JOIN warranty_survey c ON c.question_id = a.id AND c.answer_id = b.id LEFT JOIN warranty_user d ON d.id = c.user_id WHERE 1=1".$countrySql." group by a.id, b.id ) a WHERE a.question_type != 2";
        
            $surveySql2 = "SELECT a.*,
            ( SELECT COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id".$countrySql." ) as sum,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 1".$countrySql." ) as male,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 1".$countrySql." ) as male_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 0".$countrySql." ) as female,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 0".$countrySql." ) as female_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 2".$countrySql." ) as disclosed,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and d.gender = 2".$countrySql." ) as disclosed_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 24".$countrySql." ) as under24_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 25 and TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) <= 44".$countrySql." ) as under44_count,
            ( SELECT  100 * COUNT(c.id) / a.cnt FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45,
            ( SELECT  COUNT(c.id) FROM warranty_survey c INNER JOIN warranty_user d ON d.id = c.user_id WHERE c.question_id = a.question_id AND TIMESTAMPDIFF(YEAR, d.birth_date, CURDATE()) >= 45".$countrySql." ) as above45_count
            FROM ( SELECT a.question, a.question_type, b.answer, a.id as question_id, b.id as answer_id, COUNT(d.id) as cnt
            FROM warranty_questions a LEFT JOIN warranty_answers b ON b.question_id = a.id LEFT JOIN warranty_survey c ON c.question_id = a.id LEFT JOIN warranty_user d ON d.id = c.user_id WHERE 1=1".$countrySql." group by a.id ) a WHERE a.question_type = 2";
        
            $survey = $this->query($surveySql . " UNION ALL " . $surveySql2);
            
            $output = array();
            foreach($survey as $item) {
                if(!isset($output[$item['question_id']])) {
                    $output[$item['question_id']] = array();
                }
                $output[$item['question_id']][] = $item;        
            }
            
        }
        
        $module = 'survey-list';
        $title = 'Survey Result';
        // $answer['male'] = 5;
        // $answer['male_count'] = 24;
        // echo "%". number_format((float)$answer['male'], 2, '.', '')." (".$answer['male_count'].")";
        // echo '<br>';exit;
        //print_r($output);exit;
        return $this->render("support-panel/survey-list.html.twig", array(
            "countries" => $countries,
            "professions" => $professions,
            "educations" => $educations,
            "outputs" => $output
        ));
    }

    public function registrationResult(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        // $em = $this->getDoctrine()->getManager();
        // $countries = $em->getRepository(Country::class)->findBy(array(
        //     "deletedAt" => null,
        // ));

        $sql_country = $this->query("SELECT a.*, count(a.id) FROM warranty_user a group by a.country_id");

        $sql_city_month = $this->pdoQuery("SELECT p.*, c.name as country, COUNT(u.country_id) as count FROM warranty_product p INNER JOIN warranty_user as u on u.id=p.user_id INNER JOIN support_countries as c on c.id=u.country_id WHERE DATE(p.created_at) > (NOW() - INTERVAL 30 DAY) GROUP BY u.country_id");
        $sql_city_week = $this->pdoQuery("SELECT p.*, c.name as country, COUNT(u.country_id) as count FROM warranty_product p INNER JOIN warranty_user as u on u.id=p.user_id INNER JOIN support_countries as c on c.id=u.country_id WHERE DATE(p.created_at) > (NOW() - INTERVAL 7 DAY) GROUP BY u.country_id");
        $sql_city_day = $this->pdoQuery("SELECT p.*, c.name as country, COUNT(u.country_id) as count FROM warranty_product p INNER JOIN warranty_user as u on u.id=p.user_id INNER JOIN support_countries as c on c.id=u.country_id WHERE DATE(p.created_at) > (NOW() - INTERVAL 1 DAY) GROUP BY u.country_id");
        $sql = $sql_city_day;

        if(isset($_POST['choiceData'])) {
            if($_POST['choiceData'] == 1)
                $sql= $sql_city_week;
            else if($_POST['choiceData'] == 2)
                $sql = $sql_city_month;
            

                // foreach($sql as $t) {
                //     echo json_encode($t);
                // }
             echo json_encode($sql);
            exit;
        }

        return $this->render("support-panel/registration-result.html.twig", array(
            "test" => "test"
        ));
    }

    public function addFaqCategory(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        $em = $this->getDoctrine()->getManager();
        $langs = $em->getRepository(Language::class)->findBy(array(
            "dummy" => 0,
            "deletedAt" => null
        ));

        $message = "";

        if(isset($_POST["faq-submit"])) {
            $faqCategory = new FaqCategory();

            $faqCategory->setName($_POST["name"]);

            $em->persist($faqCategory);
            $em->flush();
            //print_r($faqCategory->getId());exit;
            $message = 'FAQ Category is added, go to edit for more languages. <a href="/edit-faq-category?id='.$faqCategory->getId().'" class="nav-link">Add More Languages</a>';

        }

        return $this->render("support-panel/addFaqCategories.html.twig", array(
            "langs" => $langs,
            "message" => $message
        ));
    }

    public function supportVideoList(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }

        $em = $this->getDoctrine()->getManager();

        $lang = $em->getRepository(Language::class)->find(2);

        $videoList = $em->getRepository(SupportVideos::class)->findBy(array(
            "deletedAt" => null
        ));

        $videoArray = array();
        foreach($videoList as $video) {
            $videoArray[] = array(
                "id" => $video->getId(),
                "title" => $video->getTitle(),
                "url" => $video->getVideo(),
                "category" => $video->getVideoFaqCategory()->getName()
            );
        }

        return $this->render("support-panel/videoList.html.twig", array(
            "videos" => $videoArray
        ));
    }

    public function editVideo(Request $request) {
        $session = $request->getSession();
        if(!$session) {
            session_start();
        }

        if($session->get('support-user') == "") {
            return $this->redirectToRoute("support-panel-login");
        }
        if($_GET["id"] == "") {
            return $this->redirectToRoute("video-list");
        } else {
            $em = $this->getDoctrine()->getManager();
            $supportVideo = $em->getRepository(SupportVideos::class)->find($_GET["id"]);

            $videoContents = $em->getRepository(SupportVideoContents::class)->findBy(array(
                "supportVideosText" => $supportVideo
            ));
        }

        $videoArray = array();
        foreach ($videoContents as $content) {
            $videoArray[$content->getLang()->getName()] = array(
                "title" => $content->getTitle(),
                "lang_id" => $content->getLang()->getId(),
                "description" => $content->getDescription(),
                "thumbnail" => $content->getThumbnail(),
                "url" => $content->getVideo(),
            );
        }
        //print_r($videoArray);exit;
        $langs = $em->getRepository(Language::class)->findBy(array(
            "dummy" => 0,
            "deletedAt" => null
        ));
        

        if(isset($_POST["video-submit"])) {
            if($_POST["translate"]) {
                $translates = $_POST["translate"];
                //print_r($translates);exit;
                foreach($translates as $key => $value) {
                    if($value["video"] != "") {
                        $lang = $em->getRepository(Language::class)->find($key);

                        $supportVideoContent = $em->getRepository(SupportVideoContents::class)->findOneBy(array(
                            "supportVideosText" => $supportVideo,
                            "lang" => $lang
                        ));
    
                        if($supportVideoContent) {
                            $supportVideoContent->setDescription($value["description"]);
                            $supportVideoContent->setTitle($value["title"]);
                            $supportVideoContent->setVideo($value["video"]);
    
                            $em->flush();
                        } else {
                            $newVideoContent = new SupportVideoContents();
    
                            $newVideoContent->setSupportVideosText($supportVideo);
                            $newVideoContent->setDescription($value["description"]);
                            $newVideoContent->setTitle($value["title"]);
                            $newVideoContent->setVideo($value["video"]);
                            $newVideoContent->setLang($lang);
    
                            $em->persist($newVideoContent);
                            $em->flush();
                        }
                    }

                }
                return $this->redirectToRoute("video-list");

            }
        }

        return $this->render("support-panel/editVideo.html.twig", array(
            "langs" => $langs,
            "video" => $_GET["id"],
            "videoArray" => $videoArray,
            "deletedAt" => null
        ));

        
    }

    public function query($query){
        $servername = "localhost";
        $username = "root";
        $password = "lacivert123";

        $conn = mysqli_connect($servername, $username, $password, 'toshibav13');

        $result =  $conn->query($query);
        
        return $result;
    }

    public function pdoQuery($query) {
        $servername = "localhost";
        $username = "root";
        $password = "lacivert123";

        $conn = new PDO("mysql:host=$servername;dbname=toshibav13", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = $conn->prepare($query);
        $result->execute();

        $results = $result->fetchAll(PDO::FETCH_ASSOC);
        
        return $results;
    }
}            