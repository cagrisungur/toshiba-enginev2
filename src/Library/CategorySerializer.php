<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 24.07.2018
 * Time: 11:52
 */

namespace App\Library;


use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Serializer\SerializerInterface;


class CategorySerializer implements SerializerInterface
{
    private $country;
    private $lang;




        function isExistChild($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT count(id) as say FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            return (($stmt->fetchAll())[0]["say"]) > 0 ? true:false;
        }



        function getChilds($id){
            $conn = $this->getDoctrine()->getConnection();
            $sql = 'SELECT id FROM category where parent_id='.intval($id);
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();

            $all = array();
            foreach($arr as $row){
                $all[] = $row['id'];
                if($this->isExistChild($row['id'])){
                    $res = $this->getChilds($row['id']);
                    foreach( $res as $inRow){
                        $all[] = $inRow;
                    }

                }

            }
            return $all;//
        }




    public function serialize($data, $format, array $context = array(),$secondData=array())
    {
        $dataArray = [];
        $productArray = [];

        /**
         * @var Category $category
         */
        foreach ($data as $category) {
            $categoryArr = $this->serializeProperty($category);
            foreach ($category->getImage() as $img){
                $categoryArr["images"][$img->getType()->getCode()][] = array(
                    "name" => $img->getName(),
                    "priority" => $img->getPriority(),
                    "path" => $img->getPath()
                );
            }
            $categoryArr["child"] = array();
            $categoryArr["child"] = $this->findChild($category);
            // foreach ($category->getChild() as $parent) {
            //     if($this->checkAvailable($parent, $this->country)) {
            //         $tmp2 = array(
            //             "id" => $parent->getId(),
            //             "name" => $parent->getName(),
            //             "link" => $parent->getLink(),
            //             "parentId" => $parent->getParentId()
            //         );
            //         foreach ($parent->getImage() as $childImage) {
            //             $tmp2["image"][$childImage->getType()->getCode()][] = array(
            //               "imagePath" => $childImage->getPath()
            //             );
            //         }
            //         foreach ($parent->getCatText($this->country, $this->lang) as $catText) {
            //             $tmp2["categoryText"] = array(
            //                 "name" => $catText->getName(),
            //                 "shortDescription" => $catText->getShortDescription(),
            //                 "longDescription" => $catText->getLongDescription()
            //             );
            //         }
            //         $categoryArr["child"][] = $tmp2;
            //     }
            // }

            $categoryArr["feature"] = array();

            $catFeautre = $category->getFeature();
            foreach ($catFeautre as $catFeature) {
                $newTmp = array(
                    "id" => $catFeature->getFeature()->getId(),
                    "name" => $catFeature->getFeature()->getName(),
                );

                foreach ($catFeature->getFeature()->getText($this->country, $this->lang) as $featureText) {
                    $newTmp["text"] = array(
                        "name" => $featureText->getName(),
                        "shortDesc" => $featureText->getShortDescription(),
                        "longDesc" => $featureText->getLongDescription()
                    );
                }

                foreach ($catFeature->getFeature()->getImage() as $fImage) {
                    $newTmp["images"][$fImage->getType()->getCode()][] = array(
                        "path" => $fImage->getPath(),
                    );
                }
                $categoryArr["feature"][] = $newTmp;
            }

            $categoryArr["text"] = array();
            $textList = $category->getCatText($this->country,$this->lang);

            foreach ($textList as $text) {
                $categoryArr["text"][] = array(
                    "name" => $text->getName(),
                    "shortDescription" => $text->getShortDescription(),
                    "longDescription" => $text->getLongDescription(),
                    "subDescription" => $text->getSubDescription(),
                    "subTitle" => $text->getSubtitle(),
                    "buttonName" => $text->getButton(),
                    "buttonLink" => $text->getButtonLink(),
                    "link" => $text->getLink(),
                    "keywords" => $text->getKeywords()
                );
            }

            if(count($data) == 1) {



                /*getRepository(Category::class)->findBy(
                    array(
                        "link" => $parentId,
                        "active" => 1,
                        "deletedAt" => null
                    ),
                    array("priority" => "ASC")
                );*/


                $x=0;

                if(($secondData) != array()){
                    $catProd = $secondData['em']->getManager()->getRepository(Product::class)->getProductByIdArray($secondData['em'],$secondData['data']);
                        //die("asdsad");
                }else{

                    $catProd = $data[0]->getProduct();
                }
                //$catProd = $category->getProduct();


            //    echo count($catProd);
                foreach ($catProd as $prod) {
                    if($prod->getCountryProduct($this->country)[0] != null){
//                        print_r($prod->getCountryProduct($this->country)[0]->getProduct()->getSize());exit;
                        $sizeText = "";
                        if ($prod->getCountryProduct($this->country)[0]->getProduct()->getLangText($this->country,$this->lang)[0]){
                            $sizeText = $prod->getCountryProduct($this->country)[0]->getProduct()->getLangText($this->country,$this->lang)[0]->getSize();
                        }

                        else if(!$sizeText || $sizeText == "") {
                            $sizeText = $prod->getCountryProduct($this->country)[0]->getProduct()->getSize();
                        }

                        $tmp = array(
                            "id" => $prod->getCountryProduct($this->country)[0]->getProduct()->getId(),
                            "name" => $prod->getCountryProduct($this->country)[0]->getProduct()->getName(),
                            "link" => $prod->getCountryProduct($this->country)[0]->getProduct()->getLink(),
                            "size" => $sizeText,
                            "code" => $prod->getCountryProduct($this->country)[0]->getProduct()->getCode(),
                            "priority" => $prod->getCountryProduct($this->country)[0]->getProduct()->getPriority(),
                            "createdAt" => $prod->getCountryProduct($this->country)[0]->getProduct()->getCreatedAt()
                        );

                        $tmp["text"] = array();
                        $highlightedTextList = $prod->getCountryProduct($this->country)[0]->getProduct()->getHighlightedText();
                        foreach ($highlightedTextList as $highlightedText) {
                            foreach ($highlightedText->getTranslate($this->country, $this->lang) as $trans) {
                                $tmp["text"][] = array(
                                    "text" => $trans->getText(),
                                );
                            }
                        }

                        $tmp ["images"] = array();
                        $imageList = $prod->getImages();
                        foreach ($imageList as $image) {
                            if($image->getPriority()==0) {
                                $tmp["images"][$image->getType()->getCode()] = array(
                                    "name" => $image->getName(),
                                    "path" => $image->getPath()
                                );
                            }
                        }

                        if($prod->getProductBuyCountry($this->country)) {
                            $tmp["buyNowLink"] = $prod->getProductBuyCountry($this->country)->getLink();
                        }else {
                            $tmp["buyNowLink"] = null;
                        }

                        $productArray[] = $tmp;
                    }
                }

                $dataArray["products"] = $productArray;
            }

            $dataArray["categories"][] = $categoryArr;
        }


        return json_encode($dataArray);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }

    private function checkAvailable($category, $country) {
        foreach($category->getProduct() as $product) {
            if(count($product->getCountryProduct($country)) > 0) {
                return true;
            }
        }

        foreach($category->getChild() as $subCategory) {
            if($this->checkAvailable($subCategory, $country)) {
                return true;
            }
        }

        return false;
    }

    private function findChild($category) {
        $dataArray = array();

        foreach ($category->getChild() as $parent) {
            if($this->checkAvailable($parent, $this->country)) {
                $tmp2 = array(
                    "id" => $parent->getId(),
                    "name" => $parent->getName(),
                    "link" => $parent->getLink(),
                    "parentId" => $parent->getParentId()
                );
                foreach ($parent->getImage() as $childImage) {
                    $tmp2["image"][$childImage->getType()->getCode()][] = array(
                      "imagePath" => $childImage->getPath()
                    );
                }
                foreach ($parent->getCatText($this->country, $this->lang) as $catText) {
                    $tmp2["categoryText"] = array(
                        "name" => $catText->getName(),
                        "shortDescription" => $catText->getShortDescription(),
                        "longDescription" => $catText->getLongDescription()
                    );
                }

                if(count($parent->getChild()) > 0) {
                    $tmp2["child"] = $this->findChild($parent);
                }

                $dataArray[] = $tmp2;
            }
        }

        return $dataArray;
    }
}
