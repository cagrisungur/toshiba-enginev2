<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 5.12.2018
 * Time: 11:59
 */

namespace App\Library;


use App\Entity\Faq;
use Symfony\Component\Serializer\SerializerInterface;

class FaqSerializer implements SerializerInterface
{
    private $country;
    private $lang;

    public function serialize($data, $format, array $context = array())
    {
        $dataArray = array();
        /**
         * @var Faq $faq
         */
        foreach ($data as $faq){
            if(!in_array($faq->getType()->getCode(), array_keys($dataArray))) {
                $dataArray[$faq->getType()->getCode()] = array();
                $dataArray[$faq->getType()->getCode()]["name"] = $faq->getType()->getName();
            }
            if (isset($faq->getText($this->country, $this->lang)[0])) {
                $tmp = array(
                    "question" => $faq->getText($this->country, $this->lang)[0]->getQuestion(),
                    "lang" => $faq->getText($this->country, $this->lang)[0]->getAnswer()
                );

                $dataArray[$faq->getType()->getCode()]["faq"][] = $tmp;
            }
        };

        return json_encode($dataArray);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}