<?php

namespace App\Library;

use App\Entity\StaticText;
use Symfony\Component\Serializer\SerializerInterface;

class StaticTextSerializer implements SerializerInterface
{
    private $country;
    private $lang;

    public function serialize($data, $format, array $context = array())
    {
        $dataArray = array();
        $tmp = [];
        /**
         * @var StaticText $statictext
         */
        foreach ($data as $statictext){
            $translate = $statictext->getStaticTextTranslate($this->country, $this->lang);
            foreach ($translate as $translated) {
                $tmp[$statictext->getCode()] = $translated->getData();
            }
        }
        $dataArray = $tmp;
        $upperCaseArr = array_change_key_case($dataArray, CASE_UPPER);

        return json_encode($upperCaseArr);
    }


    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}