<?php

namespace App\Library;

use App\Entity\CustomPage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class CustomPageSerializer implements SerializerInterface
{
    private $country;
    private $lang;

    public function serialize($data, $format, array $context = array())
    {
        $dataArray = array();
        $tmp = array();

        $text = "";

        /**
         * @var CustomPage $customPage
         */
        foreach ($data as $customPage){
            $pageText =$customPage->getNewPageText($this->country, $this->lang);
            if ($pageText[0]) {
                $text =  $pageText[0]->getData();
            } else {
                $text = $customPage->getPageText()[0]->getData();
            }
            $tmp[$customPage->getType()->getCode()] = array(
                "data" => htmlspecialchars($text, ENT_QUOTES, 'UTF-8'),
                "path"  => $customPage->getPath(),
                "thumbNailPath"  => $customPage->getThumbNailPath()
            );
        }
        $dataArray[] = $tmp;

        return $dataArray;
    }


    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}