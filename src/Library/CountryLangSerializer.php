<?php

namespace App\Library;

use App\Entity\Country;
use App\Entity\Language;
use App\Entity\ProductCountry;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\Serializer\SerializerInterface;

class CountryLangSerializer implements SerializerInterface
{
    public function serialize($data, $format, array $context = array())
    {
        $dataArray = [];
        /**
         * @var Country $country
         */
        foreach ($data as $country)
        {
            $countryArray = $this->serializeProperty($country);
            $countryArray["lang"] = array();
            $countryLang = $country->getCountryLang();
            foreach ($countryLang as $lang) {
                $l = $lang->getLang();
                $array = array(
                    "id" => $l->getId(),
                    "name" => $l->getName(),
                    "origName" => $l->getOrigName(),
                    "code" => $l->getCode(),
                    "main" => $lang->getMain(),
                    "priority" => $lang->getPriority()
                );
                $countryArray["lang"][] = $array;
            }

            $dataArray[] = $countryArray;
        }
        return json_encode($dataArray);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

}