<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 29.06.2018
 * Time: 14:16
 */

namespace App\Library;


use App\Document\Logs;
use App\Entity\ProductCountry;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\Category;
use App\Entity\StaticText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomController extends Controller
{
    public $country;
    public $lang;
    public $globalCountry;
    public $globalLang;
    public $staticText;
    public $category;

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {

        parent::setContainer($container);

        $sessionId=substr(number_format(time() * rand(),0,'',''),0,32);

        $headerRequest = $this->container->get("request_stack")->getCurrentRequest();

        $tokenString = str_replace("Bearer ", "", $headerRequest->headers->get("authorization"));

        if($tokenString != null) {
            $jwtManager = $this->container->get("lexik_jwt_authentication.jwt_manager");
            $tokenObj = new JWTUserToken(array(), null, $tokenString, null);
            $token = $jwtManager->decode($tokenObj);

        }else $token = null;

        $clientIp = $this->container->get("request_stack")->getCurrentRequest()->getClientIp();

        $response = new Response();
        $response->prepare($headerRequest);

        // $logs = new Logs();

        // if (isset($token["username"]) !== null) {
        //     $logs->setName("");
        // } else {
        //     $logs->setName($token["username"]);
        // }
        // if (!$headerRequest->headers->get("authorization")) {
        //     $logs->setToken("");
        // } else {
        //     $logs->setToken($tokenString);
        // }
        // $logs->setCreatedAt(new \DateTime());
        // $logs->setIp($clientIp);
        // $logs->setSessionId($sessionId);
        // $logs->setResponse($response->getStatusCode());
        // $logs->setPathInfo($headerRequest->getPathInfo());

        // $em = $this->get('doctrine_mongodb')->getManager();
        // $em->persist($logs);
        // $em->flush();

        $em = $this->getDoctrine()->getManager();
        $countryCode = $headerRequest->get('country');
        $langCode = $headerRequest->get('language');

        $countryObj = $em->getRepository(Country::class)->findOneBy(array(
            "code" => $countryCode,
            "deletedAt" => null
        ));

        $langObj = $em->getRepository(Language::class)->findOneBy(array(
            "code" => $langCode,
            "deletedAt" => null
        ));

        $globalLangObj = $em->getRepository(Language::class)->find(37);
        $globalCountryObj = $em->getRepository(Country::class)->find(33);


        //category
        $categories = $em->getRepository(Category::class)->findBy(array(
            "deletedAt" => null,
            "isFeatured" => 1
        ));
        $categoryArr = array();
        foreach ($categories as $category) {
                //  $prodArr = array();
                 $textList = $category->getCatText($langObj);
                // foreach($category->getProduct() as $prod) {
                //     if($prod->getProductBuyCountry($countryObj)) {
                //         $prodArr[] = array(
                //             "prod" => $prod->getName()
                //         );
                //     } 
                // }
                // print_r($prodArr);exit;
                // if(count($prodArr) > 0) {
                //     $prod = $prodArr[0]["prod"];
                // } else {
                //     $prod = "";
                // }
                foreach ($textList as $text) {
                    $categoryArr[] = array(
                        "name" => $text->getName(),
                        "shortDescription" => $text->getShortDescription(),
                        "longDescription" => $text->getLongDescription(),
                        "subDescription" => $text->getSubDescription(),
                        "subTitle" => $text->getSubtitle(),
                        "v1" => $text->getV1(),
                        "v2" => $text->getV2(),
                        "v3" => $text->getV3(),
                        "link" => $category->getLink(),
                        "code" => $category->getCode(),
                        "keywords" => $text->getKeywords(),
                        
                    );
            }
            
        }

        $this->category = $categoryArr;
        //print_r($categoryArr);exit;
        $staticTextObj = $em->getRepository(StaticText::class)->findBy(array(
            "deletedAt" => null
        ));


        $dataArray = array();
        $tmp = [];
        /**
         * @var StaticText $statictext
         */


         // first of all, get global language translations
         foreach ($staticTextObj as $statictext){
             $lang = $em->getRepository(Language::class)->find(2);

             $translate = $statictext->getStaticTextTranslate($lang); // 2-en lang
             foreach ($translate as $translated) {
                 $tmp[$statictext->getCode()] = $translated->getData();
             }
         }



         // secondly, get local translations
         foreach ($staticTextObj as $statictext){
             $translate = $statictext->getStaticTextTranslate($langObj);
             foreach ($translate as $translated) {
                 $tmp[$statictext->getCode()] = $translated->getData();
             }
         }




         /*
        foreach ($staticTextObj as $statictext){
            $translate = $statictext->getStaticTextTranslate($langObj);
            foreach ($translate as $translated) {
                $tmp[$statictext->getCode()] = $translated->getData();
            }
        }*/

        $dataArray = $tmp;
        $upperCaseArr = array_change_key_case($dataArray, CASE_UPPER);

        $this->staticText = $upperCaseArr;



        $this->country = $countryObj;
        $this->lang = $langObj;

        $this->globalCountry = $globalCountryObj;
        $this->globalLang = $globalLangObj;
    }
}
