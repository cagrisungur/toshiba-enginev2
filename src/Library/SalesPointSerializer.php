<?php

namespace App\Library;

use App\Entity\SalesPoint;
use Symfony\Component\Serializer\SerializerInterface;

class SalesPointSerializer implements SerializerInterface
{
    public function serialize($data, $format, array $context = array())
    {
        $tmp = [];
        /**
         * @var SalesPoint $salesPoint
         */
        foreach ($data as $salesPoint){
            $country = $salesPoint->getCountry();
            /*            $pointArray[$country->getName()] = array (
                            "id" => $country->getId(),
                            "name" => $country->getName(),
                            "code" => $country->getCode(),
                            "priority" => $country->getPriority(),
                            "origName" => $country->getOrigName()
                        );*/
            $pointArray = $this->serializeProperty($salesPoint);
            $pointArray['continent'] = array(
                "id" =>  $country->getContinent()->getId(),
                "name" => $country->getContinent()->getName()
            );
            $pointArray['country'] = $country->getName();

            $tmp[] = $pointArray;
        }

        return json_encode($tmp);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);
            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }
        }
        return $dataArray;
    }

}