<?php
/**
 * Created by PhpStorm.
 * User: Boyoz Mosaic
 * Date: 24.07.2018
 * Time: 11:52
 */

namespace App\Library\Panel;


use App\Entity\Category;
use Symfony\Component\Serializer\SerializerInterface;


class AdminCategorySerializer implements SerializerInterface
{
    public function serialize($data, $format, array $context = array())
    {
        $dataArray = [];
        $productArray = [];

        /**
         * @var Category $category
         */
        foreach ($data as $category) {
            $categoryArr = $this->serializeProperty($category);
            foreach ($category->getImage() as $img){
                $categoryArr["images"][$img->getType()->getCode()][] = array(
                    "name" => $img->getName(),
                    "priority" => $img->getPriority(),
                    "path" => $img->getPath()
                );
            }
            $categoryArr["child"] = array();
            foreach ($category->getChild() as $parent) {
                $tmp2 = array(
                    "id" => $parent->getId(),
                    "name" => $parent->getName(),
                    "link" => $parent->getLink(),
                    "parentId" => $parent->getParentId()
                );
                foreach ($parent->getImage() as $childImage) {
                    $tmp2["image"][] = array(
                        "imagePath" => $childImage->getPath()
                    );
                }
                $categoryArr["child"][] = $tmp2;
            }

            $categoryArr["text"] = array();
            $textList = $category->getText();

            foreach ($textList as $text) {
                $categoryArr["text"][] = array(
                    "name" => $text->getName(),
                    "shortDescription" => $text->getShortDescription(),
                    "longDescription" => $text->getLongDescription(),
                    "link" => $text->getLink(),
                    "keywords" => $text->getKeywords()
                );
            }

            $catProd = $category->getProduct();
            foreach ($catProd as $prod) {
                if($prod->getAdminCountryProduct()[0] != null){
                    $tmp = array(
                        "id" => $prod->getAdminCountryProduct()[0]->getProduct()->getId(),
                        "name" => $prod->getAdminCountryProduct()[0]->getProduct()->getName(),
                        "link" => $prod->getAdminCountryProduct()[0]->getProduct()->getLink(),
                        "code" => $prod->getAdminCountryProduct()[0]->getProduct()->getCode(),
                        "priority" => $prod->getAdminCountryProduct()[0]->getProduct()->getPriority()
                    );

                    $tmp["text"] = array();
                    $highlightedTextList = $prod->getAdminCountryProduct()[0]->getProduct()->getHighlightedText();
                    foreach ($highlightedTextList as $highlightedText) {
                        $tmp["text"][] = array(
                            "text" => $highlightedText->getTranslate()->getText()
                        );
                    }

                    $tmp ["images"] = array();
                    $imageList = $prod->getImages();
                    foreach ($imageList as $image) {
                        if($image->getPriority()==0) {
                            $tmp["images"][$image->getType()->getCode()] = array(
                                "name" => $image->getName(),
                                "path" => $image->getPath()
                            );
                        }
                    }

                    $productArray[] = $tmp;
                }

                /*$tmp = array(
                    "id" => $prod->getId(),
                    "category_id" => $prod->getCategory()->getId(),
                    "name" => $prod->getName(),
                    "link" => $prod->getLink(),
                    "code" => $prod->getCode(),
                    "priority" => $prod->getPriority()
                );

                $prodFeat = $prod->getFeature();
                foreach ($prodFeat as $feat) {
                    $image = $feat->getFeature()->getImage();
                    foreach ($image as $imgList) {
                        $tmp["images"][$imgList->getType()->getCode()][] = array(
                            "name" => $imgList->getType()->getName(),
                            "path" => $imgList->getPath()
                        );
                    }
                }
                $productArray[] = $tmp;*/
            }

            $dataArray["categories"][] = $categoryArr;
            $dataArray["products"] = $productArray;
        }


        return json_encode($dataArray);
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionObject($data);
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }

        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}