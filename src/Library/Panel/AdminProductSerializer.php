<?php

namespace App\Library\Panel;

use App\Entity\ProductCountry;
use App\Entity\Series;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class AdminProductSerializer implements SerializerInterface
{
    public function serialize($product, $format, array $context = array())
    {
        $dataArray = [];
        $productArray = $this->serializeProperty($product);

        $productArray["sizeImage"] = array();
        $pSeries = $product->getProductSize();

        foreach ($pSeries as $series) {
            $seriesImage = $series->getProductSizeImage();
            foreach ($seriesImage as $img) {
                $productArray["sizeImage"][$img->getType()->getCode()] = array(
                    "id" => $img->getId(),
                    "path" => $img->getPath()
                );
            }
        }

        $productArray["controller"] = array();
        $productController = $product->getProductController();
        foreach ($productController as $controller) {
            $controllerImage = $controller->getController()->getImage();
            $tmp = array(
                "id" => $controller->getController()->getId(),
                "name" => $controller->getController()->getName(),
                "active" => $controller->getController()->getActive(),
                "priority" => $controller->getController()->getPriority()
            );

            $tmp["images"] = array();
            foreach ($controllerImage as $contImage) {
                $tmp["images"] = array(
                    "id" => $contImage->getId(),
                    "path" => $contImage->getPath(),
                    "active" => $contImage->getPath(),
                    "priority" => $contImage->getPriority(),
                    "name" => $contImage->getName()
                );
                $tmp["images"]["type"] = array(
                    "id" => $contImage->getType()->getId(),
                    "name" => $contImage->getType()->getName(),
                    "code" => $contImage->getType()->getCode()
                );
            }
            $productArray["controller"][] = $tmp;
        }

        $productArray["connection"] = array();
        $productConnection = $product->getProductConnection();
        foreach ($productConnection as $connection) {
            $connImage = $connection->getConnection()->getImage();
            $tmp21 = array(
                "id" => $connection->getConnection()->getId(),
                "name" => $connection->getConnection()->getName(),
                "active" => $connection->getConnection()->getActive(),
                "priority" => $connection->getConnection()->getPriority()
            );
            $tmp21["images"] = array();
            foreach ($connImage as $cImage) {
                $tmp["images"] = array(
                    "id" => $cImage->getId(),
                    "path" => $cImage->getPath(),
                    "active" => $cImage->getActive(),
                    "priority" => $cImage->getPriority(),
                    "name" => $cImage->getName()
                );
                $tmp21["images"]["type"] = array(
                    "id" => $cImage->getType()->getId(),
                    "name" => $cImage->getType()->getName(),
                    "code" => $cImage->getType()->getCode()
                );
            }

            $productArray["connection"][] = $tmp21;
        }

//        foreach ($specList as $specification) {
//            $tmp = array(
//                "product_id" => $product->getId(),
//                "spec_id" => $specification->getSpec()->getId(),
//                "value" => $specification->getValue(),
//                "priority" => $specification->getPriority()
//            );
//
//            $tmp["specCategory"] = array();
//            $tmp["specCategory"] = array(
//                "id" => $specification->getSpec()->getCategory()->getId(),
//                "name" => $specification->getSpec()->getCategory()->getName(),
//                "priority" => $specification->getSpec()->getCategory()->getPriority()
//            );
//            $tmp["specCategory"]["spec"] = array(
//                "id" => $specification->getSpec()->getId(),
//                "name" => $specification->getSpec()->getName(),
//                "code" => $specification->getSpec()->getCode(),
//                "priority" => $specification->getSpec()->getPriority(),
//            );
//
//            foreach ($specification->getSpec()->getAdminText() as $text) {
//                $tmp2["text"] = array(
//                    "spec_id" => $text->getSpec()->getId(),
//                    "name" => $text->getName(),
//                );
//                $tmp2["text"]["country"] = array(
//                    "id" => $text->getCountry()->getId(),
//                    "name" => $text->getCountry()->getName(),
//                    "code" => $text->getCountry()->getCode()
//                );
//                $tmp2["text"]["lang"] = array(
//                    "id" => $text->getLang()->getId(),
//                    "name" => $text->getLang()->getName(),
//                    "code" => $text->getLang()->getCode(),
//                );
//                $tmp["specCategory"]["spec"][] = $tmp2;
//            }
//            print_r($tmp);
//        }

//        foreach ($specList as $spec) {
//            if ($spec->getSpec()) {
//                $category = $spec->getSpec()->getCategory();
//
//                if(!$category) {
//                    $categoryName = null;
//                }else {
//                    $categoryName = $category->getAdminText()[0]->getText();
//                }
//                if ($category) {
//                    if(empty($specArray[$category->getId()])) {
//                        $specArray[$category->getId()] = array(
//                            "id" => $category->getId(),
//                            "name" => $categoryName,
//                            "priority" => $category->getPriority()
//                        );
//                    }
//                    $specArray[$category->getId()]["spec"] = array();
//                    $tmp9 = array(
//                        "id" => $spec->getSpec()->getId(),
//                        "name" => $spec->getSpec()->getName(),
//                        "code" => $spec->getSpec()->getCode(),
//                        "priority" => $spec->getSpec()->getPriority()
//                    );
//                    $specArray[$category->getId()]["productSpec"][] = array(
//                        "product_id" => $product->getId(),
//                        "spec_id" => $spec->getSpec()->getId(),
//                        "value" => $spec->getValue(),
//                        "priority" => $spec->getPriority()
//                    );
//                    $specArray[$category->getId()]["spec"][]= $tmp9;
//                }
//            }
//        }
//
//        foreach ($specArray as $spec) {
//            $productArray["spec"][] = $spec;
//        }
        $specList = $product->getAdminSpec();
        $productArray["spec"] = array();
        foreach ($specList as $spec) {
            $productArray["spec"][] = array(
                "name" => $spec->getSpec()->getName(),
                "priority" => $spec->getPriority(),
                "value" => $spec->getValue(),
                "spec_id" => $spec->getSpec()->getId(),
                "product_id" => $product->getId(),
                "lang_id" => $spec->getLang()->getId()
            );
        }

        $productArray ["images"] = array();
        $productImages = $product->getImages();

        foreach ($productImages as $productImage) {
            if($productImage->getType() != null) {
                $tmp = array(
                    "id" => $productImage->getId(),
                    "name" => $productImage->getName(),
                    "path" => $productImage->getPath(),
                    "active" => $productImage->getActive(),
                    "priority" => $productImage->getPriority()
                );
                $tmp["type"] = array(
                    "id" => $productImage->getType()->getId(),
                    "name" => $productImage->getType()->getName(),
                    "code" => $productImage->getType()->getCode()
                );

                $productArray["images"][] = $tmp;
            }
        }

        $productArray["feature"] = array();
        $featureList = $product->getFeature();
        foreach ($featureList as $feature) {
            if($feature->getFeature() != null && $feature->getFeature()->getAdminText()[0] != null){
                $tmp = array(
                    "id" => $feature->getFeature()->getId(),
                    "name" => $feature->getFeature()->getName(),
                    "active" => $feature->getFeature()->getActive()
                );

                $tmp["type"] = array(
                    "id" => $feature->getType()->getId(),
                    "name" => $feature->getType()->getName()
                );
                $array[] = $tmp;
                $productArray["feature"] = $array;
            }
        }

        $productArray["file"] = array();
        $productFileList = $product->getFile();

        foreach ($productFileList as $file) {
            if($file->getFile() != null && $file->getFile()->getType() != null) {
                $productArray["file"][] = array(
                    "id" => $file->getFile()->getId(),
                    "name" => $file->getFile()->getName(),
                    "path" => $file->getFile()->getPath(),
                    "priority" => $file->getPriority(),
                    "typeCode" => $file->getFile()->getType()->getCode(),
                    "typeName" => $file->getFile()->getType()->getName()
                );
            }
        }

        $productArray["buyNowLink"] = array();
        if ($product->getProductBuy()) {
            $productBuy = $product->getProductBuy();
            foreach ($productBuy as $pBuy) {
                $tmp11 = array(
                    "link" => $pBuy->getLink(),
                    "active" => $pBuy->getActive()
                );
                $tmp11["country"] = array(
                    "id" => $pBuy->getCountry()->getId(),
                    "name" => $pBuy->getCountry()->getName(),
                    "code" => $pBuy->getCountry()->getCode()
                );
                $productBuy["buyNowLink"] = $tmp11;
            }
        }

        $productArray["productCountry"] = array();
        $productCountry = $product->getCountry();

        foreach ($productCountry as $pCountry) {
            $tmp14 = array(
                "product_id" => $product->getId(),
                "active" => $pCountry->getActive()
            );
            $tmp14["country"] = array(
                "id" => $pCountry->getCountry()->getId(),
                "name" => $pCountry->getCountry()->getName(),
                "code" => $pCountry->getCountry()->getCode(),
            );
            $productArray["productCountry"][] = $tmp14;
        }

        $productArray["related"] = array();
        $relatedList = $product->getRelated();
        foreach ($relatedList as $related) {

            if($related->getRelated() != null) {
                if ($related->getRelated()->getFile()[0] == null) {
                    $relatedFile = "";
                } else {
                    $relatedFile = $related->getRelated()->getFile()[0]->getFile()->getPath();
                }
                $productArray["related"][] = array(
                    "related_id" => $related->getRelated()->getId(),
                    "priority" => $related->getPriority(),
                    "name" => $related->getRelated()->getName(),
                    "code" => $related->getRelated()->getCode(),
                    "link" => $related->getRelated()->getLink(),
                    "file" => $relatedFile,
                    "keywords" => $related->getRelated()->getKeywords()
                );
            }
        }

        $productArray["text"] = array();
        $textList = $product->getText();

        foreach ($textList as $text) {
            $tmp10 = array(
                "product_id" => $product->getId(),
                "subtitle" => $text->getSubtitle(),
                "name" => $text->getName(),
                "shortDescription" => $text->getShortDescription(),
                "longDescription" => $text->getLongDescription(),
                "link" => $text->getLink(),
                "size" => $text->getSize(),
                "keywords" => $text->getKeywords()
            );
            $tmp10["country"] = array(
                "id" => $text->getCountry()->getId(),
                "name" => $text->getCountry()->getName()
            );
            $tmp10["lang"] = array(
                "id" => $text->getLang()->getId(),
                "name" => $text->getLang()->getName()
            );
            $productArray["text"][] = $tmp10;
        }

        $productArray["highlightedText"] = array();
        $highlightedTextList = $product->getHighlightedText();

        foreach ($highlightedTextList as $highlightedText) {
            if($highlightedText->getAdminTranslate()[0] != null) {
                foreach ($highlightedText->getAdminTranslate() as $trans) {
                    $tmp12 = array(
                        "id" => $highlightedText->getId(),
                        "text" => $trans->getText()
                    );
                    $tmp12["country"] = array(
                        "id" => $trans->getCountry()->getId(),
                        "name" => $trans->getCountry()->getName()
                    );
                    $tmp12["lang"] = array(
                        "id" => $trans->getLang()->getId(),
                        "name" => $trans->getLang()->getName()
                    );
                    $productArray["highlightedText"][] = $tmp12;
                }
            }
        }

        $productArray["category"] = array();
        $category = $product->getCategory();
        if ($product->getCategory()) {
            $productArray["category"][] = array(
                "id" => $category->getId(),
                "name" => $category->getName(),
                "link" => $category->getLink(),
                "keywords" => $category->getKeywords(),
                "active" => $category->getActive(),
                "parentId" => $category->getParentId()
            );
        }

        $productArray["series"] = array();
        $series = $product->getSeries();

        if($series != null) {
            $array = array(
                "id" => $series->getId(),
                "name" => $series->getName(),
                "code" => $series->getCode()
            );
            $array["products"] = array();
            if($series->getProduct($product->getId()) != null) {
                $productList = $series->getProduct($product->getId());
                foreach ($productList as $pro) {
                    $array["products"][] = array(
                        "id" => $pro->getId(),
                        "size" => $pro->getSize(),
                        "link" => $pro->getLink()
                    );
                }
            }

            $productArray["series"] = $array;
        }

        $dataArray[] = $productArray;

        return $dataArray;
    }

    public function deserialize($data, $type, $format, array $context = array())
    {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data)
    {
        $dataArray = array();
        $refClass = new \ReflectionClass(ClassUtils::getClass($data));
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if($functionName != "getSpec" && gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }
        }

        return $dataArray;
    }

}