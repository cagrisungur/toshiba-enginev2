<?php
namespace App\Library;

use App\Entity\ProductCountry;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\Serializer\SerializerInterface;

class FilterSerializer implements SerializerInterface{
    private $country;
    private $lang;

    public function serialize($data, $format, array $context = array()) {
        $serializedObj = array();

        foreach($data as $filterCategory) {


            $obj = array();

            $categoryText = $filterCategory->getText($this->country,$this->lang)[0];
            if($categoryText != null) {
                $obj["name"] = $categoryText->getName();
            }else {
                $obj["name"] = $filterCategory->getName();
            }

            $obj["filters"] = array();

            foreach($filterCategory->getFilter() as $filter) {
                $filterText = $filter->getText($this->country, $this->lang)[0];
                $filterName = "";

                if($filterText != null) {
                    $filterName = $filterText->getName();
                }else {
                    $filterName = $filter->getName();
                }

                $tmp = array(
                    "id" => $filter->getId(),
                    "name" => $filterName
                );

                $obj["filters"][] = $tmp;
            }

            $serializedObj[] = $obj;
        }

        return $serializedObj;
    }

    public function deserialize($data, $type, $format, array $context = array()) {
        // TODO: Implement deserialize() method.
    }

    public function serializeProperty($data) {
        $dataArray = array();
        $refClass = new \ReflectionClass(ClassUtils::getClass($data));
        $properties = $refClass->getProperties();

        foreach ($properties as $property) {
            $functionName = "get" . ucfirst($property->name);

            if(gettype($data->$functionName()) != "object") {
                $dataArray[$property->name] = $data->$functionName();
            }
        }

        return $dataArray;
    }

    public function setLocale($country, $lang) {
        $this->country = $country;
        $this->lang = $lang;
    }
}
?>
