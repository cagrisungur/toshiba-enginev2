$('#tvs-dropdown-button').mouseenter(function() {
  $('#tvs-dropdown-section').css('display', 'block');
})
$('#tvs-dropdown-button').mouseleave(function() {
  $('#tvs-dropdown-section').css('display', 'none');
})

$('#support-dropdown-button').mouseenter(function() {
  $('#support-dropdown-section').css('display', 'block');
})
$('#support-dropdown-button').mouseleave(function() {
  $('#support-dropdown-section').css('display', 'none');
})

$('#mobile-buttons-section').click(function() {
  $("#mobile-list-modal").toggleClass('no-display');
  console.log('modal');
})

$('#mobile-tv-button').click(function() {
  $("#tv-content").toggleClass('no-display');
})

$('#mobile-support-button').click(function() {
  $("#support-content").toggleClass('no-display');
})