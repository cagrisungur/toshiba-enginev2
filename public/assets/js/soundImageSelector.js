soundImgSelector = function() {
  var imageSelector = document.getElementById('image-selector');
  var imgSound = document.getElementById('img-sound')



  imageSelector.addEventListener('click', imageHandler);

  function imageHandler(e) {
    dataID = e.target.getAttribute('data-id');
    console.log(dataID);


    if(dataID) {
      switch(dataID) {
        case 'virtual-sound':
          $('.selection-card-selected').removeClass("selection-card-selected")
          $('#vs-container').addClass("selection-card-selected")
          changeClass('image-s-1')
          break;
        case 'volume-levelling':
          $('.selection-card-selected').removeClass("selection-card-selected")
          $('#vl-container').addClass("selection-card-selected")
          changeClass('image-s-2')
          break;
        case 'dialogue-enhancement':
          $('.selection-card-selected').removeClass("selection-card-selected")
          $('#de-container').addClass("selection-card-selected")
          changeClass('image-s-3')
          break;
          case 'bass-enhancement':
          $('.selection-card-selected').removeClass("selection-card-selected")
          $('#bs-container').addClass("selection-card-selected")
          changeClass('image-s-4')
          break;
        default:
          changeClass('image-s-1');
      }
    }
  }

  function changeClass(class1) {
    if(imgSound.className !== class1) {
      imgSound.className = class1;
    }
  }
}

soundImgSelector();