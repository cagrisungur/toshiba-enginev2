function accordion() {

    $('.accordion__button').click(function() {

      var $this = $(this);

      if ($this.hasClass('show')) {
        $this.removeClass('show');
        $this.find('h4').text("View full specifications");
        $('.accordion').removeClass('show');
        $('.accordion').slideUp(350);
      } else {
        $this.addClass('show');
        $this.find('h4').text("Hide full specifications");
        $('.accordion').addClass('show');
        $('.accordion').slideDown(350);
      }

    });

    $('.accordion__toggle').click(function() {

      var $this = $(this);

      if ($this.hasClass('show')) {
        $this.removeClass('show');
        $this.next().removeClass('show');
        $this.next().slideUp(350);
      } else {
        $this.parent().parent().parent().parent().find('li .accordion__toggle').removeClass('show');
        $this.parent().parent().parent().parent().find('li .accordion__container').removeClass('show');
        $this.parent().parent().parent().parent().find('li .accordion__container').slideUp(350);
        $this.toggleClass('show');
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
      }

    });

  }


  function showHideHeaderScroll() {
    $('body').addClass('headerInit');

    $(document).scroll(function() {
      var position = $(document).scrollTop();
      var headerHeight = $('header').outerHeight();

      if(position > headerHeight) {
        $('header').addClass('scrolled');

        // This beautiful hack hides the initial transform for the header on first scroll
        setTimeout(function () {
          $('body').removeClass('headerInit');
        }, 320);
      } else {
        $('header').removeClass('scrolled');
        $('body').addClass('headerInit');
      }
    });

    var position = $(window).scrollTop();

    $(window).scroll(function() {
      var scroll = $(window).scrollTop();
      if(scroll > position) { // you could also add some sensitivity to this if you felt like it :P
        $('body').removeClass('scrollUp');
      } else {
        $('body').addClass('scrollUp');
      }
      position = scroll;
    });
  }


  function mobileNavigation() {

    $('.header__hamburger').click(function() {

      var $this = $(this);

      if ($this.hasClass('show')) {
        $this.removeClass('show');
        $('body').removeClass('fixed');
        $('.mobile-header').slideUp(300);
      } else {
        $this.addClass('show');
        $('body').addClass('fixed');
        $('.mobile-header').slideDown(300);
      }

    });

    $('.has-children span').click(function() {

      var $this = $(this);

      if ($this.hasClass('show')) {
        $this.removeClass('show');
        $this.siblings('.mobile-header__sub-navigation').slideUp(300);
      } else {
        $this.parent().parent().find('li.has-children span').removeClass('show');
        $this.parent().parent().find('li.has-children .mobile-header__sub-navigation').slideUp(350);
        $this.addClass('show');
        $this.siblings('.mobile-header__sub-navigation').slideToggle(300);

      }

    });

  }



  function imageSlider() {

    var dragging = false,
    scrolling = false,
    resizing = false;
    var imageComparisonContainers = $('.image-slider__container');
    checkPosition(imageComparisonContainers);
    $(window).on('scroll', function() {
      if (!scrolling) {
        scrolling = true;
        (!window.requestAnimationFrame) ? setTimeout(function() {
          checkPosition(imageComparisonContainers);
        }, 100) : requestAnimationFrame(function() {
          checkPosition(imageComparisonContainers);
        });
      }
    });
    imageComparisonContainers.each(function() {
      var actual = $(this);
      drags(actual.find('.image-slider__handle'), actual.find('.image-slider__image'), actual, actual.find('.image-slider__label[data-type="original"]'), actual.find('.image-slider__label[data-type="modified"]'));
    });
    $(window).on('resize', function() {
      if (!resizing) {
        resizing = true;
        (!window.requestAnimationFrame) ? setTimeout(function() {
          checkLabel(imageComparisonContainers);
        }, 100) : requestAnimationFrame(function() {
          checkLabel(imageComparisonContainers);
        });
      }
    });
    function checkPosition(container) {
      container.each(function() {
        var actualContainer = $(this);
        if ($(window).scrollTop() + $(window).height() * 0.5 > actualContainer.offset().top) {
          actualContainer.addClass('is-visible');
        }
      });
      scrolling = false;
    }
    function checkLabel(container) {
      container.each(function() {
        var actual = $(this);
        updateLabel(actual.find('.image-slider__label[data-type="modified"]'), actual.find('.image-slider__image'), 'left');
        updateLabel(actual.find('.image-slider__label[data-type="original"]'), actual.find('.image-slider__image'), 'right');
      });
      resizing = false;
    }
    function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
      dragElement.on("mousedown vmousedown", function(e) {
        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');
        var dragWidth = dragElement.outerWidth(),
        xPosition = dragElement.offset().left + dragWidth - e.pageX,
        containerOffset = container.offset().left,
        containerWidth = container.outerWidth(),
        minLeft = containerOffset + 10,
        maxLeft = containerOffset + containerWidth - dragWidth - 10;
        dragElement.parents().on("mousemove vmousemove", function(e) {
          if (!dragging) {
            dragging = true;
            (!window.requestAnimationFrame) ? setTimeout(function() {
              animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);
            }, 100) : requestAnimationFrame(function() {
              animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);
            });
          }
        }).on("mouseup vmouseup", function(e) {
          dragElement.removeClass('draggable');
          resizeElement.removeClass('resizable');
        });
        e.preventDefault();
      }).on("mouseup vmouseup", function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
    }
    function animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement) {
      var leftValue = e.pageX + xPosition - dragWidth;
      if (leftValue < minLeft) {
        leftValue = minLeft;
      } else if (leftValue > maxLeft) {
        leftValue = maxLeft;
      }
      var widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%';
      $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
        $(this).removeClass('draggable');
        resizeElement.removeClass('resizable');
      });
      $('.resizable').css('width', widthValue);
      updateLabel(labelResizeElement, resizeElement, 'left');
      updateLabel(labelContainer, resizeElement, 'right');
      dragging = false;
    }
    function updateLabel(label, resizeElement, position) {
      if (position == 'left') {
        (label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth()) ? label.removeClass('is-hidden') : label.addClass('is-hidden');
      } else {
        (label.offset().left > resizeElement.offset().left + resizeElement.outerWidth()) ? label.removeClass('is-hidden') : label.addClass('is-hidden');
      }
    }

  };




  function toTop() {

    $('.to-top').click(function() {
        $('html, body').animate({scrollTop: 0}, 1000);
     });

     $(document).scroll(function() {
       var position = $(document).scrollTop();
       var headerHeight = $('header').outerHeight();

       if(position > headerHeight) {
         $('.to-top--mobile').addClass('scrolled');

         // This beautiful hack hides the initial transform for the header on first scroll
         setTimeout(function () {
           $('body').removeClass('headerInit');
         }, 320);
       } else {
         $('.to-top--mobile').removeClass('scrolled');
         $('body').addClass('headerInit');
       }
     });


  };



















var isBoughtKnown = true;


function checkIsRetailKnown(){
    if(isBoughtKnown){
      $(".retailerName").hide()
    }else{
      $(".retailerName").show()
    }
}


  function contactCombo(){
  var x, i, j, selElmnt, a, b, c;
  /* Look for any elements with the class "custom-select": */
  x = document.getElementsByClassName("contactSelectbox");
  for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /* For each element, create a new DIV that will act as the selected item: */
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected register-input");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;

    x[i].appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 0; j < selElmnt.length; j++) {
      if(selElmnt.options[j].value == -1){
        continue;
      }


      if(selElmnt.options[j].value == 0){
        selElmnt.options[j].innerHTML = "No Selection";
      }

      theValue = selElmnt.options[j].value;





      /* For each option in the original select element,
      create a new DIV that will act as an option item: */
      c = document.createElement("DIV");
      c.innerHTML = selElmnt.options[j].innerHTML;

      c.setAttribute("data-id",selElmnt.options[j].value);
      c.addEventListener("click", function(e) {
          var y, i, k, s, h;


          $(e.target).parent("div").parent(".contactSelectbox").find("select").find("option")
     .removeAttr('selected')
     .filter('[value='+e.target.attributes[0].value+']')
         .attr('selected', true);

         $(e.target).parent("div").parent(".contactSelectbox").val(e.target.attributes[0].value);
          //

          console.log(e.target.attributes[0].value);



          if(e.target.attributes[0].value == -9){
             isBoughtKnown = false;
             checkIsRetailKnown();
           }else{
             isBoughtKnown = true;
             checkIsRetailKnown();
           }


          s = this.parentNode.parentNode.getElementsByTagName("select")[0];
          h = this.parentNode.previousSibling;
          for (i = 0; i < s.length; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
              s.selectedIndex = i;
              h.innerHTML = this.innerHTML;
              y = this.parentNode.getElementsByClassName("same-as-selected");
              for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          h.click();
          $(e.target).parent("div").parent(".contactSelectbox").find("select").change();
      });
      b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
      /* When the select box is clicked, close any other select boxes,
      and open/close the current select box: */

      if(!$(this).parent("div").parent("div").parent("div").parent("div").parent("div").parent("div").hasClass("editMode") || ($(this).parent("div").hasClass("filterOrder"))){

        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
      /*  if($(this).parent("div").hasClass("filterOrder")){
          //filterAjax();
        }*/
      }else{
        $('.filtersBar').removeClass("editMode");
        $(this).find("div:first-child").click();
        $(this).parent("div").find(".select-items div:first-child").click();
        $(this).parent("div").hide();
        filterAjax();
      }


    });
  }
  }

  function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("select-arrow-active");
      }
    }
    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
      }
    }
  }

  /* If the user clicks anywhere outside the select box,
  then close all select boxes: */
  document.addEventListener("click", closeAllSelect);












  $(document).ready(function() {

    if($(".contactSelectbox").length > 0){
      contactCombo();
    }
    mobileNavigation();
    //fixedHeader();
    toTop();
    accordion();
    imageSlider();
    showHideHeaderScroll();

  });
