(function() {
  var model = (function() {

  })();

  var view = (function() {

    return {
      UIElements: ['idForm-0']
    }
  })();

  var controller = (function(model, view) {
    var form = document.getElementById(view.UIElements[0]);

    function initEventHandlers() {
      window.addEventListener('load', handleChecksOnReturn); // Handle UI if user returns from another page
      if(form) {
        form.addEventListener('click', checkBoxHandler);  // Handle UI if user clicks checkboxes
      }
      
    }
    
    function handleChecksOnReturn(e) {
      var allCheckbox = document.querySelectorAll('input[type=checkbox]');

      allCheckbox.forEach(function(el) {
        if(el.checked) {
          el.parentNode.parentNode.classList.toggle('bg-grey-600');
        }
      })
    }

    function checkBoxHandler(e) {
      if(e.target.type) {
        if(e.target.type === 'checkbox') {
          e.target.parentNode.parentNode.classList.toggle('bg-grey-600');
        }
      }
    }

    return {
      init: function() {
        initEventHandlers();
      }
    }

  })(model, view)

  controller.init();
})();

