var videoOpener = function () {
  var button = document.getElementById('play-icon');
  var modal = $('#video-modal')
  var video = document.getElementById('tru-video')
  button.addEventListener('click', handleClick);

  function handleClick() {
    console.log("button")
    $("#button-container").css({"display": "block"})
    $("#video-modal").css({"display":"inherit"});
    $("#close-video").click(function(){
      $("#video-modal").css({"display":"none"});
      $('iframe').attr('src', $('iframe').attr('src'));
    })
    $("#button-container1").css({"display": "block"})
    $("#close-video1").click(function(){
      $("#video-modal").css({"display":"none"});
      $('iframe').attr('src', $('iframe').attr('src'));
    })
    modal.removeClass('display-none');
    modal.on('click', handleModal);
  }

  function handleModal(e) {
    console.log(e.target.id )
    if(e.target.id === 'video-modal') {
      $("#video-modal").css({"display":"none"});
      $('iframe').attr('src', $('iframe').attr('src'));
      modal.addClass('display-none');
    }
    
  }
}

videoOpener();