

// Select element of mobile filter.
var mobileFilterSelector = (function() {

  function returnDefault(selector) {
    $(selector)[0].selectedIndex = 0;
  }

  function removeFilter(selector, selectedIndex) {
    if(selectedIndex !== 0) {
      $(selector)[0].options[selectedIndex].classList.add('display-none-important');
    }
    returnDefault(selector);
  }

  function addFilter(selector, selectedIndex) {
    console.log('add');
    $(selector)[0].options[selectedIndex].classList.remove('display-none-important');
  }

  return {
    addFilter: function(selector, selectedIndex) {
      addFilter(selector, selectedIndex);
    },
    removeFilter: function(selected, index) {
      removeFilter(selected, index);
    }
  }


})();





// Bottom section of mobile filter.
var mobileAddedFiltersHandler = (function() {

  var filterBottomSection = $('#filter-bottom-section');
  function removeFilter(selector) {
    $(selector).addClass('display-none-important');
  }

  // Add filter selections again if they are removed from bottom section.
  function addFilter(selector) {
    $(selector).removeClass('display-none-important')
  }

  return {
    addFilter: function(selector) {
      addFilter(selector);
    },

    removeFilter: function(selector) {
      removeFilter(selector);
    }
  }
})();





var mobileFilterController = (function() {
  var selectedValue ,selectedText;

  // Event handler functions
  function setupEventHandlers() {
    $("#filter-selector-mobile").click(filterSelect);
    $("#filter-bottom-section").click(bottomFilterRemove);
  }


  function filterSelect(e) {

    selectedValue = '#' + this.options[this.selectedIndex].value;

    mobileFilterSelector.removeFilter(this, this.selectedIndex);

    mobileAddedFiltersHandler.addFilter(selectedValue)    
  }

  function bottomFilterRemove(e) {
    console.log(e.target);
    var filterID = e.target.id;
    // This split needs an update for regular expressions.
    // Then, it will be able to get its seperator.
    var isFilterID = filterID.split('-');
    console.log(isFilterID[0] + isFilterID[1] === 'filterindex');
    if(isFilterID[0] + isFilterID[1] === 'filterindex') {
      mobileAddedFiltersHandler.removeFilter('#' + e.target.parentNode.id);
      
      mobileFilterSelector.addFilter('#filter-selector-mobile', isFilterID[2]);
    }
  }

  // Event handling ended

  // Scroll for different background
  function setupScrollColor() {
    var offsetMobileFilter;
    var mobileFilterTopValue = $(".filter-section-mobile").css("top").split("px")[0];
    var mobileFilterHeight = $('.filter-section-mobile').outerHeight();
    var offsetWhiteBg = $(".all-product-white-bg").offset().top;

    $(window).scroll(function() {
      offsetMobileFilter = $(".filter-section-mobile").offset().top;
      if(offsetMobileFilter > offsetWhiteBg - mobileFilterHeight) {
        console.log(offsetMobileFilter);
        $(".filter-section-mobile").css("background" , "rgba(0, 0, 0, 0.8)");
      } else {
        $(".filter-section-mobile").css("background" , "rgba(0, 0, 0, 0.4)");
      }
    })
  }
  // Scroll for different background ended

  return {
    init: function() {
      setupEventHandlers();
      setupScrollColor();
    }
  }

})();

mobileFilterController.init();
