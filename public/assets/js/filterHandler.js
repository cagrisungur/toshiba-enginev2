var filterModel = (function() {

  // Make request.
})();

var filterView = (function() {
  //  Get and send elements.
  var mainSelector = document.getElementById('selector-add-filter');
  //var options = mainSelector.options;
  var options = document.querySelectorAll('#selector-add-filter option')
  var applyButton = document.getElementById('button-apply');
  var cancelButton = document.getElementById('button-cancel');
  var optionValArray = [];

  Array.prototype.forEach.call(options, returnOptionArray);
  function returnOptionArray(el) {
    optionValArray.push(el.value);
  }

  var selectorHTML = [
    "",
    "<select class='filter-selector text-xs margin-r-xxs'> <option value='' disabled selected hidden>Screen size</option> <option value='1'>47''</option> <option value='2'>53''</option> <option value='3'>57''</option> <option value='4'>75''</option> </select>",
    "<select name='choice' class='filter-selector text-xs margin-r-xxs'> <option value='' disabled selected hidden>Resolution</option> <option value='1'>HD</option> <option value='2'>Full HD</option> <option value='3'>Ultra HD</option> <option value='4'>4K</option> </select>",
    "<select class='filter-selector text-xs margin-r-xxs'> <option value='' disabled selected hidden>Operating system</option> <option value='1'>Android</option> <option value='2'>Other</option> </select>",
    "<select class='filter-selector text-xs margin-r-xxs'> <option value='' disabled selected hidden>Voice assistant</option> <option value='1'>Yes</option> <option value='2'>No</option>> </select>",
    "<select class='filter-selector text-xs margin-r-xxs'> <option value='' disabled selected hidden>Key features</option> <option value='1'>Feature 1</option> <option value='2'>Feature 2</option> <option value='3'>Feature 3</option> <option value='4'>Feature 4</option> </select>"
  ]

  var UIElements = {
    mainSelector: mainSelector,
    options: options,
    applyButton: applyButton,
    cancelButton: cancelButton
  };

  function addSelector(type) {
    mainSelector.insertAdjacentHTML('afterend', selectorHTML[optionValArray.indexOf(type)])
  }





  // Dont forget to add X button to selectors

  return {
    returnUIElements: function() {
      return UIElements;
      // mainselector.options
    },
    addSelector: function(type) {
      addSelector(type);
    }
  }

})();


var filterController = (function(model, view) {
  var UI = view.returnUIElements();
  console.log(UI.options);

  function prepareListeners() {
  //  UI.mainSelector.addEventListener('change', );
    UI.cancelButton.addEventListener('click', handleCancel);
  }

  $(".filtersPart").delegate("#selector-add-filter","change",function(){
    console.log("act2");
      view.addSelector(this.options[this.selectedIndex].value);
  });




  function handleCancel(e) {
    var selectAll = document.querySelectorAll('#filter-left-container select');
    Array.prototype.forEach.call(selectAll, function(el) {
      if(!el.id) {
        el.parentNode.removeChild(el);
      }
    })
  }



  // Get Filter UI elements Add filter, apply, cancel. OK
  /* Add event listener to selector OK
    If element type is X and not disabled then add to screen OK not selectable doesnt work btw.

    If something is selected and Apply button is clicked, make a request

    If something is selected and Cancel button is clicked, delete every selected.
  */

  return {
    init: function() {
      prepareListeners();
    }
  }

})(filterModel, filterView);

filterController.init();
