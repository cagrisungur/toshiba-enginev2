/*var viewAllFAQButton = document.getElementById('view-all-faq');
viewAllFAQButton.addEventListener('click', handleAllFaq);*/
var faqsOpen = false;

var wrapperSlider = document.getElementById('wrapper-slider');
if(wrapperSlider) {
  slideHandler1();
}


/*function handleAllFaq() {
  $(".faq-section").each(function() {
    if(selectedFAQ && !faqsOpen) {
      $(this).removeClass('display-none');
    } else if (selectedFAQ && faqsOpen) {
      $(this).removeClass('display-none');
      $(this).addClass('display-none');
    } else {
      $(this).toggleClass('display-none')
    }
  })
  faqsOpen = !faqsOpen;
  viewAllFAQButton.innerText = (faqsOpen ? 'Hide All FAQs' : 'View All FAQs');
}*/

var selectedFAQ;

var supportQuickLinks = document.getElementById('support-quick-links');
supportQuickLinks.addEventListener('click', function(e) {
  if(e.target.getAttribute('data-id')) {
    if(!e.target.getAttribute('data-id').split('faq-button-')[1]) {
      selectedFAQ = e.target.getAttribute('data-id').split('video-button-')[1];
      $('#video-section-' + selectedFAQ).toggleClass('display-none');
    } else if (e.target.getAttribute('data-id').split('faq-button-')[1]) {
      selectedFAQ = e.target.getAttribute('data-id').split('faq-button-')[1];
      $('#faq-section-' + selectedFAQ).toggleClass('display-none');
    }
  }
});



var searchFAQSection = document.getElementById('search-faq-section');

if(searchFAQSection) {
  searchFAQSection.addEventListener('click', handleSearchFAQs);
}


function handleSearchFAQs(event) {

  searchFAQDataID = event.target.getAttribute('data-id');
  if(searchFAQDataID) {
    selectedSearchFAQID = searchFAQDataID.split('faq-question-')[1];
    $('#faq-answer-' + selectedSearchFAQID).toggleClass('display-none');
  }
}

(function() {
  var selectedTVsSlider = document.getElementById('wrapper-slider-3');
  var selectedTVsSliderDots = document.getElementById('red-dots-container-2');

  var screenSizeCardsContainer = document.getElementById('screen-size-cards-container');

  if(screenSizeCardsContainer) {
    screenSizeCardsContainer.addEventListener('click', showSelectedTV);
  }

  var selectedTVSize;

  var sliderChange = document.getElementsByClassName('sliderselect-cover');

  function showSelectedTV(event) {



    selectedTVsSlider.innerHTML = '';
    if(event.target.getAttribute('data-id')) {

      Array.prototype.forEach.call(sliderChange, function(el) {
        el.classList.remove('d-none');
      });


      selectedTVSize = event.target.getAttribute('data-id');
      document.querySelectorAll("[data-select='" + selectedTVSize + "']")[0].classList.add('d-none');
      var currentInd = 0;

      var wrapperSlider3 = document.getElementById('wrapper-slider-3');
      wrapperSlider3.innerHTML = '';

      lastCarouselContainer = document.getElementById('mobile-carousel');
      lastCarouselContainer.innerHTML = '';
      currentInd = 0;
      wrapperSlider3.innerHTML = '';
      wrapperSlider3.innerHTML = '';
      wrapperSlider3.innerHTML = "";

      lastCarouselContainer.innerHTML = '';
      lastCarouselContainer.innerHTML = '';
      lastCarouselContainer.innerHTML = "";

      console.log(wrapperSlider3.innerHTML);

      var allCarousel1 = document.querySelectorAll('.carousel-1');
      if(allCarousel1) {
        Array.prototype.forEach.call(allCarousel1, function(el) {
          el.remove();
          console.log(el);
        })
      }


      for(var i = 0 ; i < productCodeFullArray.length ; i++){
        if(productCodeFullArray[i].substring(0, 2) === selectedTVSize) {
          insertSliderTVs(productCodeFullArray[i], productLinkFullArray[i], productPhotoArray[i], i, currentInd);
          insertSliderTVsMobile(productCodeFullArray[i], productLinkFullArray[i], productPhotoArray[i], i, currentInd);
          currentInd += 1;
        }
      }
      //slideHandler2();

      var closeSlideHTML = '<div class="mb-4 mt-5 w-100 d-flex flex-row justify-content-center align-items-center close-tv-slide-section" id="close-tv-slide-section"> <div> <p class="d-inline-block color-grey700 text-m">Select your TV</p> </div> <div class="mb-1 ml-2 cursor-pointer" id="close-tv-slide-container"> <ion-icon class="text-m-l" name="close-circle-outline"></ion-icon> </div> </div>';
      screenSizeCardsSection = document.getElementById('screen-size-cards-section');

      var closeTVContainer = document.getElementById("close-tv-slide-container");
      var closeTVSection = document.getElementById("close-tv-slide-section");

      if(!closeTVSection) {
        screenSizeCardsSection.insertAdjacentHTML('beforeend', closeSlideHTML);
      }

      var closeTVContainer = document.getElementById("close-tv-slide-container");
      var closeTVSection = document.getElementById("close-tv-slide-section");

      closeTVContainer.addEventListener('click', closeSlideSection);



      $("#carousel1").on("touchstart", function(event){
        var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $("#carousel").on("touchend", function(){
            $(this).off("touchmove");
    });
});


      function closeSlideSection() {
        if(selectedTVsSlider) {
          selectedTVsSlider.innerHTML = '';
        }

        carouselIndicators = document.getElementById('carousel-indicators');
        if(carouselIndicators) {
          carouselIndicators.innerHTML = '';
        }

        Array.prototype.forEach.call(sliderChange, function(el) {
          el.classList.add('d-none');
        });

        currentInd = 0;

        closeTVSection.remove();

        closeTVContainer.removeEventListener('click', closeSlideSection);
      }

    }
  }

  var selectedTVHTML;

  function insertSliderTVs(productCode, productLink, productPhoto, index, currentInd) {
    var currentSlide = Math.floor(currentInd / 3).toString();


    var wrapperSlider3 = document.getElementById('wrapper-slider-3');

    var currentSlideText = "slide-carousel-container-" + currentSlide;
    var carouselContainerFirst="<div class='carousel-1 carousel-item active'><div class='w-100 d-flex flex-row justify-content-between' id='slide-carousel-container-0'></div></div>";
    var carouselContainerStd="<div class='carousel-1 carousel-item'><div class='w-100 d-flex flex-row justify-content-between' id="+ currentSlideText +"></div></div>";

    var carouselIndicatorFirst = "<li data-target='#carousel1' data-slide-to='0' class='carousel-1 active'></li>";
    var carouselIndicatorStd = "<li data-target='#carousel1' data-slide-to="+ currentSlide +" class='carousel-1'></li>";

    var lastCarouselIndicatorContainer = document.getElementById('carousel-indicators');

    if(currentInd === 0) {
      wrapperSlider3.insertAdjacentHTML('beforeend', carouselContainerFirst)
      lastCarouselIndicatorContainer.insertAdjacentHTML('beforeend', carouselIndicatorFirst);
    }

    if(currentInd % 3 === 0 && currentInd !== 0) {
      wrapperSlider3.insertAdjacentHTML('beforeend', carouselContainerStd);
      lastCarouselIndicatorContainer.insertAdjacentHTML('beforeend', carouselIndicatorStd);

    }

    lastCarouselContainer = document.getElementById(currentSlideText);
    var href = productLink;
    selectedTVHTML = "<div class='col-4' id='tv-" + index + "'> <div class='all-product-box-wrapper gray-100-bg'> <div class='height-280 overflow-hidden wrapper-row-align-center-center'><img draggable='false' class='all-product-box-img' src='"+ productPhoto + "'/></div> <p class='margin-t-xs product-header color-grey700 text-m-l-bold'>" + productCode +"</p> <div class='product-year-section'> <p></p> </div> <a href='"+href+"'> <button class='all-product-box-button-text text-xs color-white all-product-box-button red-500-bg'> Learn more </button> </a> </div> </div>";

    lastCarouselContainer.insertAdjacentHTML('beforeend', selectedTVHTML);


  }

      // --------------------------

    // MOBILE SLIDER

    // -------------------------------

  function insertSliderTVsMobile(productCode, productLink, productPhoto, index, currentInd) {



    lastCarouselContainer = document.getElementById('mobile-carousel');
    var href = productLink;
    selectedTVHTML = "<div class='col-12' id='tv-" + index + "'> <div class='all-product-box-wrapper gray-100-bg'> <div class='height-280 overflow-hidden wrapper-row-align-center-center'><img draggable='false' class='all-product-box-img' src='"+ productPhoto + "'/></div> <p class='margin-t-xs product-header color-grey700 text-m-l-bold'>" + productCode +"</p> <div class='product-year-section'> <p></p> </div> <a href='"+href+"'> <button class='all-product-box-button-text text-xs color-white all-product-box-button red-500-bg'> Learn more </button> </a> </div> </div>";

    lastCarouselContainer.insertAdjacentHTML('beforeend', selectedTVHTML);

  }



})();






/*


  FAQ Section
  New Product Slide Section
  Screen Size lar hide olacak.
*/
