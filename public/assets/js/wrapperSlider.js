console.log('WRAPPERSLIDER');

slideHandler1 = function (){

var wrapperSlider = document.getElementById("wrapper-slider");
console.log('wrapper-slider1');
console.log(wrapperSlider);


// Constants ------------------
var movementLimit = 300;
var nextPosition = -534;

// Constants ended -----------


// Declared variables
var maxRenderState = wrapperSlider.childElementCount - 2;

// Declared variables ended


// Defined variables.
var entered = 0;
var moved = 0;
var position = 0;
var prevPosition = 0;
var renderState = 0;

// Defined variables ended ------------


// HTML to be inserted
var redDot;

// HTML to be inserted ended ----------

// Insert redDots to bottom of TV cards
insertRedDots();


// Handle the selected one at first according to renderState equals to zero.
handleRedDots();


function insertRedDots() {
  redDotsContainer = document.getElementById('red-dots-container');
  for (var i = 0 ; i < maxRenderState; i++) {
    redDot = '<div class="padding-all-20"> <div class="outer-circle white-bg" id="red-dot-'+ i +'"> <div class="inner-circle red-500-bg"> </div> </div> </div>';
    redDotsContainer.insertAdjacentHTML('beforeend', redDot); 
  }
}


function handleRedDots() {
  allRedDots = document.querySelectorAll('.outer-circle');
  allRedDots.forEach(function(el) {
    el.classList.remove("outer-circle-border");
  })
  selectedRedDot = document.getElementById('red-dot-' + renderState);
  selectedRedDot.classList.add("outer-circle-border");
}


//DRY
function positionSlide() {
  prevPosition = nextPosition * renderState;
  wrapperSlider.classList.add('wrapper-slider-transition');
  wrapperSlider.style.transform = "translateX("+ prevPosition + "px)";
  handleRedDots();
}


// Slide to proper card places. Not different place.
function slideToProperPlace() {
  if(prevPosition > 0) {
    wrapperSlider.style.transform = "translateX(0px)";
    prevPosition = 0;
    renderState = 0;
    positionSlide();
  } else {
    if(moved < -movementLimit && renderState < maxRenderState - 1) {
      renderState +=1;
      positionSlide();
    } else if (moved > movementLimit) {
      renderState -=1;
      positionSlide();
    } else {
      positionSlide();
    }
  }
  moved = 0;
  console.log(renderState);

}


// Slide to anywhere when mouse is clicked.
function addMoveListener(event) {
  moved = (event.clientX - entered);
  position = (prevPosition + moved);
  wrapperSlider.style.transform = "translateX(" + position + "px)";
  //console.log(event.clientX);
  //console.log(entered);
}


// Delete event listeners when mouse is not clicked. Instead of if statement, this worked better.
function deleteMoveListener(event) {
  wrapperSlider.removeEventListener('mousemove', addMoveListener);
  window.removeEventListener('mouseup', deleteMoveListener);
  prevPosition = position;
  slideToProperPlace()
  console.log(prevPosition);
}


// When mousedown event, addEventListeners and call all the Slider Movement Handling functions above.
wrapperSlider.addEventListener("mousedown", function(e) {
  entered = e.clientX;
  wrapperSlider.classList.remove('wrapper-slider-transition');
  wrapperSlider.addEventListener('mousemove', addMoveListener);
  window.addEventListener('mouseup', deleteMoveListener);
});
}





















var slideHandler2 = function() {
  
var wrapperSlider = document.getElementById("wrapper-slider-2");
//console.log(wrapperSlider.childElementCount);


// Constants ------------------
var movementLimit = 300;
var nextPosition = -534;

// Constants ended -----------


// Declared variables
var maxRenderState = wrapperSlider.childElementCount;

// Declared variables ended


// Defined variables.
var entered = 0;
var moved = 0;
var position = 0;
var prevPosition = 0;
var renderState = 0;

// Defined variables ended ------------


// HTML to be inserted
var redDot;

// HTML to be inserted ended ----------

// Insert redDots to bottom of TV cards
insertRedDots();


// Handle the selected one at first according to renderState equals to zero.
handleRedDots();


function insertRedDots() {
  redDotsContainer = document.getElementById('red-dots-container-2');
  for (var i = 0 ; i < maxRenderState; i++) {
    redDot = '<div class="padding-all-20"> <div class="outer-circle-2 white-bg" id="red-dot-2-'+ i +'"> <div class="inner-circle red-500-bg"> </div> </div> </div>';
    redDotsContainer.insertAdjacentHTML('beforeend', redDot); 
  }
}


function handleRedDots() {
  allRedDots = document.querySelectorAll('.outer-circle-2');
  allRedDots.forEach(function(el) {
    el.classList.remove("outer-circle-border");
  })
  console.log('redstate');
  console.log(renderState);
  selectedRedDot = document.getElementById('red-dot-2-' + renderState);
  selectedRedDot.classList.add("outer-circle-border");
}


//DRY
function positionSlide() {
  prevPosition = nextPosition * renderState;
  wrapperSlider.classList.add('wrapper-slider-transition');
  wrapperSlider.style.transform = "translateX("+ prevPosition + "px)";
  handleRedDots();
}


// Slide to proper card places. Not different place.
var round;
function slideToProperPlace() {
  if(prevPosition > 0) {
    prevPosition = 0;
    renderState = 0;
    positionSlide();
  } else {
    if(moved < -movementLimit && renderState < maxRenderState - 1) {
      round = Math.round(moved / -movementLimit);
      console.log(round);
      if(round + renderState >= maxRenderState) {
        renderState = maxRenderState - 1;
      } else {
        renderState += round;
      }
      positionSlide();
    } else if (moved > movementLimit) {
      round = Math.round(moved / movementLimit);
      console.log(round);
      if(round + renderState <= 0) {
        renderState = 0;
      } else {
        renderState -= round;
      }
      positionSlide();
    } else {
      positionSlide();
    }
  }
  moved = 0;
  console.log(renderState);

}


// Slide to anywhere when mouse is clicked.
function addMoveListener(event) {
  moved = (event.clientX - entered);
  position = (prevPosition + moved);
  wrapperSlider.style.transform = "translateX(" + position + "px)";
  //console.log(event.clientX);
  //console.log(entered);
}


// Delete event listeners when mouse is not clicked. Instead of if statement, this worked better.
function deleteMoveListener(event) {
  wrapperSlider.removeEventListener('mousemove', addMoveListener);
  window.removeEventListener('mouseup', deleteMoveListener);
  prevPosition = position;
  slideToProperPlace();
}


// When mousedown event, addEventListeners and call all the Slider Movement Handling functions above.
wrapperSlider.addEventListener("mousedown", function(e) {
  entered = e.clientX;
  wrapperSlider.classList.remove('wrapper-slider-transition');
  wrapperSlider.addEventListener('mousemove', addMoveListener);
  window.addEventListener('mouseup', deleteMoveListener);
});
}




