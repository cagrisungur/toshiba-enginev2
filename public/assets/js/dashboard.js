(function() {
  var model = (function() {

  })()

  var view = (function() {
    return {
      getUIElements: ['img-large-btn', 'proof-modal']
    }
  })();

  var controller = (function(model, view) {
    var imgModalButton = document.getElementById(view.getUIElements[0]);
    var proofModal = document.getElementById(view.getUIElements[1]);

    function initEventListeners() {
      imgModalButton.addEventListener('click', handleClick);
      proofModal.addEventListener('click', handleExit);
    }


    function handleClick(e) {
      console.log(proofModal)
      proofModal.classList.remove('display-none');
      console.log('handle click!');
    }

    function handleExit() {
      proofModal.classList.toggle('display-none');
    }

    return {
      init: function() {
        console.log('init');
        initEventListeners();
      }
    }
  })(model, view);

  controller.init();


})();