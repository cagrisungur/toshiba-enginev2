var tableHandler = function() {

  inchesButton = document.getElementById('buying-guide-button-1');
  cmButton = document.getElementById('buying-guide-button-2')



  var inches = [["24-32''", "43-50''"], ["43-50''", "55-65''"], ["55-65''", "75''"], ["75''","75''"]];

  var cm = [["60-80", "108-126"], ["108-126", "139-164"], ["139-164", "189"], ["189","189"]];

  tables = document.getElementsByClassName('table-size');
  console.log(tables[1].childNodes);
  var generalIndex;
  var localIndex;
  var className;

  inchesButton.addEventListener('click', showInches);
  cmButton.addEventListener('click', showCm);

  function showCm() {
    console.log(cmButton.className.split('buying-guide-inchcm ')[1]);
    className = inchesButton.className;
    className = className.split('buying-guide-inchcm-selected ')[1];
    className = 'buying-guide-inchcm ' + className;
    inchesButton.className = className;

    className = cmButton.className;
    className = className.split('buying-guide-inchcm ')[1];
    className = 'buying-guide-inchcm-selected ' + className;
    cmButton.className = className;

    generalIndex = 0;
    Array.prototype.forEach.call(tables, function(el, index) {
      localIndex = 0;
      console.log(el.childNodes);
      Array.prototype.forEach.call(el.childNodes, function(node, index) {
        if(node.nodeName === 'DIV') {
          console.log(index)
          node.childNodes[1].innerText = cm[generalIndex][localIndex];
          localIndex += 1;
        }

      })
      generalIndex += 1;
    });
  }

  function showInches() {
    className = cmButton.className;
    className = className.split('buying-guide-inchcm-selected ')[1];
    className = 'buying-guide-inchcm ' + className;
    cmButton.className = className;

    className = inchesButton.className;
    className = className.split('buying-guide-inchcm ')[1];
    className = 'buying-guide-inchcm-selected ' + className;
    inchesButton.className = className;
    generalIndex = 0;
    Array.prototype.forEach.call(tables, function(el, index) {
      localIndex = 0;
      console.log(el.childNodes);
      Array.prototype.forEach.call(el.childNodes, function(node, index) {
        if(node.nodeName === 'DIV') {
          console.log(index)
          node.childNodes[1].innerText = inches[generalIndex][localIndex];
          localIndex += 1;
        }

      })
      generalIndex += 1;
    });
  }
}

tableHandler();

function calculate() {
  var resolution = $("#calculator-resolution option:selected").val();
  var distance = $("#calculator-distance option:selected").val();
  var size;
  if (resolution == 'HD' && distance == '1-2') {
    size = '24-32';
  } else if (
    (resolution == 'HD' && distance == '2-3')
    || (resolution == '4K' && distance == '1-2')
  ) {
    size = '43-50';
  } else if (
    (resolution == 'HD' && distance == '3-4')
    || (resolution == '4K' && distance == '2-3')
  ) {
    size = '55-65';
  } else {
    size = '75';
  }

  $("#calculator-result").html(`${size}"`);
}

$("#calculator-resolution, #calculator-distance").change(function(){
  calculate();
}).change();




$(document).ready(function(){
  $("#buying-guide-button-1").click();
})
