(
  function() {
    
    var model = (
      function() {
        console.log('SupportHomeIIFE loaded!');
      }
    )();

    var view = (
      function() {

        UIElements= {
          TVSliderSection: 'wrapper-slider-3',
          sizeCardSection: 'screen-size-cards-section',
          sizeCardClose: 'close-screen-size-section',
          slideClose: 'close-tv-slide-section',
          bgTVChange: 'bg-tvchange',
          carouselIndicators: 'carousel-indicators',

        };

        return {
          getUIElements: function() {
            return UIElements;
          }
        }

      }
    )();

    var controller = (
      function(model, view) {

        var UIElements, cardSection, cardClose, slideClose, bgTVChange, TVSliderSection, carouselIndicators;

        function getUI() {
          UIElements = view.getUIElements();
          cardSection = document.getElementById(UIElements.sizeCardSection);
          cardClose = document.getElementById(UIElements.sizeCardClose);
          slideClose = document.getElementsByClassName(UIElements.slideClose);
          bgTVChange = document.getElementsByClassName(UIElements.bgTVChange);
          TVSliderSection = document.getElementById(UIElements.TVSliderSection);
          carouselIndicators = document.getElementById(UIElements.carouselIndicators);
        }

        function setEventListeners() {
          cardClose.addEventListener('click', closeCardSection);

        }

        function closeCardSection() {

          if(TVSliderSection) {
            TVSliderSection.innerHTML = '';
          }

          if(carouselIndicators) {
            carouselIndicators.innerHTML = '';
          }

          console.log('slide-close');
          console.log(slideClose);
          if(slideClose) {
            Array.prototype.forEach.call(slideClose, function(el) {
              console.log(el);
              el.remove();
            });
          }
          
          cardSection.classList.toggle('display-none');
          Array.prototype.forEach.call(bgTVChange, function(el) {
            $(el).find('.absolute-full')[0].classList.add('d-none');
          });
        }

        return {
          init: function() {
            getUI();
            setEventListeners();
          }
        }

      }
    )(model, view);

    controller.init();

  }
)()