var datePickerCustomStyler = (function() {
  var model = (function() {

  })();

  var view = (function() {
    var elements = ['idForm-0-input-2', 'ui-datepicker-div'];
    return {
      getElements: function() {
        console.log(elements)
        return elements;
      }
    }
  })();

  var controller = (function(model, view) {
    var UIElements = view.getElements();

      function handleDatePicker() {
        $(".is-datepicker").datepicker("option", "beforeShow", function(input, inst){
          $(inst.dpDiv).position({
             my: "bottom",
             at: "top",
             of: $('#idForm-0-input-2')
        });
      });
    }
    
    return {
      init: function() {
        handleDatePicker();
      }
    }

  })(model, view);

  controller.init();

})();