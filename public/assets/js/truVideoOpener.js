var videoOpener = function () {
  var button = document.getElementById('video-button');
  var modal = $('#video-modal')
  var video = document.getElementById('tru-video')
  button.addEventListener('click', handleClick);

  function handleClick() {
    modal.removeClass('display-none');
    modal.on('click', handleModal);
  }
  
  function handleModal(e) {
    if(e.target.id === 'video-modal') {
      modal.addClass('display-none');
      video.pause();
    }
  }
}

videoOpener();