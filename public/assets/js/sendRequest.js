(function() {
  var model = (function() {
    // Model definition
  })()

  var view = (function() {
    // View here

    return {
      UIElements: ['#idForm-0', 'idForm-button-0']
    }
  })();

  var controller = (function(model, view) {
    var perm;

    var form = $(view.UIElements[0]);
    var button = document.getElementById(view.UIElements[1]);

    function initEventListeners() {
      button.addEventListener('click', sendRequest);
    }

    function sendRequest(e) {
      e.preventDefault();
      console.log($(view.UIElements[0]));
      var formData = new FormData($(view.UIElements[0])[0]);
      console.log(Array.from(formData));
    }

    return {
      init: function() {
        initEventListeners();
      }
    }

  })(model, view)

  controller.init();

})();