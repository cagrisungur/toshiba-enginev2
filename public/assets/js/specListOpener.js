var opener = function() {
  var selectedItem = document.getElementById('fullspec-container');
  selectedItem.addEventListener('click', handleClick, true);

  function handleClick(e) {
    if(e.target.id) {
      selectedID = e.target.id.split('allproduct-fullspecs-list-expander-')[1];
      document.getElementById('spec-list-' + selectedID).classList.toggle('display-none');
    }

    if(e.target.getAttribute('data-id')) {
      selectedID = e.target.getAttribute('data-id');
      document.getElementById('spec-list-' + selectedID).classList.toggle('display-none');
    }
  }
}

opener();