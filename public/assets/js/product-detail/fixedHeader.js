function fixedHeader() {
  console.log('fixedHeader');
  
    $('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#0"]')
  
    .click(function(event) {
  
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        )
      {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
  
        if (target.length) {
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000, function() {
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              return false;
            } else {
              $target.attr('tabindex','-1');
              $target.focus();
            };
          });
        }
  
      }
  
    });
  
    $(".fixed-sub-header__link").click(function() {
      $("a.fixed-sub-header__link.active").removeClass('active');
      $(this).addClass('active');
    });
  
    var position = $(document).scrollTop();
    var staticHeader = $('.sub-header__marker');
    var top = staticHeader.offset().top;
  
    $('.section').each(function(i) {
      if($(this).position().top <= position) {
        $("a.fixed-sub-header__link.active").removeClass('active');
        $("a.fixed-sub-header__link").eq(i).addClass('active');
      }
    });
  
    if($(window).scrollTop() >= top) {
      $('.fixed-sub-header').css("display", "block");
    } else {
      $('.fixed-sub-header').css("display", "none");
    }
  
    $(document).scroll(function() {
  
      var position = $(document).scrollTop();
      var staticHeader = $('.sub-header__marker');
      var top = staticHeader.offset().top;
      var bottom = staticHeader.offset().top + staticHeader.outerHeight(true);
  
      if ($(window).width() > 1200) {
  
        if($(window).scrollTop() >= bottom) {
          $('.fixed-sub-header').slideDown(200);
        } else {
          $('.fixed-sub-header').slideUp(200);
        }
  
        $('.section').each(function(i) {
          if($(this).position().top <= position) {
            $("a.fixed-sub-header__link.active").removeClass('active');
            $("a.fixed-sub-header__link").eq(i).addClass('active');
          }
        });
  
      } else {
  
        if($(window).scrollTop() >= top) {
          $('.fixed-sub-header').css("display", "block");
        } else {
          $('.fixed-sub-header').css("display", "none");
        }
  
      };
  
    });
  
  };

  fixedHeader();