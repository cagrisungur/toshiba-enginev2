(function() {

    var selectedImg;

    var thumbnailSection = document.getElementById('tv-tn-img');
    var videoOpen = document.getElementById('tv-tn-video');
    var images = document.getElementsByClassName('tv-main-img');
    var videoSection = document.getElementById('video-section');

    thumbnailSection.addEventListener('click', changeImg);
    videoOpen.addEventListener('click', openVideo);

    function changeImg() {
        if(event.target.getAttribute('data-tn-id')) {
            selectedImg = event.target.getAttribute('data-tn-id');

            Array.prototype.forEach.call(images, function(el) {
                if(el.getAttribute('data-tv-id') !== selectedImg ) {
                    el.classList.add('d-none');
                }
                else {
                    el.classList.remove('d-none');
                }
            })
        }
    }

    function openVideo() {
        if(event.target.getAttribute('id') === 'video-opener') {
            videoSection.classList.remove('d-none');
        }
    }


    videoSection.addEventListener('click', exitModal);

    function exitModal() {
        if(event.target.tagName === 'DIV') {
            videoSection.classList.add('d-none');

            var iframe = videoSection.querySelector('iframe');
            var video = videoSection.querySelector('video');
            if (iframe) {
                var iframeSrc = iframe.src;
                iframe.src = iframeSrc;
            }

            if (video) {
                video.pause();
            }

        }
    }



})();