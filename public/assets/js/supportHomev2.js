$(document).ready(function(){

  $(".vidButton ").click(function(){
    $(this).parent("div").parent("div").parent("div").parent("div").find(".video-section").toggleClass("display-none");
  });
  $(".faqButton ").click(function(){
    $(this).parent("div").parent("div").parent("div").parent("div").find(".faq-section").toggleClass("display-none");
  });

  $(".faq-section .faqQ ").click(function(){
    $(this).parent("div").find(".faqA").toggleClass("display-none");
  });

//

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({alwaysShowClose:true});
            });


  $(".bg-tvchange ").click(function(){

        $("#screen-size-cards-containe").hide();
        $("#SHResult").hide();
        $(".categorySelector  .active").removeClass("active");

      $("#screen-size-cards-container").hide();
      $("#screen-size-cards-section").show();
      $("#screen-size-cards-section .loading").show();

      var cat = $(this).attr("data-cat");
      $(this).parent("div").find("> div").removeClass("active");
      $(this).parent("div").addClass("active");
      $(this).addClass("active");
        $.ajax({
        url:'/uk/uk/support-filter-product?action=products&cat='+cat,
        type: "GET",
        dataType: "json",
        data: {action: 'products'},
        async: true,
        success: function(data){
          console.log(data);

            $("#screen-size-cards-container").html("");
          $.each(data, function(index, val){
            //var grayOut = '<div class="d-none position-absolute sliderselect-cover absolute-full z-2002" data-select="'+val+'"></div>';
            //var htmlInserted = '<div class="slider-change w-100 margin-t-20" data-click='+ val +' data-id=' +val + '> <div class="screen-size-selector vertical-center text-align-center h-full gray-100-bg-content cursor-pointer" data-id=' + val + '> <p class="screen-size-selector-text margin-b-0 text-l-bold" data-id=' + val + '>' + val + "''" + '</p> </div> </div>';
            //var tmp = '<div class="col col-lg-2 position-relative selectSize" data-size="'+val+'">'+htmlInserted+'</div>';
            var tmp = '<div class="col col-lg-2 position-relative selectSize" data-size="'+val+'"><div>'+val+'"</div></div>';
            $("#screen-size-cards-container").append(tmp);

            $("#screen-size-cards-container").show();
            $("#screen-size-cards-section .loading").hide();

          });

        }
    });
  });

  $("#supportHomeProductSelector").delegate(".selectSize","click",function(){

      $("#SHResult").hide();
      $("#screen-size-cards-container .active").removeClass("active");
      $(this).addClass("active");
      $("#SHResultContainer ").hide();
      $("#SHResult").show();
      $("#SHResult .loading").show();
      var size = $(this).attr("data-size");
      var cat = $(".bg-tvchange.active").attr("data-cat");
        $.ajax({
        url:'/uk/uk/support-filter-product?action=products&cat='+cat+'&size='+size,
        type: "GET",
        dataType: "json",
        data: {action: 'products'},
        async: true,
        success: function(data){
          console.log(data);

            $(".SHSlider .carousel-inner").html("");
          $.each(data, function(index, val){
            tmp = "<div class='item'> <div class='all-product-box-wrapper gray-100-bg'>"+
            " <div class='height-280 overflow-hidden wrapper-row-align-center-center'>"+
            "<img draggable='false' class='all-product-box-img' src='"+ val.frontImage + "'/></div>"+
            " <p class='margin-t-xs product-header color-grey700 text-m-l-bold'>" + val.code +"</p>"+
            " <div class='product-year-section'> <p></p> </div> <a href='"+val.baseUrl+"'> "+
            "<button class='all-product-box-button-text text-xs color-white all-product-box-button red-500-bg'> "+
            "Learn more </button> </a> </div> </div>";
            $("#SHResultContainer .MultiCarousel-inner").append(tmp);
            if(index==(data.length-1)){
              ResCarouselSize();
            }

          });

        }
    });
  });


  $('#close-model-section').click(function () {
      $("#SHResult").hide();
      $("#screen-size-cards-container .active").removeClass("active");

  });

  $('#close-screen-size-section').click(function () {
      $("#screen-size-cards-containe").hide();
      $(".categorySelector  .active").removeClass("active");

  });


/*MultiCarouselBEG*/
var itemsMainDiv = ('.MultiCarousel');
var itemsDiv = ('.MultiCarousel-inner');
var itemWidth = "";

$('.leftLst, .rightLst').click(function () {
    var condition = $(this).hasClass("leftLst");
    if (condition)
        click(0, this);
    else
        click(1, this)
});










ResCarouselSize();




$(window).resize(function () {
    ResCarouselSize();
});

//this function define the size of the items
function ResCarouselSize() {

      $("#SHResult .loading").hide();
      $("#SHResultContainer").show();
    var incno = 0;
    var dataItems = ("data-items");
    var itemClass = ('.item');
    var id = 0;
    var btnParentSb = '';
    var itemsSplit = '';
    var sampwidth = $(itemsMainDiv).width();
    console.log(sampwidth);
    var bodyWidth = $('body').width();
    $(itemsDiv).each(function () {
        id = id + 1;
        var itemNumbers = $(this).find(itemClass).length;
        btnParentSb = $(this).parent().attr(dataItems);
        itemsSplit = btnParentSb.split(',');
        $(this).parent().attr("id", "MultiCarousel" + id);


        if (bodyWidth >= 1200) {
            incno = itemsSplit[3];
            itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 992) {
            incno = itemsSplit[2];
            itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 768) {
            incno = itemsSplit[1];
            itemWidth = sampwidth / incno;
        }
        else {
            incno = itemsSplit[0];
            itemWidth = sampwidth / incno;
        }
        $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
        $(this).find(itemClass).each(function () {
            $(this).outerWidth(itemWidth);
        });

        $(".leftLst").addClass("over");
        $(".rightLst").removeClass("over");

    });

}


//this function used to move the items
function ResCarousel(e, el, s) {
    var leftBtn = ('.leftLst');
    var rightBtn = ('.rightLst');
    var translateXval = '';
    var divStyle = $(el + ' ' + itemsDiv).css('transform');
    var values = divStyle.match(/-?[\d\.]+/g);
    var xds = Math.abs(values[4]);
    if (e == 0) {
        translateXval = parseInt(xds) - parseInt(itemWidth * s);
        $(el + ' ' + rightBtn).removeClass("over");

        if (translateXval <= itemWidth / 2) {
            translateXval = 0;
            $(el + ' ' + leftBtn).addClass("over");
        }
    }
    else if (e == 1) {
        var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
        translateXval = parseInt(xds) + parseInt(itemWidth * s);
        $(el + ' ' + leftBtn).removeClass("over");

        if (translateXval >= itemsCondition - itemWidth / 2) {
            translateXval = itemsCondition;
            $(el + ' ' + rightBtn).addClass("over");
        }
    }
    $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
}

//It is used to get some elements from btn
function click(ell, ee) {
    var Parent = "#" + $(ee).parent().attr("id");
    var slide = $(Parent).attr("data-slide");
    ResCarousel(ell, Parent, slide);
}
/*MultiCarouselEND*/

})
