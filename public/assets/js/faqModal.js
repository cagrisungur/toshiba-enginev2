var modalFunction = function(targetFaq, faqQ, faqA) {
  var targetID, targetQuestion, targetQuestionText, targetAnswer, targetAnswerText;

  var modalSection = document.getElementById('faq-modal-section');
  var modalSectionWrapper = document.getElementById('faq-modal-section-wrapper');
  var modalSectionQuestion = document.getElementById('faq-modal-question');
  var modalSectionAnswer = document.getElementById('faq-modal-answer')
  var bottomFAQSection = document.getElementById(targetFaq);


  bottomFAQSection.addEventListener('click', bottomFAQHandler);

  function bottomFAQHandler(e) {
    console.log('bottomFAQ');
    console.log(e.target);
    targetID = null;
    targetQuestion = null;
    targetAnswer = null;
    if(e.target.getAttribute('data-id')) {
      targetID = e.target.getAttribute('data-id').split('faq-')[1];
      targetQuestion = document.getElementById(faqQ + targetID);
      if(targetQuestion) {
        targetQuestionText = targetQuestion.innerHTML;
      }
      targetAnswer = document.getElementById(faqA + targetID);
      if(targetAnswer) {
        targetAnswerText = targetAnswer.innerHTML;
      }
    }
    fillModal();
  }

  function openModal() {
    console.log('openmodal')
    if(targetID) {
      $(modalSection).removeClass('display-none');
      $(modalSection).addClass('display-flex');
      modalSection.addEventListener('click', closeModal);
    }
    
  }

  function closeModal(e) {
    console.log('closemodal')
    if(e.target.id === 'faq-modal-section') {
      $(modalSection).removeClass('display-flex');
      $(modalSection).addClass('display-none');
      modalSectionQuestion.innerHTML = '';
      modalSectionAnswer.innerHTML = '';
      modalSection.removeEventListener('click', closeModal);
    }
    
  }

  function fillModal() {
    console.log('fillmodal')
    if(targetQuestionText && targetAnswerText) {
      modalSectionQuestion.innerHTML = '';
      modalSectionAnswer.innerHTML = '';
      modalSectionQuestion.insertAdjacentHTML('beforeend', targetQuestionText);
      modalSectionAnswer.insertAdjacentHTML('beforeend', targetAnswerText)
      openModal();
    }
  }
}

modalFunction('support-quick-links', 'faq-q-', 'faq-a-');
modalFunction('search-faq-section', 'faq-a2-', 'faq-q2-');