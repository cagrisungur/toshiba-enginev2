# This file is a "template" of which env vars need to be defined for your application
# Copy this file to .env file for development, create environment variables when deploying to production
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=17a489cbc66267dd2fa8843b73cb038d
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS=localhost,example.com
###< symfony/framework-bundle ###

###> lexik/jwt-authentication-bundle ###
# Key paths should be relative to the project directory
JWT_PRIVATE_KEY_PATH=config/jwt/private.pem
JWT_PUBLIC_KEY_PATH=config/jwt/public.pem
JWT_PASSPHRASE=182a5c4a452bcfd3c9d55f6e4bd0ce41
###< lexik/jwt-authentication-bundle ###

###> doctrine/doctrine-bundle ###
DATABASE_URL=mysql://root@127.0.0.1:3306/toshiba

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://localhost:?[0-9]*$
###< nelmio/cors-bundle ###

###> doctrine/mongodb-odm-bundle ###
MONGODB_URL=mongodb://localhost:27017
MONGODB_DB=symfony
###< doctrine/mongodb-odm-bundle ###

###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
MAILER_URL=null://localhost
###< symfony/swiftmailer-bundle ###

###> knplabs/knp-snappy-bundle ###
WKHTMLTOPDF_PATH=/usr/local/bin/wkhtmltopdf
WKHTMLTOIMAGE_PATH=/usr/local/bin/wkhtmltoimage
###< knplabs/knp-snappy-bundle ###
