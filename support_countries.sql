-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 11 Ağu 2020, 00:48:55
-- Sunucu sürümü: 10.4.6-MariaDB
-- PHP Sürümü: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `old_toshiba_live`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `support_countries`
--

CREATE TABLE `support_countries` (
  `id` int(15) NOT NULL,
  `active` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `code` varchar(5) CHARACTER SET utf8 NOT NULL,
  `deleted` int(11) NOT NULL,
  `defaultlang` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `support_countries`
--

INSERT INTO `support_countries` (`id`, `active`, `name`, `code`, `deleted`, `defaultlang`) VALUES
(1, 0, 'Alamanya', 'de', 1465721354, 0),
(2, 0, 'deneme2', 'de2', 1465721338, 0),
(3, 7, 'deneme', 'tr', 1465721346, 0),
(4, 7, 'Bosnia Herzegovina', 'ba', 1465801097, 4),
(5, 7, 'Bulgaria', 'bg', 1490272116, 7),
(6, 7, 'Czech Republic', 'cz', 0, 8),
(7, 7, 'DACH', 'de', 0, 9),
(8, 7, 'Hungary', 'hu', 0, 10),
(9, 7, 'Italy', 'it', 0, 11),
(10, 7, 'Lithuania', 'lt', 0, 12),
(11, 7, 'Macedonia', 'mk', 1490272226, 13),
(12, 7, 'Montenegro', 'me', 1490272250, 14),
(13, 7, 'Romania', 'ro', 1490272267, 15),
(14, 7, 'Iberia', 'es', 0, 16),
(15, 7, 'France', 'fr', 0, 17),
(16, 0, 'Kosovo', 'xk', 1490272201, 0),
(17, 7, 'Croatia', 'hr', 1490272167, 5),
(18, 7, 'Slovenia', 'si', 1490272296, 20),
(19, 7, 'Poland', 'pl', 0, 21),
(20, 7, 'Sweden', 'se', 0, 22),
(21, 7, 'Greece', 'gr', 0, 23),
(22, 7, 'Netherlands', 'nl', 0, 2),
(23, 0, 'Belgium', 'be', 1490272141, 0),
(24, 0, 'Malta', 'mt', 1490272234, 25),
(25, 7, 'Switzerland', 'ch', 1490272312, 9),
(26, 7, 'Serbia', 'rs', 0, 6),
(27, 0, 'Albania', 'al', 1490272110, 0),
(28, 0, 'Russia', 'ru', 1490272278, 26),
(29, 0, 'Moldova', 'md', 1490272243, 0),
(30, 0, 'Canary Islands', 'ic', 1490272156, 31),
(31, 7, 'Austria', 'at', 1490272132, 9),
(32, 7, 'Bosnia Herzegovina', 'ba', 1490272149, 4),
(33, 7, 'Europe', 'eu', 0, 2),
(34, 7, 'Turkey', 'tr', 0, 1),
(35, 7, 'Estonia', 'ee', 1490272175, 27),
(36, 7, 'Latvia', 'lv', 1490272217, 28),
(37, 0, 'Portugal', 'pt', 0, 29),
(38, 7, 'Slovakia', 'sk', 0, 30),
(39, 7, 'United Kingdom', 'uk', 0, 35),
(40, 7, 'Norway', 'no', 0, 32),
(41, 7, 'Denmark', 'dk', 0, 33),
(42, 7, 'Finland', 'fi', 0, 34),
(43, 7, 'Austria', 'at', 0, 9),
(44, 7, 'Switzerland', 'ch', 0, 9),
(45, 7, 'Bosnia & Herzegovina', 'ba', 0, 4),
(46, 7, 'Bulgaria', 'bg', 0, 7),
(47, 7, 'Croatia', 'hr', 0, 5),
(48, 7, 'Romania', 'ro', 0, 15),
(49, 7, 'Slovenia', 'si', 0, 20),
(50, 7, 'Ukraine', 'ua', 0, 36),
(51, 7, 'Cyprus', 'cy', 0, 23),
(52, 7, 'Malta', 'mt', 0, 2),
(54, 7, 'Ireland', 'IR', 0, 35);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `support_countries`
--
ALTER TABLE `support_countries`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `support_countries`
--
ALTER TABLE `support_countries`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
