-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 10 Ağu 2020, 20:14:02
-- Sunucu sürümü: 10.4.6-MariaDB
-- PHP Sürümü: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `toshiba_engine`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `support_users`
--

CREATE TABLE `support_users` (
  `id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `group_id` tinyint(2) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `deleted` int(15) DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `support_users`
--

INSERT INTO `support_users` (`id`, `name`, `surname`, `username`, `password`, `group_id`, `country_id`, `deleted`, `deleted_at`) VALUES
(25, 'Furkan', 'Kavlak', 'furkankavlak', 'ec2221ce3753e3092820a0a163ddc0f9', 2, NULL, 0, NULL),
(26, 'Fatih', 'Korkmaz', 'fatihkorkmaz', 'f6e5ea0c548e6e2ba2e597bcfc41e7f1', 2, NULL, 0, NULL),
(27, 'Cihangir', 'Korkmaz', 'cihangirkorkmaz', '533fa9332c4443e3b4813c5fb39658c3', 2, NULL, 0, NULL),
(28, 'Stefan', 'Bogner', 'stefanbogner', '130dd05c1f71fbd5dbf07dc353dc0076', 1, NULL, 0, NULL),
(29, 'Ismet', 'Yavuz', 'ismetyavuz', 'dfad539204b7bd0928f49fe07c24c66c', 2, NULL, 0, NULL),
(31, 'Doğa', 'Tümerdem', 'doga', '827ccb0eea8a706c4c34a16891f84e7b', 2, 0, 0, NULL),
(34, 'Suna', 'Şanal', 'sunasanal', '6e956e967bac1bce54587e57e7072f74', 1, NULL, 0, NULL),
(35, 'Burcu', 'Yankovan', 'burcu', '92e825d161002c795fd60357ae45587f', 2, NULL, 0, NULL),
(36, 'Tugce', 'Tanyeri', 'tugcetanyeri', '130dd05c1f71fbd5dbf07dc353dc0076', 2, NULL, 0, NULL),
(37, 'Sibel', 'Taranci', 'sibeltaranci', '26881d93cecb15168b14b1a8a2dbcf81', 1, 0, 0, NULL),
(38, 'iclal', 'deniz', 'iclal', 'c8be08a919cff1ada0ec26ddae3193c2', 1, NULL, 0, NULL),
(39, 'huseyin', 'ergon', 'huseyin', 'cf8d64dd5818877aa2ddc28182935e20', 2, NULL, 0, NULL),
(40, 'cagri', 'sungur', 'cagri', '827ccb0eea8a706c4c34a16891f84e7b', 2, 34, 0, NULL),
(41, 'vestel', 'turkey', 'vestel.tr', '7da152579e62c059e0aadb859d83f13e', 3, 34, 0, NULL),
(42, 'vestel', 'uk', 'vestel.uk', '315aaf3959595cb0642bd55212662a12', 3, 39, 0, NULL),
(43, 'vestel', 'turkey admin', 'vestel.tr.admin', '5c3af58080e4214a2322bc33c097e29e', 4, 34, 0, NULL),
(44, 'vestel', 'uk admin', 'vestel.uk.admin', 'e05110b28f4e6cebeac06956c206735d', 4, 39, 0, NULL),
(45, 'Andrew', 'Cherrett', 'andrew', '9d4701a21820b4af1bbbd3d4a5667c85', 2, 0, 0, NULL);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `support_users`
--
ALTER TABLE `support_users`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `support_users`
--
ALTER TABLE `support_users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
